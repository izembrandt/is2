from django import forms
from .funciones import *

class Fase_Form_Field(forms.ModelMultipleChoiceField):
    '''
    Campo de Formulario que representa a Usuarios
    '''
    def label_from_instance(self, obj):
        return "{} - {}".format(obj.numero, obj.nombre)


class Form_Crear_Roles(forms.Form):
    """Clase para crear roles

    **Atributos**

    nombre
      Nombre del rol. Tiene una longitud máxima de 50 caracteres.

    descripcion
      Descripción del rol. Utiliza el tag <textarea> de multilínea de HTML

    permisos_proyecto
      Permisos que tiene el proyecto

    fase
      Fases del proyecto

    permisos_fase
      Representan los permisos de la fase

    """
    nombre = forms.CharField(label="Nombre del rol", max_length=50, required=True)
    descripcion = forms.CharField(label="Descripción", widget=forms.Textarea, required=False)
    permisos_proyecto = forms.MultipleChoiceField(label="Permisos del Proyecto", choices=lista_permisos_proyecto_form, required=False)
    fases = Fase_Form_Field(label="Fases", queryset=None, required=False)
    permisos_fase = forms.MultipleChoiceField(label="Permisos de fase", choices=lista_permisos_fase_form, required=False)

    def __init__(self, *args, **kwargs):
        self.proyecto_id = kwargs.pop('proyecto_id', None)
        super().__init__(*args, **kwargs)
        if self.proyecto_id != None:
            self.fields['fases'].queryset = lista_de_fases_form(self.proyecto_id)


