# Generated by Django 3.0.4 on 2020-03-31 12:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('roles', '0009_rol_usuarios'),
    ]

    operations = [
        migrations.AddField(
            model_name='rol',
            name='es_gerente',
            field=models.BooleanField(default=False, verbose_name='Si el rol representa al Gerente del Proyecto'),
        ),
    ]
