from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.conf import settings
from fases.models import Fase
from django.contrib.postgres.fields import JSONField
import uuid
from guardian.shortcuts import assign_perm

from fases.permisos import PermisoFase
from proyecto.models import Proyecto
from proyecto.permisos import PermisoProyecto


class Rol(models.Model):
    """Modelo de django que representá a un rol de proyecto en el sistema

    **Atributos**

    uuid
      Identificador Universal del Rol, se utiliza para identificar el rol en la base de datos y es generada aleatoriamente

    nombre
      Nombre que se le da al Rol. Tiene una longitud máxima de 50 caracteres.

    descripción
      Descripción que se le da al Rol. No tiene longitud máxima de caracteres.

    proyecto
      Proyecto a la cual el rol pertenece. Si el proyecto se elimina de la base de datos, también el rol

    grupo
      Grupo (Modelo de Autenticación de django) a la cual el rol le asigna los permisos. Si el grupo es eliminado, también es el rol.

    fases
      Fases en los cuales los permisos de fase se aplicarán.

    permisos_proyecto
      Permisos de proyecto que posee el rol. Es una lista de cadenas, donde las cadenas son los **codenames** de los permisos
      de proyecto.

    permisos_fases
      Permisos de fase que posee el rol en las fases. Es una lista de cadenas, donde las cadenas son los **codenames** de los permisos
      de proyecto.

    usuarios
      Usuarios (Modelo de Autenticación de django) a la cual el rol esta asignado.

    es_gerente
      Booleano para saber si el rol es el de gerente. **True** si lo es y **False** si no lo es.

    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nombre = models.CharField("Nombre del Rol", max_length=50)
    descripcion = models.TextField("Descripción del Rol")
    proyecto = models.ForeignKey('proyecto.Proyecto', on_delete=models.CASCADE)
    grupo = models.ForeignKey('auth.Group', on_delete=models.CASCADE)
    fases = models.ManyToManyField(Fase)
    permisos_proyecto = JSONField(default=list)
    permisos_fases = JSONField(default=list)
    usuarios = models.ManyToManyField(settings.AUTH_USER_MODEL)
    es_gerente = models.BooleanField("Si el rol representa al Gerente del Proyecto", default=False)

    def setear_permisos_nuevos(self, permisos_proyecto: [str], permisos_fase: [str]):
        """Setea los permisos al rol

        :param permisos_proyecto: Lista de permisos de proyecto que se desea setear
        :param permisos_fase: Lista de permisos de fase que se desea setear
        """
        from .funciones import crear_grupo_de_rol
        # Creamos el grupo nuevo
        viejo_grupo = self.grupo
        nuevo_grupo = crear_grupo_de_rol(permisos_proyecto, self.proyecto, permisos_fase, list(self.fases.all()))
        self.grupo = nuevo_grupo
        self.permisos_fases = permisos_fase
        self.permisos_proyecto = permisos_proyecto
        # Efectuamos cambios
        self.save()
        viejo_grupo.delete()
        self.reasignar_grupos_usuarios()

    def a_dict_json(self):
        """Retorna la representación JSON"""
        return {
            'uuid': self.uuid,
            'nombre': self.nombre,
            'descripcion': self.descripcion,
            'proyecto': self.proyecto.id,
            'fases': [fase.id for fase in self.fases.all()],
            'permisos_proyecto': self.permisos_proyecto,
            'permisos_fases': self.permisos_fases,
        }

    @staticmethod
    def listar_roles_del_proyecto_json(proyecto_id):
        """Retorna la lista de roles de un proyecto identificado por un proyecto_id

        :param proyecto_id: Identificador del Proyecto
        :return: Lista de roles del proyecto en formato json
        """
        return [rol.a_dict_json() for rol in Rol.objects.filter(proyecto_id=proyecto_id).filter(es_gerente=False)]

    @staticmethod
    def listar_roles_del_usuario_en_proyecto_json(usuario, proyecto_id):
        """Retorna la lista de roles del usuario"""
        return [rol.a_dict_json() for rol in Rol.lista_roles_de_usuario_en_proyecto_queryset(proyecto_id, usuario)]

    def agregar_fase(self, fase: Fase):
        """Agrega una fase al rol donde actuan sus permisos

        :param fase: Fase a la que el rol se agrega
        :return: True si fue exitoso, false si ya existia
        """
        # Verificar si ya existe dentro del rol
        if fase in self.fases.all():
            return False

        # Asigna los permisos al grupo del rol
        for permiso in self.permisos_fases:
            assign_perm(permiso, self.grupo, fase)
        # Agrega la fase a la lista de fases
        self.fases.add(fase)
        return True

    def asignar_usuario(self, usuario):
        """Asigna este rol al usuario

        :type usuario: User
        :param usuario: Usuario a la cual se asigna el rol
        """
        usuario.groups.add(self.grupo)
        self.usuarios.add(usuario)

    def desasignar_usuario(self, usuario):
        """Desasigna este rol al usuario

        :type usuario: User
        :param usuario: Usuario a la cual se desasigna el rol
        """
        usuario.groups.remove(self.grupo)
        self.usuarios.remove(usuario)

    def reasignar_grupos_usuarios(self):
        """Vuelve a reasignar a todos los usuarios los permisos del rol.

        """
        self.grupo.user_set.set(self.usuarios.all())

    def delete(self, *args, **kwargs):
        """Override del método delete para borrar el grupo antes de eliminar el Rol
        """
        self.grupo.delete()
        super().delete(*args, **kwargs)

    @staticmethod
    def lista_roles_de_proyecto_queryset(proyecto_id):
        """Busca la lista de roles de un proyecto, exceptuando el rol del gerente y returna la lista

        :param proyecto_id: Identificador del proyecto donde se buscará los roles
        :return: La lista de roles de un proyecto
        """
        return Rol.objects.filter(proyecto_id=proyecto_id).exclude(es_gerente=True)

    @staticmethod
    def desasignar_todos_los_roles_del_proyecto(proyecto_id, usuario):
        """Desasigna a un usuario de todos los roles de un proyecto. Es utíl cuando eliminamos 
        a un usuario de un proyecto y debemos desasociarlo de todos sus roles en el proyecto

        :type usuario: User
        :param proyecto_id: Identificador del Proyecto
        :param usuario: Usuario
        """
        roles = usuario.rol_set.filter(proyecto_id=proyecto_id)

        for rol in roles:
            rol.desasignar_usuario(usuario)

    @staticmethod
    def lista_roles_de_usuario_en_proyecto_queryset(proyecto_id, usuario):
        """Retorna la lista de roles que un usuario especifico posee en un proyecto especifico.
        Utíl para saber que roles posee en el proyecto.

        :type usuario: User
        :param proyecto_id: Identificador del Proyecto
        :param usuario: Usuario
        :return: Lista de roles que el usuario posee en el proyecto
        """
        return usuario.rol_set.filter(proyecto_id=proyecto_id)

    @staticmethod
    def listar_permisos_proyecto_opciones():
        """Retorna una lista de opciones de permisos asignables en un proyecto. Estos son los permisos
        que funcionan a nivel proyecto y no de fase

        :return: Una lista de los permisos
        :rtype: list
        """
        from django.contrib.auth.models import Permission
        lista_permisos = []
        for codename in settings.PERMISOS_USUARIO_PROYECTO:
            permiso = Permission.objects.get(codename=codename,
                                             content_type=ContentType.objects.get_for_model(Proyecto))
            lista_permisos.append({"codigo": permiso.codename, "nombre": permiso.name})

        return lista_permisos

    @staticmethod
    def listar_permisos_fase_opciones():
        """Retorna una lista de opciones de permisos de fase asignables en un proyecto. Estos son los permisos
        que funcionan a nivel fase y no de proyecto

        :return: Una lista de los permisos
        :rtype: list
        """
        from django.contrib.auth.models import Permission
        lista_permisos = []
        for codename in settings.PERMISOS_USUARIO_FASES:
            permiso = Permission.objects.get(codename=codename,
                                             content_type=ContentType.objects.get_for_model(Fase))
            lista_permisos.append({"codigo": permiso.codename, "nombre": permiso.name})

        return lista_permisos

    def __str__(self):
        return "{}".format(self.nombre)

    @classmethod
    def presets(cls):
        return [
            {
                "nombre": "QA",
                "permisos_proyecto": PermisoProyecto.permisos_qa(),
                "permisos_fases": PermisoFase.permiso_qa(),
            },
            {
                "nombre": "Developer",
                "permisos_proyecto": PermisoProyecto.permisos_desarrollador(),
                "permisos_fases": PermisoFase.permiso_desarrollador(),
            }
        ]
