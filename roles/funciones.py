from django.conf import settings
from proyecto.models import Proyecto
from fases.models import Fase
from django.contrib.contenttypes.models import ContentType
from .models import Rol


def lista_permisos_proyecto_form():
    """Crea una lista de tuplas de permisos en base a los permisos que se pueden asignar
    a los usuarios que afectan al proyecto

    Esta función se utiliza para poder mostrar los campos de los permisos en el formulario.

    :rtype: [(String, String)]
    :return: Lista de permisos como para mostrar en las opciones del formulario
    """
    from django.contrib.auth.models import Permission
    lista_permisos = []
    for codename in settings.PERMISOS_USUARIO_PROYECTO:
        permiso = Permission.objects.get(codename=codename, content_type=ContentType.objects.get_for_model(Proyecto))
        lista_permisos.append((permiso.codename, permiso.name))

    return lista_permisos


def lista_permisos_fase_form():
    """Crea una lista de tuplas de permisos en base a los permisos que se pueden asignar
        a los usuarios que afectan a las fases de proyecto

        Esta función se utiliza para poder mostrar los campos de los permisos en el formulario.

        :rtype: [(String, String)]
        :return: Lista de permisos como para mostrar en las opciones del formulario
        """
    from django.contrib.auth.models import Permission
    lista_permisos = []
    for codename in settings.PERMISOS_USUARIO_FASES:
        permiso = Permission.objects.get(codename=codename, content_type=ContentType.objects.get_for_model(Fase))
        lista_permisos.append((codename, permiso.name))

    return lista_permisos


def lista_de_fases_form(proyecto_id):
    """QuerySet para obtener todas las fases de un proyecto. Se utilizá
    para crear las opciones

    :param proyecto_id: Identificador del proyecto
    :return: QuerySet de las fases
    :rtype: QuerySet<Fase>
    """
    from fases.models import Fase
    return Fase.objects \
        .filter(proyecto_id=proyecto_id) \
            .order_by('numero')


def crear_grupo_de_rol(permisos_proyecto, proyecto, permisos_fases, fases):
    """Crea un grupo en el sistema con los permisos correspondientes a los
    parametros.

    Se utilizá para crear los grupos que irán asociados a los roles.

    :type fases: [Fase]
    :type permisos_fases: [String]
    :type proyecto: Proyecto
    :type permisos_proyecto: [String]
    :param permisos_proyecto: Lista de codenames de permisos de proyecto
    :param proyecto: Proyecto a la cual irá asociado el rol.
    :param permisos_fases: Lista de codenames de permisos dentro de las fases
    :param fases: Lista de fases donde el grupo tendrá permisos de fase
    :return: Grupo creado
    :rtype: Group
    """
    from django.contrib.auth.models import Group
    from guardian.shortcuts import assign_perm
    import uuid
    # Crear Grupos
    grupo = Group.objects.create(name=uuid.uuid4().__str__())

    # Asignar permisos nivel proyecto al grupo
    for codename in permisos_proyecto:
        assign_perm(codename, grupo, proyecto)

    # Asignar permisos nivel fases al grupo
    for fase in fases:
        for codename in permisos_fases:
            assign_perm(codename, grupo, fase)

    grupo.save()
    return grupo


def listar_roles_dict(proyecto_id):
    """Lista todos los roles de un proyecto y los devuelve como una lista en vez de queryset

    :rtype: [{uuid: "", nombre: "",..,},...,{}]
    :param proyecto_id: Identificador del proyecto
    :return: Lista de roles
    """
    return list(Rol.objects.filter(proyecto_id=proyecto_id).filter(es_gerente=False).values())

def obtener_rol_dict(proyecto_id, uuid):
    """Obtiene el diccionario del rol que se identifica por el **uuid** y pertenece al proyecto
    que se identifica por **proyecto_id**

    :param proyecto_id: Identificador del Proyecto
    :param uuid: Identificador Universal del Rol
    :return: {uuid: "", nombre: "",.., permisos_proyecto: ['...',..]}
    """
    return list(Rol.objects.filter(uuid=uuid).filter(proyecto_id=proyecto_id).values())[0]


def obtener_rol_fases_list(proyecto_id, uuid):
    """Retorna una lista de identificadores de fases de un rol

    :param proyecto_id: Identificador del Proyecto
    :param uuid: Identificador Universal de la Fase
    :return: ['37','57',...,'93']
    """
    rol = Rol.objects.get(uuid=uuid, proyecto_id=proyecto_id)
    fases = rol.fases.all()
    lista_fase = []
    for fase in fases:
        lista_fase.append("{}".format(fase.pk))

    return lista_fase


def crear_rol_gerente_de_proyecto(proyecto):
    """Crea un rol con todos los permisos que posee un gerente de proyecto.

    :type proyecto: Proyecto
    :param proyecto: Proyecto a la cual será asignado el rol
    :return: Rol creado
    :rtype: Rol
    """
    grupo = crear_grupo_de_rol(settings.PERMISOS_GERENTE_PROYECTO, proyecto, settings.PERMISOS_GERENTE_FASES, [])
    rol = Rol(nombre='Gerente {}'.format(proyecto.pk), descripcion='Rol de Gerente de {}'.format(proyecto.nombre),
               proyecto=proyecto, grupo=grupo, permisos_proyecto=settings.PERMISOS_GERENTE_PROYECTO,
               permisos_fases=settings.PERMISOS_GERENTE_FASES, es_gerente=True)
    rol.save()
    return rol
