# Create your views here.
from django.contrib.auth.models import Group
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required, permission_required
from guardian.decorators import permission_required_or_403
from .forms import *
from .funciones import *
from .models import Rol
import logging

logger = logging.getLogger(__name__)


# Ajax views
@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.crear_roles', (Proyecto, 'id', 'proyecto_id'))
def ajax_crear_roles(request, proyecto_id):
    """AJAX que crea roles dentro del proyecto y retorna la lista de roles caso sea exitoso

    :param request: AJAX Request del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: JsonResponse con la lista de roles
    """
    # Verificar metodo valido
    if request.method == 'POST':
        # Obtener datos del form
        form = Form_Crear_Roles(request.POST, proyecto_id=proyecto_id)

        if form.is_valid():

            # Buscamos el proyecto
            try:
                proyecto = Proyecto.objects.get(pk=proyecto_id)
            except Proyecto.DoesNotExist:
                return JsonResponse({"error": "No existe el proyecto"}, status=400)

            # Extraemos los datos
            nombre = form.cleaned_data['nombre']
            desc = form.cleaned_data['descripcion']
            fases = form.cleaned_data['fases']
            permisos_fases = form.cleaned_data['permisos_fase']
            permisos_proyecto = form.cleaned_data['permisos_proyecto']
            grupo = crear_grupo_de_rol(permisos_proyecto, proyecto, permisos_fases, fases)

            # Creamos el rol
            rol = Rol(nombre=nombre, descripcion=desc, proyecto=proyecto, grupo=grupo,
                      permisos_proyecto=permisos_proyecto, permisos_fases=permisos_fases)
            rol.save()
            rol.fases.set(fases)

            # Logging
            logger.info("{} creó el rol '{}' en el proyecto {}".format(request.user.email, rol, proyecto))

            # Respondemos el request
            return JsonResponse({"roles": Rol.listar_roles_del_proyecto_json(proyecto_id)}, status=200)

        else:
            # Respondemos si no pudo procesar el formulario
            return JsonResponse({"error": form.errors}, status=400)

    # Error desconocido
    return JsonResponse({"error": ""}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.crear_roles', (Proyecto, 'id', 'proyecto_id'))
def ajax_listar_roles(request, proyecto_id):
    """AJAX que retorna la lista de roles del proyecto

    :param request: AJAX Request
    :param proyecto_id: Identificador del Proyecto
    :return: JsonResponse con la lista de roles dle proyecto
    """
    from django.contrib.auth.models import Permission
    # Verificar metodo valido
    if request.method == 'GET':
        # Buscamos los roles del proyecto
        roles_proyecto = Rol.listar_roles_del_proyecto_json(proyecto_id)

        # Enviamos el response
        return JsonResponse({"roles": roles_proyecto,
                             "permisos_proyecto": Rol.listar_permisos_proyecto_opciones(),
                             "permisos_fase": Rol.listar_permisos_fase_opciones(),
                             "presets": Rol.presets()}, status=200)

    # Error desconocido
    return JsonResponse({"error": "No es GET"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.modificar_roles', (Proyecto, 'id', 'proyecto_id'))
def ajax_obtener_rol(request, proyecto_id):
    # Verificar método valido
    if request.is_ajax and request.method == 'GET':
        rol = obtener_rol_dict(proyecto_id, request.GET['uuid'])
        fases = obtener_rol_fases_list(proyecto_id, request.GET['uuid'])
        # Enviamos el response
        return JsonResponse({"rol": rol,
                             "fases": fases}, status=200)

    # Error desconocido
    return JsonResponse({"error": "No es GET"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.modificar_roles', (Proyecto, 'id', 'proyecto_id'))
def ajax_editar_roles(request, proyecto_id):
    """AJAX que crea roles dentro del proyecto y retorna la lista de roles caso sea exitoso

    :param request: AJAX Request del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: JsonResponse con la lista de roles
    """
    # Verificar metodo valido
    if request.method == 'POST':
        # Obtener datos del form
        form = Form_Crear_Roles(request.POST, proyecto_id=proyecto_id)

        if form.is_valid():
            # Buscamos el proyecto
            try:
                proyecto = Proyecto.objects.get(pk=proyecto_id)
            except Proyecto.DoesNotExist:
                return JsonResponse({"error": "No existe el proyecto"}, status=400)

            # Buscamos el rol

            rol = Rol.objects.get(uuid=request.POST['uuid'], proyecto_id=proyecto_id)

            # Extraemos los datos
            rol.nombre = form.cleaned_data['nombre']
            rol.descripcion = form.cleaned_data['descripcion']
            rol.permisos_fases = form.cleaned_data['permisos_fase']
            rol.permisos_proyecto = form.cleaned_data['permisos_proyecto']
            grupo = crear_grupo_de_rol(rol.permisos_proyecto, proyecto, rol.permisos_fases, form.cleaned_data['fases'])

            # Reemplazamos el grupo anterior
            grupo_viejo = rol.grupo
            rol.grupo = grupo
            rol.save()
            grupo_viejo.delete()

            # Guardamos y asiganmos las nuevas fases despues

            rol.fases.set(form.cleaned_data['fases'])
            rol.reasignar_grupos_usuarios()

            # Logging
            logger.info("{} editó el rol '{}' en el proyecto {}".format(request.user.email, rol, proyecto))

            # Respondemos el request
            return JsonResponse({"roles": Rol.listar_roles_del_proyecto_json(proyecto_id)}, status=200)

        else:
            # Respondemos si no pudo procesar el formulario
            return JsonResponse({"error": form.errors}, status=400)

    # Error desconocido
    return JsonResponse({"error": ""}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.eliminar_roles', (Proyecto, 'id', 'proyecto_id'))
def ajax_eliminar_rol(request, proyecto_id):
    # Verificar método valido
    if request.method == 'POST':
        # Buscamos el rol
        rol = Rol.objects.get(uuid=request.POST['uuid'], proyecto_id=proyecto_id)

        # Logging
        logger.info("{} eliminó el rol '{}' en el proyecto {}".format(request.user.email, rol, rol.proyecto))

        grupo: Group = Group.objects.get(pk=rol.grupo.pk)

        rol.delete()
        grupo.delete()

        # Enviamos el response
        return JsonResponse({"roles": Rol.listar_roles_del_proyecto_json(proyecto_id)}, status=200)

    # Error desconocido
    return JsonResponse({"error": "No es AJAX ó POST"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_obtener_roles_del_usuario(request, proyecto_id):
    """Obtiene los roles del usuario en un proyecto

    :param request: Request del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: Lista de roles
    """
    # Verificar que el metodo sea valido
    if request.method != 'GET':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Buscar el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Verificar si es participante
    if not request.user in proyecto.participantes.all():
        return JsonResponse({"error": "No eres participante del proyecto"}, status=403)

    # Retornar roles
    return JsonResponse(Rol.listar_roles_del_usuario_en_proyecto_json(request.user, proyecto_id), status=200,
                        safe=False)
