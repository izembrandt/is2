import pytest
from django.contrib.auth.models import User, Group, Permission
from django.test import RequestFactory
from django.urls import reverse
from .views import *
from django.utils import timezone


# test de funciones

@pytest.mark.django_db
def test_integridad_permisos_proyecto():
    """Verifica que todos los permisos de proyecto declarados en settings existan.
    """
    # Verificar todos los permisos de proyecto
    permisos = []
    for codename in settings.PERMISOS_ADMINISTRADOR_PROYECTO:
        permisos.append(
            Permission.objects.get(codename=codename, content_type=ContentType.objects.get_for_model(Proyecto)))

    assert len(permisos) == len(
        settings.PERMISOS_ADMINISTRADOR_PROYECTO), "No se obtuvieron todos los permisos de proyecto ó hay permisos duplicados"


@pytest.mark.django_db
def test_integridad_permisos_fases():
    """Verifica que todos los permisos de fase declarados en settings existan.
    """
    # Verificar todos los permisos de proyecto
    permisos = []
    for codename in settings.PERMISOS_ADMINISTRADOR_FASES:
        permisos.append(
            Permission.objects.get(codename=codename, content_type=ContentType.objects.get_for_model(Fase)))

    assert len(permisos) == len(
        settings.PERMISOS_ADMINISTRADOR_FASES), "No se obtuvieron todos los permisos de fase ó hay permisos duplicados"


@pytest.mark.django_db
def test_lista_permisos_proyecto_form():
    """Verifica que se generé la lista entera de permisos que se mostrarán en el forms para crear roles.
    """
    lista_permisos = lista_permisos_proyecto_form()
    assert len(lista_permisos) == len(
        settings.PERMISOS_USUARIO_PROYECTO), "No se obtuvieron todos los permisos de proyecto referente al usuario"


@pytest.mark.django_db
def test_lista_permisos_fase_form():
    """Verifica que se generé la lista entera de permisos que se mostrarán en el forms para crear roles.
    """
    lista_permisos = lista_permisos_fase_form()
    assert len(lista_permisos) == len(
        settings.PERMISOS_USUARIO_FASES), "No se obtuvieron todos los permisos de fase referente al usuario"


@pytest.mark.django_db
def test_crear_grupo_rol():
    """Verifica que se pueda crear el grupo que se le asignará al rol
    """
    from django.utils import timezone
    proyecto = Proyecto(nombre="TEST", fecha_creacion=timezone.now())
    proyecto.save()
    fase = Fase(proyecto=proyecto)
    fase.save()
    grupo = crear_grupo_de_rol(settings.PERMISOS_GERENTE_PROYECTO, proyecto, settings.PERMISOS_USUARIO_FASES, [fase])
    assert grupo is not None, "No se creó el grupo exitosamente"


@pytest.mark.django_db
def test_listar_roles_dict():
    """Verifica que se generé la lista entera de todas los roles de un proyecto.
    """
    # Creamos una fecha y asignamos la fecha junto a otros datos a un objeto proyecto
    fecha = timezone.now()
    p = Proyecto(nombre="Testp", descripcion="testp", estado='P', fecha_creacion=fecha)
    p.save()
    # #Creamos una nueva fase para el proyecto
    f = Fase(nombre="Testf", descripcion="testf", proyecto=p, numero=1, estado='A')
    f.save()
    g = crear_grupo_de_rol('', '', '', '')
    r = Rol(nombre="Testr", descripcion="testr", proyecto=p, grupo=g,
            permisos_proyecto='', permisos_fases='')
    r.save()
    # obtenemos el return de la funcion a testear para comparar
    response = listar_roles_dict(p.id)
    # comprobamos que obtiene la lista si pasa el id del rol coincide con el asignado
    assert response[0]['uuid'] == r.uuid, "Se deberia tener una instancia del rol"
