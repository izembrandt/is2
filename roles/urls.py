from django.urls import path
from . import views


urlpatterns = [
    # AJAX
    path('crearRoles/', views.ajax_crear_roles, name='Crear roles'),
    path('listarRoles/', views.ajax_listar_roles, name='Listar roles'),
    path('obtenerRol/', views.ajax_obtener_rol, name='AJAX Obtener Rol'),
    path('editarRol/', views.ajax_editar_roles, name='AJAX Editar Rol'),
    path('eliminarRol/', views.ajax_eliminar_rol, name='AJAX Eliminar Rol'),
    # Tablero
    path('obtenerRoles/', views.ajax_obtener_roles_del_usuario, name='AJAX Obtener Roles del Usuario del Proyecto'),
]