import pytest
import uuid
from django.contrib.auth.models import User


@pytest.mark.django_db
def crear_proyecto_dummy(usuario):
    from proyecto.models import Proyecto
    # Creamos el Proyecto
    nuevo_proyecto = Proyecto.crear_proyecto(nombre='DUMMY', descripcion='DUMMY DESCRIPCIÓN')
    # Agregamos al usuario como gerente
    nuevo_proyecto.asignar_gerente(usuario)
    # Crear Fases aleatorias
    for numero in range(1, 5):
        # Crear fase
        nuevo_proyecto.crear_fase(nombre='DUMMY', descripcion='DUMMY')

    # Crear items
    for fase in nuevo_proyecto.fase_set.all():
        pass

    return nuevo_proyecto
