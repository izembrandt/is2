from __future__ import annotations
from typing import List
from django.contrib.postgres.fields import JSONField
from django.conf import settings
from django.utils import timezone
import uuid
from django.core.files.storage import default_storage
from .constantes import EstadosItem
from fases.models import *
from datetime import datetime

# Modelos

class Archivo(models.Model):
    """Contenedor de un archivo, se identifica por un identificador universal UUID que será utilizado
    para poder consultar el archivo

    :var uuid: Identificador Universal del Archivo
    :var nombre: Nombre del Archivo
    :var archivo: Archivo que se contiene (Guarda el path relativo)

    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nombre = models.CharField("Nombre del archivo", max_length=100)
    archivo = models.FileField(upload_to=settings.CARPETA_ARCHIVOS_SUBIDOS)

    @staticmethod
    def crear_archivo(nombre_recurso: str):
        """Crea un nuevo archivo a partir del nombre del recurso

        :return: El nuevo archivo
        :rtype: Archivo

        """
        # Creamos el archivo
        nuevo_archivo = Archivo(nombre=nombre_recurso)
        nuevo_archivo.archivo.name = nombre_recurso
        nuevo_archivo.save()
        # Hacemos publico para su descarga
        blob = default_storage.bucket.get_blob(nombre_recurso)
        blob.make_public()
        return nuevo_archivo

    @staticmethod
    def obtener_url_subida(nombre: str, content_type: str) -> str:
        """Crea una url de subida para el archivo con tal nombre y tipo

        :param nombre: Nombre del archivo
        :type nombre: str
        :param content_type: Content Type del Archivo
        :type content_type: str
        :return: La URL temporal de subida
        :rtype: str

        """

        return default_storage.obtener_url_subida(nombre, content_type)

    @staticmethod
    def obtener_nombre_disponible(nombre: str):
        """Retorna un nombre no tomado del servidor de almacenamiento

        :param nombre: Nombre original del archivo
        :return: Nombre con caracteres si ya esta tomado

        """
        return default_storage.get_available_name(nombre)

    def __str__(self):
        return "{} - {}".format(self.nombre, self.uuid)


class Atributo(models.Model):
    """Representa un atributo de item, en el se define que tipo es, su nombre, la posición que llevará
    en la lista de atributos, su obligatoriedad y el valor.

    :var nombre: Nombre del Atributo
    :var posicion: Posición del atributo
    :var obligatorio: Si el atributo es obligatorio o no.
    :var tipo: Tipo de Atributo

      .. code-block:: python

         TIPOS_DE_ATRIBUTOS = (
          ('F', 'Fecha'),
          ('N', 'Numero'),
          ('T', 'Texto'),
          ('A', 'Archivo'),
         )

    :var valor: El valor del atributo. Si es un archivo es el ``uuid`` del Archivo

    """
    TIPOS_DE_ATRIBUTOS = (
        ('F', 'Fecha'),
        ('N', 'Numero'),
        ('T', 'Texto'),
        ('A', 'Archivo'),
    )
    nombre = models.CharField("Nombre del atributo", max_length=50)
    posicion = models.IntegerField("Posición del atributo")
    obligatorio = models.BooleanField("Obligatoriedad")
    tipo = models.CharField("Tipo de atributo", max_length=1, choices=TIPOS_DE_ATRIBUTOS)
    valor = models.TextField("Valor del atributo")

    def __str__(self):
        return "{} - {} - {}".format(self.nombre, self.tipo, self.valor)

    def a_dict_json(self):
        """
        Retorna la representación JSON del Atributo

        .. code-block:: python

           {
            "id": ID_DE_ATRIBUTO,
            "nombre": "NOMBRE_DEL_ATRIBUTO",
            "tipo": "TIPO_DE_ATRIBUTO",
            "posicion": "POSICION_DEL_ATRIBUTO",
            "obligatorio": SI_ES_OBLIGATORIO(True_o_False),
            "valor": "VALOR_DEL_ATRIBUTO" // Si es archivo el valor es el nombre del archivo
            "url": "URL_DEL_ARCHIVO" //Solamente si es archivo
           }

        :return: Diccionario que representa el archivo
        :rtype: dict

        """
        dato = {
            'id': self.id,
            'nombre': self.nombre,
            'tipo': self.tipo,
            'posicion': self.posicion,
            'obligatorio': self.obligatorio,
        }

        # Campo Valor
        if self.tipo != 'A' or self.valor == '':
            dato['valor'] = self.valor
        else:
            dato['url'] = Archivo.objects.get(uuid=self.valor).archivo.url
            dato['valor'] = Archivo.objects.get(uuid=self.valor).nombre

        return dato


class TipoItem(models.Model):
    """Representa a un tipo de item que almacena nombre, descripción, prefijo y las estructura de los atributos

    **Atributos**

    nombre
      Nombre del tipo de item. Tiene una longitud máxima de 50 caracteres.

    descripcion
      Descripción del tipo de item. No tiene longitud máxima

    prefijo
      Prefijo que será utilizado en los códigos de los items al ser autoenumerados. Tienen
      una longitud máxima de 4 caracteres.

    atributos
      Estructura de los atributos del tipo de item. Se utiliza JSON para guardar un vector de esta estructura.

      **Estructura del JSON de un Atributo**::

        {
            "nombre": "Nombre del Atributo",
            "tipo": 'Tipo de Atributo',
            "posicion": "Posición del atributo",
            "obligatorio": "True ó False dependiendo de la obligatoriedad",
        }

    proyecto
      Proyecto a la cual pertenece este tipo de item.

    fase
      Fase a la que esta asignada el proyecto
    """
    nombre = models.CharField("Nombre del tipo de item", max_length=50)
    descripcion = models.TextField("Descripción del tipo de item")
    prefijo = models.CharField("Prefijo de Código", max_length=4)
    atributos = JSONField("Estructura de los atributos")
    proyecto = models.ForeignKey('proyecto.Proyecto', on_delete=models.CASCADE)
    fase = models.ForeignKey('fases.Fase', on_delete=models.CASCADE, related_name='tipos_de_item')

    @property
    def items(self):
        """Retorna los items del tipo de item"""
        return self.item_set.all()

    @property
    def usado(self):
        """Retorna si ya se uso el tipo de item"""
        return self.items.count() > 0

    @staticmethod
    def Tipos_Items_Importables_queryset(proyecto_id, usuario):
        """Retorna el queryset de los tipos de item que el usuario puede importar y
        excluye el proyecto_id asi como los prefijos ya incluidos en el proyecto

        :param proyecto_id: Identificador del Proyecto a excluir
        :param usuario: Usuario
        :return: QuerySet<TipoItem>
        """
        # Obtengo los tipos de item que puedo importar
        roles_que_importan = usuario.rol_set.filter(permisos_proyecto__contains='importar_tipo_de_item').exclude(
            proyecto_id=proyecto_id)
        tipos = TipoItem.objects.filter(proyecto__rol__in=roles_que_importan)
        # obtengo los tipos de item del proyecto y sus prefijos
        tipos_local = TipoItem.objects.filter(proyecto=proyecto_id)
        prefijos_local = [tipo.prefijo for tipo in tipos_local]
        # obtengo una lista de los tipos de item que no tienen prefijo ya definido en el proyecto
        tipos_distintos = [x for x in tipos if x.prefijo not in prefijos_local]
        # creo un queryset de los items aceptados
        tipos_distintos_qs = TipoItem.objects.filter(id__in=[o.id for o in tipos_distintos])

        return tipos_distintos_qs

    @staticmethod
    def listar_tipo_items_de_proyecto_queryset(proyecto_id):
        """Retorna un queryset con los tipos de item de un proyecto

        :param proyecto_id: Identificador del proyecto
        :return: QuerySet<TipoItem>
        """
        queryset = TipoItem.objects.filter(proyecto_id=proyecto_id)
        return queryset

    @staticmethod
    def listar_tipo_items_de_proyecto_json(proyecto_id):
        """Retorna los tipos de item del proyecto en su representación JSON (DICT)

        :param proyecto_id: Identificador del Proyecto
        :return: Array con los diccionarios
        """
        return [tipo.a_json_dict() for tipo in TipoItem.objects.filter(proyecto_id=proyecto_id)]

    def a_json_dict(self) -> dict:
        """
        Representación JSON de un tipo de item en un diccionario

        .. code-block:: python

           tipo = {
           'id': ID_TIPO_ITEM,
           'nombre': NOMBRE_TIPO_ITEM,
           'descripcion': DESCRIPCIÓN_TIPO_ITEM,
           'prefijo': PREFIJO_TIPO_ITEM,
           'atributos': ATRIBUTOS_TIPO_ITEM,
           'proyecto': PROYECTO_ID,
           'fase': FASE_DONDE_ACTUA,
           'usado': SI_EL_TIPO_POSEE_ITEMS,
           }

        :rtype: dict
        """
        datos = {
            'id': self.pk,
            'nombre': self.nombre,
            'descripcion': self.descripcion,
            'prefijo': self.prefijo,
            'atributos': self.atributos,
            'proyecto': self.proyecto.pk,
            'fase': self.fase.id,
            'usado': self.usado,
        }
        return datos

    def __str__(self):
        return self.nombre


class Item(models.Model):
    """Modelo de datos de django del Item de un proyecto.

    :meth:`Representación JSON <item.models.Item.a_dict_json>` del item.

    :var uuid: Identificador universal del item. **Llave Primaria**
    :var nombre: Nombre del Item. Longitud máxima de 50 caracteres
    :var descripcion: Descripción del item. No tiene longitud máxima
    :var fecha: Fecha de ultima modificación del item. Cuando se crea, es por **default** la fecha actual
      de la creación
    :var numero: Numero del item que se utiliza en el codigo
    :var costo: Costo de impacto del item
    :var estado: Estado del tipo de item. Se tiene las siguientes opciones

      .. code-block:: python

         ESTADOS_ITEM = [
          ('ED', 'En desarrollo'),
          ('PA', 'Pendiente de Aprobación'),
          ('IA', 'Aprobado'),
          ('LB', 'Aprobado (En línea base)'),
          ('DA', 'Desactivado'),
         ]

    :var tipo_de_item: Tipo de item de la cual el item obtiene su estructura
    :var atributos: Atributos del item
    :var relaciones:
      - Si el item es la ultima versión, son los items hijos y sucesores.
      - Si el item no es la ultima versión, son los items padres y antecesores.

    :var version:  Numero de version del item. **Default** = 1
    :var versiones_anteriores: Items de versiones anteriores
    :var linea_base: La línea base a la que corresponde

    """
    ESTADOS_ITEM = [
        (EstadosItem.DESARROLLO.value, 'En desarrollo'),
        (EstadosItem.PENDIENTE.value, 'Pendiente de Aprobación'),
        (EstadosItem.APROBADO.value, 'Aprobado'),
        (EstadosItem.LINEA_BASE.value, 'Aprobado (En línea base)'),
        (EstadosItem.DESACTIVADO.value, 'Desactivado'),
        (EstadosItem.REVISION.value, 'En revisión'),
        (EstadosItem.BORRADOR.value, 'Borrador'),
    ]
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nombre = models.CharField("Nombre del item", max_length=50)
    descripcion = models.TextField("Descripción del item")
    fecha = models.DateTimeField("Fecha de modificación", default=timezone.now)
    numero = models.IntegerField("Numero del item en la fase", null=False)
    costo = models.IntegerField("Costo del item (Impacto)", null=False)
    estado = models.CharField("Estado del Item", max_length=2, choices=ESTADOS_ITEM)
    tipo_de_item = models.ForeignKey('item.TipoItem', on_delete=models.PROTECT)
    atributos = models.ManyToManyField(Atributo)
    relaciones = models.ManyToManyField('self', symmetrical=False, related_name='conjunto_relaciones')
    version = models.IntegerField("Versión del Item", default=1)
    versiones_anteriores = models.ManyToManyField('self', symmetrical=False, related_name='conjunto_versiones')
    fase = models.ForeignKey('fases.Fase', on_delete=models.PROTECT)
    es_ultimo = models.BooleanField("Ultima version", default=True)
    linea_base = models.ForeignKey('linea_base.LineaBase', on_delete=models.SET_NULL, related_name='items', null=True)

    @staticmethod
    def crear(nombre: str, descripcion: str, costo: int, tipo_de_item: TipoItem,
              atributos: List[Atributo]) -> Item:
        """Crea un nuevo item con los datos especificados

        :param nombre: Nombre del Item
        :param descripcion: Descripción del Item
        :param costo: Costo del Item
        :param tipo_de_item: Tipo del Item
        :param atributos: Lista de atributos del Item
        :return: El item creado
        """
        assert atributos is not None, "No se pasó la lista de atributos"
        assert tipo_de_item.fase.proyecto.en_ejecucion(), "El proyecto no esta iniciado"

        # Obtenemos su numero
        numero = tipo_de_item.item_set.filter(es_ultimo=True).count() + 1

        nuevo_item = Item(nombre=nombre, descripcion=descripcion, costo=costo, estado=EstadosItem.DESARROLLO.value,
                          tipo_de_item=tipo_de_item, fase=tipo_de_item.fase, numero=numero)

        nuevo_item.save()
        nuevo_item.atributos.set(atributos)
        return nuevo_item

    def desactivar_item(self):
        """Desactiva el item, returna true si lo hizo correctamente, o false si no lo hizo"""
        if self.es_modificable() and self.relaciones.count() == 0:
            # Versionar
            self.versionar()
            # Eliminar relaciones padres e hijos
            self.quitar_todos_los_padres_antecesores()
            # Establecer estado y guardar
            self.estado = 'DA'
            self.save()
            return True
        else:
            return False

    @property
    def ultima_version(self):
        """Busca la ultima versión del item y lo retorna

        :return: Ultima versión del Item
        :rtype: Item

        """
        if self.es_ultimo:
            return self
        else:
            return self.conjunto_versiones.get(es_ultimo=True)

    def quitar_todos_los_padres_antecesores(self):
        """Remueve todas las relaciones a padres y antecesores

        """
        self.conjunto_relaciones.remove(*self.obtener_padres_queryset())
        self.conjunto_relaciones.remove(*self.obtener_antecesores_queryset())

    def restaurar(self):
        """Restaura un item a la versión anterior, el item que llama es la versión anterior

        También crea un versión del item antes de ser escrito encima

        :return: Item Nuevo
        :rtype: Item

        """
        item_ultimo = self.ultima_version

        assert not self.es_ultimo, 'No podes restaurar la ultima versión'
        assert item_ultimo.es_modificable(), 'No se puede restaurar un item que no esta en desarrollo'

        item_ultimo.versionar()
        # Recuperar Atributos
        item_ultimo.atributos.set(self.atributos.all())
        # Recuperar relaciones con máximo esfuerzo (Solo recuperamos padres y antecesores de items activos) y eliminar anteriores
        item_ultimo.quitar_todos_los_padres_antecesores()

        for padre in self.obtener_padres_queryset():
            if not padre.estaDesactivado():
                # El padre no esta desactivado, lo relacionamos entonces
                padre.relacionar_a_item(item_ultimo)

        for antecesor in self.obtener_antecesores_queryset():
            if not antecesor.estaDesactivado():
                # El padre no esta desactivado, lo relacionamos entonces
                antecesor.relacionar_a_item(item_ultimo)

        # Recuperar nombre, descripción, costo
        item_ultimo.nombre = self.nombre
        item_ultimo.descripcion = self.descripcion
        item_ultimo.costo = self.costo

        # Guardamos
        item_ultimo.save()

        return item_ultimo

    def es_modificable(self):
        """Retorna **True** si el item se puede modificar, caso contrario, retorna **False**

        :return: Si el item es modificable
        :rtype: bool

        """
        return self.estado == 'ED' and self.fase.proyecto.en_ejecucion() and self.es_ultimo

    def puede_relacionar_a_item(self, item_destino: Item) -> bool:
        """Retorna si es que se puede relacionar el item a itemDestino

        :type item_destino: Item
        :param item_destino: Item a la cual se relaciona
        :return: True si se puede relacionar, False caso contrario
        :rtype: bool

        """
        # No son del mismo proyecto
        if self.fase.proyecto != item_destino.fase.proyecto:
            return False

        # Proyecto desactivado o completo
        if not self.fase.proyecto.en_ejecucion():
            return False

        # Ya esta relacionado
        if item_destino in self.relaciones.all():
            return False

        # Solo se puede relacionar entre fases pegadas o misma fase
        if not (self.fase == item_destino.fase or self.fase.numero == item_destino.fase.numero - 1):
            return False

        # Si es misma fase debe estar aprobado o en linea base cerrada
        if self.fase == item_destino.fase and self.estado not in [EstadosItem.APROBADO, EstadosItem.LINEA_BASE]:
            return False

        # Si es distinta fase debe estar si o si en línea base cerrada
        if (self.fase.numero == item_destino.fase.numero - 1) and self.estado != EstadosItem.LINEA_BASE:
            return False

        # Si el item origen esta en linea base, esta tiene que estar cerrada
        if self.estado == EstadosItem.LINEA_BASE and not self.linea_base.esta_cerrado():
            return False

        # El destino es modificable
        if not item_destino.es_modificable():
            return False

        # No debe formarse ciclos que se pueden dar cuando son misma fase
        if item_destino.fase == self.fase:
            from queue import Queue
            cola_aux = Queue(maxsize=0)
            cola_aux.put(item_destino)
            fase_objetivo_id = item_destino.fase.id

            while not cola_aux.empty():
                # Vamos buscando si el origen es un hijo indirecto
                item_actual = cola_aux.get()

                if self == item_actual:
                    # Forma Ciclo
                    return False

                # Ponemos las demas relaciones que pueden crear ciclos
                for item_posible in item_actual.relaciones.all():
                    if item_posible.fase.id == fase_objetivo_id:
                        # Puede crear problema
                        cola_aux.put(item_posible)

        return True

    def obtener_padres_queryset(self):
        """Obtiene los items padres del item

        :return: Items padres del item
        :rtype: QuerySet<Item>

        """
        if self.es_ultimo:
            return self.conjunto_relaciones.filter(es_ultimo=True, fase=self.fase)
        else:
            return self.relaciones.filter(es_ultimo=True, fase=self.fase)

    def obtener_antecesores_queryset(self):
        """Obtiene los items antecesores del item

        :return: Items antecesores del item
        :rtype: QuerySet<Item>

        """
        if self.es_ultimo:
            return self.conjunto_relaciones.filter(es_ultimo=True, fase__numero=self.fase.numero - 1)
        else:
            return self.relaciones.filter(es_ultimo=True, fase__numero=self.fase.numero - 1)

    def obtener_hijos_queryset(self):
        """Obtiene los items hijos del item

        :return: Items hijos del item
        :rtype: QuerySet<Item>

        """
        if self.es_ultimo:
            return self.relaciones.filter(es_ultimo=True, fase=self.fase)
        else:
            return None

    def obtener_sucesores_queryset(self):
        """Obtiene los items sucesores del item

        :return: Items sucesores del item
        :rtype: QuerySet<Item>

        """
        if self.es_ultimo:
            return self.relaciones.filter(es_ultimo=True, fase__numero=self.fase.numero + 1)
        else:
            return None

    def relacionar_a_item(self, itemDestino, seguro=False):
        """Relaciona un item al otro. El item que llama es el origen.

        La relación final queda self-->itemDestino.

        :type itemDestino: Item
        :param itemDestino: Item a la cual se relaciona.
        :type seguro: Boolean
        :param seguro: True si ya esta verificado que el item puede relacionarse, falso de lo contrario

        """

        assert isinstance(itemDestino, Item), "El destino no es un item"

        if not seguro:
            assert self.puede_relacionar_a_item(itemDestino), 'No puede relacionar a este item'

        self.relaciones.add(itemDestino)

    def puede_desrelacionar_a_item(self, item_destino: Item) -> bool:
        """Retorna si se puede eliminar la relación del item al item destino

        Para que un item se pueda desrelacionar de otro:

        - El origen no debe estar desactivado
        - El destino debe poder ser modificable
        - El destino debe estar relacionado al origen
        - El destino no se puede quedar sin relaciones
        - El proyecto debe estar en ejecución

        :type item_destino: Item
        :param item_destino: Item a la cual se desrelaciona
        :return: True si se puede, False si no
        :rtype: bool

        """
        assert isinstance(item_destino, Item), "Item Destino no es un Item"

        if not item_destino.es_modificable() or self.estaDesactivado():
            return False

        # No esta relacionado
        if not (item_destino in self.relaciones.all()):
            return False

        # Es su unica relación y esta en segunda fase
        if item_destino.fase.numero == 2:
            cantidad_padres = item_destino.obtener_padres_queryset().count()
            cantidad_antecesores = item_destino.obtener_antecesores_queryset().count()
            if cantidad_padres + cantidad_antecesores == 1:
                return False

        # Proyecto desactivado o completo
        if not self.fase.proyecto.en_ejecucion():
            return False

        return True

    def desrelacionar_a_item(self, itemDestino, seguro=False):
        """Rompe una relación a otro item

        :type itemDestino: Item
        :param itemDestino: Item a la cual se desrelaciona

        """
        assert isinstance(itemDestino, Item), "El destino no es un item"

        if not seguro:
            assert self.puede_desrelacionar_a_item(itemDestino)

        self.relaciones.remove(itemDestino)

    def estaDesactivado(self):
        """Retorna true si el item esta desactivado, false si no lo esta

        :return: Si el item esta desactivado
        :rtype: bool

        """
        return self.estado == 'DA'

    def listar_atributos_queryset(self):
        """Retorna el query set de la lista de atributos del item"""
        return self.atributos

    def a_dict_json(self):
        """Obtiene el diccionario que se utiliza para representar como json un item

        La estructura JSON del Item es:

        .. code-block:: python

           {
            'uuid': "UUID_DEL_ITEM",
            'nombre': "NOMBRE_DEL_ITEM",
            'descripcion': "DESCRIPCION_DEL_ITEM",
            'fecha': "FECHA_ULTIMA_MODIFICACIÓN",
            'numero': NUMERO_DEL_CODIGO,
            'costo': COSTO_DEL_ITEM,
            'tipo_de_item': ID_TIPO_ITEM,
            'version': NUMERO_DE_VERSION,
            'fase': "ID_DE_LA_FASE",
            'prefijo': "PREFIJO_DEL_CODIGO",
            'atributos': [atributos],
            'versiones_anteriores': ["UUID_DE_LA_VERSIÓN_ANTERIOR"],
            'relaciones': ["UUID_DEL_ITEM_RELACIONADO"],
            'estado': ESTADO_DEL_ITEM,
            'es_ultimo': TRUE_SI_ES_ULTIMA_VERSIÓN_FALSE_CASO_CONTRARIO,
            'es_aprobable': SI_SE_PUEDE_SOLICITAR_APROBACIÓN,
            'linea_base': LINEA_BASE_DEL_ITEM,
           }

        :return: El diccionario que representa el item
        :rtype: dict

        """
        return {
            'uuid': self.uuid,
            'nombre': self.nombre,
            'descripcion': self.descripcion,
            'fecha': self.fecha_formateada,
            'numero': self.numero,
            'costo': self.costo,
            'tipo_de_item': self.tipo_de_item_id,
            'version': self.version,
            'fase': self.fase_id,
            'prefijo': self.tipo_de_item.prefijo,
            'atributos': self.listar_atributos_json(),
            'versiones_anteriores': self.listar_versiones_anteriores(),
            'relaciones': [item.uuid for item in self.relaciones.all()],
            'antecesores': [item.uuid for item in self.conjunto_relaciones.filter(es_ultimo=True)],
            'estado': self.estado,
            'es_ultimo': self.es_ultimo,
            'es_aprobable': self.puede_solicitar_aprobacion(),
            'linea_base': self.linea_base.uuid if self.linea_base is not None else None,
        }

    def item_version_anterior(self):
        """Retorna el item que corresponde a la versión anterior. Si es la primera versión
        retorna None

        :return: Versión anterior del item
        :rtype: Item

        """
        if (self.version == 1):
            return None

        return self.versiones_anteriores.get(version=self.version - 1)

    def versionar(self):
        """Crea una copia versionada del item y aumenta la versión del actual.

        La versión anterior del item guarda los padres y antecesores en las relaciones.

        """
        # Verificar que se usa en el caso donde se puede versioanr
        assert self.es_modificable(), 'No se puede modificar el item'
        # Creamos la copia
        versionVieja = Item(nombre=self.nombre, descripcion=self.descripcion, fecha=self.fecha, numero=self.numero,
                            costo=self.costo, tipo_de_item=self.tipo_de_item, version=self.version, fase=self.fase,
                            estado=self.estado, es_ultimo=False)
        versionVieja.save()
        versionVieja.atributos.set(self.atributos.all())
        versionVieja.versiones_anteriores.set(self.versiones_anteriores.all())

        # Guardamos todos los padres, antecesores
        versionVieja.relaciones.add(*self.obtener_padres_queryset())
        versionVieja.relaciones.add(*self.obtener_antecesores_queryset())

        # Aumentamos la version del item
        self.version = self.version + 1
        self.fecha = timezone.now()
        self.save()
        self.versiones_anteriores.add(versionVieja)

    def listar_versiones_json(self):
        """Retorna la representación JSON de las versiones de un item

        :return: Lista de versiones

        """
        return [item.a_dict_json() for item in self.versiones_anteriores.all()]

    def listar_atributos_json(self):
        """Retorna la representación JSON de los atributos de un item

        :return: Lista de atributos
        """
        return [atributo.a_dict_json() for atributo in self.atributos.all()]

    @staticmethod
    def listar_items_de_fase_queryset(fase_id: int):
        """Lista los items de una fase

        :param fase_id: Identificador de la fase
        :type fase_id: int
        :return: Lista de items de la fase
        """
        return Item.objects.filter(fase_id=fase_id)

    @staticmethod
    def listar_items_de_un_proyecto_queryset(proyecto_id):
        """Lista los items de un proyecto

        :param proyecto_id: Identificador del proyecto
        :type proyecto_id: int
        :return: Lista de items del proyecto
        :rtype: QuerySet<Item>

        """
        return Item.objects.filter(fase__proyecto_id=proyecto_id) \
            .order_by('tipo_de_item__prefijo', 'numero')

    @property
    def fecha_formateada(self) -> str:
        """Retorna la fecha del item formateada de la forma ``%d-%m-%Y  %H:%M:%S``

        :return: Fecha Formateada
        :rtype: str

        """
        return "{}".format(datetime.date(self.fecha))

    @staticmethod
    def listar_items_de_proyecto_json(proyecto_id):
        """Retorna la lista de los items de un proyecto en representación JSON

        :param proyecto_id: Identificador del Proyecto
        :type proyecto_id: int
        :return: Lista de items del proyecto
        :rtype: list

        """
        queryset = Item.listar_items_de_un_proyecto_queryset(proyecto_id)
        return [item.a_dict_json() for item in queryset]

    @property
    def codigo(self):
        """Código del item. Se define por PREFIJO.NUMERO.

        :return: Código del item
        :rtype: String

        """
        return "{}.{}".format(self.tipo_de_item.prefijo, self.numero)

    def puede_solicitar_aprobacion(self):
        """Retorna True si el item puede solicitar aprobación, caso contrario retorna False

        :return: Si el item puede solicitar aprobación
        :rtype: bool

        """

        # Verificar que este en desarrollo
        if not self.es_modificable():
            return False

        # Verificar Antecesores y sucesores
        padres = self.obtener_padres_queryset()

        if self.fase.numero == 1:  # Si es de la primera fase
            # Si no tiene padres puede ser aprobado
            if padres.count() == 0:
                return True
            # Si tiene padres y todos ellos estan aprobados, puede ser aprobado
            else:
                return padres.count() == padres.filter(estado__in=['IA', 'LB']).count()

        else:
            # Si no es de la primera fase, todos sus padres y todos sus antecesores deben estar aprobados

            antecesores = self.obtener_antecesores_queryset()  # Obtener antecesores

            # Debe tener por lo menos alguna relación
            if padres.count() + antecesores.count() == 0:
                return False

            # Tods sus padres y antecesores deben estar aprobados
            return padres.count() == padres.filter(estado__in=['IA', 'LB']).count() \
                   and antecesores.count() == antecesores.filter(estado__in=['IA', 'LB']).count()

    def solicitar_aprobacion(self):
        """Cambia el estado de un item a 'Pendiente de aprobacion'"""
        assert self.es_modificable(), "No es modificable"
        self.estado = "PA"
        self.save()

    def puede_cancelar_solicitud_aprobacion(self):
        """Retorna True si es que se puede cancelar la solicitud de aprobación, caso contrario retorna False

        :return: Si se puede cancelar la solicitud del item
        :rtype: bool

        """
        return self.estado == 'PA'

    def _sin_descendientes(self, estado: str):
        """Retorna True si es que el item no posee descendientes con el **estado** especificado,
        caso contrario, retorna False

        :param estado: Estado del item que se especifica
        :return: Si no posee descendientes
        :rtype: bool

        """
        return self.relaciones.filter(estado=estado).count() == 0

    def cancelar_solicitud_aprobacion(self):
        """Cancela la solicitud de aprobación del Item"""
        assert self.puede_cancelar_solicitud_aprobacion(), "El item no posee solicitud de aprobación"
        self.estado = "ED"
        self.save()

    def puede_aprobarse(self) -> bool:
        """Verifica que el item se pueda aprobar.

        :return: ``True`` si es que se puede ó ``False`` si es que no puede
        :rtype: bool
        """
        return self.estado == EstadosItem.PENDIENTE

    def aprobar(self):
        """Cambia el estado de un item a 'Item aprobado'"""
        assert self.puede_aprobarse(), "No se puede aprobar el item"
        self.estado = "IA"
        self.save()

    def puede_desaprobarse(self) -> bool:
        """Retorna si es que el item puede desaprobarse.

        Para que un ítem se pueda desaprobar se necesita que:

        - El ítem este aprobado.
        - El ítem debe estar fuera de línea base.
        - No debe tener descendientes (hijos/sucesores) que esten pendiente de aprobación.
        - No debe tener descendientes (hijos/sucesores) que esten aprobados.
        - No debe tener descendientes (hijos/sucesores) que esten en línea base.

        :return: True, si es que puede, False si es que no
        :rtype: bool

        """
        # si el item esta en estado pendiente de aprobacion puede ser aprobado
        if self.estado in [EstadosItem.APROBADO]:
            # si ademas todos sus desendientes no estan aprobados ni pendientes de aprobacion puede ser desaprobado
            return self._sin_descendientes(EstadosItem.PENDIENTE) \
                   and self._sin_descendientes(EstadosItem.APROBADO) \
                   and self._sin_descendientes(EstadosItem.LINEA_BASE)
        else:
            return False

    def desaprobar(self):
        """Cambia el estado de un item a 'En desarrollo'"""
        assert self.puede_desaprobarse(), "No se puede desaprobar el item"
        self.estado = 'ED'
        self.save()

    def listar_versiones_anteriores(self):
        """Retorna una lista ordenada de los items anteriores"""
        lista = [item for item in self.versiones_anteriores.all()]
        lista.sort(key=lambda x: x.version, reverse=True)
        return [item.uuid for item in lista]

    def cambiar_estado_a_en_linea_base(self) -> bool:
        """Cambia el estado de un item para que este dentro de una linea base.

        :return: Si la operación fue exitosa
        :rtype: bool
        """
        # Verificar si se puede meter
        if self.estado != 'IA':
            return False

        assert self.linea_base is None, "El item esta asociado a una línea base"
        # Asignar nuevo estado
        self.estado = 'LB'
        self.save()

        return True

    def sacar_de_linea_base(self) -> bool:
        """Cambia el estado del Item para ser sacado de la linea base y saca las lineas bases
        del item

        :return: Si la operación fue exitosa
        :rtype: bool
        """
        # Verificar si esta en linea base
        if self.linea_base is None:
            return False

        # Verificar si la linea base esta cerrada
        if self.linea_base.esta_cerrado():
            return False

        if self.linea_base.esta_abierto():
            # Estaba abierta la línea base
            self.estado = EstadosItem.APROBADO.value
        elif self.linea_base.esta_roto():
            # Se acaba de romper la línea base
            self.estado = EstadosItem.REVISION.value
        else:
            return False

        self.linea_base = None
        self.save()
        return True

    def poner_en_revision(self) -> bool:
        """Pone el ítem en revisión. Solo se puede poner un ítem en revisión si el proyecto esta en
        ejecución, el ítem esta aprobado o en LB y la fase esta abierta

        :return: True si es que se pudo poner en revisión, False caso contrario
        """
        from fases.models import EstadoFase
        assert self.fase.proyecto.en_ejecucion(), "El proyecto debe estar en ejecución"
        assert self.estado not in [EstadosItem.DESACTIVADO, EstadosItem.DESARROLLO, EstadosItem.PENDIENTE], \
            "El ítem tiene que estar aprobado o en lb"
        assert self.fase.estado == EstadoFase.ABIERTO, "La fase debe estar abierta"
        self.estado = EstadosItem.REVISION.value
        self.save()
        return True

    def poner_en_desarrollo(self):
        assert self.estado in [EstadosItem.REVISION, EstadosItem.PENDIENTE, EstadosItem.APROBADO], \
            "Debe estar en revisión, pendiente de aprobación o aprobado"
        self.estado = EstadosItem.DESARROLLO.value
        self.save()

    def puede_sacar_de_revision(self) -> bool:
        # Verificar que este en desarrollo
        if self.estado != EstadosItem.REVISION:
            return False

        # Verificar Antecesores y sucesores
        padres = self.obtener_padres_queryset()

        if self.fase.numero == 1:  # Si es de la primera fase
            # Si no tiene padres puede ser aprobado
            if padres.count() == 0:
                return True
            # Si tiene padres y todos ellos estan aprobados, puede ser aprobado
            else:
                return padres.count() == padres.filter(
                    estado__in=[EstadosItem.APROBADO, EstadosItem.LINEA_BASE]).count()

        else:
            # Si no es de la primera fase, todos sus padres y todos sus antecesores deben estar aprobados

            antecesores = self.obtener_antecesores_queryset()  # Obtener antecesores

            # Debe tener por lo menos alguna relación
            if padres.count() + antecesores.count() == 0:
                return False

            # Tods sus padres y antecesores deben estar aprobados
            return \
                padres.count() == padres.filter(estado__in=[EstadosItem.APROBADO, EstadosItem.LINEA_BASE]).count() \
                and antecesores.count() == \
                antecesores.filter(estado__in=[EstadosItem.APROBADO, EstadosItem.LINEA_BASE]).count()

    def sacar_de_revision(self):
        # Ver si esta en LB o No
        assert self.estado == EstadosItem.REVISION, "El ítem no esta en revisión"
        assert self.puede_sacar_de_revision(), "El ítem no cumple los requisitos de aprobación"

        if self.linea_base is not None:
            self.estado = EstadosItem.LINEA_BASE
        else:
            self.estado = EstadosItem.APROBADO

        self.save()

    def puede_revisar(self) -> bool:
        """Si es que se puede revisar el ítem.

        :return: True si es que puede, False si es que no se puede
        """
        return self.puede_sacar_de_revision() and self.linea_base is None

    def revisar(self):
        """Pone el ítem de revisión a desarrollo. Todos sus relaciones hijos y padres se
        ponen en revisión
        """
        assert self.puede_revisar(), "El ítem no se puede revisar"

        # Poner en desarrollo
        self.poner_en_desarrollo()

        # Poner todas las relaciones del item en revisión
        for item in self.obtener_sucesores_queryset() \
                .filter(estado__in=[EstadosItem.APROBADO, EstadosItem.LINEA_BASE]):
            item.poner_en_revision()

        for item in self.obtener_hijos_queryset() \
                .filter(estado__in=[EstadosItem.APROBADO, EstadosItem.LINEA_BASE]):
            item.poner_en_revision()

        # Actualizamos el ítem
        self.refresh_from_db()

    def __str__(self):
        return "{}. {} ({})".format(self.codigo, self.nombre, self.uuid)

