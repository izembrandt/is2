import json

from django.http import JsonResponse, HttpRequest, HttpResponseBadRequest, HttpResponseForbidden
from django.core import serializers
from django.contrib.auth.decorators import login_required, permission_required
from django.views.decorators.http import require_http_methods
from guardian.decorators import permission_required_or_403
from guardian.shortcuts import get_perms

from comite_de_cambio.forms import FormAceptarRevision, FormRevisarItem
from proyecto.models import *
from fases.models import *
from .models import *
from .forms import *
from .tasks import *
import logging

logger = logging.getLogger(__name__)


# Ajax views
@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.crear_tipo_de_item', (Proyecto, 'id', 'proyecto_id'))
def ajax_crear_tipo_item(request, proyecto_id):
    """AJAX para crear un nuevo tipo de item en un proyecto. Recibé un llamado AJAX POST
    basado en el formulario para crear tipo de item. Si es válido el formulario, continua con
    la creación del tipo de item y retorna la lista de tipos de items como respuesta. Caso contrario,
    se retorna una respuesta json con el error.

    :param request: HttpRequest con los valores del nuevo tipo de item que se va crear
    :param proyecto_id: Identificador del proyecto
    :return: JsonResponse con la lista de tipos de items si fue exitoso, sino un JsonResponse con el error.

    """
    # Verificar metodo valido
    if request.method == 'POST':
        # Buscamos el proyecto
        try:
            proyecto = Proyecto.objects.get(pk=proyecto_id)
        except Proyecto.DoesNotExist:
            return JsonResponse({"error": "No existe el proyecto"}, status=400)

        # Verificamos si se puede por el estado del proyecto
        if not (proyecto.en_ejecucion() or proyecto.en_configuracion()):
            return JsonResponse({"error": "El proyecto no es modificable."}, status=400)

        # Obtener datos del form
        form = Form_Crear_Tipo_Item(request.POST, proyecto_id=proyecto_id)

        if form.is_valid():
            # Guardamos el nuevo tipo
            nombre = form.cleaned_data['nombre']
            desc = form.cleaned_data['descripcion']
            prefijo = form.cleaned_data['prefijo']
            fase = form.cleaned_data['fase']
            atributos = []

            # Extraemos los datos de los atributos
            nombre_lista = request.POST.getlist('atributos[nombre]')
            tipo_lista = request.POST.getlist('atributos[tipo]')
            obligatorio_lista = request.POST.getlist('atributos[obligatorio]')

            # verificamos que no haya prefijos repetidos TODO: Usar query para saber si existe
            lista_tipo_item = TipoItem.objects.filter(proyecto_id=proyecto_id)
            tipos_iguales = [x for x in lista_tipo_item if x.prefijo == prefijo]
            if (len(tipos_iguales) == 0):
                # Agregamos los atributos al json
                for indice_int in range(len(nombre_lista)):
                    atributos.append({
                        "nombre": nombre_lista[indice_int],
                        "tipo": tipo_lista[indice_int],
                        "posicion": indice_int,
                        "obligatorio": obligatorio_lista[indice_int] == "True",
                    })

                # Creamos el tipo de item
                tipo = TipoItem(nombre=nombre, descripcion=desc, prefijo=prefijo, atributos=atributos,
                                proyecto=proyecto,
                                fase=fase)
                tipo.save()

                # Logging
                logger.info("{} creó el tipo de item '{}' en el proyecto {}".format(request.user.email, tipo, proyecto))

                # Respondemos el request
                return JsonResponse({"tipos": TipoItem.listar_tipo_items_de_proyecto_json(proyecto_id)}, status=200)
            else:
                return JsonResponse({"error": "Prefijo ya existente"}, status=400)
        else:
            # Respondemos si no pudo procesar el formulario
            return JsonResponse({"error": form.errors}, status=400)

    # Error desconocido
    return JsonResponse({"error": ""}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_listar_tipo_item(request, proyecto_id):
    """AJAX para listar los tipos de item de un proyecto. Recibe una llamada AJAX GET y retorna la lista de tipos de items
    del proyecto solicitado. Retorna un respuesta json error si el metodo no es GET o no se usá AJAX

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: JsonResponse con la lista de tipos de item si fue exitosa, caso contrario un error.
    """
    # Verificar metodo valido
    if request.method == 'GET':
        # Enviamos el response
        return JsonResponse({"tipos": TipoItem.listar_tipo_items_de_proyecto_json(proyecto_id),
                             "tipos_importables": [tipo.a_json_dict() for tipo in
                                                   TipoItem.Tipos_Items_Importables_queryset(proyecto_id,
                                                                                             request.user)]
                             }, status=200)

    # Error desconocido
    return JsonResponse({"error": "No es AJAX ó GET"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.eliminar_tipo_de_item', (Proyecto, 'id', 'proyecto_id'))
def ajax_eliminar_tipo_item(request, proyecto_id):
    """AJAX para eliminar los tipos de item de un proyecto. Recibe una llamada AJAX POST y retorna la
    nueva lista de tipos de items si el tipo de item se borrá con exito. Retorna un respuesta json error con
    estado 400 si el metodo no es POST o no se usá AJAX

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: JsonResponse con la lista de tipos de item si fue exitosa, caso contrario un error.
    """
    # Verificar metodo valido
    if request.method == 'POST':
        # Buscamos el proyecto
        try:
            proyecto = Proyecto.objects.get(pk=proyecto_id)
        except Proyecto.DoesNotExist:
            return JsonResponse({"error": "No existe el proyecto"}, status=400)

        # Verificamos si se puede por el estado del proyecto
        if not (proyecto.en_ejecucion() or proyecto.en_configuracion()):
            return JsonResponse({"error": "El proyecto no es modificable."}, status=400)

        # Buscamos el tipo de item
        try:
            tipo_item = TipoItem.objects.get(pk=request.POST['pk'], proyecto_id=proyecto_id)
        except TipoItem.DoesNotExist:
            return JsonResponse({"error": "No existe el tipo de item"}, status=400)

        # Buscamos si ya se usó
        if tipo_item.usado:
            return JsonResponse({"error": "Ya se han declarado items"}, status=400)

        # Logging
        logger.info("{} eliminó el tipo de ítem '{}' en el proyecto {}".format(request.user.email, tipo_item, proyecto))

        # Eliminar
        tipo_item.delete()

        # Enviamos el response
        return JsonResponse({"tipos": TipoItem.listar_tipo_items_de_proyecto_json(proyecto_id)}, status=200)

    # Error desconocido
    return JsonResponse({"error": "No es AJAX ó POST"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_verificar_tipo_item_modificable(request, proyecto_id):
    """AJAX para verificar si el item se puede modificar/eliminar

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: JsonResponse con el tipo de item y un valor valido
    """
    # Verificar metodo valido
    if request.method == 'GET':
        # Buscamos el tipo de item
        tipo_item = TipoItem.objects.get(pk=request.GET['pk'], proyecto_id=proyecto_id)

        # Verificamos los permisos
        if not request.user.has_perm('modificar_tipo_de_item', tipo_item.proyecto) and \
                not request.user.has_perm('eliminar_tipo_de_item', tipo_item.proyecto):
            return JsonResponse({"error": "El posees permisos"}, status=403)

        # Verificamos si se puede por el estado del proyecto
        if not (tipo_item.proyecto.en_ejecucion() or tipo_item.proyecto.en_configuracion()):
            return JsonResponse({"error": "El proyecto no es modificable."}, status=400)

        # Prohibir si tiene items
        if tipo_item.items.count() > 0:
            return JsonResponse({
                "error": "Ya se han declarado items",
                "valido": False,
            }, status=400)

        # Enviamos el response
        return JsonResponse({"valido": True, "tipo": tipo_item.a_json_dict()}, status=200)

    # Error desconocido
    return JsonResponse({"error": "No es AJAX ó GET"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.modificar_tipo_de_item', (Proyecto, 'id', 'proyecto_id'))
def ajax_editar_tipo_item(request, proyecto_id):
    """AJAX para editar los tipos de item de un proyecto.

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: JsonResponse con la lista de tipos de item si fue exitosa, caso contrario un error.
    """
    # Verificar metodo valido
    if request.method == 'POST':
        # Buscamos el tipo de item
        tipo_item = TipoItem.objects.get(pk=request.POST['pk'], proyecto_id=proyecto_id)

        # Verificamos si se puede por el estado del proyecto
        if not (tipo_item.proyecto.en_ejecucion() or tipo_item.proyecto.en_configuracion()):
            return JsonResponse({"error": "El proyecto no es modificable."}, status=400)

        # Buscamos si ya se usó
        if tipo_item.items.count() > 0:
            return JsonResponse({"error": "Ya se han declarado items"}, status=400)

        # Validar formulario
        form = Form_Crear_Tipo_Item(request.POST, proyecto_id=proyecto_id)

        if form.is_valid():
            # Actualizamos los campos
            tipo_item.nombre = form.cleaned_data['nombre']
            tipo_item.descripcion = form.cleaned_data['descripcion']
            tipo_item.prefijo = form.cleaned_data['prefijo']
            tipo_item.fase = form.cleaned_data['fase']
            atributos = []

            # Extraemos los datos de los atributos
            nombre_lista = request.POST.getlist('atributos[nombre]')
            tipo_lista = request.POST.getlist('atributos[tipo]')
            obligatorio_lista = request.POST.getlist('atributos[obligatorio]')

            # Verificamos que no tenga un prefijo repetido TODO: Usar query
            lista_tipo_item = TipoItem.objects.filter(proyecto_id=proyecto_id).exclude(id=tipo_item.id)
            tipos_iguales = [x for x in lista_tipo_item if x.prefijo == tipo_item.prefijo]
            if (len(tipos_iguales) == 0):
                # Agregamos los atributos al json
                for indice_int in range(len(nombre_lista)):
                    atributos.append({
                        "nombre": nombre_lista[indice_int],
                        "tipo": tipo_lista[indice_int],
                        "posicion": indice_int,
                        "obligatorio": obligatorio_lista[indice_int] == "True",
                    })

                tipo_item.atributos = atributos

                # Guardamos el cambio
                tipo_item.save()

                # Logging
                logger.info(
                    "{} editó el tipo de ítem '{}' en el proyecto {}".format(request.user.email, tipo_item,
                                                                             tipo_item.proyecto))
            else:
                return JsonResponse({"error": "prefijo repetido"}, status=400)
        else:
            return JsonResponse({"error": "Form Invalido"}, status=400)

        # Enviamos el response
        return JsonResponse({"tipos": TipoItem.listar_tipo_items_de_proyecto_json(proyecto_id)}, status=200)

    # Error desconocido
    return JsonResponse({"error": "No es AJAX ó GET"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.crear_tipo_de_item', (Proyecto, 'id', 'proyecto_id'))
def ajax_importar_tipo(request, proyecto_id):
    """AJAX para importar los tipos de item de un proyecto.

    :param request: Request del Usuario
    :type request: HttpRequest
    :param proyecto_id: Identificador del Proyecto
    :return: La lista de tipos de item del proyecto
    :rtype: JsonResponse
    """
    # Verificar metodo valido
    if request.method == 'POST':
        # Buscamos el proyecto
        try:
            proyecto = Proyecto.objects.get(pk=proyecto_id)
        except Proyecto.DoesNotExist:
            return JsonResponse({"error": "No existe el proyecto"}, status=400)

        # Verificamos si se puede por el estado del proyecto
        if not (proyecto.en_ejecucion() or proyecto.en_configuracion()):
            return JsonResponse({"error": "El proyecto no es modificable."}, status=400)

        # Obtener datos del form
        form = Form_Importar_Tipo_de_Item(request.POST, proyecto_id=proyecto_id, usuario=request.user)

        if form.is_valid():
            # Guardamos el nuevo tipo
            origen = form.cleaned_data['tipo_de_item_a_importar']
            fase = form.cleaned_data['fase']

            # Buscamos el proyecto
            try:
                proyecto = Proyecto.objects.get(pk=proyecto_id)
            except Proyecto.DoesNotExist:
                return JsonResponse({"error": "No existe el proyecto"}, status=400)

            # Creamos el tipo de item
            tipo = TipoItem(nombre=origen.nombre, descripcion=origen.descripcion, prefijo=origen.prefijo,
                            atributos=origen.atributos, proyecto=proyecto, fase=fase)
            tipo.save()
            # Logging
            logger.info(
                "{} importó el tipo de ítem '{}' en el proyecto {}".format(request.user.email, tipo, proyecto))

            # Respondemos el request
            return JsonResponse({"tipos": TipoItem.listar_tipo_items_de_proyecto_json(proyecto_id)}, status=200)

        else:
            # Respondemos si no pudo procesar el formulario
            return JsonResponse({"error": form.errors}, status=400)

    # Error desconocido
    return JsonResponse({"error": "Metodo invalido"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_obtener_tipo_de_item(request, proyecto_id):
    """Obtiene un tipo de item especifico del proyecto en su representación JSON

    Debe ser método GET y se debe pasar el campo 'tipo_id', que es el id del tipo de item.

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: JsonResponse
    """
    # Verificar que el metodo sea valido
    if request.method != 'GET':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Buscar el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Verificar si es participante
    if not request.user in proyecto.participantes.all():
        return JsonResponse({"error": "No eres participante del proyecto"}, status=403)

    # Buscar el Tipo de Item
    try:
        tipo = TipoItem.objects.get(proyecto_id=proyecto_id, pk=request.GET['tipo_id'])
    except TipoItem.DoesNotExist:
        return JsonResponse({"error": "No existe el tipo de item en el proyecto"}, status=400)

    return JsonResponse(tipo.a_json_dict(), status=200)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_obtener_tipos_de_item_de_proyecto(request, proyecto_id):
    """Retorna los tipos de item de un proyecto en su representación JSON

    :param request: Request del Usuario
    :type request: HttpRequest
    :param proyecto_id: Identificador del Proyecto
    :type proyecto_id: int
    :return: Retorna todos los tipos de item del proyecto
    :rtype: JsonResponse

    """
    # Verificar que el metodo sea valido
    if request.method != 'GET':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Buscar el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Verificar si es participante
    if not request.user in proyecto.participantes.all():
        return JsonResponse({"error": "No eres participante del proyecto"}, status=403)

    return JsonResponse(TipoItem.listar_tipo_items_de_proyecto_json(proyecto_id), status=200, safe=False)


# Items
@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_crear_item_en_fase(request, proyecto_id):
    """Crea un item dentro de una fase sí el usuario posee permisos para crear item (fases.crear_item)
    en dicha fase.

    Retorna la lista de items de la fase si hubo una creación existosa, sino retorna un 400.

    Si el usuario no posee permisos para crear items en dicha fase, se retorna un estado 403.

    Se espera que en el request.POST posea el campo fase_id con el id de la fase y que
    haya una entrada para cada Atributo. Ej: request.POST['Atributo N'] es
    el valor del atributo N. Si un atributo X es un archivo, se espera que este en request.FILES['Atributo X']

    :param request: Request del Usuario
    :type request: HttpRequest
    :param proyecto_id: Identificador del Proyecto
    :type proyecto_id: int
    :return: Retorna el item creado
    :rtype: JsonResponse

    """
    # PERMISOS
    permiso_crear_item = 'fases.crear_item'

    # Verificar que el metodo sea valido
    if request.method != 'POST':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Buscar Fase, Si la fase no corresponde al proyecto, retornar un status=400
    try:
        fase_id = request.POST['fase_id']
        fase = Fase.objects.get(pk=fase_id, proyecto_id=proyecto_id)
    except Fase.DoesNotExist:
        return JsonResponse({"error": "No existe la fase en el proyecto"}, status=400)

    # Verificamos si se puede por el estado del proyecto
    if not fase.proyecto.en_ejecucion():
        return JsonResponse({"error": "No se pueden agregar items a la fase."}, status=400)

    # Verificar Permisos
    if not request.user.has_perm(permiso_crear_item, fase):
        return JsonResponse({"error": "No posee un rol que permita crear item en la fase"}, status=403)

    # Verificar Formulario
    form = Form_crear_item(request.POST, proyecto_id=proyecto_id, fase_id=fase_id)
    if form.is_valid():
        # Formulario es Valido, obtener tipo de item para procesar atributos
        tipo_de_item = form.cleaned_data['tipo_de_item']
        antecesor: Item = form.cleaned_data['antecesor']

        if tipo_de_item.fase != fase:
            return JsonResponse({"error": "Tipo de item invalido para la fase"}, status=400)

        if fase.numero > 1 and antecesor is None:
            return JsonResponse({"error": "Necesita un item antecesor o padre para poder crear en esta fase"},
                                status=400)

        assert tipo_de_item is not None, "No existe el tipo de item"

        # Por cada atributo crear su instancia
        atributos_del_item = []

        for atributoJSON in tipo_de_item.atributos:
            nombreAtributo = 'Atributo {}'.format(atributoJSON['posicion'])
            if atributoJSON['tipo'] == 'A':  # Es archivo
                # Crear archivo
                if request.POST[nombreAtributo] != '':
                    nuevoArchivo = Archivo.crear_archivo(request.POST[nombreAtributo])
                    valor = "{}".format(nuevoArchivo.uuid)
                elif atributoJSON['obligatorio']:
                    # Era obligatorio y no se mandó
                    return JsonResponse({"error": "Hay archivos obligatorios sin subir"}, status=400)
                else:
                    # No es obligatorio
                    valor = ""
            else:
                # Obtener Valor
                if nombreAtributo in request.POST:
                    # Se mandaron los atributos TODO: Verificar para cada tipo de datos si no hubo tampering
                    valor = request.POST[nombreAtributo]
                else:
                    return JsonResponse({"error": "Hay atributos sin subir"}, status=400)

            # Crear Atributo
            atrAux = Atributo(nombre=atributoJSON['nombre'],
                              posicion=atributoJSON['posicion'],
                              tipo=atributoJSON['tipo'],
                              obligatorio=atributoJSON['obligatorio'],
                              valor=valor)
            atrAux.save()

            # Agregar Atributo a la lista
            atributos_del_item.append(atrAux)

        # Crear el item

        # Obtener ultimo numero del tipo de item
        items_del_tipo = tipo_de_item.item_set.filter(es_ultimo=True)

        #  es valido por que un item no se borra, por ende siempre aumenta
        numero = items_del_tipo.count() + 1

        # Asignamos los atributos al nuevo item
        assert len(atributos_del_item) == len(tipo_de_item.atributos), "No se procesaron todos los atributos"

        nuevo_item = Item.crear(nombre=form.cleaned_data['nombre'],
                                descripcion=form.cleaned_data['descripcion'],
                                costo=form.cleaned_data['costo'],
                                tipo_de_item=tipo_de_item,
                                atributos=atributos_del_item)

        # Logging
        logger.info(
            "{} creó el ítem '{}' en el proyecto {}".format(request.user.email, nuevo_item, tipo_de_item.fase.proyecto))

        if antecesor is not None:
            antecesor.relacionar_a_item(nuevo_item)

        return JsonResponse(Item.listar_items_de_proyecto_json(proyecto_id=proyecto_id),
                            status=200, safe=False)
    else:
        # Formulario es invalido
        return JsonResponse({"error": "El formulario es invalido"}, status=400)


# TODO: Verificar que el form tenga datos distintos del item original para poder versionar
@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_modificar_item_en_fase(request, proyecto_id):
    """MOdifica un item dentro de una fase sí el usuario posee permisos para modificar item (fases.modificar_item)
    en dicha fase.

    Retorna la lista de items de la fase si hubo una modificacion existosa, sino retorna un 400.

    Si el usuario no posee permisos para crear items en dicha fase, se retorna un estado 403.

    Se espera que en el request.POST posea el campo fase_id con el id de la fase y que
    haya una entrada para cada Atributo. Ej: request.POST['Atributo N'] es
    el valor del atributo N. Si un atributo X es un archivo, se espera que este en request.FILES['Atributo X']

    :param request: Request del Usuario
    :type request: HttpRequest
    :param proyecto_id: Identificador del Proyecto
    :type proyecto_id: int
    :return: Retorna el nuevo item y su versión anterior
    :rtype: JsonResponse

    """
    # PERMISOS
    permisoModificarItem = 'fases.modificar_item'

    # Verificar que el metodo sea valido
    if request.method != 'POST':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Buscar item original, si no existe retornar un error 400
    try:
        item = Item.objects.get(uuid=request.POST['uuid'])
        fase = Fase.objects.get(pk=item.fase.id, proyecto_id=proyecto_id)
    except Fase.DoesNotExist:
        return JsonResponse({"error": "No existe la fase en el proyecto"}, status=400)
    except Item.DoesNotExist:
        return JsonResponse({"error": "No existe el item en el proyecto"}, status=400)

    # Verificamos si se puede por el estado del proyecto
    if not fase.proyecto.en_ejecucion():
        return JsonResponse({"error": "El proyecto no es modificable."}, status=400)

    # Verificar Permisos
    if not request.user.has_perm(permisoModificarItem, fase):
        return JsonResponse({"error": "No posee un rol que permita crear item en la fase"}, status=403)

    # Verificar Formulario
    form = Form_modificar_item(request.POST)
    if form.is_valid():
        # Formulario es Valido, obtener tipo de item para procesar atributos
        tipo_de_item = item.tipo_de_item

        # Por cada atributo crear su instancia
        atributos_del_item = []
        for atributoJSON in tipo_de_item.atributos:
            # Crear Atributo
            atrAux = Atributo(nombre=atributoJSON['nombre'],
                              posicion=atributoJSON['posicion'],
                              tipo=atributoJSON['tipo'],
                              obligatorio=atributoJSON['obligatorio'],
                              valor="")

            nombreAtributo = 'Atributo {}'.format(atributoJSON['posicion'])

            if atributoJSON['tipo'] == 'A':  # Es archivo
                # Crear archivo
                if request.POST[nombreAtributo] != '':
                    # Verificar si existe el archivo
                    try:
                        nuevoArchivo = Archivo.objects.get(nombre=request.POST[nombreAtributo])
                    except Archivo.DoesNotExist:
                        # No existia, agregar a la base de datos
                        nuevoArchivo = Archivo.crear_archivo(nombre_recurso=request.POST[nombreAtributo])

                    atrAux.valor = "{}".format(nuevoArchivo.uuid)
                elif atributoJSON['obligatorio']:
                    # No se subio ningun archivo pero era obligatorio
                    return JsonResponse({"error": "Hay archivos obligatorios sin subir"}, status=400)

            else:
                # Obtener Valor
                if nombreAtributo in request.POST:
                    # Se mandaron los atributos
                    atrAux.valor = request.POST[nombreAtributo]
                else:
                    atrAux.valor = ""

            atrAux.save()

            # Agregar Atributo a la lista
            atributos_del_item.append(atrAux)

        assert len(atributos_del_item) == len(tipo_de_item.atributos), "No se procesaron todos los atributos"

        # Versionar y modificar
        item.versionar()

        item.atributos.set(atributos_del_item)
        item.nombre = form.cleaned_data['nombre']
        item.descripcion = form.cleaned_data['descripcion']
        item.costo = form.cleaned_data['costo']
        item.save()

        # Logging
        logger.info("{} modificó el ítem '{}' en el proyecto {}".format(request.user.email, item, item.fase.proyecto))

        return JsonResponse(
            {
                'item_nuevo': item.a_dict_json(),
                'item_viejo': item.item_version_anterior().a_dict_json(),
            },
            status=200)
    else:
        # Formulario es invalido
        return JsonResponse({"error": "El formulario es invalido"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_desactivar_item_en_fase(request, proyecto_id):
    """Desactiva un item dentro de una fase sí el usuario posee permisos para desactivar item en dicha fase
    y el item esta solamente fuera de una linea base y no ha sido desactivado previamente.

    Recibe { "uuid":"UUID DEL ITEM" }

    Retorna la lista de items de la fase si hubo una creación existosa, sino retorna un 400.

    Si el usuario no posee permisos para desactivar items o el item no se puede
    desactivar en dicha fase, se retorna un estado 403.

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del Proyecto
    :param fase_id: Identificador de la fase
    :return: JsonResponse

    """
    permisoDesactivarItem = 'eliminar_item'

    # Verificar que el metodo sea valido
    if request.method != 'POST':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Obtener item a desactivar
    try:
        item = Item.objects.get(uuid=request.POST['uuid'], fase__proyecto_id=proyecto_id)
    except:
        return JsonResponse({"error": "No corresponde el item al proyecto"}, status=400)

    # Verificamos si se puede por el estado del proyecto
    if not item.fase.proyecto.en_ejecucion():
        return JsonResponse({"error": "El proyecto no es modificable."}, status=400)

    # Verificar permisos de la fase
    if not request.user.has_perm(permisoDesactivarItem, item.fase):
        return JsonResponse({"error": "No posee un rol que permita crear item en la fase"}, status=403)

    if item.desactivar_item():
        # Logging
        logger.info("{} desactivó el ítem '{}' en el proyecto {}".format(request.user.email, item, item.fase.proyecto))
        return JsonResponse(
            # Mostramos todos los items por que cambian tb los padres e hijos
            Item.listar_items_de_proyecto_json(proyecto_id),
            status=200, safe=False)
    else:
        return JsonResponse({"error": "Item ya esta desactivado o en Linea Base"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_obtener_items_de_proyecto(request, proyecto_id):
    """Retorna los items de un proyecto en su representación JSON

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: Jsonresponse

    """
    # Verificar que el metodo sea valido
    if request.method != 'GET':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Buscar el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Verificar si es participante
    if not request.user in proyecto.participantes.all():
        return JsonResponse({"error": "No eres participante del proyecto"}, status=403)

    # Enviar los items
    return JsonResponse(Item.listar_items_de_proyecto_json(proyecto_id=proyecto_id), status=200, safe=False)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_relacionar_item(request, proyecto_id):
    """Relaciona un item a otro dentro de un proyecto.

    El request debe poseer un campo **uuid_origen** y otro **uuid_destino** que representa la
    relación origen->destino.

    Responde con los siguiente campos:
    - item_origen: Nuevo Item Origen
    - item_destino: Nuevo Item Destino
    - item_versionado: Nuevo Item versionado (Versión antes de que se agregue la relación)

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: JsonResponse con el Item Origen, Destino y versionado si se relacionó, caso contrario contiene el error

    """
    permisoRelacionarItem = 'relacionar_item'

    # Verificar que el metodo sea valido
    if request.method != 'POST':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Buscar el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Verificamos si se puede por el estado del proyecto
    if not proyecto.en_ejecucion():
        return JsonResponse({"error": "El proyecto no es modificable."}, status=400)

    # Verificar si es participante
    if not request.user in proyecto.participantes.all():
        return JsonResponse({"error": "No eres participante del proyecto"}, status=403)

    # Buscar Items
    try:
        itemOrigen = Item.objects.get(uuid=request.POST['uuid_origen'])
        itemDestino = Item.objects.get(uuid=request.POST['uuid_destino'])
    except Item.DoesNotExist:
        return JsonResponse({"error": "No existe el item"}, status=400)

    # Verificar permisos
    if not request.user.has_perm(permisoRelacionarItem, itemDestino.fase):
        return JsonResponse({"error": "No tiene permisos para relacionar este item"}, status=403)

    # Verificar que sea una relación posible
    if itemOrigen.puede_relacionar_a_item(itemDestino):
        # Versionar y relacionar items
        itemDestino.versionar()
        itemOrigen.relacionar_a_item(itemDestino, seguro=True)
        # Logging
        logger.info("{} relacionó el ítem '{}' en el proyecto {}".format(request.user.email, itemDestino,
                                                                         itemDestino.fase.proyecto))
        return JsonResponse({
            'item_origen': itemOrigen.a_dict_json(),
            'item_destino': itemDestino.a_dict_json(),
            'item_versionado': itemDestino.item_version_anterior().a_dict_json(),
        }, status=200)
    else:
        # No se puede
        return JsonResponse({"error": "No se pueden relacionar estos items"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_desrelacionar_item(request, proyecto_id):
    """Desrelaciona un item de otro en un proyecto.

    El request (POST y AJAX) debe poseer un campo **uuid_origen** y otro **uuid_destino** que representa la
    relación origen->destino.

    Responde con los siguiente campos:
    - item_origen: Nuevo Item Origen
    - item_destino: Nuevo Item Destino
    - item_versionado: Nuevo Item versionado (Versión antes de que se agregue la relación)

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: JsonResponse con el Item Origen si se eliminó la relación, caso contrario contiene el error.

    """
    permisoRelacionarItem = 'relacionar_item'

    # Verificar que el metodo sea valido
    if request.method != 'POST':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Buscar el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Verificamos si se puede por el estado del proyecto
    if not proyecto.en_ejecucion():
        return JsonResponse({"error": "El proyecto no es modificable."}, status=400)

    # Verificar si es participante
    if not request.user in proyecto.participantes.all():
        return JsonResponse({"error": "No eres participante del proyecto"}, status=403)

    # Buscar Items
    try:
        itemOrigen = Item.objects.get(uuid=request.POST['uuid_origen'])
        itemDestino = Item.objects.get(uuid=request.POST['uuid_destino'])
    except Item.DoesNotExist:
        return JsonResponse({"error": "No existe el item"}, status=400)

    # Verificar permisos
    if not request.user.has_perm(permisoRelacionarItem, itemDestino.fase):
        return JsonResponse({"error": "No tiene permisos para desrelacionar este item"}, status=403)

    # Verificar que sea una desrelación posible
    if itemOrigen.puede_desrelacionar_a_item(itemDestino):
        # Versionar y desrelacionar items
        itemDestino.versionar()
        itemOrigen.desrelacionar_a_item(itemDestino, seguro=True)

        # Logging
        logger.info("{} desrelacionó el ítem '{}' en el proyecto {}".format(request.user.email, itemDestino,
                                                                            itemDestino.fase.proyecto))
        return JsonResponse({
            'item_origen': itemOrigen.a_dict_json(),
            'item_destino': itemDestino.a_dict_json(),
            'item_versionado': itemDestino.item_version_anterior().a_dict_json(),
        }, status=200)
    else:
        # No se puede
        return JsonResponse({"error": "No se pueden desrelacionar estos items"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_restaurar_item(request, proyecto_id):
    """AJAX para restaurar la versión anterior de item.

    Requiere permiso para modificar item.

    Recibe un request POST con el campo 'uuid' del item que se va a restaurar.

    Retorna un JsonResponse que contiene el item versionado antes de la restauración
    'item_viejo' y el item nuevo 'item_nuevo'.

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador de Proyecto
    :return: JsonResponse con los nuevos items

    """
    permisoModificarItem = 'modificar_item'

    # Verificar que el metodo sea valido
    if request.method != 'POST':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Buscar el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Verificar si es participante
    if not request.user in proyecto.participantes.all():
        return JsonResponse({"error": "No eres participante del proyecto"}, status=403)

    # Verificar que el proyecto este en ejecución
    if not proyecto.en_ejecucion():
        return JsonResponse({"error": "Proyecto no esta en Ejecución"}, status=400)

    # Buscar Items
    try:
        item_version_anterior = Item.objects.get(uuid=request.POST['uuid'], fase__proyecto=proyecto)
    except Item.DoesNotExist:
        return JsonResponse({"error": "No existe el item"}, status=400)

    # Verificar permiso
    if not request.user.has_perm(permisoModificarItem, item_version_anterior.fase):
        return JsonResponse({"error": "No tienes permiso para restaurar este item"}, status=403)

    # Verificar que el item sea una versión valida
    if item_version_anterior.es_ultimo:
        return JsonResponse({"error": "No se puede restaurar la ultima versión"}, status=400)

    # Verificar que se pueda restaurar el item
    if not item_version_anterior.ultima_version.es_modificable():
        return JsonResponse({"error": "No se puede restaurar por que el item no se puede modificar"}, status=400)

    # Restaurar item
    item_version_anterior.restaurar()

    # Logging
    logger.info("{} restauró el ítem '{}' en el proyecto {}".format(request.user.email, item_version_anterior,
                                                                    item_version_anterior.fase.proyecto))

    return JsonResponse(Item.listar_items_de_proyecto_json(proyecto_id), status=200, safe=False)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_obtener_url_de_subida(request, proyecto_id):
    """AJAX para generar una url de subida temporal para subir archivos.

    Recibe un request GET con el campo 'nombre' y el 'content_type'

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: JsonResponse con la url y el nombre del recurso

    """
    # Verificar que el metodo sea valido
    if request.method != 'POST':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Buscar el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Verificamos si se puede por el estado del proyecto
    if not proyecto.en_ejecucion():
        return JsonResponse({"error": "El proyecto no es modificable."}, status=400)

    # Verificar si es participante
    if not request.user in proyecto.participantes.all():
        return JsonResponse({"error": "No eres participante del proyecto"}, status=403)

    # Generar un nombre seguro
    nombre_recurso = Archivo.obtener_nombre_disponible(request.POST['nombre'])

    # Logging
    logger.info("{} solicitó subir un archivo en el proyecto {}".format(request.user.email, proyecto))

    return JsonResponse({
        "url": Archivo.obtener_url_subida(nombre_recurso, request.POST['content_type']),
        "nombre_recurso": nombre_recurso,
    }, status=200)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_solicitud_aprobacion_item(request, proyecto_id):
    """Cambia el estado de un item a solicitud de aprobacion, para que este sea aprobado

    El request (POST y AJAX) debe poseer un campo **uuid** para obtener los datos del item

    Responde con los siguiente campos:
    - items: Items del proyecto

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: JsonResponse con los items del proyecto, caso contrario contiene el error.

    """
    permiso_solicitud = 'modificar_item'

    # Verificar que el metodo sea valido
    if request.method != 'POST':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Buscar el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Verificamos si se puede por el estado del proyecto
    if not proyecto.en_ejecucion():
        return JsonResponse({"error": "El proyecto no es modificable."}, status=400)

    # Verificar si es participante
    if not request.user in proyecto.participantes.all():
        return JsonResponse({"error": "No eres participante del proyecto"}, status=403)

    # Buscar Items
    try:
        item = Item.objects.get(uuid=request.POST['uuid'])
    except Item.DoesNotExist:
        return JsonResponse({"error": "No existe el item"}, status=400)

    # Verificar permisos
    if not request.user.has_perm(permiso_solicitud, item.fase):
        return JsonResponse({"error": "No tiene permisos para hacer solicitud de este item"}, status=403)

    # Verificar que se pueda aprobar el item
    emails = []
    participantes = proyecto.participantes.all()
    for participante in participantes:
        if participante.has_perm(PermisoFase.APROBAR_ITEM, item.fase):
            emails.append(participante.email)

    if item.puede_solicitar_aprobacion():
        item.solicitar_aprobacion()
        # Logging
        logger.info(
            "{} solicitó la aprobación del ítem '{}' en el proyecto {}".format(request.user.email, item, proyecto))
        correo_aprobar_item.delay(emails, proyecto.a_dict_json())

        return JsonResponse(
            {
                'item': item.a_dict_json(),
            },
            status=200)

    else:
        # No se puede
        return JsonResponse({"error": "No se puede hacer solicitud de este item"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_cancelar_solicitud_aprobacion_item(request, proyecto_id):
    """Cambia el estado de un item a solicitud de aprobacion, para que este sea aprobado

    El request (POST y AJAX) debe poseer un campo **uuid** para obtener los datos del item

    Responde con los siguiente campos:
    - item: Item con estado en solicitud de aprobacion

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: JsonResponse con los items del proyecto, caso contrario contiene el error.

    """
    permiso_solicitud = 'modificar_item'

    # Verificar que el metodo sea valido
    if request.method != 'POST':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Buscar el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Verificamos si se puede por el estado del proyecto
    if not proyecto.en_ejecucion():
        return JsonResponse({"error": "El proyecto no es modificable."}, status=400)

    # Verificar si es participante
    if not request.user in proyecto.participantes.all():
        return JsonResponse({"error": "No eres participante del proyecto"}, status=403)

    # Buscar Items
    try:
        item = Item.objects.get(uuid=request.POST['uuid'])
    except Item.DoesNotExist:
        return JsonResponse({"error": "No existe el item"}, status=400)

    # Verificar permisos
    if not request.user.has_perm(permiso_solicitud, item.fase):
        return JsonResponse({"error": "No tiene permisos para cancelar solicitud de este item"}, status=403)

    emails = []
    participantes = proyecto.participantes.all()
    for participante in participantes:
        if participante.has_perm(PermisoFase.MODIFICAR_ITEM, item.fase):
            emails.append(participante.email)

    # Verificar que se pueda cancelar la solicitud de aprobaciom
    if item.puede_cancelar_solicitud_aprobacion():
        item.cancelar_solicitud_aprobacion()
        correo_cancelar_solicitud_aprobacion.delay(emails, item.a_dict_json())
        # Logging
        logger.info(
            "{} canceló la solicitud la aprobación del ítem '{}' en el proyecto {}".format(request.user.email, item,
                                                                                           proyecto))
        return JsonResponse(
            {
                'item': item.a_dict_json(),
            },
            status=200)
    else:
        # No se puede
        return JsonResponse({"error": "No se puede cancelar la solicitud de este item"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_aprobar_item(request, proyecto_id):
    """Cambia el estado de un item a aprobado

    El request (POST y AJAX) debe poseer un campo **uuid** para obtener los datos del item

    Responde con los siguiente campos:
    - items: Items del proyecto

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: JsonResponse con el Item Origen si se eliminó la relación, caso contrario contiene el error.

    """
    permiso_aprobar = 'aprobar_item'

    # Verificar que el metodo sea valido
    if request.method != 'POST':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Buscar el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Verificamos si se puede por el estado del proyecto
    if not proyecto.en_ejecucion():
        return JsonResponse({"error": "El proyecto no es modificable."}, status=400)

    # Verificar si es participante
    if not request.user in proyecto.participantes.all():
        return JsonResponse({"error": "No eres participante del proyecto"}, status=403)

    # Buscar Items
    try:
        item = Item.objects.get(uuid=request.POST['uuid'])
    except Item.DoesNotExist:
        return JsonResponse({"error": "No existe el item"}, status=400)

    # Verificar permisos
    if not request.user.has_perm(permiso_aprobar, item.fase):
        return JsonResponse({"error": "No tiene permisos para hacer solicitud de este item"}, status=403)
    emails = []
    participantes = proyecto.participantes.all()
    for participante in participantes:
        if participante.has_perm(PermisoFase.MODIFICAR_ITEM, item.fase):
            emails.append(participante.email)

    # Verificar que se pueda aprobar el item
    if item.puede_aprobarse():
        item.aprobar()
        correo_aprobacion_item.delay(emails, item.a_dict_json())
        # Logging
        logger.info(
            "{} aprobó el ítem '{}' en el proyecto {}".format(request.user.email, item, proyecto))
        return JsonResponse(
            {
                'item': item.a_dict_json(),
            },
            status=200)
    else:
        # No se puede
        return JsonResponse({"error": "No se puede aprobar este item"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_desaprobar_item(request, proyecto_id):
    """Cambia el estado de un item a desaprobado

    El request (POST y AJAX) debe poseer un campo **uuid** para obtener los datos del item

    Responde con los siguiente campos:
    - items: Items del proyecto

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: JsonResponse con el Item Origen si se eliminó la relación, caso contrario contiene el error.
    """
    permiso_desaprobar = 'desaprobar_item'

    # Verificar que el metodo sea valido
    if request.method != 'POST':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Buscar el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Verificamos si se puede por el estado del proyecto
    if not proyecto.en_ejecucion():
        return JsonResponse({"error": "El proyecto no es modificable."}, status=400)

    # Verificar si es participante
    if not request.user in proyecto.participantes.all():
        return JsonResponse({"error": "No eres participante del proyecto"}, status=403)

    # Buscar Items
    try:
        item = Item.objects.get(uuid=request.POST['uuid'])
    except Item.DoesNotExist:
        return JsonResponse({"error": "No existe el item"}, status=400)

    # Verificar permisos
    if not request.user.has_perm(permiso_desaprobar, item.fase):
        return JsonResponse({"error": "No tiene permisos para desaprobar de este item"}, status=403)

    proyecto = Proyecto.objects.get(pk=proyecto_id)
    item = Item.objects.get(uuid=request.POST['uuid'])
    emails = []
    participantes = proyecto.participantes.all()
    for participante in participantes:
        if (participante.has_perm(PermisoFase.MODIFICAR_ITEM, item.fase)) \
                or (participante.has_perm(PermisoFase.APROBAR_ITEM, item.fase)):
            emails.append(participante.email)

    # Verificar que se pueda desaprobar el item
    if item.puede_desaprobarse():
        item.desaprobar()
        correo_cancelar_aprobacion.delay(emails, item.a_dict_json())
        # Logging
        logger.info(
            "{} desaprobó el ítem '{}' en el proyecto {}".format(request.user.email, item, proyecto))
        return JsonResponse(
            {
                'item': item.a_dict_json(),
            },
            status=200)
    else:
        # No se puede
        return JsonResponse({"error": "No se puede desaprobar este item"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["POST"])
def ajax_aceptar_revision_item(request: HttpRequest, proyecto_id: int) -> JsonResponse:
    # Obtener el proyecto
    try:
        proyecto: Proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=HttpResponseBadRequest.status_code)

    # Formulario
    form: FormAceptarRevision = FormAceptarRevision(request.POST, proyecto=proyecto)

    # Validamos el formulario
    if form.is_valid():
        item: Item = form.cleaned_data['item']

        if not request.user.has_perm(PermisoFase.APROBAR_ITEM, item.fase):
            return JsonResponse({"error": "No posee los permisos para aceptar la revisión del item"},
                                status=HttpResponseForbidden.status_code)

        if not item.puede_sacar_de_revision():
            return JsonResponse({"error": "No cumple los requisitos el ítem"},
                                status=HttpResponseBadRequest.status_code)

        item.sacar_de_revision()
        # Logging
        logger.info(
            "{} aceptó la revisión del ítem '{}' en el proyecto {}".format(request.user.email, item, proyecto))

        # Enviar los items
        return JsonResponse(Item.listar_items_de_proyecto_json(proyecto_id=proyecto_id), status=200, safe=False)

    else:
        return JsonResponse({"error": json.loads(form.errors.as_json())}, status=HttpResponseBadRequest.status_code)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["POST"])
def ajax_revisar_item(request: HttpRequest, proyecto_id: int) -> JsonResponse:
    # Obtener el proyecto
    try:
        proyecto: Proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=HttpResponseBadRequest.status_code)

    # Formulario
    form: FormRevisarItem = FormRevisarItem(request.POST, proyecto=proyecto)

    # Validamos el formulario
    if form.is_valid():
        item: Item = form.cleaned_data['item']

        if not request.user.has_perm(PermisoFase.DESAPROBAR_ITEM, item.fase):
            return JsonResponse({"error": "No posee los permisos para aceptar la revisión del item"},
                                status=HttpResponseForbidden.status_code)

        if not item.puede_revisar():
            return JsonResponse({"error": "No cumple los requisitos el ítem para revisar"},
                                status=HttpResponseBadRequest.status_code)

        item.revisar()
        # Logging
        logger.info(
            "{} revisó el ítem '{}' en el proyecto {}".format(request.user.email, item, proyecto))

        # Enviar los items
        return JsonResponse(Item.listar_items_de_proyecto_json(proyecto_id=proyecto_id), status=200, safe=False)

    else:
        return JsonResponse({"error": json.loads(form.errors.as_json())}, status=HttpResponseBadRequest.status_code)
