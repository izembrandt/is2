import pytest
from django.test import RequestFactory
from django.urls import reverse
from django.contrib.auth.models import User, Group
from .ajax import *
from django.utils import timezone
from roles.views import *






@pytest.mark.django_db
def test_ajax_crear_tipo_item():
    """Verifica si que la funcion crear tipos de item responda correctamente al request
    """
    # Creamos una fecha y creamos un proyecto
    fecha = timezone.now()
    p = Proyecto(nombre="Test",descripcion="test", estado='P', fecha_creacion=fecha)
    p.save()
    f = crear_fase("test_Crear",p)

    #Buscamos el path de lal funcion
    path = reverse('Crear tipo de item',args=[p.id])
    request = RequestFactory().post(
            path,
            {
                'nombre': 'Test',
                'descripcion': 'bla bla',
                'prefijo' : 'tst',
                'atributos': [],
                'fase': f.pk,
            }
        )
    # Creamos un usuario que pueda acceder al proyecto
    request.user = User.objects.create_user(username='tester', email='tester@gmail.com', password='password')
    grupo = Group.objects.get(name='Usuario')
    request.user.groups.add(grupo)
    # permisos
    grupo_rol = crear_grupo_de_rol(['listar_proyectos', 'crear_tipo_de_item'], p, [], [])
    rol = Rol(nombre='test', descripcion='test', proyecto=p, grupo=grupo_rol,
              permisos_proyecto=['listar_proyectos', 'crear_tipo_de_item'], permisos_fases='')
    rol.save()
    rol.asignar_usuario(request.user)
    # Creamos la fase
    response = ajax_crear_tipo_item(request,proyecto_id=p.id)
    # comprobamos que fue un exito si se obtiene un response con codigo 200
    assert  response.status_code == 200, "Un usuario con permisos deberia poder recibir un request correcta si tiene los permisos"

@pytest.mark.django_db
def test_ajax_editar_tipo_item():
    """Verifica si que la funcion editar tipos de item responda correctamente al request
    """
    # Creamos una fecha y creamos un proyecto
    fecha = timezone.now()
    p = Proyecto(nombre="Test",descripcion="test", estado='P', fecha_creacion=fecha)
    p.save()
    f = crear_fase("test",p)
    #Creamos un tipo de item
    tipo = TipoItem(nombre='Tipo test', descripcion='bla bla', prefijo='tst', atributos=[], proyecto=p, fase = f)
    tipo.save()
    #Buscamos el path de lal funcion
    path = reverse('Editar tipo de item',args=[p.id])
    request = RequestFactory().post(
            path,
            {
                'pk': tipo.id,
                'nombre': 'nuevo nombre',
                'descripcion': 'nueva descripcion ',
                'prefijo' : 'abc',
                'atributos': [],
                'fase': f.pk,
            }
        )
    # Creamos un usuario que pueda acceder al proyecto
    request.user = User.objects.create_user(username='tester', email='tester@gmail.com', password='password')
    grupo = Group.objects.get(name='Usuario')
    request.user.groups.add(grupo)
    # permisos
    grupo_rol = crear_grupo_de_rol(['listar_proyectos', 'modificar_tipo_de_item'], p, [], [])
    rol = Rol(nombre='test', descripcion='test', proyecto=p, grupo=grupo_rol,
              permisos_proyecto=['listar_proyectos', 'modificar_tipo_de_item'], permisos_fases='')
    rol.save()
    rol.asignar_usuario(request.user)
    # Creamos la fase
    response = ajax_editar_tipo_item(request,proyecto_id=p.id)
    print(response.content)
    # comprobamos que fue un exito si se obtiene los datos modificados del tipo de item
    tiposQuery = TipoItem.objects \
        .filter(pk=tipo.id) \
        .values()
    assert  tiposQuery[0]['nombre'] == 'nuevo nombre', "El tipo de item debe tener como nombre el nombre editado"
    assert  tiposQuery[0]['descripcion'] == 'nueva descripcion', "El tipo de item debe tener la descripcion actualizadaq"
    assert  tiposQuery[0]['prefijo'] == 'abc', "El item debe tener el prefijo actualizado"

@pytest.mark.django_db
def test_ajax_eliminar_tipo_item():
    """Verifica si que la funcion eliminar tipos de item responda correctamente al request
    """
    # Creamos una fecha y creamos un proyecto
    fecha = timezone.now()
    p = Proyecto(nombre="Test", descripcion="test", estado='P', fecha_creacion=fecha)
    p.save()
    f = crear_fase("nombre",p)
    # Creamos un tipo de item
    tipo = TipoItem(nombre='Tipo test', descripcion='bla bla', prefijo='tst', atributos=[], proyecto=p, fase=f)
    tipo.save()
    cant_tipos = TipoItem.objects \
        .filter(pk=tipo.id) \
        .values().count()
    assert cant_tipos == 1, "No se obtuvo ninguna instancia del item que creamos y la funcion eliminar no podra ser comprobada"
    # Buscamos el path de lal funcion
    path = reverse('Eliminar tipo de item', args=[p.id])
    request = RequestFactory().post(
        path,
        {
            'pk': tipo.id,
        }
    )
    # Creamos un usuario que pueda acceder al proyecto
    request.user = User.objects.create_user(username='tester', email='tester@gmail.com', password='password')
    grupo = Group.objects.get(name='Usuario')
    request.user.groups.add(grupo)
    # permisos
    grupo_rol = crear_grupo_de_rol(['listar_proyectos', 'eliminar_tipo_de_item'], p, [], [])
    rol = Rol(nombre='test', descripcion='test', proyecto=p, grupo=grupo_rol,
              permisos_proyecto=['listar_proyectos', 'eliminar_tipo_de_item'], permisos_fases='')
    rol.save()
    rol.asignar_usuario(request.user)
    # Creamos la fase
    response = ajax_eliminar_tipo_item(request, proyecto_id=p.id)
    # comprobamos que fue un exito si se obtiene 0 instancias del tipo de item
    cant_tipos = TipoItem.objects \
        .filter(pk=tipo.id) \
        .values().count()
    assert cant_tipos == 0, "No se elimino el tipo de item, porque sigue existiendo un tipo de item con el id del item que quisimos eliminar"

@pytest.mark.django_db
def test_ajax_crear_item_en_fase():
    """Verifica se creé correctamente los items

    CU-009-01. Crear ítem
    """
    # Creamos una fecha y asignamos como descripcion de la fase
    identificador = "{}".format(uuid.uuid4())
    #creamos proyecto
    p = crear_proyecto(identificador)
    # Creamos una fase
    f = crear_fase(identificador,p)
    # Ejecutamos
    p.ejecutar()
    cant_fases = Fase.objects.filter(proyecto_id=p.id).values().count()
    assert cant_fases == 1, "El proyecto deberia tener 1 fase para testear la funcion crear"
    # Creamos tipo de item
    t = crear_tipo_item(identificador,f,p)
    # Buscamos el path de lal funcion
    path = reverse('AJAX Crear Item en Fase', args=[p.id])
    request = RequestFactory().post(
        path,
        {
            'fase_id': f.pk,
            'nombre': identificador,
            'descripcion': identificador,
            'costo': 1,
            'tipo_de_item': "{}".format(t.pk)

        },
        HTTP_X_REQUESTED_WITH='XMLHttpRequest'
    )
    # Creamos un usuario que pueda acceder al proyecto
    request.user = User.objects.create_user(username=identificador, email=identificador, password=identificador)
    grupo = Group.objects.get(name='Usuario')
    request.user.groups.add(grupo)
    # permisos
    grupo_rol = crear_grupo_de_rol([], p, ['crear_item'], [f])
    rol = Rol(nombre='test', descripcion='test', proyecto=p, grupo=grupo_rol,
              permisos_proyecto=[], permisos_fases=['crear_item'])
    rol.save()
    rol.asignar_usuario(request.user)
    rol.fases.add(f)
    # Crear item
    response = ajax_crear_item_en_fase(request, proyecto_id=p.pk)
    print(response.content)
    assert response.status_code == 200, "No se pudo crear el item"

@pytest.mark.django_db
def test_ajax_modificar_item_en_fase():
    """Verifica se creé correctamente los items

    CU-010-03. Modificar atributos de un ítem
    """
    # Creamos una fecha y asignamos como descripcion de la fase
    #Creamos una id
    identificador = "{}".format(uuid.uuid4());
    #creamos proyecto
    p = crear_proyecto(identificador)
    # Creamos una fase
    f = crear_fase(identificador,p)
    # Creamos tipo de item
    t = crear_tipo_item(identificador,f,p)
    # Ejecutamos
    p.ejecutar()
    # Creamos item
    i = crear_item(identificador, f, t, 'ED')
    cant_fases = Fase.objects.filter(proyecto_id=p.id).values().count()
    assert cant_fases == 1, "El proyecto deberia tener 1 fase para testear la funcion modificar"

    # Buscamos el path de lal funcion
    path = reverse('AJAX Modificar Item en Fase', args=[p.id])
    request = RequestFactory().post(
        path,
        {
            'uuid': i.uuid,
            'nombre': "nuevo nombre",
            'descripcion': identificador,
            'costo': 1,

        },
        HTTP_X_REQUESTED_WITH='XMLHttpRequest'
    )
    # Creamos un usuario que pueda acceder al proyecto
    request.user = User.objects.create_user(username=identificador, email=identificador, password=identificador)
    grupo = Group.objects.get(name='Usuario')
    request.user.groups.add(grupo)
    # permisos
    grupo_rol = crear_grupo_de_rol([], p, ['modificar_item'], [f])
    rol = Rol(nombre='test', descripcion='test', proyecto=p, grupo=grupo_rol,
              permisos_proyecto=[], permisos_fases=['modificar_item'])
    rol.save()
    rol.asignar_usuario(request.user)
    rol.fases.add(f)
    # Crear item
    response = ajax_modificar_item_en_fase(request, proyecto_id=p.pk)
    print(response.content)
    assert response.status_code == 200, "No se pudo modificar el item"

@pytest.mark.django_db
def test_ajax_desactivar_item_en_fase():
    """Verifica se desactive correctamente los items

    CU-009-02. Desactivar ítem
    """
    #Creamos una id
    identificador = "{}".format(uuid.uuid4());
    #creamos proyecto
    p = crear_proyecto(identificador)
    # Creamos una fase
    f = crear_fase(identificador,p)
    cant_fases = Fase.objects.filter(proyecto_id=p.id).values().count()
    assert cant_fases == 1, "El proyecto deberia tener 1 fase para testear la funcion desactivar"
    # Ejecutamos
    p.ejecutar()
    # Creamos tipo de item
    t = crear_tipo_item(identificador,f,p)
    # Creamos item
    i = crear_item(identificador, f, t, 'ED')
    # Buscamos el path de lal funcion
    path = reverse('AJAX Desactivar Item en Fase', args=[p.id])
    request = RequestFactory().post(
        path,
        {
            'fase_id': f.pk,
            'uuid': "{}".format(i.uuid),
        },
        HTTP_X_REQUESTED_WITH='XMLHttpRequest'
    )
    # Creamos un usuario que pueda acceder al proyecto
    request.user = User.objects.create_user(username=identificador, email=identificador, password=identificador)
    grupo = Group.objects.get(name='Usuario')
    request.user.groups.add(grupo)
    # permisos
    grupo_rol = crear_grupo_de_rol([], p, ['eliminar_item'], [f])
    rol = Rol(nombre='test', descripcion='test', proyecto=p, grupo=grupo_rol,
              permisos_proyecto=[], permisos_fases=['eliminar_item'])
    rol.save()
    rol.asignar_usuario(request.user)
    rol.fases.add(f)
    # Desactivar item
    response = ajax_desactivar_item_en_fase(request, proyecto_id=p.pk)
    print(response.content)
    # Verificar si se logro desactivar
    assert response.status_code == 200, "No se pudo desactivar el item"
    # Verificar si se desactivo
    i.refresh_from_db()
    assert i.estaDesactivado(), 'No se desactivó el item'

@pytest.mark.django_db
def test_model_versionar():
    """Verifica se se solicita aprobacion correctamente de un item
    """
    # Creamos una fecha y asignamos como descripcion de la fase
    #Creamos una id
    identificador = "{}".format(uuid.uuid4())
    #creamos proyecto
    p = crear_proyecto(identificador)
    # Creamos una fase
    f = crear_fase(identificador,p)
    # Ejecutamos
    p.ejecutar()
    # Creamos tipo de item
    t = crear_tipo_item(identificador,f,p)
    # Creamos item
    i = crear_item(identificador, f, t, 'ED')
    assert i.version == 1, 'No se puedo crear correctamente el item'
    i.versionar()
    assert i.version == 2, 'No se pudo versionar correctamente el item'

@pytest.mark.django_db
def test_model_solicitar_aprobacion():
    """Verifica si se solicita correctamente la aprobacion de un item
    """
    #Creamos una id
    identificador = "{}".format(uuid.uuid4());
    #creamos proyecto
    p = crear_proyecto(identificador)
    # Creamos una fase
    f = crear_fase(identificador,p)
    # Creamos tipo de item
    t = crear_tipo_item(identificador,f,p)
    # Ejecutamos
    p.ejecutar()
    # Creamos item
    i = crear_item(identificador, f, t, 'ED')
    i.solicitar_aprobacion()
    assert i.estado == 'PA', 'No se pudo solicitar la aprobacion correctamente el item'

@pytest.mark.django_db
def test_model_aprobar():
    """Verifica si se aprueba correctamente un item
    """
    #Creamos una id
    identificador = "{}".format(uuid.uuid4());
    #creamos proyecto
    p = crear_proyecto(identificador)
    # Creamos una fase
    f = crear_fase(identificador,p)
    # Creamos tipo de item
    t = crear_tipo_item(identificador,f,p)
    # Ejecutamos
    p.ejecutar()
    # Creamos item
    i = crear_item(identificador, f, t, 'PA')
    #Hacemos un llamado de la funcion aprobar
    i.aprobar()
    assert i.estado == 'IA', 'No se pudo aprobar correctamente el item'

@pytest.mark.django_db
def test_model_cancelar_solicitud():
    """Verifica si se versione correctamente un item
    """
    #Creamos una id
    identificador = "{}".format(uuid.uuid4());
    #creamos proyecto
    p = crear_proyecto(identificador)
    # Creamos una fase
    f = crear_fase(identificador,p)
    # Creamos tipo de item
    t = crear_tipo_item(identificador,f,p)
    # Ejecutamos
    p.ejecutar()
    # Creamos item
    i = crear_item(identificador, f, t, 'PA')
    #hacemos un llamado  de la solicitud de aprobacion
    i.cancelar_solicitud_aprobacion()
    assert i.estado == 'ED', 'No se pudo cancelar la solicitud de aprobacion correctamente el item'

@pytest.mark.django_db
def test_model_desaprobar():
    """Verifica si se desaprueba correctamente un item
    """
    #Creamos una id
    identificador = "{}".format(uuid.uuid4());
    #creamos proyecto
    p = crear_proyecto(identificador)
    # Creamos una fase
    f = crear_fase(identificador,p)
    # Creamos tipo de item
    t = crear_tipo_item(identificador,f,p)
    # Ejecutamos
    p.ejecutar()
    # Creamos item
    i = crear_item(identificador, f, t, 'IA')
    #Hacemos un llamado de la funcion desaprobar
    i.desaprobar()
    assert i.estado == 'ED', 'No se pudo desaprobar correctamente el item'

def crear_proyecto(id):
    fecha = timezone.now()
    proy = Proyecto(nombre=id,
                 descripcion=id,
                 estado='P',
                 fecha_creacion=fecha)
    proy.save()
    return proy

def crear_fase(id,proy):
    fas = Fase(nombre=id,
             descripcion=id,
             proyecto=proy,
             numero=1,
             estado='A')
    fas.save()
    return fas

def crear_tipo_item(id,fas,proy):
    tip = TipoItem(nombre=id,
                 descripcion=id,
                 prefijo='TEST',
                 atributos=[],
                 proyecto_id=proy.pk,
                 fase = fas
                 )
    tip.save()
    return tip
def crear_item(id,fas,tip,cod):
    ite = Item(fase_id=fas.pk,
             nombre=id,
             descripcion=id,
             costo=1,
             tipo_de_item=tip,
             numero=10,
             estado=cod
             )
    ite.save()
    return ite