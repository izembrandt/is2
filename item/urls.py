from django.urls import path
from . import ajax

urlpatterns = [
    path('crearTipo/', ajax.ajax_crear_tipo_item, name='Crear tipo de item'),
    path('listarTipo/', ajax.ajax_listar_tipo_item, name='Listar tipo de item'),
    path('verificarTipo/', ajax.ajax_verificar_tipo_item_modificable, name='Verificar tipo de item'),
    path('eliminarTipo/', ajax.ajax_eliminar_tipo_item, name='Eliminar tipo de item'),
    path('editarTipo/', ajax.ajax_editar_tipo_item, name='Editar tipo de item'),
    path('importarTipo/', ajax.ajax_importar_tipo, name='AJAX Importar Tipo'),
    path('obtenerTipo/', ajax.ajax_obtener_tipo_de_item, name='AJAX Obtener tipo de item'),
    path('obtenerTipos/', ajax.ajax_obtener_tipos_de_item_de_proyecto, name='AJAX Obtener tipos de item de proyecto'),
    # AJAX Item
    path('crearItem/', ajax.ajax_crear_item_en_fase, name='AJAX Crear Item en Fase'),
    path('modificarItem/', ajax.ajax_modificar_item_en_fase, name='AJAX Modificar Item en Fase'),
    path('desactivarItem/', ajax.ajax_desactivar_item_en_fase, name='AJAX Desactivar Item en Fase'),
    path('obtenerItemsProyecto/', ajax.ajax_obtener_items_de_proyecto, name='AJAX Obtener Items del Proyecto'),
    path('relacionarItem/', ajax.ajax_relacionar_item, name='AJAX Relacionar items de proyecto'),
    path('desrelacionarItem/', ajax.ajax_desrelacionar_item, name='AJAX Desrelacionar items de proyecto'),
    path('solicitudAprobacionItem/', ajax.ajax_solicitud_aprobacion_item,
         name='AJAX Solicitud Aprobar items de proyecto'),
    path('cancelarsolicitudAprobacionItem/', ajax.ajax_cancelar_solicitud_aprobacion_item,
         name='AJAX Cancelar Solicitud Aprobar items de proyecto'),
    path('AprobarItem/', ajax.ajax_aprobar_item, name='AJAX Aprobar items de proyecto'),
    path('DesaprobarItem/', ajax.ajax_desaprobar_item, name='AJAX Desaprobar items de proyecto'),
    path('restaurarItem/', ajax.ajax_restaurar_item, name='AJAX Restaurar Item de Proyecto'),
    # AJAX Archivos
    path('solicitarSubidaArchivo/', ajax.ajax_obtener_url_de_subida, name='AJAX Obtener URL de Subida'),
    # AJAX Comite
    path('aceptarRevision/', ajax.ajax_aceptar_revision_item, name='AJAX Aceptar revisión de ítem'),
    path('revisarItem/', ajax.ajax_revisar_item, name="AJAX Revisar ítem"),
]
