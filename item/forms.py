from django import forms

from item.constantes import EstadosItem
from item.models import TipoItem, Item
from fases.models import Fase


class Fase_Choice_Field(forms.ModelChoiceField):

    def label_from_instance(self, obj: Fase):
        return "{}. {}".format(obj.numero, obj.nombre)


class Form_Crear_Tipo_Item(forms.Form):
    """Formulario de django para crear tipos de item en un proyecto

    **Campos**

    nombre
      Nombre del tipo de item. Tiene una longitud máxima de 50 caracteres y es obligatorio.

    descripcion
      Descripción del tipo de item. No tiene longitud máxima de caracteres y tampoco es obligatorio.

    prefijo
      Prefijo del tipo de item. Tiene una longitud máxima de 4 caracteres y es obligatorio. Distingue
      entre Mayúsculas y Minusculas.

    atributos
      Estructura de los atributos del tipo de item. Se utiliza JSON para guardar un vector de esta estructura.

      **Estructura del JSON de un Atributo**::

        {
            "nombre": "Nombre del Atributo",
            "tipo": 'Tipo de Atributo',
            "posicion": "Posición del atributo",
            "obligatorio": "True ó False dependiendo de la obligatoriedad",
        }

    fase
      Fase donde el tipo de item puede estar
    """
    nombre = forms.CharField(label="Nombre del tipo de item", max_length=50, required=True)
    descripcion = forms.CharField(label="Descripción", widget=forms.Textarea, required=False)
    prefijo = forms.CharField(label="Prefijo del código", max_length=4, required=True)
    fase = Fase_Choice_Field(label="Fase", empty_label="Seleccionar una fase", queryset=None, required=True)

    def __init__(self, *args, **kwargs):
        self.proyecto_id = kwargs.pop('proyecto_id', None)
        super().__init__(*args, **kwargs)
        if self.proyecto_id != None:
            self.fields['fase'].queryset = Fase.objects.filter(proyecto_id=self.proyecto_id)


class Tipo_Item_Importable_Field(forms.ModelChoiceField):

    def label_from_instance(self, obj):
        return "{} - {}".format(obj.nombre, obj.proyecto.nombre)


class Form_Importar_Tipo_de_Item(forms.Form):
    tipo_de_item_a_importar = Tipo_Item_Importable_Field(label="Tipo de item a importar", queryset=None, required=True)
    fase = Fase_Choice_Field(label="Fase", empty_label="Seleccionar una fase", queryset=None, required=True)

    def __init__(self, *args, **kwargs):
        self.proyecto_id = kwargs.pop('proyecto_id', None)
        self.usuario = kwargs.pop('usuario', None)
        super().__init__(*args, **kwargs)
        if self.proyecto_id != None and self.usuario != None:
            self.fields['fase'].queryset = Fase.objects.filter(proyecto_id=self.proyecto_id)
            self.fields['tipo_de_item_a_importar'].queryset = TipoItem.Tipos_Items_Importables_queryset(
                self.proyecto_id, self.usuario)


class Form_crear_item(forms.Form):
    """
    Formulario para crear un item dentro de una fase, los atributos dinamicos se manejan en el view
    correspondiente.

    **Uso**

    Formulario vacio - Form_crear_item(proyecto_id=**IDENTIFICADOR_DEL_PROYECTO**)

    Formulario para validar - Form_crear_item(**request.POST**,proyecto_id=**IDENTIFICADOR_DEL_PROYECTO**)

    **Campos**

    nombre
      Nombre del item **Requerido**

    descripcion
      Descripción del item

    costo
      Costo de impacto del item. **Requerido**

    tipo_de_item
      Tipo de item de la cual el item hereda sus atributos. Se obtienen los tipos de item del proyecto. **Requerido**


    """
    nombre = forms.CharField(label="Nombre del Item", max_length=50, required=True)
    descripcion = forms.CharField(label="Descripción del Item", widget=forms.Textarea, required=False)
    costo = forms.IntegerField(label="Costo de impacto", required=True)
    tipo_de_item = forms.ModelChoiceField(label="Tipo de item", empty_label="Seleccionar Tipo de Item", queryset=None,
                                          required=True)
    antecesor = forms.ModelChoiceField(label="Antecesor o padre del item", queryset=None, required=False)

    def __init__(self, *args, **kwargs):
        from item.models import TipoItem
        self.proyecto_id = kwargs.pop('proyecto_id', None)
        self.fase_id = kwargs.pop('fase_id', None)
        super().__init__(*args, **kwargs)
        if self.proyecto_id is not None and self.fase_id is not None:
            self.fields['tipo_de_item'].queryset = TipoItem.objects.filter(proyecto_id=self.proyecto_id)
            fase = Fase.objects.get(pk=self.fase_id)
            items_candidatos = Item.objects.filter(es_ultimo=True, fase=fase,
                                                   estado__in=[EstadosItem.APROBADO, EstadosItem.LINEA_BASE])
            if fase.numero > 1:
                items_candidatos = items_candidatos | Item.objects.filter(es_ultimo=True, fase__numero=fase.numero - 1,
                                                                          fase__proyecto=fase.proyecto,
                                                                          estado=EstadosItem.LINEA_BASE.value)

            self.fields['antecesor'].queryset = items_candidatos


class Form_modificar_item(forms.Form):
    """
    Formulario para modificar un item dentro de una fase, los atributos dinamicos se manejan en el view
    correspondiente.

    **Campos**

    nombre
      Nombre del item **Requerido**

    descripcion
      Descripción del item

    costo
      Costo de impacto del item. **Requerido**

    """
    nombre = forms.CharField(label="Nombre del Item", max_length=50, required=True)
    descripcion = forms.CharField(label="Descripción del Item", widget=forms.Textarea, required=False)
    costo = forms.IntegerField(label="Costo de impacto", required=True)
