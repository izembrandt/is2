# Estados de Item
from enum import Enum


class EstadosItem(str, Enum):
    DESARROLLO = 'ED'
    PENDIENTE = 'PA'
    APROBADO = 'IA'
    LINEA_BASE = 'LB'
    DESACTIVADO = 'DA'
    REVISION = 'RE'
    BORRADOR = 'BO'
