from typing import List

from celery import shared_task
from django.core.mail import send_mail
from django.template.loader import render_to_string


@shared_task
def correo_aprobar_item(email: List, proyecto: dict):
    contexto = {
        "proyecto": proyecto,
    }
    html = render_to_string('email_templates/aprobacion_item.html', context=contexto)
    send_mail(subject='Notificación: Item para aprobar', message="Se ha realizado una solicitud para aprobar un item.",
              recipient_list=email, from_email=None, fail_silently=False,
              html_message=html)
    return None


@shared_task
def correo_aprobacion_item(email: List, item: dict):
    contexto = {
        "item": item,
    }
    html = render_to_string('email_templates/item_aprobado.html', context=contexto)
    send_mail(subject='Notificación: Item aprobado', message="Se ha aprobado el item.",
              recipient_list=email, from_email=None, fail_silently=False,
              html_message=html)
    return None


@shared_task
def correo_cancelar_solicitud_aprobacion(email: List, item: dict):
    contexto = {
        "item": item,
    }
    html = render_to_string('email_templates/cancelar_solicitud_aprobacion.html', context=contexto)
    send_mail(subject='Notificación: Solicitud de aprobación del Item cancelado', message="Se ha cancelado la solicitud del item.",
              recipient_list=email, from_email=None, fail_silently=False,
              html_message=html)
    return None


@shared_task
def correo_cancelar_aprobacion(email: List, item: dict):
    contexto = {
        "item": item,
    }
    html = render_to_string('email_templates/cancelar_aprobacion.html', context=contexto)
    send_mail(subject='Notificación: Item desaprobado', message="Se ha desaprobado el item.",
              recipient_list=email, from_email=None, fail_silently=False,
              html_message=html)
    return None