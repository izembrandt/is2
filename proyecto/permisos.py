class PermisoProyecto:
    LISTAR_PROYECTOS = "listar_proyectos"
    AGREGAR_PROYECTO = "agregar_proyecto"
    ELIMINAR_PROYECTO = "eliminar_proyecto"
    MODIFICAR_PROYECTO = "modificar_proyecto"
    # Gestion del proyecto
    EJECUTAR_PROYECTO = "ejecutar_proyecto"
    CANCELAR_PROYECTO = "cancelar_proyecto"
    FINALIZAR_PROYECTO = "finalizar_proyecto"
    # Tipo de Item
    VER_TIPO_ITEM = "ver_tipo_de_item"
    CREAR_TIPO_ITEM = "crear_tipo_de_item"
    MODIFICAR_TIPO_ITEM = "modificar_tipo_de_item"
    IMPORTAR_TIPO_ITEM = "importar_tipo_de_item"
    ELIMINAR_TIPO_ITEM = "eliminar_tipo_de_item"
    # Fases
    VER_FASES = 'ver_fases'
    CREAR_FASES = 'crear_fases'
    ELIMINAR_FASES = 'eliminar_fases'
    MODIFICAR_FASES = 'modificar_fases'
    CERRAR_FASE = 'cerrar_fase'
    # Cómite
    ASIGNAR_COMITE = 'asignar_comite'
    # Roles
    VER_ROLES = "ver_roles"
    CREAR_ROLES = "crear_roles"
    MODIFICAR_ROLES = "modificar_roles"
    ELIMINAR_ROLES = "eliminar_roles"
    ASIGNAR_ROLES = "asignar_roles"
    # Participantes
    VER_PARTICIPANTE = 'ver_participantes'
    AGREGAR_PARTICIPANTE = 'agregar_participantes'
    QUITAR_PARTICIPANTE = 'quitar_participantes'

    @classmethod
    def permisos_usuario(cls):
        return [
            # TIPO ITEM
            cls.VER_TIPO_ITEM,
            cls.CREAR_TIPO_ITEM,
            cls.MODIFICAR_TIPO_ITEM,
            cls.IMPORTAR_TIPO_ITEM,
            cls.ELIMINAR_TIPO_ITEM,
            # Fases
            cls.VER_FASES,
            cls.CREAR_FASES,
            cls.ELIMINAR_FASES,
            cls.MODIFICAR_FASES,
        ]

    @classmethod
    def permisos_gerente(cls):
        return cls.permisos_usuario() + [
            # Roles
            cls.VER_ROLES,
            cls.CREAR_ROLES,
            cls.MODIFICAR_ROLES,
            cls.ELIMINAR_ROLES,
            cls.ASIGNAR_ROLES,
            # Gestión Proyecto
            cls.EJECUTAR_PROYECTO,
            cls.CANCELAR_PROYECTO,
            cls.FINALIZAR_PROYECTO,
            # Comite
            cls.ASIGNAR_COMITE,
            # Participantes
            cls.AGREGAR_PARTICIPANTE,
            cls.VER_PARTICIPANTE,
            cls.QUITAR_PARTICIPANTE,
            # Fase
            cls.CERRAR_FASE,
        ]

    @classmethod
    def permisos_qa(cls):
        return []

    @classmethod
    def permisos_desarrollador(cls):
        return [
            # TIPO ITEM
            cls.VER_TIPO_ITEM,
            cls.CREAR_TIPO_ITEM,
            cls.MODIFICAR_TIPO_ITEM,
            cls.IMPORTAR_TIPO_ITEM,
            cls.ELIMINAR_TIPO_ITEM,
            # FASES
            cls.VER_FASES,
            cls.CREAR_FASES,
            cls.ELIMINAR_FASES,
            cls.MODIFICAR_FASES,
        ]
