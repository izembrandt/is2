from django.http import JsonResponse, HttpResponseBadRequest, HttpResponseForbidden, HttpRequest
from datetime import datetime

from django.http import JsonResponse, HttpRequest, HttpResponse
from django.contrib.auth.models import User
from django.views.decorators.http import require_http_methods
from guardian.shortcuts import get_perms

from fases.models import Fase
from item.models import Item
from item.constantes import EstadosItem
from .models import Proyecto
from django.contrib.auth.decorators import login_required, permission_required
from guardian.decorators import permission_required_or_403
from django.shortcuts import get_object_or_404, redirect
from .forms import *
from .permisos import PermisoProyecto
from .tasks import *
import logging

# Logger
logger = logging.getLogger(__name__)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.ejecutar_proyecto', (Proyecto, 'id', 'proyecto_id'))
def ejecutar_proyecto(request, proyecto_id):
    """Ejecuta un proyecto pendiente, es decir, cambia el estado del Proyecto identificado por **proyecto_id**

    Requiere que el usuario tenga permiso ``proyecto.ejecutar_proyecto`` con el proyecto


    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: HttpRedirect al proyecto ejecutado
    """
    # Verificar que es metodo POST para evitar que algunos navegadores ejecuten al hacer pre fetch
    if request.method == 'POST':
        proyecto = Proyecto.objects.get(pk=proyecto_id)  # Existe siempre, de otro modo, el guardian tira 403
        if proyecto.puede_ejecutar():
            # Ejecución
            proyecto.ejecutar()

            # Envio del correo
            emails = list(Proyecto.objects.get(pk=proyecto_id).participantes.all().values('email'))
            correo_proyecto_ejecutado.delay([user['email'] for user in emails], proyecto.a_dict_json())

            # Logging
            logger.info("{} ejecutó el proyecto {}".format(request.user.email, proyecto))

        else:
            return JsonResponse({"error": "No se puede ejecutar el proyecto"}, status=400)

    return redirect('React Proyecto Dashboard', proyecto_id)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.cancelar_proyecto', (Proyecto, 'id', 'proyecto_id'))
def cancelar_proyecto(request, proyecto_id):
    """Cancela un proyecto, es decir, cambia el estado del Proyecto identificado por **proyecto_id**

    Requiere que el usuario tenga permiso ``proyecto.cancelar_proyecto`` con el proyecto

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: HttpRedirect al proyecto cancelado
    """
    # Verificar que es metodo POST para evitar que algunos navegadores ejecuten al hacer pre fetch
    if request.method == 'POST':
        proyecto = Proyecto.objects.get(pk=proyecto_id)

        # Verificamos el estado anterior del proyecto
        if proyecto.estado in ['P', 'E']:
            # Cancelamos el proyecto
            proyecto.cancelar()

            # Enviamos el correo
            emails = list(Proyecto.objects.get(pk=proyecto_id).participantes.all().values('email'))
            correo_proyecto_cancelado.delay([user['email'] for user in emails], proyecto.a_dict_json())

            # Logging
            logger.info("{} canceló el proyecto {}".format(request.user.email, proyecto))

    return redirect('React Proyecto Dashboard', proyecto_id)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.agregar_participantes', (Proyecto, 'id', 'proyecto_id'))
def agregar_usuario(request, proyecto_id):
    """Agrega un usuario al proyecto, es decir, agrega el usuario a la lista de **participantes** del proyecto

    Requiere que el usuario tenga permiso ``proyecto.agregar_participantes`` con el proyecto

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: HttpRedirect al proyecto involcrado
    """
    # funcion get_object_or_404 para obtener instancia del proyecto
    proyecto = get_object_or_404(Proyecto, pk=proyecto_id)

    # si el request es de post, porcesar los datos
    if request.method == 'POST':
        form = FormAgregarUsuarioProyecto(request.POST)

        if form.is_valid():
            # se almacena el dato recibido de email
            nuevo_email = form.cleaned_data['email']
            try:
                usuario = User.objects.get(email=nuevo_email)
            except User.DoesNotExist:
                return JsonResponse({"error": "No existe el usuario"}, status=400)

            proyecto.participantes.add(usuario)

            # Notificar por correo al usuario
            correo_participante_agregado.delay(usuario.email, proyecto.a_dict_json())

            # Logging
            logger.info("{} agregó a {} al proyecto {}".format(request.user.email, usuario.email, proyecto))

            return JsonResponse({"participantes": listar_usuarios_dict(proyecto_id, request.user)}, status=200)

    return JsonResponse({"error": "Metodo invalido"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.quitar_participantes', (Proyecto, 'id', 'proyecto_id'))
def eliminar_usuario(request, proyecto_id):
    """Elimina un usuario del proyecto, es decir, lo remueve de la lista de **participantes**.

    Requiere que el usuario tenga permiso ``proyecto.quitar_participantes`` con el proyecto

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: HttpRedirect al proyecto involucrado
    """
    # funcion get_object_or_404 para obtener instancia del proyecto
    proyecto = get_object_or_404(Proyecto, pk=proyecto_id)

    # si el request es de post, procesar los datos
    if request.method == 'POST':
        # se almacena el dato recibido de email
        email = request.POST['email']

        if request.user.email == email:
            return JsonResponse({"error": "No se puede borrar a si mismo"}, status=400)

        # Buscamos el usuario y lo eliminamos
        try:
            usuario = User.objects.get(email=email)
        except User.DoesNotExist:
            return JsonResponse({"error": "No existe el usuario"}, status=400)

        # Lo eliminamos si no forma parte del comité
        if hasattr(proyecto, "comite") and usuario in proyecto.comite.integrantes.all():
            return JsonResponse({"error": "El usuario forma parte del comité"}, status=400)

        Rol.desasignar_todos_los_roles_del_proyecto(proyecto_id, usuario)
        proyecto.participantes.remove(usuario)

        # Logging
        logger.info("{} eliminó a {} del proyecto {}".format(request.user.email, usuario.email, proyecto))

        return JsonResponse({"participantes": listar_usuarios_dict(proyecto_id, request.user)}, status=200)

    return JsonResponse({"error": "No es post"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_listar_usuarios(request, proyecto_id):
    """AJAX que retorna la lista de usuarios del proyecto,

    Requiere que el usuario tenga permiso ``proyecto.ver_participantes`` con el proyecto

    :param request: AJAX Request
    :param proyecto_id: Identificador del Proyecto
    :return: JsonResponse con la lista de usuarios dle proyecto
    """
    # Verificar metodo valido
    if request.method == 'GET':
        # Buscamos los roles del proyecto
        usuarios_proyecto = listar_usuarios_dict(proyecto_id, request.user)

        # Enviamos el response
        return JsonResponse({"participantes": usuarios_proyecto}, status=200)

    # Error desconocido
    return JsonResponse({"error": "No es GET"}, status=400)


@login_required
@permission_required('proyecto.agregar_proyecto', raise_exception=True)
def ajax_actualizar_gerente(request, proyecto_id):
    """AJAX para asignar el gerente del proyecto. Solo puede haber **1** gerente por proyecto-

    Solo los administradores tienen permiso para asignar gerente.

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: JsonResponse con status 200 si fue exito, caso contrario el error
    """
    from roles.funciones import crear_rol_gerente_de_proyecto
    from roles.models import Rol
    if request.method == 'POST':
        # Buscar el proyecto
        try:
            proyecto = Proyecto.objects.get(pk=proyecto_id)
        except Proyecto.DoesNotExist:
            return JsonResponse({"error": "No existe el proyecto"}, status=400)

        # Validar Form
        form = Form_Establecer_Gerente_Proyecto(request.POST)

        if form.is_valid():
            # Asignar

            # Buscamos el User
            email = form.cleaned_data['email_gerente']

            try:
                user = User.objects.get(email=email)
            except User.DoesNotExist:
                return JsonResponse({"error": "No existe el usuario"}, status=400)

            # Agregamos al usuario al proyecto, por si no lo estaba
            proyecto.participantes.add(user)

            # Buscamos el rol de gerente del proyecto
            try:
                rol = Rol.objects.get(proyecto_id=proyecto_id, es_gerente=True)
            except Rol.DoesNotExist:
                # Creamos el rol, no existia
                rol = crear_rol_gerente_de_proyecto(proyecto)

            # Asignamos al usuario
            rol.usuarios.set([user])
            rol.reasignar_grupos_usuarios()

            # Notificamos al gerente por correo
            correo_gerente_asignado.delay(user.email, proyecto.a_dict_json())

            # Logging
            logger.info("{} asignó a {} como gerente del proyecto {}".format(request.user.email, user.email, proyecto))

            # Respondemos con exito
            return JsonResponse(
                {"proyecto": proyecto.a_dict_json(), "permisos_proyecto": get_perms(request.user, proyecto)},
                status=200, safe=False)

        return JsonResponse({"error": "Formulario invalido"}, status=400)

    else:
        return JsonResponse({"error": "Método invalido"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', raise_exception=True)
@permission_required_or_403('proyecto.agregar_participantes', (Proyecto, 'id', 'proyecto_id'))
def ajax_puede_asignar(request, proyecto_id):
    """AJAX para saber si el usuario puede asignar rol, innecesario pero se utiliza en caso de
    la posible implementación de que un usuario no gerente pueda asignar roles.

    Requiere que el usuario tenga permiso ``proyecto.agregar_participantes`` con el proyecto

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: JsonResponse con True si es que puede asignar, caso contrario False.
    """
    # Verificar el request
    if request.is_ajax and request.method == 'GET':
        # Buscar el proyecto
        try:
            proyecto = Proyecto.objects.get(pk=proyecto_id)
        except Proyecto.DoesNotExist:
            return JsonResponse({"error": "No existe el proyecto"}, status=400)

        # Responder solicitud
        mensaje = {
            "asignar_rol": request.user.has_perm('asignar_roles', proyecto)
        }

        return JsonResponse(mensaje, status=200)

    return JsonResponse({"error": "Metodo invalido"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', raise_exception=True)
@permission_required_or_403('proyecto.asignar_roles', (Proyecto, 'id', 'proyecto_id'))
def ajax_asignar_roles(request, proyecto_id):
    """AJAX para asignar roles a un usuario en el Proyecto identificado por el **proyecto_id**.

    Requiere que el usuario tenga permiso ``proyecto.asignar_roles`` con el proyecto

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: JsonResponse con la lista de participantes.
    """
    # Verificar el request
    if request.method == 'POST':
        # Buscar el proyecto
        try:
            proyecto = Proyecto.objects.get(pk=proyecto_id)
        except Proyecto.DoesNotExist:
            return JsonResponse({"error": "No existe el proyecto"}, status=400)

        # Limpiar el form

        form = Form_Asignar_Roles(request.POST, proyecto_id=proyecto_id)

        if form.is_valid():
            # Obtener datos
            roles = form.cleaned_data['roles_asignados']
            email = form.cleaned_data['usuario_correo']
            usuario = User.objects.get(email=email)

            Rol.desasignar_todos_los_roles_del_proyecto(proyecto_id, usuario)

            for rol in roles:
                rol.asignar_usuario(usuario)
                # Logging
                logger.info(
                    "{} agregó a {} el rol {} al proyecto {}".format(request.user.email, usuario.email, rol, proyecto))

            return JsonResponse({"participantes": listar_usuarios_dict(proyecto_id, request.user)}, status=200)

    return JsonResponse({"error": "Metodo invalido"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', raise_exception=True)
@permission_required_or_403('proyecto.ver_roles', (Proyecto, 'id', 'proyecto_id'))
def ajax_obtener_roles_de_usuario(request, proyecto_id):
    """AJAX para obtener los roles que posee el usuario, se utiliza para popular el form al asignar roles
    a un usuario

    Requiere que el usuario tenga permiso ``proyecto.ver_roles`` con el proyecto

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: JsonResponse con la lista de roles identificados por su uuid
    """
    # Verificar el request
    if request.is_ajax and request.method == 'GET':
        usuario = User.objects.get(email=request.GET['correo'])
        roles = Rol.lista_roles_de_usuario_en_proyecto_queryset(proyecto_id, usuario)

        respuesta = {
            "roles": list(roles.values('uuid')),
        }

        return JsonResponse(respuesta, status=200)

    return JsonResponse({"error": "Metodo invalido"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', raise_exception=True)
@permission_required_or_403('proyecto.ejecutar_proyecto', (Proyecto, 'id', 'proyecto_id'))
@require_http_methods(["POST"])
def actualizar_info_proyecto(request, proyecto_id):
    """Actualiza el nombre y la descripción del proyecto identificado por el **proyecto_id**

    Requiere que el usuario tenga permiso ``proyecto.ejecutar_proyecto`` con el proyecto

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: redirect al dashboard del proyecto
    """
    from menu_principal.forms import Form_Crear_Proyecto
    # Verificar request

    form = Form_Crear_Proyecto(request.POST)

    # Verificar si el proyecto existe
    proyecto = get_object_or_404(Proyecto, pk=proyecto_id)

    if form.is_valid():
        proyecto.nombre = form.cleaned_data['nombre']
        proyecto.descripcion = form.cleaned_data['descripcion']
        proyecto.save()

        # Enviamos el correo
        emails = list(Proyecto.objects.get(pk=proyecto_id).participantes.all().values('email'))
        correo_proyecto_actualizado.delay([user['email'] for user in emails], proyecto.a_dict_json())

        # Logging
        logger.info("{} actualizó la info del proyecto {}".format(request.user.email, proyecto))

    return redirect('React Proyecto Dashboard', proyecto_id)


def listar_usuarios_dict(proyecto_id, usuario):
    """Función Auxiliar para obtener la lista de usuarios del proyecto identificado por **proyecto_id**
     en un diccionario. Se excluye al **usuario**

    :param proyecto_id: Identificador del proyecto
    :param usuario: Usuario que se excluye
    :return: Lista de usuarios en diccionario
    """
    lista = list(
        Proyecto.objects.get(pk=proyecto_id).participantes.all().exclude(email=usuario.email).values('email',
                                                                                                     'first_name',
                                                                                                     'last_name'))
    # Agregamos los roles que le corresponde
    return [{**participante, "roles": Rol.listar_roles_del_usuario_en_proyecto_json(
        usuario=User.objects.get(email__exact=participante['email']), proyecto_id=proyecto_id)} for participante in
        lista]


@login_required
@permission_required('proyecto.listar_proyectos', raise_exception=True)
def ajax_obtener_info_proyecto(request, proyecto_id):
    """Retorna un JSONResponse con los datos de un proyecto

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: JSONResponse con los datos
    """
    # Verificar si el proyecto existe
    proyecto = get_object_or_404(Proyecto, pk=proyecto_id)

    # Verificar si el usuario es un participante del proyecto
    participantes = proyecto.participantes.all()

    if request.user in participantes:
        return JsonResponse({"proyecto": proyecto.a_dict_json(),
                             "permisos_proyecto": get_perms(request.user, proyecto),
                             "roles": Rol.listar_roles_del_usuario_en_proyecto_json(request.user, proyecto_id)
                             }, status=200)

    return JsonResponse({"error": "No eres participante del proyecto"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["POST"])
def ajax_finalizar_proyecto(request: HttpRequest, proyecto_id: int) -> JsonResponse:
    """Finaliza el proyecto identificado por proyecto_id.

    Para que se pueda finalizar el proyecto, todas las fases deben estar cerradas

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: Respuesta en JSON
    """
    # Obtener el proyecto
    try:
        proyecto: Proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=HttpResponseBadRequest.status_code)

    # Verificar que el usuario tenga permisos para finalizar el proyecto
    if not request.user.has_perm(PermisoProyecto.FINALIZAR_PROYECTO, proyecto):
        return JsonResponse({"error": "No tienes permiso para finalizar este proyecto"},
                            status=HttpResponseForbidden.status_code)

    # Verificar que todas las fases esten cerradas
    if proyecto.fase_set.filter(estado='A').exists():
        # Hay al menos uno abierto
        return JsonResponse({"error": "Hay fase(s) abierta(s) en este proyecto"},
                            status=HttpResponseBadRequest.status_code)
    else:
        proyecto.finalizar()
        # Enviamos el correo
        emails = list(Proyecto.objects.get(pk=proyecto_id).participantes.all().values('email'))
        correo_proyecto_finalizado.delay([user['email'] for user in emails], proyecto.a_dict_json())

        # Logging
        logger.info("{} finalizó el proyecto {}".format(request.user.email, proyecto))

        return JsonResponse({"proyecto": proyecto.a_dict_json(),
                             "permisos_proyecto": get_perms(request.user, proyecto),
                             "roles": Rol.listar_roles_del_usuario_en_proyecto_json(request.user, proyecto_id)
                             }, status=200)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def informe_items(request: HttpRequest, proyecto_id: int):
    """View que genera un informe de los items del proyecto en estado de DESARROLLO y REVISION

    :param request:
    :param proyecto_id:
    :return: PDF del informe de fase
    """
    from io import BytesIO
    from django.template.loader import get_template
    from xhtml2pdf import pisa
    import pytz
    # Obtener el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Obtener la línea base
    fases = Fase.objects.filter(proyecto=proyecto)
    items = Item.objects.filter(es_ultimo=True, estado__in=[EstadosItem.DESARROLLO, EstadosItem.PENDIENTE],
                                fase__proyecto=proyecto)
    # Generamos el pdf
    template = get_template('pdf_templates/InformeItems.html')
    asu = pytz.timezone('America/Asuncion')
    html = template.render(
        {"fases": fases, "items": items, "fecha": datetime.date(datetime.now(asu)),
         "hora": datetime.time(datetime.now(asu))})
    resultado = BytesIO()
    pisa.pisaDocument(BytesIO(html.encode("UTF-8")), resultado)

    response = HttpResponse(resultado.getvalue(), content_type="application/pdf")
    response['Content-Disposition'] = 'attachment; filename="informe_items.pdf"'

    # Logging
    logger.info("{} solicitó un informe de los items en el proyecto {}".format(request.user.email, proyecto))

    return response
