class EstadoProyecto:
    """Estados del proyecto
    """
    PENDIENTE = 'P'
    EJECUCION = 'E'
    CANCELADO = 'C'
    FINALIZADO = 'F'
