from django.urls import path, include
from .views import *

urlpatterns = [
    path('<int:proyecto_id>/obtenerInfo/', ajax_obtener_info_proyecto, name='Obtener Info de proyecto'),
    path('<int:proyecto_id>/ejecutar/', ejecutar_proyecto, name='ejecutar_proyecto'),
    path('<int:proyecto_id>/cancelar/', cancelar_proyecto, name='cancelar_proyecto'),
    path('<int:proyecto_id>/modificar/', actualizar_info_proyecto, name='actualizar proyecto'),
    path('<int:proyecto_id>/finalizar/', ajax_finalizar_proyecto, name='AJAX Finalizar Proyecto'),
    path('<int:proyecto_id>/generarInformeItems/', informe_items, name='informe_items'),

    path(r'<int:proyecto_id>/agregar/', agregar_usuario, name="agregar_usuario"),
    path(r'<int:proyecto_id>/eliminar/', eliminar_usuario, name="eliminar_usuario"),
    path(r'<int:proyecto_id>/ajax_listar_usuarios/', ajax_listar_usuarios, name="ajax_listar_usuarios"),

    path('<int:proyecto_id>/fase/', include('fases.urls')),
    path('<int:proyecto_id>/item/', include('item.urls')),
    path('<int:proyecto_id>/roles/', include('roles.urls')),
    path('<int:proyecto_id>/lineaBase/', include('linea_base.urls')),
    path('<int:proyecto_id>/cambio/', include('comite_de_cambio.urls')),
    # Asignar gerente
    path('<int:proyecto_id>/asignarGerente/', ajax_actualizar_gerente, name='AJAX Actualizar Gerente Proyecto'),
    # Ajax para saber si tiene permisos para asignar rol
    path('<int:proyecto_id>/puedeAsignar/', ajax_puede_asignar, name='AJAX Puede Asignar?'),
    path('<int:proyecto_id>/asignarRol/', ajax_asignar_roles, name='AJAX Asignar Rol'),
    path('<int:proyecto_id>/ObtenerRoles/', ajax_obtener_roles_de_usuario, name='AJAX Obtener Roles Usuario'),

]
