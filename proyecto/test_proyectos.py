import json

import pytest
from django.test import RequestFactory
from django.urls import reverse
from menu_principal.views import *
from fases.utils import cerrar_fase_de_proyecto
from .constantes import EstadoProyecto
from .views import ajax_listar_usuarios, agregar_usuario, eliminar_usuario, ajax_finalizar_proyecto


@pytest.fixture
def proyecto_vacio(db, proyecto_factory) -> Proyecto:
    """Crea un proyecto vació de fixture en tests de proyectos
    :return: Proyecto
    """
    proyecto = proyecto_factory(nombre="TEST CREAR PROYECTO", descripcion="TEST")
    assert Proyecto.objects.filter(pk=proyecto.pk).exists(), "No se creó el proyecto"
    return proyecto


@pytest.fixture
def proyecto_ejecutado(db, proyecto_factory, fase_factory, tipo_item_factory,
                       item_factory, linea_base_factory, usuario_qa: User) -> Proyecto:
    proyecto = proyecto_factory(nombre="TEST PROYECTO EJECUTADO")
    # Fase de ejemplo
    fase = fase_factory(proyecto=proyecto)

    # Tipos de ejemplo
    tipo = tipo_item_factory(proyecto=proyecto, fase=fase, prefijo='xdnt')

    # Ejecutamos
    proyecto.ejecutar()
    proyecto.refresh_from_db()

    # Crearemos un ítem aprobado y en LB
    item = item_factory(tipo_item=tipo, atributos=[])
    item.solicitar_aprobacion()
    item.aprobar()
    linea = linea_base_factory(fase=fase, creador=usuario_qa)
    linea.agregarItem(item)
    linea.cerrar()

    # Cerrar fase
    cerrar_fase_de_proyecto(fase)

    return proyecto


def test_crear_proyecto(db, proyecto_vacio):
    """Verifica si que la funcion crear proyectos responda correctamente al request
    """
    # Creamos un proyecto y verificamos
    assert Proyecto.objects.filter(pk=proyecto_vacio.pk).exists(), "No se creó correctamente el proyecto"
    # Probamos el correo
    correo_proyecto_creado.delay("is2.09.fc@gmail.com", proyecto_vacio.a_dict_json())


def test_ejecutar_proyecto(db, proyecto_ejecutado):
    """Verifica si que la funcion de ejecutar_proyecto cambie el estado de 'P' a 'E'
    """
    assert proyecto_ejecutado.estado == 'E', "El proyecto no se puso en ejecución"


def test_cancelar_proyecto(db, proyecto_ejecutado: Proyecto, proyecto_vacio: Proyecto):
    """Verifica si que la funcion de cancelar cambie el estado del mismo de 'P' o 'E' a 'C'
    """
    # Probamos un proyecto que antes estaba en configuración
    proyecto_vacio.cancelar()
    assert proyecto_vacio.estado == 'C', "No se cerró un proyecto pendiente de ejecución"

    # Probamos un proyecto que antes estaba en ejecución
    proyecto_ejecutado.cancelar()
    assert proyecto_ejecutado.estado == 'C', "No se cerró el proyecto en ejecución"


def test_ajax_listar_usuarios(db, proyecto_ejecutado: Proyecto, usuario_gerente: User):
    """Verifica si que la funcion listar usuarios retorna el estado esperado
    """
    # buscamos la id del nuevo proyecto que fue inicializado con estado 'P'
    path = reverse('ajax_listar_usuarios', args=[proyecto_ejecutado.id])
    request = RequestFactory().get(path)

    # Actualizamos el estado del proyecto
    request.method = 'GET'
    request.user = usuario_gerente
    response = ajax_listar_usuarios(request, proyecto_id=proyecto_ejecutado.id)

    # comprobamos que fue un exito si tuvo estado 200
    assert response.status_code == 200, "No se pudo obtener la lista"


def test_agregar_usuario(db, proyecto_ejecutado: Proyecto, user_factory, usuario_gerente: User):
    """Verifica si que la funcion agregar usarios, asigne correctamente el usuario al proyecto
    """
    # Creamos un usuario de ejemplo
    usuario: User = user_factory(username="testeragregar")

    # Buscamos el path de lal funcion
    path = reverse('agregar_usuario', args=[proyecto_ejecutado.id])

    # Creamos el request
    request = RequestFactory().post(
        path,
        {
            'email': usuario.email,
        }
    )

    # Actualizamos el proyecto
    request.user = usuario_gerente
    assert agregar_usuario(request, proyecto_id=proyecto_ejecutado.id).status_code == 200, \
        "No se pudo agregar al usuario"

    proyecto_ejecutado.refresh_from_db()

    # Comprobamos si el usuario esta dentro de la lista de participantes
    assert usuario in proyecto_ejecutado.participantes.all(), "El usuario no fue agregado al proyecto"


def test_eliminar_usuario(db, proyecto_ejecutado: Proyecto, usuario_desarrollador: User, usuario_gerente: User):
    """Verifica si que la eliminar usuarios, remueve al usuario seleccionado del proyecto
    """
    # Buscamos el path de lal funcion
    path = reverse('eliminar_usuario', args=[proyecto_ejecutado.id])
    # Creamos el request
    request = RequestFactory().post(
        path,
        {
            'email': usuario_desarrollador.email,
        }
    )
    request.user = usuario_gerente

    # Ejecutamos el request
    print([*proyecto_ejecutado.participantes.all()])
    assert usuario_desarrollador in proyecto_ejecutado.participantes.all(), "El usuario no forma parte"
    assert eliminar_usuario(request, proyecto_id=proyecto_ejecutado.id).status_code == 200, \
        "No se pudo eliminar al usuario"

    # comprobamos que fue un exito si los datos se actualizaron
    proyecto_ejecutado.refresh_from_db()
    assert not (usuario_desarrollador in proyecto_ejecutado.participantes.all()), \
        "El usuario sigue formando parte del proyecto"


def test_finalizar_proyecto(db, rf, proyecto_ejecutado, usuario_gerente) -> None:
    """Verifica la finalización de un proyecto
    """
    # Creamos el caso de uso
    request = rf.post(
        path=reverse("AJAX Finalizar Proyecto", args=[proyecto_ejecutado.id]),
        data={}
    )

    request.user = usuario_gerente

    # Probamos
    response = ajax_finalizar_proyecto(request=request, proyecto_id=proyecto_ejecutado.id)

    print(json.loads(response.content))

    proyecto_ejecutado.refresh_from_db()
    assert proyecto_ejecutado.estado == EstadoProyecto.FINALIZADO, \
        "El estado de proyecto no figura como finalizado"
