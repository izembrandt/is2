from typing import List

from celery import shared_task
from django.core.mail import send_mail
from django.template.loader import render_to_string


@shared_task
def correo_gerente_asignado(email: str, proyecto: dict):
    """Task que envia un correo asincrono si un nuevo gerente es asignado a un proyecto

    :param email: correo del nuevo gerente asignado al proyecto
    :param proyecto: el Proyecto actual
    :return: No retorna nada
    :rtype: None

    """
    #Se pasa informacion del proyecto al html
    contexto = {
        "proyecto": proyecto,
    }
    #Se renderiza el template para asignar gerente de html como cadena
    html = render_to_string('email_templates/gerente_asignado.html', context=contexto)
    #Se manda el correo con la informacion al nuevo gerente del proyecto
    send_mail(subject='Notificación: Un gerente ha sido designado al proyecto', message="Se ha designado el gerente del proyecto.",
              recipient_list=[email], from_email=None, fail_silently=False,
              html_message=html)
    return None


@shared_task
def correo_proyecto_ejecutado(email: List, proyecto: dict):
    """Task que envia un correo asincrono si el proyecto pasa a estado ejecutado

    :param email: lista de correos de todos los miembros del proyecto
    :param proyecto: el Proyecto actual
    :return: No retorna nada
    :rtype: None

    """
    #Se pasa informacion del proyecto al html
    contexto = {
        "proyecto": proyecto,
    }
    #Se renderiza el template del html para ejecutar proyecto como cadena
    html = render_to_string('email_templates/proyecto_ejecutado.html', context=contexto)
    #Se manda el correo con la informacion del estado del proyecto a todos los integrantes del proyecto
    send_mail(subject='Notificación: Se ha ejecutado el proyecto', message="Se ha realizado la ejecución del proyecto.",
              recipient_list=email, from_email=None, fail_silently=False,
              html_message=html)
    return None


@shared_task
def correo_proyecto_cancelado(email: List, proyecto: dict):
    """Task que envia un correo asincrono si el proyecto se cancela

    :param email: lista de correos de todos los miembros del proyecto
    :param proyecto: el Proyecto actual
    :return: No retorna nada
    :rtype: None

    """
    #Se pasa informacion del proyecto al html
    contexto = {
        "proyecto": proyecto,
    }
    #Se renderiza el template del html para cancelar un proyecto como cadena
    html = render_to_string('email_templates/proyecto_cancelado.html', context=contexto)
    #Se manda el correo con el estado del proyecto a todos los integrantes del proyecto
    send_mail(subject='Notificación: Se ha cancelado el proyecto', message="Se ha realizado la cancelación del proyecto.",
              recipient_list=email, from_email=None, fail_silently=False,
              html_message=html)
    return None

@shared_task
def correo_proyecto_finalizado(email: List, proyecto: dict):
    """Task que envia un correo asincrono si el proyecto es finalizado

    :param email: lista de correos de todos los miembros del proyecto
    :param proyecto: el Proyecto actual
    :return: No retorna nada
    :rtype: None

    """
    #Se pasa informacion del proyecto al html
    contexto = {
        "proyecto": proyecto,
    }
    #Se renderiza el template de html para finalizar proyecto como cadena
    html = render_to_string('email_templates/proyecto_finalizado.html', context=contexto)
    #Se manda el correo con el estado del proyecto a todos los integrantes del proyecto
    send_mail(subject='Notificación: Se ha finalizado el proyecto', message="Se ha realizado la finalización del proyecto.",
              recipient_list=email, from_email=None, fail_silently=False,
              html_message=html)
    return None

@shared_task
def correo_proyecto_actualizado(email: List, proyecto: dict):
    """Task que envia un correo asincrono si el proyecto se actualiza la informacion del proyecto

    :param email: lista de correos de todos los miembros del proyecto
    :param proyecto: el Proyecto actual
    :return: No retorna nada
    :rtype: None

    """
    #Se pasa informacion del proyecto al html
    contexto = {
        "proyecto": proyecto,
    }
    #Se renderiza el template de html para actualizar datos del proyecto como cadena
    html = render_to_string('email_templates/proyecto_actualizado.html', context=contexto)
    #Se manda el correo con los datos del proyecto a todos los integrantes del proyecto
    send_mail(subject='Notificación: Se ha actualizado el proyecto', message="Se ha realizado una actualizacion del proyecto.",
              recipient_list=email, from_email=None, fail_silently=False,
              html_message=html)
    return None


@shared_task
def correo_participante_agregado(email: str, proyecto: dict):
    """Task que envia un correo asincrono si un nuevo miembro es agregado al proyecto

    :param email: Correo del nuevo miembreo del Proyecto
    :param proyecto: el Proyecto actual
    :return: No retorna nada
    :rtype: None

    """
    #Se pasa informacion del proyecto al html
    contexto = {
        "proyecto": proyecto,
    }
    #Se renderiza el template de html para agregar participantes como cadena
    html = render_to_string('email_templates/participante_agregado.html', context=contexto)
    #Se manda el correo al nuevo participante  con la informacion del proyecto
    send_mail(subject='Notificación: Se ha agregado un participante al proyecto', message="Se ha añadido correctamente el participante al proyecto.",
              recipient_list=[email], from_email=None, fail_silently=False,
              html_message=html)
    return None