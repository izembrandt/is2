from django import forms
from roles.models import Rol


class FormAgregarUsuarioProyecto(forms.Form):
    """Formulario para agregar usuario

    email
      Correo electronico del usuario a agregar.

    """
    email = forms.EmailField(label="Mail del Usuario a Agregar")


class FormEliminarUsuarioProyecto(forms.Form):
    """Formulario para eliminar usuario

        email
          Correo electronico del usuario a agregar.

        """
    email = forms.EmailField(label="Mail del Usuario a Eliminar")


class Form_Establecer_Gerente_Proyecto(forms.Form):
    """Formulario para asignar gerente del proyecto

        email_gerente
          Correo electronico del usuario a agregar.

    """
    email_gerente = forms.EmailField(label="Seleccione el gerente")


class Form_Asignar_Roles(forms.Form):
    """Formulario para asignar roles a usuario

    usuario_correo
      Correo electronico del usuario a agregar.

    roles_asignados
      Roles que se le asignan al usuario.

    """
    roles_asignados = forms.ModelMultipleChoiceField(label="Roles Asignados", queryset=None, required=False)
    usuario_correo = forms.EmailField(label='Correo del usuario', widget=forms.HiddenInput, required=True)

    def __init__(self, *args, **kwargs):
        self.proyecto_id = kwargs.pop('proyecto_id', None)
        super().__init__(*args, **kwargs)
        if self.proyecto_id is not None:
            self.fields['roles_asignados'].queryset = Rol.lista_roles_de_proyecto_queryset(self.proyecto_id)
