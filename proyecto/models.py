from datetime import datetime

from django.db import models
from django.conf import settings
import pytz
from .permisos import PermisoProyecto
from .constantes import EstadoProyecto


# Create your models here.

class Proyecto(models.Model):
    """Modelo para representar un proyecto en el sistema

    **Atributos**

    nombre
      Nombre que se le asigna al proyecto. Tiene una longitud máxima de 50 caracteres.

    descripcion
      Descripción del proyecto. Este atributo no posee longitud máxima

    estado
      Estado actual del Proyecto. Se identifica por 1 carácter.

      Valores:

      #. Pendiente: 'P'
      #. En Ejecución: 'E'
      #. Cancelado: 'C'
      #. Finalizado: 'F'

    fecha_creacion
      Fecha de creación del Proyecto. Guarda Fecha y Hora.

    participantes
      Usuarios que estan asociados al proyecto.

    """
    ESTADOS_PROYECTO = (
        ('P', "Pendiente"),
        ('E', "En Ejecución"),
        ('C', "Cancelado"),
        ('F', "Finalizado"),
    )
    nombre = models.CharField("Nombre del Proyecto", max_length=50)
    descripcion = models.TextField("Descripción del Proyecto")
    estado = models.CharField("Estado del Proyecto", max_length=1, choices=ESTADOS_PROYECTO)
    fecha_creacion = models.DateTimeField("Fecha de Creación")
    participantes = models.ManyToManyField(settings.AUTH_USER_MODEL)

    def __str__(self):
        return "'{} - {}'".format(self.pk, self.nombre)

    def en_ejecucion(self):
        """Retorna True si es que el proyecto esta en Ejecución, caso contrario retorna False"""
        return self.estado == 'E'

    @staticmethod
    def crear_proyecto(nombre: str, descripcion: str):
        """Retorna un nuevo proyecto en el sistema"""
        from django.utils import timezone
        nuevo_proyecto = Proyecto(nombre=nombre, descripcion=descripcion, estado=EstadoProyecto.PENDIENTE,
                                  fecha_creacion=timezone.now())
        nuevo_proyecto.save()
        return nuevo_proyecto

    def cancelar(self) -> None:
        """Cancela el proyecto
        """
        # Precondiciones
        assert self.estado in [EstadoProyecto.PENDIENTE, EstadoProyecto.EJECUCION], \
            "El proyecto debe estar pendiente de ejecución o en ejecución"

        # Cambio del estado
        self.estado = EstadoProyecto.CANCELADO
        self.save()

    def puede_ejecutar(self):
        """Retorna True si es que se puede ejecutar, False caso contrario"""
        return self.en_configuracion() and self.fase_set.exists()

    def en_configuracion(self):
        """Retorna True si es que el proyecto esta en estado de configuración (Pendiente), caso contrario retorna
        False"""
        return self.estado == EstadoProyecto.PENDIENTE

    def a_dict_json(self):
        """Retorna un diccionario con la representación JSON del Proyecto"""
        asuncion = pytz.timezone("America/Asuncion")

        return {
            'nombre': self.nombre,
            'descripcion': self.descripcion,
            'fecha_creacion': datetime.date(self.fecha_creacion.astimezone(asuncion)),
            'id': self.id,
            'estado': self.estado,
            'nombre_gerente': self.nombre_gerente if self.nombre_gerente is not None else 'Sin asignar',
            'finalizable': not self.fase_set.filter(estado='A').exists() and self.estado == EstadoProyecto.EJECUCION,
        }

    @staticmethod
    def lista_proyectos_del_usuario(usuario: settings.AUTH_USER_MODEL):
        return [proyecto.a_dict_json() for proyecto in usuario.proyecto_set.all()]

    @property
    def gerente(self):
        """Retorna el usuario gerente si es que posee, sino retorna None"""
        return self.rol_gerente.usuarios.all().first()

    @property
    def nombre_gerente(self):
        """Retorna el nombre completo del gerente del proyecto, si no posee gerente, se retorna None

        :rtype: str
        """
        # Buscamos el gerente
        gerente = self.gerente

        if gerente != None:
            return "{} {}".format(gerente.first_name, gerente.last_name)

        return None

    def ejecutar(self):
        """Ejecuta el proyecto. Requiere que este en estado pendiente y que cuente con al menos una fase.
        """
        assert self.puede_ejecutar()
        self.estado = EstadoProyecto.EJECUCION
        self.save()

    def agregar_participante(self, usuario: settings.AUTH_USER_MODEL):
        """Agrega un usuario a la lista de participantes del Proyecto

        :param usuario: Usuario que queremos agregar al proyecto
        """
        assert not (usuario in self.participantes.all()), "El usuario ya esta entre los participantes"
        self.participantes.add(usuario)

    def asignar_gerente(self, usuario: settings.AUTH_USER_MODEL):
        """Asigna un gerente al proyecto"""

        # Quitamos al gerente anterior
        for usuarioAnterior in self.rol_gerente.usuarios.all():
            self.rol_gerente.desasignar_usuario(usuarioAnterior)

        # Agregamos al Usuario si no es participante
        if not (usuario in self.participantes.all()):
            self.agregar_participante(usuario)

        # Agregamos al nuevo gerente
        self.rol_gerente.asignar_usuario(usuario)

    def crear_fase(self, nombre: str, descripcion: str):
        """Crea una fase en el proyecto en la ultima posición"""
        from fases.models import Fase
        nueva_fase = Fase(nombre=nombre, descripcion=descripcion, proyecto=self, numero=self.fases.count() + 1,
                          estado='A')
        nueva_fase.save()
        return nueva_fase

    def finalizar(self) -> None:
        """Finaliza este proyecto. Cambia su estado desde en Ejecución a Finalizado
        """
        # Precondiciones
        assert self.fase_set.exists(), "No hay fases en este proyectos"
        assert not self.fase_set.filter(estado='A').exists(), "Hay fases abiertas en este proyecto"

        # Hacemos el cambio
        self.estado = EstadoProyecto.FINALIZADO
        self.save()

    @property
    def fases(self):
        """Retorna las fases del proyecto"""
        return self.fase_set.all().order_by('numero')

    @property
    def rol_gerente(self):
        """Retorna el rol de gerente del proyecto TODO: Revisar para BUGS cuando se crea proyecto capaz se duplique"""
        from roles.models import Rol

        try:
            rol_gerente = self.rol_set.get(es_gerente=True)
        except Rol.DoesNotExist:
            # Crear Rol
            from roles.funciones import crear_rol_gerente_de_proyecto
            rol_gerente = crear_rol_gerente_de_proyecto(self)

        return rol_gerente

    class Meta:
        permissions = [
            # CRUD ( GENERAL )
            (PermisoProyecto.LISTAR_PROYECTOS, "Puede ver el listado de proyectos y acceder al sistema."),
            (PermisoProyecto.AGREGAR_PROYECTO, "Puede crear proyectos en el Sistema"),
            (PermisoProyecto.ELIMINAR_PROYECTO, "Puede eliminar proyectos del Sistema"),
            (PermisoProyecto.MODIFICAR_PROYECTO, "Puede modificar los atributos de un proyecto"),
            # Gestión del Proyecto
            (PermisoProyecto.EJECUTAR_PROYECTO, "Puede ejecutar el proyecto"),
            (PermisoProyecto.CANCELAR_PROYECTO, "Puede cancelar el proyecto"),
            (PermisoProyecto.FINALIZAR_PROYECTO, "Puede finalizar el proyecto"),
            # Tipo de item
            (PermisoProyecto.VER_TIPO_ITEM, "Puede ver la lista de tipos de item"),
            (PermisoProyecto.CREAR_TIPO_ITEM, "Puede crear tipos de item en el proyecto"),
            (PermisoProyecto.MODIFICAR_TIPO_ITEM, "Puede modificar tipos de items del proyecto"),
            (PermisoProyecto.IMPORTAR_TIPO_ITEM, "Puede importar tipos de items a otro proyecto"),
            (PermisoProyecto.ELIMINAR_TIPO_ITEM, "Puede eliminar tipos de items del proyecto"),
            # Fases
            (PermisoProyecto.VER_FASES, 'Puede ver la lista de fases del proyecto'),
            (PermisoProyecto.CREAR_FASES, 'Puede crear fases en el proyecto'),
            (PermisoProyecto.ELIMINAR_FASES, 'Puede eliminar fases del proyecto'),
            (PermisoProyecto.MODIFICAR_FASES, 'Puede modificar fases del proyecto'),
            (PermisoProyecto.CERRAR_FASE, 'Puede cerrar fases del proyecto'),
            # Comité
            (PermisoProyecto.ASIGNAR_COMITE, 'Puede asignar/desasignar usuarios como comité de cambios'),
            # Roles
            (PermisoProyecto.VER_ROLES, "Puede ver la lista de roles del proyecto"),
            (PermisoProyecto.CREAR_ROLES, "Puede crear roles en el proyecto"),
            (PermisoProyecto.MODIFICAR_ROLES, "Puede modificar roles en el proyecto"),
            (PermisoProyecto.ELIMINAR_ROLES, "Puede eliminar roles en el proyecto"),
            (PermisoProyecto.ASIGNAR_ROLES, "Puede asignar/desasignar roles en el proyecto"),
            # Participantes
            (PermisoProyecto.VER_PARTICIPANTE, 'Puede ver la lista de participantes del proyecto'),
            (PermisoProyecto.AGREGAR_PARTICIPANTE, 'Puede agregar participantes al proyecto'),
            (PermisoProyecto.QUITAR_PARTICIPANTE, 'Puede remover participantes del proyecto'),

        ]
