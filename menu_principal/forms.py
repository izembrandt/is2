from django import forms

class Usuarios_Form_Field(forms.ModelMultipleChoiceField):
    '''
    Campo de Formulario que representa a Usuarios
    '''
    def label_from_instance(self, obj):
        """Función que construye la representación de un usuario antes un formulario.

        Los usuarios se representarán de la siguiente manera:

        "**Nombre** **Apellido** - **Correo Electrónico**"

        Ejemplo::

          Mateo Fidabel - mateofidabel@gmail.com



        :param obj: Instancia del Usuario
        :return: Representación del usuario
        :rtype: String
        """
        return "{} {} - {}".format(obj.first_name, obj.last_name, obj.email)


class Form_Crear_Proyecto(forms.Form):
    '''
    Formulario utilizado cuando un Administrador crea un proyecto desde el menú principal.
    '''
    nombre = forms.CharField(label='Nombre del Proyecto', max_length=50)
    descripcion = forms.CharField(widget=forms.Textarea, required=False)


class Form_Autorizar_Usuario(forms.Form):
    """
    Formulario utilizado para autorizar usuarios dentro del sistema.
    """
    from django.contrib.auth.models import User
    usuarios = Usuarios_Form_Field(queryset=User.objects
                                   .exclude(groups__permissions__codename__contains='listar_proyecto')
                                   .exclude(email='')
                                   )


class Form_Desautorizar_Usuario(forms.Form):
    """
    Formulario utilizado para desautorizar usuarios dentro del sistema.
    """
    from django.contrib.auth.models import User
    usuarios = Usuarios_Form_Field(queryset=User.objects
                                   .filter(groups__permissions__codename__contains='listar_proyecto')
                                   .exclude(email='')
                                   .exclude(groups__name__contains='Administrador')
                                   )

class Form_Informacion_Personal(forms.Form):
    """
    Formulario utilizado al ingresar a la interfaz de "Mi perfil" en los ajustes. Solo permite modificar el nombre y
    el apellido. El correo electronico no podrá modificarse y llevará la etiqueta **disabled** en el HTML. Tenga en
    cuenta que aún si el usuario manipula el valor del correo electronico al enviar al servidor, se ignorará ese cambio.

    .. note::
      La justificación de que no se pueda cambiar el correo es para asegurar que cada usuario se pueda identificar unicamente
      a través de su correo en el login social de Google. Como Google no permite más de un usuario con el mismo correo, se logra
      la unicidad de los correos entre los usuarios.

    :var nombre: Nombre del Usuario que se mostrará en los menues y en las barras de navegación. Longitud Máxima de 30 Caracteres.
    :var apellido: Apellido del Usuario que se mostrará en los menues y en las barras de navegación. Longitudo Máxima de 150 Caracteres.
    :var email: Correo electronico del usuario que se utiliza para identificarlo en el sistema.

    """
    nombre = forms.CharField(label="Nombre", max_length=30)
    apellido = forms.CharField(label="Apellidos", max_length=150)
    email = forms.CharField(label="Correo Electronico", disabled=True, required=False)
