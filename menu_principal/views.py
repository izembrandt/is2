from django.core.mail import send_mail
from django.http import JsonResponse
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import F
from django.utils import timezone
from django.contrib.auth.models import User
from proyecto.models import Proyecto
from . import forms
from .tasks import correo_proyecto_creado
import logging

logger = logging.getLogger(__name__)


@login_required
@permission_required('proyecto.agregar_proyecto', login_url='Acceso Pendiente')
def crear_proyecto(request):
    """Procesa el formulario de la Creación de un Proyecto, verifica los datos son validos y si el usuario que
    envió el formulario tiene permisos para crear proyectos

    :param request: HttpRequest del Usuario
    :return: HttpRedirect hacia el menu principal
    """
    if request.method == 'POST' and request.user.has_perm('proyecto.agregar_proyecto'):
        # Crear un formulario de CrearProyecto
        form = forms.Form_Crear_Proyecto(request.POST)
        # Verificar si el formulario es valido
        if form.is_valid():
            # Obtener los datos del formulario
            nombre = form.cleaned_data['nombre']
            desc = form.cleaned_data['descripcion']
            estado = 'P'
            fecha = timezone.now()

            # Crear Proyecto
            proyecto = Proyecto(nombre=nombre, descripcion=desc, estado=estado, fecha_creacion=fecha)
            proyecto.save()

            # Agregar al Administrador al proyecto
            proyecto.participantes.add(request.user)

            # Logging
            logger.info("{} creó el proyecto {}".format(request.user.email, proyecto))

            # Enviamos el correo
            correo_proyecto_creado.delay(request.user.email, proyecto.a_dict_json())

            return redirect('React Proyecto Dashboard', proyecto.id)

    return redirect('Frontend App')


@login_required
@permission_required('auth.change_user', raise_exception=True)
def autorizar_usuarios(request):
    """Agrega el Rol de Usuario a los usuarios seleccionados en el form. Esto permitirá a los usuarios
    seleccionados poder utilizar el sistema.

    :param request: HttpRequest del Usuario generado por el Form
    :return: HttpRedirect hacia el menu de ajustes
    """
    from django.contrib.auth.models import Group

    if request.method == 'POST':
        form = forms.Form_Autorizar_Usuario(request.POST)

        if form.is_valid():
            usuarios = form.cleaned_data['usuarios']
            grupo = Group.objects.get(name='Usuario')

            # Logging
            logger.info("{} autorizó usuarios al sistema".format(request.user.email))

            # Agregar Rol de usuario a cada user seleccionado
            for user in usuarios:
                user.groups.add(grupo)
                logger.info("{} fue autorizado a utilizar el sistema por {}".format(user.email, request.user.email))

            return _json_response_usuarios_autorizados()

    return JsonResponse({"error": "Método Invalido."}, status=400)


@login_required
@permission_required('auth.change_user', raise_exception=True)
def desautorizar_usuarios(request):
    """Remueve el Rol de Usuario a los usuarios seleccionados en el form. Esto prohibirá a los usuarios
    seleccionados poder utilizar el sistema.

    :param request: HttpRequest del Usuario generado por el Form
    :return: HttpRedirect hacia el menu de ajustes
    """
    from django.contrib.auth.models import Group
    if request.method == 'POST':
        form = forms.Form_Desautorizar_Usuario(request.POST)

        if form.is_valid():
            usuarios = form.cleaned_data['usuarios']
            grupo = Group.objects.get(name='Usuario')

            # Logging
            logger.info("{} desautorizó usuarios al sistema".format(request.user.email))

            # Agregar Rol de usuario a cada user seleccionado
            for user in usuarios:
                user.groups.remove(grupo)
                logger.info("{} fue desautorizado de utilizar el sistema por {}".format(user.email, request.user.email))

            return _json_response_usuarios_autorizados()

    return JsonResponse({"error": "Método Invalido."}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def actualizar_informacion_personal(request):
    # Verificamos que el metodo sea post
    if request.method == 'POST':
        # Cargamos el formulario
        form = forms.Form_Informacion_Personal(request.POST)

        # Verificamos el formulario y actualizamos
        if form.is_valid():
            usuario = request.user

            usuario.first_name = form.cleaned_data['nombre']
            usuario.last_name = form.cleaned_data['apellido']

            usuario.save()

            #Logging
            logger.info("{} actualizó su información personal".format(request.user.email))

            return _json_response_datos_usuario(request)

    return JsonResponse({"error": "Formulario o Método Invalido"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required('auth.change_user', raise_exception=True)
def obtener_usuarios_autorizados(request):
    """Retorna los usuarios autorizados y no autorizados

    :param request: HttpRequest del Usuario
    """
    if request.method == 'GET':
        return _json_response_usuarios_autorizados()

    return JsonResponse({"error": "Método Invalido"}, status=400)


# Retorna los USUARIOS Autorizados y No Autorizados como JSON RESPONSE
def _json_response_usuarios_autorizados():
    usuarios_autorizados = User.objects \
        .filter(groups__permissions__codename__contains='listar_proyecto') \
        .exclude(email='') \
        .exclude(groups__name__contains='Administrador') \
        .values('id', apellido=F('last_name'), correo=F('email'), nombre=F('first_name'))
    usuarios_no_autorizados = User.objects \
        .exclude(groups__permissions__codename__contains='listar_proyecto') \
        .exclude(email='') \
        .values('id', apellido=F('last_name'), correo=F('email'), nombre=F('first_name'))

    return JsonResponse({
        "autorizados": list(usuarios_autorizados),
        "noAutorizados": list(usuarios_no_autorizados)
    }, status=200)


# Retorna los datos de un usuario como JSON RESPONSE
def _json_response_datos_usuario(request):
    usuario = request.user
    return JsonResponse({"usuario": {
        "nombre": usuario.first_name,
        "apellido": usuario.last_name,
        "correo": usuario.email,
        "administrador": usuario.has_perm('auth.change_user')
    }}, status=200)
