from django.http import JsonResponse
from django.contrib.auth.decorators import login_required, permission_required


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_listar_proyectos_usuario(request):
    """AJAX que permite listar los proyectos de un usuario

    :type request: HttpRequest
    """
    from proyecto.models import Proyecto
    # Verificar que el metodo sea valido y AJAX
    if request.method == 'GET' and request.is_ajax:
        # Enviamos el response
        return JsonResponse({"proyectos": Proyecto.lista_proyectos_del_usuario(request.user)}, status=200)

    # No es valido el request
    return JsonResponse({"error": "No es AJAX ó GET"}, status=400)
