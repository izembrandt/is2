from celery import shared_task
from django.core.mail import send_mail
from django.template.loader import render_to_string


@shared_task
def correo_proyecto_creado(email: str, proyecto: dict):
    contexto = {
        "proyecto": proyecto,
    }
    html = render_to_string('email_templates/proyecto_nuevo.html', context=contexto)
    send_mail(subject='Notificación: Un proyecto ha sido creado', message="Se ha creado el proyecto.",
              recipient_list=[email], from_email=None, fail_silently=False,
              html_message=html)
    return None
