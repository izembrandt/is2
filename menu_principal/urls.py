from django.urls import path
from . import views, ajax

urlpatterns = [
    # Views
    path('crearproyecto/', views.crear_proyecto, name='crear_proyecto'),
    path('ajustes/actualizarPerfil', views.actualizar_informacion_personal, name='Actualizar Información Personal Procesar'),
    path('ajustes/autorizar/', views.autorizar_usuarios, name='Autorizar Usuarios'),
    path('ajustes/desautorizar/', views.desautorizar_usuarios, name='Desautorizar Usuarios'),

    # AJAX
    path('listarProyectos/', ajax.ajax_listar_proyectos_usuario, name='AJAX Listar Proyectos Usuario'),
    path('ajustes/obtenerUsuarioAutorizados/', views.obtener_usuarios_autorizados, name='GET Usuarios Autorizados'),
]
