import React, {useEffect} from "react";
import {Modal, Row, Col, Popover} from "react-bootstrap";
import {fetchComiteProyecto, seleccionarComiteProyecto} from "../../redux/proyecto/comite"
import {useDispatch, useSelector} from "react-redux";
import {
    fetchListaParticipantes, seleccionarListaParticipantesProyecto,
} from "../../redux/proyecto/participantes";
import {seleccionarInfoProyecto, verificarPermisoFase, verificarPermisoProyecto} from "../../redux/proyecto/proyecto";
import {ModalesComite} from "../../components/proyecto/Comite/ModalesComite";
import ContenedorFluido from "../../components/Contenido/ContenedorFluido";
import Button from "react-bootstrap/Button";
import {abrirModal, abrirModificarItem} from "../../redux/proyecto/modales";
import {INTERFAZ_MODAL} from "../../constantes/modales";
import {ListaIntegrantesComite} from "../../components/proyecto/Comite/ListaIntegrantesComite";
import {verificarSiPuedeSoliciar} from "../../redux/proyecto/items";
import {PERMISOS_FASE, PERMISOS_PROYECTO} from "../../constantes/permisos";
import {ESTADO_ITEM, ESTADO_PROYECTO} from "../../constantes/estados";
import {OverlayTrigger} from "react-bootstrap";

const PaginaComite = () => {
    const dispatch = useDispatch();

    //DATOS
    let proyecto = useSelector(seleccionarInfoProyecto);
    let comite = useSelector(seleccionarComiteProyecto);
    const listaParticipantes = useSelector(seleccionarListaParticipantesProyecto);

    //FUNCIONES
    const modalAbrirCrearComite = () => dispatch(abrirModal(INTERFAZ_MODAL.CREAR_COMITE))
    const modalAbrirModificarComite = () => dispatch(abrirModal(INTERFAZ_MODAL.MODIFICAR_COMITE))

    let puedeAsignar = useSelector(verificarPermisoProyecto(PERMISOS_PROYECTO.ASIGNAR_COMITE))
        && (proyecto.estado === ESTADO_PROYECTO.EJECUCION || proyecto.estado === ESTADO_PROYECTO.PENDIENTE);

    //Descargar lista de participantes
    useEffect(() => {
        if (proyecto.id !== 0) {
            dispatch(fetchComiteProyecto({id: proyecto.id}));
        }
    }, [dispatch, proyecto]);

    let sinEvento = () => "";
    let erroresAsignar = [];
    if(!useSelector(verificarPermisoProyecto(PERMISOS_PROYECTO.ASIGNAR_COMITE))){
        erroresAsignar.push((<li key="asignar_comite">No posee el permiso para asignar comité</li>))
    }
    if(proyecto.estado === ESTADO_PROYECTO.CANCELADO){
        erroresAsignar.push((<li key="proyecto_cancelado">EL proyecto no debe estar cancelado</li>))
    }
    if(proyecto.estado === ESTADO_PROYECTO.FINALIZADO){
        erroresAsignar.push((<li key="proyecto_finalizado">EL proyecto no debe estar finalizado</li>))
    }
     const popover_info = (titulo, contenido) => {
     return <Popover id="popover-basic">
            <Popover.Title as="h3">{titulo}</Popover.Title>
            <Popover.Content>
                {contenido}
            </Popover.Content>
        </Popover>;
    };
   const popover_error = (titulo, errores) =>
    {
        return <Popover id="popover-basic">
            <Popover.Title as="h3">{titulo}</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    };

    return (
        <ContenedorFluido>
            {comite === null
                ?
                <div style={{
                    maxWidth: "460px", margin: "auto", textAlign: "center"
                }}>
                    <h3 className="text-gray-900 p-3 m-0"><b>Cómite de cambios</b></h3>
                    <p>Todavía no se formó un cómite de cambios en este proyecto. Un comité permitirá
                        que se puedan crear solicitudes de cambio en el proyecto</p>
                    <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeAsignar?
                        popover_info ("Crear Comité", "Puede crear el comité de cambio")
                        : popover_error ("No puede crear el comité de cambio",erroresAsignar)}>
                    <Button variant={puedeAsignar? "success": "secondary"  } onClick={puedeAsignar? modalAbrirCrearComite : sinEvento}>Crear cómite de cambios</Button>
                    </OverlayTrigger>
                </div>
                :
                <ListaIntegrantesComite comite={comite}/>
            }
            <ModalesComite/>
        </ContenedorFluido>
    );
}

export default PaginaComite;