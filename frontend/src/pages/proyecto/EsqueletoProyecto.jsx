import React, {useEffect} from "react";
//SIDEBAR Y TOP BAR
import SidebarMenu from "../../components/Sidebar/SidebarMenu/SidebarMenu";
import SidebarBrand from "../../components/Sidebar/SidebarBrand/SidebarBrand";
import SidebarDivider from "../../components/Sidebar/SidebarDivider/SidebarDivider";
import NavItem from "../../components/Sidebar/NavItem/NavItem";
import SidebarHeading from "../../components/Sidebar/SidebarHeading/SidebarHeading";
import SidebarToggler from "../../components/Sidebar/SidebarToggler/SidebarToggler";
import Contenido from "../../components/Contenido/Contenido";
import Topbar from "../../components/Topbar/Topbar";
import SidebarToggleTopbar from "../../components/Topbar/SidebarToggleTopbar/SidebarToggleTopbar";
import TopbarNavbar from "../../components/Topbar/TopbarNavbar/TopbarNavbar";
//URLS
import {Link, Route, Switch, useParams} from "react-router-dom";
import NavItemMenuColapsable from "../../components/Sidebar/NavItemMenuColapsable/NavItemMenuColapsable";
import {
    urlComite,
    urlFases,
    urlLineasBases, urlMenuPrincipal,
    urlParticipantes,
    urlProyectoID,
    urlRoles,
    urlSolicitudCambio,
    urlTablero,
    urlTiposItem
} from "../../url";
//Paginas
import PaginaDashboard from "./PaginaDashboard";
import PaginaParticipantes from "./PaginaParticipantes";
import PaginaFases from "./PaginaFases";
import PaginaRoles from "./PaginaRoles";
import PaginaTiposItems from "./PaginaTiposItems";
import PaginaTablero from "./PaginaTablero";
import PaginaSolicitudCambio from "./PaginaSolcitudCambio";
import PaginaComite from "./PaginaComite";
import {useDispatch, useSelector} from "react-redux";
import {
    fetchInfoProyecto,
    seleccionarEstadoProyecto, seleccionarInfoProyecto,
    seleccionarPermisosProyecto,
    vaciarProyecto
} from "../../redux/proyecto/proyecto";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../constantes/estados";
import {PERMISOS_PROYECTO} from "../../constantes/permisos";
import PaginaLineaBase from "./PaginaLineaBase";
import {cerrarTodo} from "../../redux/proyecto/modales";
import "react-toastify/dist/ReactToastify.css";
import {ToastContainer} from "react-toastify";
import {Col, OverlayTrigger, Popover, Accordion} from "react-bootstrap";


const EsqueletoProyecto = () => {
    let {idProyecto} = useParams(); //ID del Proyecto

    let estadoProyecto = useSelector(seleccionarEstadoProyecto);
    let dispatch = useDispatch();
    let permisos = useSelector(seleccionarPermisosProyecto);
    let proyecto = useSelector(seleccionarInfoProyecto);
    let puedeVerParticipantes = permisos.includes(PERMISOS_PROYECTO.VER_PARTICIPANTES);
    let puedeVerRoles = permisos.includes(PERMISOS_PROYECTO.VER_ROLES);
    let puedeVerFases = permisos.includes(PERMISOS_PROYECTO.VER_FASES);
    let puedeVerTiposItem = permisos.includes(PERMISOS_PROYECTO.VER_TIPO);
    useEffect(() => {
        switch (estadoProyecto) {
            case ESTADO_STATE.VACIO:
                dispatch(fetchInfoProyecto({id: idProyecto}));
                break;
            case ESTADO_STATE.COMPLETADO:
                return () => dispatch(vaciarProyecto());
        }

    }, [dispatch, estadoProyecto]);
    const popover = (titulo, contenido) => {
        return <Popover id="popover-basic">
            <Popover.Title as="h3">{titulo}</Popover.Title>
            <Popover.Content>
                {contenido}
            </Popover.Content>
        </Popover>;
    };
    return (
        <div id="wrapper">
            {/* Sidebar */}
            <SidebarMenu>
                <SidebarBrand href={urlMenuPrincipal}/>
                <SidebarDivider className="my-0"/>
                <NavItem icono="fas fa-fw fa-tachometer-alt" to={urlProyectoID(idProyecto)}>Dashboard</NavItem>
                <>
                    <SidebarDivider/>
                    <SidebarHeading>Ajustes</SidebarHeading>
                </>
                <Accordion>
                <>
                    {/* Menu Colapsable de Usuarios */}
                    <NavItemMenuColapsable icono="fas fa-fw fa-user-friends" texto="Usuarios">
                        <>
                            <h6 className="collapse-header">Participantes</h6>
                            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeVerParticipantes
                                    ? popover ("Participantes del proyecto", "Puede ver los participantes del proyecto actual")
                                    : popover ("No puede ver las participantes del proyecto", "No tiene permisos para ver los participantes del proyecto")}>
                            <Link className="collapse-item" style={puedeVerParticipantes ? {} :{color: '#aaaaaa'}} to={puedeVerParticipantes ? urlParticipantes(idProyecto) : "#"}>Participantes</Link>
                            </OverlayTrigger>
                        </>
                        <>
                            <h6 className="collapse-header">Roles y Permisos</h6>
                            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeVerRoles
                                    ? popover ("Roles del proyecto", "Puede ver los roles del proyecto")
                                    : popover ("No puede ver las roles del proyecto", "No tiene permisos para ver los roles del proyecto")}>
                            <Link className="collapse-item" style={puedeVerRoles ? {} :{color: '#AAA'}} to={puedeVerRoles ? urlRoles(idProyecto) : "#"}>Roles</Link>
                            </OverlayTrigger>
                        </>
                    </NavItemMenuColapsable>
                </>
                {/* Menu Colapsable de Desarrollo */}
                <NavItemMenuColapsable icono="fas fa-fw fa-wrench" texto="Desarrollo">
                    <h6 className="collapse-header">Elementos de desarrollo</h6>
                    {/*Fases del proyecto*/}
                    <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeVerFases
                                    ? popover ("Fases del proyecto", "Puede ver las fases del proyecto actual")
                                    : popover ("No puede ver las fases del proyecto", "No tiene permisos para ver las fases")}>
                        <Link className="collapse-item" style={puedeVerFases ? {} :{color: '#AAA'}} to={puedeVerFases ? urlFases(idProyecto) : "#"}>Fases</Link>
                    </OverlayTrigger>
                    {/*Tipos de item*/}
                    <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeVerTiposItem
                                    ? popover ("Tipos de item del proyecto", "Puede ver los tipos de item del proyecto actual")
                                    : popover ("No puede ver las fases del proyecto", "No tiene permisos para ver los tipos de item")}>
                    <Link className="collapse-item" style={puedeVerTiposItem ? {} :{color: '#AAA'}} to={puedeVerTiposItem ? urlTiposItem(idProyecto) : "#"}>Tipos de Item</Link>
                    </OverlayTrigger>
                </NavItemMenuColapsable>
                </Accordion>

                {/* Proyecto */}
                {proyecto.estado !== ESTADO_PROYECTO.PENDIENTE &&
                    <SidebarDivider/>
                }
                {proyecto.estado !== ESTADO_PROYECTO.PENDIENTE &&
                <div onClick={() => dispatch(cerrarTodo())}>
                    <NavItem icono="fas fa-fw fa-columns" to={urlTablero(idProyecto)}>Tablero</NavItem>
                </div>
                }
                {proyecto.estado !== ESTADO_PROYECTO.PENDIENTE &&
                    <NavItem icono="fas fa-fw fa-chart-area" to={urlLineasBases(idProyecto)}>Líneas Base</NavItem>
                }



                <SidebarDivider/>

                <NavItem icono="fas fa-fw fa-users" to={urlComite(idProyecto)}>Comite</NavItem>

                {proyecto.estado !== ESTADO_PROYECTO.PENDIENTE &&
                    <NavItem icono="fas fa-fw fa-table" to={urlSolicitudCambio(idProyecto)}>Solicitud de Cambio</NavItem>
                }
                {/* Boton colapsar*/}
                <SidebarDivider className="d-none d-md-block"/>
                <SidebarToggler/>
            </SidebarMenu>
            {/* Contenido*/}
            <Contenido>
                <Topbar>
                    <SidebarToggleTopbar/>
                    <h4 className="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 float-left">{proyecto.nombre}</h4>
                    <TopbarNavbar/>

                </Topbar>
                {/* Pagina a renderizar */}
                <ToastContainer autoClose={2000} />
                <Switch>
                    <Route path={urlProyectoID(idProyecto)} exact>
                        <PaginaDashboard idProyecto={idProyecto}/>
                    </Route>
                    <Route path={urlParticipantes(idProyecto)}>
                        <PaginaParticipantes idProyecto={idProyecto}/>
                    </Route>
                    <Route path={urlFases(idProyecto)}>
                        <PaginaFases idProyecto={idProyecto}/>
                    </Route>
                    <Route path={urlRoles(idProyecto)}>
                        <PaginaRoles idProyecto={idProyecto}/>
                    </Route>
                    <Route path={urlTiposItem(idProyecto)}>
                        <PaginaTiposItems idProyecto={idProyecto}/>
                    </Route>
                    <Route path={urlTablero(idProyecto)}>
                        <PaginaTablero idProyecto={idProyecto}/>
                    </Route>
                    <Route path={urlLineasBases(idProyecto)}>
                        <PaginaLineaBase/>
                    </Route>
                    <Route path={urlSolicitudCambio(idProyecto)}>
                        <PaginaSolicitudCambio idProyecto={idProyecto}/>
                    </Route>
                    <Route path={urlComite(idProyecto)}>
                        <PaginaComite idProyecto={idProyecto}/>
                    </Route>
                </Switch>
            </Contenido>
        </div>
    );
}


export default EsqueletoProyecto;