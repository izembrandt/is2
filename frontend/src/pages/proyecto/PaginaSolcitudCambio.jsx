import React, {useEffect} from "react";
import ContenedorFluido from "../../components/Contenido/ContenedorFluido";
import Cabecera from "../../components/proyecto/Cabecera/Cabecera";
import {Col, Row} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {fetchComiteProyecto, fetchSolicitudCambio, seleccionarComiteProyecto} from "../../redux/proyecto/comite";
import {ListaSolicitudes} from "../../components/proyecto/Solicitud/ListaSolicitudes";
import {fetchItemsProyecto, seleccionarEstadoItems} from "../../redux/proyecto/items";
import {fetchLineasBase, seleccionarEstadoLB} from "../../redux/proyecto/lineasBase";
import {ESTADO_STATE} from "../../constantes/estados";
import ModalesSolicitud from "../../components/proyecto/Solicitud/ModalesSolicitud";
import {fetchListaParticipantes} from "../../redux/proyecto/participantes";

const PaginaSolicitudCambio = ({idProyecto}) => {
    let dispatch = useDispatch();
    let estadoItem = useSelector(seleccionarEstadoItems) === ESTADO_STATE.COMPLETADO;
    let estadoLB = useSelector(seleccionarEstadoLB) === ESTADO_STATE.COMPLETADO;

    useEffect(() => {
        dispatch(fetchItemsProyecto({ id: idProyecto }));
        dispatch(fetchLineasBase({ id: idProyecto}));
        dispatch(fetchSolicitudCambio({id: idProyecto}));
        dispatch(fetchComiteProyecto({id: idProyecto}));
        dispatch(fetchListaParticipantes(idProyecto));
    }, [dispatch]);

    return (
        <ContenedorFluido>
            {/* Aca va el Contenido de la página, hagan nomás el esquelo, los datos vamos a agregar despues */}
            <Row>
                <Col xs={12} xl={9} lg={9} md={8}>
                    <Cabecera titulo="Solicitud de cambio"
                              descripcion="Son las solicitudes de cambios para items que estan dentro de una linea de base cerrada.">
                    </Cabecera>
                </Col>
            </Row>
            <Row>
                {estadoItem && estadoLB &&
                    <ListaSolicitudes/>
                }
            </Row>
            <ModalesSolicitud/>
        </ContenedorFluido>
    );
}


export default PaginaSolicitudCambio;