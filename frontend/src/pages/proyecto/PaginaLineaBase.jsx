import React, {useEffect} from "react";
import ContenedorFluido from "../../components/Contenido/ContenedorFluido";
import {useDispatch, useSelector} from "react-redux";
import {seleccionarInfoProyecto} from "../../redux/proyecto/proyecto";
import {fetchFasesProyecto, seleccionarFasesProyecto} from "../../redux/proyecto/fases";
import {fetchItemsProyecto} from "../../redux/proyecto/items";
import {cerrarTodo} from "../../redux/proyecto/modales";
import {ListaLineaBase} from "../../components/proyecto/LineaBase/ListaLineaBase";
import {urlObtenerLineasBase} from "../../url";
import { fetchLineasBase } from "../../redux/proyecto/lineasBase";
import {ModalesLB} from "../../components/proyecto/LineaBase/ModalesLB";

import '../../components/proyecto/Tablero/Tablero.scss';

const PaginaLineaBase = () => {

    let proyecto = useSelector(seleccionarInfoProyecto);
    let fases = useSelector(seleccionarFasesProyecto);
    const fases_ordenados = JSON.parse(JSON.stringify(fases)).sort((a, b) => (a.numero - b.numero));
    let dispatch = useDispatch();


    useEffect(() => {
        if (proyecto.id !== 0) {
            dispatch(cerrarTodo());
            dispatch(fetchLineasBase({id: proyecto.id}));
            dispatch(fetchFasesProyecto(proyecto.id));
            dispatch(fetchItemsProyecto({id: proyecto.id}));
        }
    }, [dispatch, proyecto]);

    return (
        <ContenedorFluido>
            <div className="ui">
                <div className="lists">
                    {
                        fases_ordenados.map(fase => <ListaLineaBase fase={fase} key={fase.id}/>)
                    }
                </div>
            </div>
            <ModalesLB/>
        </ContenedorFluido>
    );

}

export default PaginaLineaBase;