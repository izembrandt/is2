import React from "react";
import ContenedorFluido from "../../components/Contenido/ContenedorFluido";
import InformacionProyecto from "../../components/proyecto/Dashboard/InformacionProyecto";
import GerenteProyecto from "../../components/proyecto/Dashboard/GerenteProyecto";
import AccionesProyecto from "../../components/proyecto/Dashboard/AccionesProyecto";

import {useSelector} from "react-redux";
import {esAdministrador} from "../../redux/perfil";


const PaginaDashboard = () => {
    let esAdmin = useSelector(esAdministrador);
    return (
        <ContenedorFluido>
            {/* Aca va el Contenido de la página, hagan nomás el esquelo, los datos vamos a agregar despues */}
            <div className="row mb-4">
                <div className="col">
                    <h1 className="h3 mb-0 text-gray-800">Dashboard</h1>
                </div>
            </div>

            <div className="row my-2">
                <InformacionProyecto/>
                <AccionesProyecto/>
            </div>
            {esAdmin &&
                <div className="row my-2">
                    <GerenteProyecto/>
                </div>
            }
        </ContenedorFluido>
    );
}

export default PaginaDashboard;