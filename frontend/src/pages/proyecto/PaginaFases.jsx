import React, {useEffect, useState} from "react";
import ContenedorFluido from "../../components/Contenido/ContenedorFluido";
import {Button, Col, Row} from "react-bootstrap";
import Cabecera from "../../components/proyecto/Cabecera/Cabecera";
import ListaFases from "../../components/proyecto/Fases/ListaFases";
import {useDispatch, useSelector} from "react-redux";
import {
    fetchFasesProyecto, seleccionarFasesProyecto, vaciarFases
} from "../../redux/proyecto/fases";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../constantes/estados";
import AgregarFase from "../../components/proyecto/Fases/AgregarFase";
import {fetchSolicitudCambio} from "../../redux/proyecto/comite";


const PaginaFases = ({ idProyecto }) => {
    const dispatch = useDispatch();
    const estadosFase = useSelector(state => state.fases.estado);
    const listaFases = useSelector(seleccionarFasesProyecto);

    //Descargar lista de fases
    useEffect(() => {
        if ( estadosFase !== ESTADO_STATE.PENDIENTE) {
            dispatch(fetchFasesProyecto(idProyecto));
            dispatch(fetchSolicitudCambio({id: idProyecto}));
        }
    }, [dispatch]);

    return(
        <ContenedorFluido>
            {/* Aca va el Contenido de la página, hagan nomás el esquelo, los datos vamos a agregar despues */}
            <Row>
                <Col xs={12} xl={10} lg={9} md={8}>
                        <Cabecera titulo="Fases del Proyecto"
                              descripcion="Son las Fases que el usuario puede ver">
                    </Cabecera>
                </Col>
                {/*Agregar Fase Boton*/}
                <AgregarFase idProyecto={idProyecto}/>
            </Row>
            <Row>
                <ListaFases fases={listaFases}/>
            </Row>
        </ContenedorFluido>
    );
}

export default PaginaFases;