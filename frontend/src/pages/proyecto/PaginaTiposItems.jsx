import React, {useEffect} from "react";
import ContenedorFluido from "../../components/Contenido/ContenedorFluido";
import Cabecera from "../../components/proyecto/Cabecera/Cabecera";
import {useDispatch, useSelector} from "react-redux";
import {fetchTiposItem, seleccionarEstadoTiposItem, vaciarTipos} from "../../redux/proyecto/tipoItem";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../constantes/estados";
import {ListaTipoItem} from "../../components/proyecto/TipoItem/ListaTipoItem";
import {Row, Col, Button} from "react-bootstrap";
import {AgregarTipoItem} from "../../components/proyecto/TipoItem/AgregarTipoItem";
import {ImportarTipoItem} from "../../components/proyecto/TipoItem/ImportarTipoItem";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../redux/proyecto/proyecto";
import {PERMISOS_PROYECTO} from "../../constantes/permisos";

const PaginaTiposItems = ({idProyecto}) => {
    let dispatch = useDispatch();
    let estadoTipos = useSelector(seleccionarEstadoTiposItem);
    const permisos = useSelector(seleccionarPermisosProyecto);
    let { estado } = useSelector(seleccionarInfoProyecto);
    let puedeImportar = permisos.includes(PERMISOS_PROYECTO.CREAR_TIPO) && estado !== ESTADO_PROYECTO.CANCELADO;
    let puedeCrear = permisos.includes(PERMISOS_PROYECTO.CREAR_TIPO) && estado !== ESTADO_PROYECTO.CANCELADO;
    //Hacemos fetch de los tipos de item
    useEffect(() => {
        switch (estadoTipos) {
            case ESTADO_STATE.VACIO:
                dispatch(fetchTiposItem({id: idProyecto}));
                break;
            case ESTADO_STATE.COMPLETADO:
                return () => dispatch(vaciarTipos());
        }
    }, [dispatch, estadoTipos]);

    return (
        <ContenedorFluido>
            {/* Aca va el Contenido de la página, hagan nomás el esquelo, los datos vamos a agregar despues */}
            <Row>
                <Col xs={6} xl={10} lg={9} md={8}>
                    <Cabecera titulo="Tipos de Item"
                              descripcion="Describen la estructura de los items y sus atributos."/>
                </Col>
                {/*Boton Agregar tipo Item*/}
                <AgregarTipoItem idProyecto={idProyecto}/>

            </Row>

            <Row>
                <ListaTipoItem idProyecto={idProyecto}/>
                {/*Boton importar tipo de item*/}
                <ImportarTipoItem idProyecto={idProyecto}/>

            </Row>


        </ContenedorFluido>
    );
}

export default PaginaTiposItems;