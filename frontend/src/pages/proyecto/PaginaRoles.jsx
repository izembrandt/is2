import React, {useEffect, useState} from "react";
import ContenedorFluido from "../../components/Contenido/ContenedorFluido";
import Cabecera from "../../components/proyecto/Cabecera/Cabecera";
import ListaRoles from "../../components/proyecto/Roles/ListaRoles";
import {Button, Col, Row} from "react-bootstrap";
import AgregarRol from "../../components/proyecto/Roles/AgregarRol";
import {useDispatch, useSelector} from "react-redux";
import {
    fetchListaRoles,
    seleccionarListaRolesProyecto,
    vaciarRoles
} from "../../redux/proyecto/roles";
import {ESTADO_STATE} from "../../constantes/estados";
import {fetchFasesProyecto} from "../../redux/proyecto/fases";


const PaginaRoles = ({idProyecto}) => {
    const dispatch = useDispatch();
    const estadosLista = useSelector(state => state.roles.estado);
    const listaRoles = useSelector(seleccionarListaRolesProyecto);
    //Descargar lista de roles
    useEffect(() => {
        if (idProyecto !== 0) {
            dispatch(fetchFasesProyecto(idProyecto))
                .then(() => dispatch(fetchListaRoles(idProyecto)));
        }
    }, [dispatch]);
    //TODO: Agregar opcion de buscar roles
    return (
        <ContenedorFluido>
            {/* Aca va el Contenido de la página, hagan nomás el esquelo, los datos vamos a agregar despues */}
            <Row>
                <Col xs={12} xl={10} lg={9} md={8}>
                    <Cabecera titulo="Roles del Proyecto"
                              descripcion="Son los roles que el usuario puede asignar o sacar a los participantes del proyecto.">

                    </Cabecera>
                </Col>
                {/*Se muestra la opcion de agregar Rol*/}
                <AgregarRol idProyecto={idProyecto}/>
            </Row>
            <Row>
                <ListaRoles roles={listaRoles} idProyecto={idProyecto}/>
            </Row>
        </ContenedorFluido>
    );
}


export default PaginaRoles;