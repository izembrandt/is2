import React, {useEffect, useState} from "react";
import ContenedorFluido from "../../components/Contenido/ContenedorFluido";
import Cabecera from "../../components/proyecto/Cabecera/Cabecera";
import ListaParticipantes from "../../components/proyecto/Participantes/ListaParticipantes";
import {Button, Col, Row} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {
    fetchListaParticipantes,
    seleccionarListaParticipantesProyecto,
    vaciarParticipantes
} from "../../redux/proyecto/participantes";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../constantes/estados";
import {AgregarParticipante} from "../../components/proyecto/Participantes/AgregarParticipante";
import {fetchComiteProyecto} from "../../redux/proyecto/comite";


const PaginaParticipantes = ({ idProyecto }) => {
    const dispatch = useDispatch();
    const listaParticipantes = useSelector(seleccionarListaParticipantesProyecto);
    //Descargar lista de participantes
    useEffect(() => {
        if (idProyecto !== 0) {
            dispatch(fetchListaParticipantes(idProyecto));
            dispatch(fetchComiteProyecto({id: idProyecto}));
        }
    }, [dispatch]);

    return (
        <ContenedorFluido>
            {/* Aca va el Contenido de la página, hagan nomás el esquelo, los datos vamos a agregar despues */}
            <Row>
                <Col xs={12} xl={9} lg={9} md={8}>
                    <Cabecera titulo="Participantes del Proyecto"
                              descripcion="Son los participantes del proyecto.">

                    </Cabecera>
                </Col>
                {/*Boton para agregar participante*/}
                <AgregarParticipante/>
            </Row>
            <Row>
                <ListaParticipantes participantes={listaParticipantes}/>
            </Row>
        </ContenedorFluido>
    );
}


export default PaginaParticipantes;