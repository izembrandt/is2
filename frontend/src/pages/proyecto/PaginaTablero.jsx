import React, {useEffect} from "react";
import ContenedorFluido from "../../components/Contenido/ContenedorFluido";
import {useDispatch, useSelector} from "react-redux";
import {fetchFasesProyecto, seleccionarFasesProyecto} from "../../redux/proyecto/fases";
import {ListaItems} from "../../components/proyecto/Tablero/ListaItems";
import {seleccionarEstadoProyecto, seleccionarInfoProyecto} from "../../redux/proyecto/proyecto";

import '../../components/proyecto/Tablero/Tablero.scss';
import {fetchItemsProyecto} from "../../redux/proyecto/items";
import {fetchTiposItem} from "../../redux/proyecto/tipoItem";
import {Modales} from "../../components/proyecto/Tablero/Modales";
import {seleccionarModalAbierto, cerrarTodo} from "../../redux/proyecto/modales";
import {INTERFAZ_MODAL} from "../../constantes/modales";
import {fetchLineasBase} from "../../redux/proyecto/lineasBase";
import {TrazabilidadFases} from "../../components/proyecto/Tablero/Impacto/TrazabilidadFases";
import {fetchSolicitudCambio} from "../../redux/proyecto/comite";

const PaginaTablero = () => {
    let dispatch = useDispatch();
    let estadoProyecto = useSelector(seleccionarEstadoProyecto);
    let proyecto = useSelector(seleccionarInfoProyecto);
    let fasesProyecto = useSelector(seleccionarFasesProyecto);
    let modalActivo = useSelector(seleccionarModalAbierto);

    //Despachar items
    useEffect(() => {
        if (proyecto.id !== 0) {
            dispatch(cerrarTodo());
            dispatch(fetchFasesProyecto(proyecto.id));
            dispatch(fetchItemsProyecto({id: proyecto.id}));
            dispatch(fetchTiposItem({id: proyecto.id}));
            dispatch(fetchLineasBase({id: proyecto.id}));
            dispatch(fetchSolicitudCambio({ id: proyecto.id}));
        }
    }, [dispatch, estadoProyecto]);

    return (
        <ContenedorFluido>
            {modalActivo !== INTERFAZ_MODAL.TRAZABILIDAD
                ?
                <div className="ui">
                    <div className="lists">
                        {
                            fasesProyecto.map(fase => (<ListaItems key={fase.id} fase={fase}/>))
                        }
                    </div>
                </div>
                : <TrazabilidadFases/>
            }
            <Modales/>
        </ContenedorFluido>
    );
}

export default PaginaTablero;