import React, {Component} from "react";
import {
    Switch,
    Route,
} from "react-router-dom";

import SidebarMenu from "../../components/Sidebar/SidebarMenu/SidebarMenu";
import SidebarBrand from "../../components/Sidebar/SidebarBrand/SidebarBrand";
import SidebarDivider from "../../components/Sidebar/SidebarDivider/SidebarDivider";
import NavItem from "../../components/Sidebar/NavItem/NavItem";
import SidebarHeading from "../../components/Sidebar/SidebarHeading/SidebarHeading";
import SidebarToggler from "../../components/Sidebar/SidebarToggler/SidebarToggler";
import Contenido from "../../components/Contenido/Contenido";
import Topbar from "../../components/Topbar/Topbar";
import SidebarToggleTopbar from "../../components/Topbar/SidebarToggleTopbar/SidebarToggleTopbar";
import TopbarNavbar from "../../components/Topbar/TopbarNavbar/TopbarNavbar";
import PaginaProyecto from "./PaginaProyecto";
import PaginaPerfil from "./PaginaPerfil";
import PaginaAutorizar from "./PaginaAutorizar";
import {urlAjustesPerfil, urlAutorizarUsuario, urlMenuPrincipal} from "../../url";
import {useSelector} from "react-redux";
import {esAdministrador} from "../../redux/perfil";

const MenuPrincipal = (props) => {
        let esAdmin = useSelector(esAdministrador);

        return (
            <div id="wrapper">
                <SidebarMenu>
                    <SidebarBrand/>
                    <SidebarDivider className="my-0"/>
                    <NavItem icono="far fa-fw fa-file" to={urlMenuPrincipal}>Mis Proyectos</NavItem>
                    <SidebarDivider/>
                    <SidebarHeading>Ajustes</SidebarHeading>
                    <NavItem icono="fas fa-fw fa-user-circle" to={urlAjustesPerfil}>Mi perfil</NavItem>
                    {esAdmin &&
                    <NavItem icono="fas fa-fw fa-user-shield" to={urlAutorizarUsuario}>Autorizar Usuarios</NavItem>
                    }
                    <SidebarDivider className="d-none d-md-block"/>
                    <SidebarToggler/>
                </SidebarMenu>
                <Contenido>
                    <Topbar>
                        <SidebarToggleTopbar/>
                        <TopbarNavbar/>
                    </Topbar>

                    <Switch>
                        <Route path={urlAjustesPerfil}>
                            <PaginaPerfil/>
                        </Route>
                        {esAdmin &&
                        <Route path={urlAutorizarUsuario}>
                            <PaginaAutorizar/>
                        </Route>
                        }
                        <Route path={urlMenuPrincipal}> {/* Si es que entra a la lista de proyectos (default) */}
                            <PaginaProyecto/>
                        </Route>
                    </Switch>
                </Contenido>
            </div>
        );
}

export default MenuPrincipal;