import React, {useEffect, useState} from "react";
import Alert from "react-bootstrap/Alert";

import HeaderMenuProyectos from "../../components/menu_principal/proyecto/HeaderMenuProyectos";
import ListaProyectos from "../../components/menu_principal/proyecto/ListaProyectos";
import ContenedorFluido from "../../components/Contenido/ContenedorFluido";
import {useDispatch, useSelector} from "react-redux";
import {fetchListaProyectos, seleccionarListaProyectos, vaciarLista} from "../../redux/listaProyectos";
import {esAdministrador} from "../../redux/perfil";

const PaginaProyecto = () => {
    let [mostrarAlerta, setMostrarAlerta] = useState(false);
    let estadosLista = useSelector(state => state.listaProyectos.estado);
    let listaProyectos = useSelector(seleccionarListaProyectos);
    let esAdmin = useSelector(esAdministrador);
    let dispatch = useDispatch();


    //Se encarga de descargar la lista de proyectos al crear el componente
    useEffect(() => {
        switch (estadosLista) {
            case "vacio":
                dispatch(fetchListaProyectos());
                break;
            case "completado":
                return () => dispatch(vaciarLista());
        }
    }, [dispatch, listaProyectos]);

    //El renderizado
    return (
        <ContenedorFluido>
            {mostrarAlerta &&//Muestra la alerta solo si es participante en algún proyecto
            <Alert variant="info" onClose={() => setMostrarAlerta(false)} dismissible>
                No eres participante en ningún proyecto.
            </Alert>
            }
            <HeaderMenuProyectos puedeAgregarProyectos={esAdmin}/>
            <ListaProyectos proyectos={listaProyectos}/>
        </ContenedorFluido>
    );
};

export default PaginaProyecto;
