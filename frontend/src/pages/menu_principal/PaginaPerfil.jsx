import React, {useEffect, useState} from "react";
import {Col, Row, Form, Button} from "react-bootstrap";
import ContenedorFluido from "../../components/Contenido/ContenedorFluido";
import {useDispatch, useSelector} from "react-redux";
import {actualizarPerfil, seleccionarPerfilUsuario} from "../../redux/perfil";
import CSRFToken from "../../components/CSRFToken";

const PaginaPerfil = () => {
    //Datos Iniciales
    let perfilUsuario = useSelector(seleccionarPerfilUsuario);
    let [nombreUsuario, setNombreUsuario] = useState("");
    let [apellidoUsuario, setApellidoUsuario] = useState("");

    useEffect(()=>{
        setNombreUsuario(perfilUsuario.nombre);
        setApellidoUsuario(perfilUsuario.apellido);
    }, [perfilUsuario]);

    //Formulario
    const cambiarNombre = event => setNombreUsuario(event.target.value);
    const cambiarApellido = event => setApellidoUsuario(event.target.value);

    let dispatch = useDispatch();
    let manejarSubmit = (evento) => {
        evento.preventDefault();
        dispatch(actualizarPerfil(new FormData(evento.target)));
    }

    //Render
    return(
        <ContenedorFluido>
            <Row>
                <Col lg={4}>
                    <h4>Datos Personales</h4>
                    <p>La información basica que tenemos sobre vos</p>
                </Col>
                <Col lg={8}>
                    <Form onSubmit={manejarSubmit}>
                        <CSRFToken/>
                        <Form.Group>
                            <Form.Label>Nombre*</Form.Label>
                            <Form.Control
                                type="text"
                                name="nombre"
                                value={nombreUsuario}
                                onChange={cambiarNombre}
                                maxLength="30"
                                required/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Apellido*</Form.Label>
                            <Form.Control
                                type="text"
                                name="apellido"
                                value={apellidoUsuario}
                                onChange={cambiarApellido}
                                maxLength="150"
                                required/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Correo Electrónico</Form.Label>
                            <Form.Control type="text" name="email" value={perfilUsuario.correo} disabled/>
                        </Form.Group>
                        <Button type="submit" variant="primary my-2">
                            Actualizar
                        </Button>
                    </Form>
                </Col>
            </Row>
        </ContenedorFluido>
    );
}

export default PaginaPerfil;