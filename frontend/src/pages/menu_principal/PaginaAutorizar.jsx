import React, {useEffect} from "react";
import {Row, Col, Card, Form, Button} from "react-bootstrap";
import ContenedorFluido from "../../components/Contenido/ContenedorFluido";
import CSRFToken from "../../components/CSRFToken";
import {useDispatch, useSelector} from "react-redux";
import {
    autorizarUsuarioPOST, desautorizarUsuarioPOST,
    fetchUsuariosAutorizados,
    seleccionarUsuariosAutorizados,
    seleccionarUsuariosDesautorizados,
    vaciarUsuarios
} from "../../redux/usuariosAutorizados";


const PaginaAutorizar = () => {
    //Obtener los usuarios
    let estadoUsuarios = useSelector(state => state.usuariosAutorizados.estado);
    let usuariosAutorizados = useSelector(seleccionarUsuariosAutorizados);
    let usuariosNoAutorizados = useSelector(seleccionarUsuariosDesautorizados);

    let dispatch = useDispatch();

    useEffect(() => {
        switch (estadoUsuarios){
            case "vacio":
                dispatch(fetchUsuariosAutorizados());
                break;
            case "completado":
                return () => dispatch(vaciarUsuarios());
        }
    }, [dispatch, usuariosAutorizados, usuariosNoAutorizados]);

    return (
        <ContenedorFluido>
            <Row>
                <FormSeleccionarUsuario
                    titulo="Autorizar Usuarios"
                    textoBoton="Autorizar"
                    opciones={usuariosNoAutorizados}
                    accion={autorizarUsuarioPOST}
                />
                <FormSeleccionarUsuario
                    titulo="Desautorizar Usuarios"
                    textoBoton="Desautorizar"
                    opciones={usuariosAutorizados}
                    accion={desautorizarUsuarioPOST}
                />
            </Row>
        </ContenedorFluido>
    );
};

const FormSeleccionarUsuario = ({titulo, opciones = [], textoBoton, accion}) => {
    let opcionesUsuarios = opciones.map(usuario => <option key={usuario.id}
                                                           value={usuario.id}>{usuario.nombre + " " + usuario.apellido + " - " + usuario.correo}</option>)

    let dispatch = useDispatch();

    const onSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target)
        dispatch(accion(formData));
    }

    return (
        <Col lg={6}>
            <Card className="shadow mb-4">
                <Card.Header className="py-3">
                    <h6 className="m-0 font-weight-bold text-primary">{titulo}</h6>
                </Card.Header>
                <Card.Body>
                    <Form onSubmit={onSubmit}>
                        <CSRFToken/>
                        <Form.Group>
                            <Form.Label>Usuarios</Form.Label>
                            <Form.Control as="select" multiple name="usuarios">
                                {opcionesUsuarios}
                            </Form.Control>
                        </Form.Group>
                        <Button type="submit" variant="primary">{textoBoton}</Button>
                    </Form>
                </Card.Body>
            </Card>
        </Col>
    );
}


export default PaginaAutorizar;