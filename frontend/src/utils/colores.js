// https://www.w3.org/TR/AERT/#color-contrast
export const hallarColorTexto = colorFondoHex => {
    let colorRGB = hexToRgb(colorFondoHex);

    const brillo = ( colorRGB.r * 299 + colorRGB.g * 587 + colorRGB.b * 114) / 1000;

    return (brillo > 125) ? "#000000" : "#FFFFFF"
}

// https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
const hexToRgb = hex => {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

export const coloresLineaBaseDefault = [
    "#8FDD3E",
    "#00B159",
    "#2553B6",
    "#00AEDB",
    "#F37735",
    "#FFC425",
    "#3F3F3F",
    "#FC4FB5",
    "#EE4057",
    "#D11141",
]
