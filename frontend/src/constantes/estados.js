//Estados de un state
export const ESTADO_STATE = {
    VACIO: 'vacio',
    PENDIENTE: 'pendiente',
    COMPLETADO: 'completado',
    FALLADO: 'fallado'
}

export const ESTADO_PROYECTO = {
    PENDIENTE: 'P',
    EJECUCION: 'E',
    CANCELADO: 'C',
    FINALIZADO: 'F'
}

export const ESTADO_ITEM = {
    DESARROLLO: 'ED',
    DESACTIVADO: 'DA',
    PENDIENTE: 'PA',
    APROBADO: 'IA',
    EN_LINEA_BASE: 'LB',
    REVISION: 'RE',
    BORRADOR: 'BO'
}

export const ESTADO_LINEA = {
    ABIERTO: 'A',
    CERRADO: 'C',
    ROTO: 'R',
}

export const ESTADO_SOLICITUD = {
    PENDIENTE: 'P',
    APROBADO: 'A',
    RECHAZADO: 'R',
    CANCELADO: 'C',
}