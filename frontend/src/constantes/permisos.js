//Globales
export const PERMISOS_GLOBALES = {
    ADMINISTRADOR: "auth.change_user"
}

//Proyecto
export const PERMISOS_PROYECTO = {
    //GESTION DEL PROYECTO
    EJECUTAR_PROYECTO: "ejecutar_proyecto",
    CANCELAR_PROYECTO: "cancelar_proyecto",
    FINALIZAR_PROYECTO: "finalizar_proyecto",
    //TIPO DE ITEM
    VER_TIPO: "ver_tipo_de_item",
    CREAR_TIPO: "crear_tipo_de_item",
    MODIFICAR_TIPO: "modificar_tipo_de_item",
    IMPORTAR_TIPO: "importar_tipo_de_item",
    ELIMINAR_TIPO: "eliminar_tipo_de_item",
    //FASES
    VER_FASES: 'ver_fases',
    CREAR_FASES: 'crear_fases',
    ELIMINAR_FASES: 'eliminar_fases',
    MODIFICAR_FASES: 'modificar_fases',
    CERRAR_FASES: 'cerrar_fase',
    //Comite
    ASIGNAR_COMITE: 'asignar_comite',
    DESASIGNAR_COMITE: 'desasignar_comite',
    //ROLES
    VER_ROLES: "ver_roles",
    CREAR_ROLES: "crear_roles",
    MODIFICAR_ROLES: "modificar_roles",
    ELIMINAR_ROLES: "eliminar_roles",
    ASIGNAR_ROLES: "asignar_roles",
    DESASIGNAR_ROLES: "sacar_roles",
    //PARTICIPANTES
    VER_PARTICIPANTES: 'ver_participantes',
    AGREGAR_PARTICIPANTES: 'agregar_participantes',
    QUITAR_PARTICIPANTES: 'quitar_participantes',
}


export const PERMISOS_FASE = {
    //ITEMS
    CREAR_ITEM: 'crear_item',
    MODIFICAR_ITEM: 'modificar_item',
    APROBAR_ITEM: 'aprobar_item',
    DESAPROBAR_ITEM: 'desaprobar_item',
    ELIMINAR_ITEM: 'eliminar_item',
    RELACIONAR_ITEM: 'relacionar_item',
    //LINEA BASE
    CREAR_LINEA_BASE: 'crear_linea_base',
    MODIFICAR_LINEA_BASE: 'modificar_linea_base',
    ELIMINAR_LINEA_BASE: 'eliminar_linea_base',
    CERRAR_LINEA_BASE: 'cerrar_linea_base',
    ROMPER_LINEA_BASE: 'romper_linea_base',
    //SOLICITUD DE CAMBIOS
    CREAR_SOLICITUD_CAMBIO: 'solicitar_cambio',
}