//General
export const urlCerrarSesion = "/accounts/logout/"; //POST
export const urlFetchUsuario = "/usuarios/fetchUsuario/"; //GET

//Menu Principal
export const urlMenuPrincipal = "/app/menu";
export const urlAjustesPerfil = "/app/menu/ajustes";
export const urlAutorizarUsuario = "/app/menu/autorizar";
export const urlCrearProyecto = "/menu/crearproyecto/"; //POST
export const urlFetchListaProyectos = "/menu/listarProyectos"; //GET
export const urlActualizarPerfil = "/menu/ajustes/actualizarPerfil"; //POST
export const urlAutorizarUsuarioPOST = "/menu/ajustes/autorizar/"; //POST
export const urlDesautorizarUsuarioPOST = "/menu/ajustes/desautorizar/"; //POST
export const urlObtenerUsuariosAutorizados = "/menu/ajustes/obtenerUsuarioAutorizados/"; //GET

//Proyecto - Dashboard
export const urlProyecto = "/app/proyecto/";
export const urlProyectoObtenerInfo = id => urlProyectoAPI(id) + "obtenerInfo/";
export const urlProyectoID = id => urlProyecto + id + "/"; //URL del Proyecto con el ID
export const urlEjecutarProyecto = id => urlProyectoAPI(id) + "ejecutar/";
export const urlCancelarProyecto = id => urlProyectoAPI(id) + "cancelar/";
export const urlModificarProyecto = id => urlProyectoAPI(id) + "modificar/";
export const urlAsignarGerente = id => urlProyectoAPI(id) + "asignarGerente/";
export const urlFinalizarProyecto = id => urlProyectoAPI(id) + "finalizar/";
export const urlGenerarInformeItems = id => urlProyectoAPI(id) + "generarInformeItems/";

//Participantes
export const urlParticipantes = id => urlProyectoID(id) + "participantes/";
export const urlProyectoAPI = id => "/proyecto/" + id + "/";
export const urlListaParticipantes = id => urlProyectoAPI(id) + "ajax_listar_usuarios/";
export const urlAgregarParticipantes = id => urlProyectoAPI(id) + "agregar/";
export const urlAsignarRol = id => urlProyectoAPI(id) + "asignarRol/";
export const urlEliminarParticipante = id => urlProyectoAPI(id) + "eliminar/";

//Roles
export const urlRoles = id => urlProyectoID(id) + "roles/";
export const urlRolesAPI = id => "/proyecto/" + id + "/roles/";
export const urlListaRoles = id => urlRolesAPI(id) + "listarRoles/";
export const urlCrearRol = id => urlRolesAPI(id) + "crearRoles/";
export const urlEliminarRol = id => urlRolesAPI(id) + "eliminarRol/";
export const urlEditarRol = id => urlRolesAPI(id) + "editarRol/";

//Fases
export const urlFases = id => urlProyectoID(id) + "fases/";
export const urlFasesAPI = id => "/proyecto/" + id + "/fase/";
export const urlListarFases = id => urlFasesAPI(id) + "listar/";
export const urlCrearFase = id => urlFasesAPI(id) + "agregar/";
export const urlEditarFase = id => urlFasesAPI(id) + "editar/";
export const urlEliminarFase = id => urlFasesAPI(id) + "eliminar/";
export const urlCerrarFase= id => urlFasesAPI(id) + "cerrar/";
export const urlSubirFase = id => urlFasesAPI(id) + "subir/";
export const urlBajarFase = id => urlFasesAPI(id) + "bajar/";
export const urlGenerarInformeFase = (id,faseId) => urlFasesAPI(id) + 'generarInformeFase/' + faseId;


//Tipos de Item
export const urlTiposItem = id => urlProyectoID(id) + "tipos/";
export const urlTipoItemAPI = id => "/proyecto/" + id + "/item/";
export const urlListarTipoItem = id => urlTipoItemAPI(id) + "listarTipo/";
export const urlCrearTipoItem = id => urlTipoItemAPI(id) + "crearTipo/";
export const urlEditarTipoItem = id => urlTipoItemAPI(id) + "editarTipo/";
export const urlEliminarTipoItem = id => urlTipoItemAPI(id) + "eliminarTipo/";
export const urlImportarTipoItem = id => urlTipoItemAPI(id) + "importarTipo/";

//Tablero - Workshop
export const urlTablero = id => urlProyectoID(id) + "tablero/";

//Lineas bases
export const urlLineasBases = id => urlProyectoID(id) + "lineaBase/";
export const urlLineaBaseAPI = id => urlProyectoAPI(id) + "lineaBase/";
export const urlObtenerLineasBase = id => urlLineaBaseAPI(id) + "obtenerLineas/";
export const urlCrearLineaBase = id => urlLineaBaseAPI(id) + 'crearLB/';
export const urlModificarLineaBase = id => urlLineaBaseAPI(id) + 'modificarLB/';
export const urlCerrarLineaBase = id => urlLineaBaseAPI(id) + 'cerrarLB/';
export const urlAgregarItemLineaBase = id => urlLineaBaseAPI(id) + 'agregarItem/';
export const urlEliminarItemLineaBase = id => urlLineaBaseAPI(id) + 'eliminarItem/';
export const urlGenerarInformeLineaBase = (id, uuid) => urlLineaBaseAPI(id) + 'generarInforme/' + uuid;



//Comite de Cambio
export const urlSolicitudCambio = id => urlProyectoID(id) + "solicitudes/";
export const urlComite = id => urlProyectoID(id) + "comite/";
export const urlComiteCambioAPI = id => urlProyectoAPI(id) + "cambio/";
export const urlObtenerComite = id => urlComiteCambioAPI(id) + "obtenerComite/";
export const urlCrearComite = id => urlComiteCambioAPI(id) + "crearComite/";
export const urlAsignarComite = id => urlComiteCambioAPI(id) + "asignarComite/";
export const urlObtenerSolicitudes = id => urlComiteCambioAPI(id) + "obtenerSolicitud/";
export const urlCrearSolicitudCambio = id => urlComiteCambioAPI(id) + "crearSolicitud/";
export const urlEjercerVoto = id => urlComiteCambioAPI(id) + "ejercerVoto/";
export const urlReporteSolicitud = id => urlComiteCambioAPI(id) + "cantidadSolicitud/";

//Item
export const urlItemAPI = id => "/proyecto/" + id + "/item/";
export const urlObtenerItemsProyecto = id => urlItemAPI(id) + "obtenerItemsProyecto/";
export const urlCrearItemProyecto = id => urlItemAPI(id) + "crearItem/";
export const urlModificarItemProyecto = id => urlItemAPI(id) + "modificarItem/";
export const urlDesactivarItem = id => urlItemAPI(id) + 'desactivarItem/';
export const urlRelacionarItem = id => urlItemAPI(id) + 'relacionarItem/';
export const urlDesrelacionarItem = id => urlItemAPI(id) + 'desrelacionarItem/';
export const urlRestaurarItem = id => urlItemAPI(id) + 'restaurarItem/';
export const urlSolicitarSubida = id => urlItemAPI(id) + 'solicitarSubidaArchivo/';
export const urlSolicitarAprobarItem = id => urlItemAPI(id) + 'solicitudAprobacionItem/';
export const urlCancelarSolicitudAprobarItem = id => urlItemAPI(id) + 'cancelarsolicitudAprobacionItem/';
export const urlAprobarItem = id => urlItemAPI(id) + 'AprobarItem/';
export const urlDesaprobarItem = id => urlItemAPI(id) + 'DesaprobarItem/';
export const urlAceptarRevision = id => urlItemAPI(id) + 'aceptarRevision/';
export const urlRevisarItem = id => urlItemAPI(id) + 'revisarItem/';
