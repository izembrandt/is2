import App from "./components/App";
import {render} from "react-dom";
import React from "react";
import {Provider} from "react-redux";
import store from "./store";
import {fetchUsuario} from "./redux/perfil";

const container = document.getElementById("app");

store.dispatch(fetchUsuario());

render(
    <Provider store={store}>
        <App/>
    </Provider>,
    container
);
