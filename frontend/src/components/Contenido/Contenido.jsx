import React from 'react';

const Contenido = ({children}) => {
    return (
        <div id="content-wrapper" className="d-flex flex-column">
            <div id="content">
                {children}
            </div>
        </div>
    );
}

export default Contenido;