import React from 'react';

const ContenedorFluido = ({children}) => (
    <div className="container-fluid">
        {children}
    </div>
)

export default ContenedorFluido;