import React, {Component} from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";


import MenuPrincipal from "../pages/menu_principal/MenuPrincipal";
import {urlMenuPrincipal, urlProyecto} from "../url";
import EsqueletoProyecto from "../pages/proyecto/EsqueletoProyecto";

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <Router>
                <Switch>
                    <Route path={urlMenuPrincipal}>
                        <MenuPrincipal/>
                    </Route>
                    <Route path={urlProyecto + ":idProyecto/"}>
                        <EsqueletoProyecto/>
                    </Route>
                    <Redirect to={urlMenuPrincipal}/> {/* Default */}
                </Switch>
            </Router>
        );
    }

}


export default App;
