import React from "react";

const SidebarToggler = () => (
    <div className="text-center d-none d-md-inline">
        <button className="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
)

export default SidebarToggler;