import React from 'react';
import {Link} from "react-router-dom";

const NavItem = ({ to="#", children, icono }) => {
    return (
        <li className="nav-item">
            <Link className="nav-link" to={to}>
                <i className={icono}></i>
                <span>{children}</span>
            </Link>
        </li>
    );
}

export default NavItem;