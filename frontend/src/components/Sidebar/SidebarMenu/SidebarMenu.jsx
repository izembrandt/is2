import React from 'react';

const SidebarMenu = ({ children }) => (
    <ul className='navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled' id="accordionSidebar">
        {children}
    </ul>
)

export default SidebarMenu;