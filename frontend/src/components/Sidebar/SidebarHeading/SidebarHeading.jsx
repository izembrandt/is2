import React from "react";

const SidebarHeading = ({ children }) => {
    return(
        <div className="sidebar-heading">
            {children}
        </div>
    );
}

export default SidebarHeading;