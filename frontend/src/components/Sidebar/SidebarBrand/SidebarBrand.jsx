import React from "react";

const SidebarBrand = ({ icono='fas fa-project-diagram' ,href='#' }) => {
    return(
        <a className="sidebar-brand d-flex align-items-center justify-content-center" href={href}>
            <div className="sidebar-brand-icon rotate-n-15">
                <i className={icono}></i>
            </div>
            <div className="sidebar-brand-text mx-3">R3M</div>
        </a>
    );
}

export default SidebarBrand;