import React from "react";

const SidebarDivider = ({className = ""}) => {
    return (
        <hr className={"sidebar-divider " + className}/>
    );
};

export default SidebarDivider;