import React, {useState} from 'react';
import { Accordion, Nav } from "react-bootstrap";
import { v4 as uuidv4 } from 'uuid';

const NavItemMenuColapsable = ({ texto, icono, children }) => {
    const [eventKey, _] = useState(uuidv4());

    return (
        <Nav.Item>
            <Accordion.Toggle as={Nav.Link} eventKey={eventKey}>
                <i className={icono}/>
                <span>{texto}</span>
            </Accordion.Toggle>
            <Accordion.Collapse eventKey={eventKey}>
                <div className="bg-white py-2 collapse-inner rounded">
                    {children}
                </div>
            </Accordion.Collapse>
        </Nav.Item>
    );
}

export default NavItemMenuColapsable;


/*
* <Dropdown as={Nav.Item}>
            <Dropdown.Toggle as={Nav.Link}>
                <i className={icono}/>
                <span>{texto}</span>
            </Dropdown.Toggle>
            <Dropdown.Menu>
                <div className="bg-white py-2 collapse-inner rounded">
                    {children}
                </div>
            </Dropdown.Menu>
        </Dropdown>
* */