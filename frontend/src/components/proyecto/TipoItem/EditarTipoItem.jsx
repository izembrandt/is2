import React, {useEffect, useState} from "react";
import {Button, Card, Col, Form, ListGroup, Modal, OverlayTrigger, Popover, Row} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {AtributoTipoItem} from "./AtributoTipoItem";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {v4 as uuidv4} from "uuid";
import {useDispatch, useSelector} from "react-redux";
import {editarTipoItem, seleccionarEstadoTiposItem, seleccionarTipoItemPorID} from "../../../redux/proyecto/tipoItem";
import {seleccionarFasesProyecto} from "../../../redux/proyecto/fases";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import styles from "../Fases/css/estilo.module.css";

export const EditarTipoItem = ({id, idProyecto}) => {
    //States
    let dispatch = useDispatch();
    let estadoTipo = useSelector(seleccionarEstadoTiposItem);
    let tipoSeleccionado = useSelector(seleccionarTipoItemPorID(id));
    let listaFases = useSelector(seleccionarFasesProyecto);
    let [textoBoton, setTextoBoton] = useState("Editar");
    const permisos = useSelector(seleccionarPermisosProyecto);
    let { estado } = useSelector(seleccionarInfoProyecto);
    let puedeEditar = permisos.includes(PERMISOS_PROYECTO.MODIFICAR_TIPO) && !tipoSeleccionado.usado && estado !== ESTADO_PROYECTO.CANCELADO && estado !== ESTADO_PROYECTO.FINALIZADO;
    //Efecto para tipos
    useEffect(() => {
        switch (estadoTipo) {
            case ESTADO_STATE.COMPLETADO:
                setTextoBoton("Editar");
                ocultarModal();
                break;
            case ESTADO_STATE.FALLADO:
                setTextoBoton("Error.");
                break;
            case ESTADO_STATE.PENDIENTE:
                setTextoBoton("Cargando..");
                break;
            case ESTADO_STATE.VACIO:
                setTextoBoton("No disponible");
                break;
        }
    }, [dispatch, estadoTipo]);

    //Atributos
    let [atributos, setAtributos] = useState([]);
    let agregarAtributo = (_) => setAtributos(prevState => {
        let newState = [...prevState];
        newState.push(uuidv4());
        return newState;
    });
    let eliminarAtributo = uuid => setAtributos(prevState => {
        let newState = [];
        prevState.forEach(value => {
            if (value !== uuid) newState.push(value);
        })
        return newState;
    });

    //Modal
    const [modalActivo, setModalActivo] = useState(false);

    let mostrarModal = () => {
        setModalActivo(true)
        setAtributos(tipoSeleccionado.atributos.map(atributo => `${atributo.posicion}`));
    };
    let ocultarModal = () => {
        setModalActivo(false);
        setAtributos([]);
    };


    //Submit Editar
    let submitEditar = event => {
        //Prevenimos el refresco
        event.preventDefault();
        //Recogemos los datos
        let datos = new FormData(event.target);
        datos.append("pk", id);
        //Enviamos al servidor
        dispatch(editarTipoItem({
                id: idProyecto,
                datos: datos
            })
        );
    }

    let errores = [];
    if(estado === ESTADO_PROYECTO.CANCELADO){
        errores.push((<li key="proyecto_cancelado">El proyecto no debe estar en estado cancelado</li>))
    }
    if(estado === ESTADO_PROYECTO.FINALIZADO){
        errores.push((<li key="proyecto_finalizado">El proyecto no debe estar en estado finalizado</li>))
    }
    if(!permisos.includes(PERMISOS_PROYECTO.MODIFICAR_TIPO)){
        errores.push((<li key="modificar_tipo">No posee el permiso para modificar tipo de item</li>))
    }
   if(tipoSeleccionado.usado){
        errores.push((<li key="tipo_seleccionado">El tipo de item esta siendo usado por un item</li>))
    }

    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Editar Tipo de Ítem</Popover.Title>
            <Popover.Content>
                Editar tipo de item del proyecto
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede editar tipo de item</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    //Render
    return (
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeEditar ? popover_info : popover_error}>
            <a href={"#"} className={puedeEditar ? "" : styles.isDisabled} onClick={puedeEditar ? mostrarModal : ocultarModal}>Editar</a>
             </OverlayTrigger>
            <Modal show={modalActivo} onHide={ocultarModal} size="lg">
                <Modal.Header>
                    <Modal.Title>
                        Editar tipo de item
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={submitEditar}>
                    <Modal.Body>
                        <CSRFToken/>
                        {/* Nombre del tipo de item */}
                        <Form.Group>
                            <Form.Label>Nombre del tipo de item*</Form.Label>
                            <Form.Control type="text" name="nombre" maxLength="50"
                                          defaultValue={tipoSeleccionado.nombre} required/>
                        </Form.Group>
                        {/* Descripción del tipo de item */}
                        <Form.Group>
                            <Form.Label>Descripción</Form.Label>
                            <Form.Control as="textarea" name="descripcion" cols="40" rows="10"
                                          defaultValue={tipoSeleccionado.descripcion}/>
                        </Form.Group>
                        {/* Prefijo del Código */}
                        <Form.Group>
                            <Form.Label>Prefijo del código*</Form.Label>
                            <Form.Control type="text" name="prefijo" maxLength="4" required
                                          defaultValue={tipoSeleccionado.prefijo}/>
                        </Form.Group>
                        {/* Fase de proyecto */}
                        <Form.Group>
                            <Form.Label>Fase*</Form.Label>
                            <Form.Control as="select" name="fase" defaultValue={tipoSeleccionado.fase} required>
                                {   /* Opciones de fase */
                                    listaFases.map(fase => <option key={fase.id}
                                                                   value={fase.id}>{fase.numero + ". " + fase.nombre}</option>)
                                }
                            </Form.Control>
                        </Form.Group>
                        {/* Atributos de un tipo de item */}
                        <Form.Group>
                            <Card style={{width: "100%"}}>
                                <Card.Header>
                                    <Row>
                                        <Col xs={8} className="m-auto">Atributos</Col>
                                        <Col xs={4}>
                                            <Button variant="success" className="btn-icon-split"
                                                    onClick={agregarAtributo}>
                                                <span className="icon text-white-50">
                                                    <i className="fas fa-plus"></i>
                                                </span>
                                                <span className="text">Agregar atributo</span>
                                            </Button>
                                        </Col>
                                    </Row>
                                </Card.Header>
                                <ListGroup variant="flush">
                                    {/* Todos los atributos */
                                        atributos.map(uuid => {
                                            let atributo = tipoSeleccionado.atributos.find(attr => `${attr.posicion}` === uuid);
                                            return atributo !== undefined
                                                ? <AtributoTipoItem key={uuid}
                                                                    eliminar={() => eliminarAtributo(uuid)}
                                                                    defaultNombre={atributo.nombre}
                                                                    defaultTipo={atributo.tipo}
                                                                    defaultObligatorio={atributo.obligatorio ? "True" : "False"}/>
                                                : <AtributoTipoItem key={uuid}
                                                                    eliminar={() => eliminarAtributo(uuid)}/>
                                        })
                                    }
                                </ListGroup>
                            </Card>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Cancelar</Button>
                        <Button variant="primary" type="submit"
                                disabled={estadoTipo !== ESTADO_STATE.COMPLETADO}>{textoBoton}</Button>
                    </Modal.Footer>
                </Form>
            </Modal>

        </>
    );
}
