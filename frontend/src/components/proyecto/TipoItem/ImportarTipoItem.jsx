import React, {useState} from "react";
import {Button, Modal, Form, Col, Popover, OverlayTrigger} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {useDispatch, useSelector} from "react-redux";
import {
    fetchTiposItem,
    importarTipoItem,
    seleccionarEstadoTiposItem,
    seleccionarImportables
} from "../../../redux/proyecto/tipoItem";
import {seleccionarFasesProyecto} from "../../../redux/proyecto/fases";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";

export const ImportarTipoItem = ({idProyecto}) => {
    let [modalActivo, setModalActivo] = useState(false);
    let [textoBoton, setTextoBoton] = useState("Importar");
    let estadoTipo = useSelector(seleccionarEstadoTiposItem);
    let tipos_importables = useSelector(seleccionarImportables);
    let fases = useSelector(seleccionarFasesProyecto);
    let dispatch = useDispatch();
    let mostrarModal = () => setModalActivo(true);
    let ocultarModal = () => setModalActivo(false);
    const permisos = useSelector(seleccionarPermisosProyecto);
    let { estado } = useSelector(seleccionarInfoProyecto);
    let puedeImportar = permisos.includes(PERMISOS_PROYECTO.CREAR_TIPO) && (estado !== ESTADO_PROYECTO.CANCELADO && estado !== ESTADO_PROYECTO.FINALIZADO);

    let submitImportar = evento => {
        //Prevenir actualizar
        evento.preventDefault();
        //Recoger datos
        let datos = new FormData(evento.target);
        //Despachar
        dispatch(importarTipoItem({
                id: idProyecto,
                datos: datos
            })
        )
            .then( () => dispatch(fetchTiposItem({id: idProyecto})))
            .then(ocultarModal);
    }
    let errores = [];
    if(estado === ESTADO_PROYECTO.CANCELADO){
        errores.push((<li key="proyecto_cancelado">El proyecto no debe estar en estado cancelado</li>))
    }
    if(estado === ESTADO_PROYECTO.FINALIZADO){
        errores.push((<li key="proyecto_finalizado">El proyecto no debe estar en estado finalizado</li>))
    }
    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Importar Tipo de Ítem</Popover.Title>
            <Popover.Content>
                Importar tipo de item del proyecto
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede importar tipo de item</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    //Render

    return (
        <Col xs={12} xl={2} lg={3} md={4}>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeImportar ? popover_info : popover_error}>
            <Button variant={puedeImportar ? "primary" : "secondary"} onClick={puedeImportar ? mostrarModal : ocultarModal}>Importar</Button>
             </OverlayTrigger>
            <Modal show={modalActivo} onHide={ocultarModal} size="lg">
                <Modal.Header>
                    <Modal.Title>
                        Importar tipo de item
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={submitImportar}>
                    <Modal.Body>
                        <CSRFToken/>
                        <Form.Group>
                            <Form.Label>Tipo de item a importar*</Form.Label>
                            <Form.Control as="select" name="tipo_de_item_a_importar" required>
                                {
                                    tipos_importables.map(tipo => <option
                                        key={tipo.id} value={tipo.id}>{tipo.prefijo + ". " + tipo.nombre}</option>)
                                }
                            </Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Fase*</Form.Label>
                            <Form.Control as="select" name="fase" required>
                                {
                                    fases.map(fase => <option key={fase.id}
                                                              value={fase.id}>{fase.numero + ". " + fase.nombre}</option>)
                                }
                            </Form.Control>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Cancelar</Button>
                        <Button variant="primary" type="submit"
                                disabled={estadoTipo !== ESTADO_STATE.COMPLETADO}>{textoBoton}</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </Col>
    );
}