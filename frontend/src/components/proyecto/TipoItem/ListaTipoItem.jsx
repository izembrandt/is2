import React from "react";

//Tablas
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';

//React Bootstrap
import {Card} from "react-bootstrap";
import {useSelector} from "react-redux";
import {seleccionarTiposItem} from "../../../redux/proyecto/tipoItem";
import {EditarTipoItem} from "./EditarTipoItem";
import {EliminarTipoItem} from "./EliminarTipoItem";
import {VerTipoItem} from "./VerTipoItem";

const AccionesTipo = ({row, idProyecto}) => {
    return (
        <>
            {/*Boton Ver Tipo Item*/}
                <VerTipoItem id={row.id}/>
                <a > / </a>
            {/*Boton editar Tipo Item*/}
                <EditarTipoItem id={row.id} idProyecto={idProyecto}/>
                <a > / </a>
            {/*Boton eliminar tipo de item*/}
                <EliminarTipoItem id={row.id} idProyecto={idProyecto}/>
        </>
    );
}

export const ListaTipoItem = ({idProyecto}) => {
    let Columnas = [
        {
            text: 'Nombre',
            dataField: 'nombre',
            sort: true,
        },
        {
            text: 'Prefijo',
            dataField: 'prefijo',

        },
        {
            text: 'Acciones',
            dataField: 'id',
            formatter: (col, row) => <AccionesTipo row={row} idProyecto={idProyecto}/>,
        },
    ]

    let datos = useSelector(seleccionarTiposItem);


    return (
        <Card className="shadow mb-4">
            <Card.Header className="py-3 container-fluid">
                <div className="row">
                    <div className="col-12 col-xl-10 col-lg-9 col-md-8 my-auto">
                        <h6 className="m-0 font-weight-bold text-primary">Lista de tipos de item</h6>
                    </div>
                </div>
            </Card.Header>
            <Card.Body>
                <BootstrapTable
                    keyField="id"
                    data={datos}
                    columns={Columnas}
                    noDataIndication="No hay tipos de item creados"
                    pagination={paginationFactory()}
                />
            </Card.Body>
        </Card>
    );
};
