import React, {useEffect, useState} from "react";
import {Button, Card, Col, Form, ListGroup, Modal, OverlayTrigger, Popover, Row} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {AtributoDisabledTipoItem, AtributoTipoItem} from "./AtributoTipoItem";
import {useSelector} from "react-redux";
import {seleccionarTipoItemPorID} from "../../../redux/proyecto/tipoItem";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import {seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import styles from "../Fases/css/estilo.module.css";
import {seleccionarFasePorID} from "../../../redux/proyecto/fases";

export const VerTipoItem = ({id}) => {
    //States
    let tipoSeleccionado = useSelector(seleccionarTipoItemPorID(id));
    const permisos = useSelector(seleccionarPermisosProyecto);
    let puedeVer = permisos.includes(PERMISOS_PROYECTO.VER_TIPO);
    let faseTipoItem = useSelector(seleccionarFasePorID(tipoSeleccionado.fase));
    //Atributos
    let [atributos, setAtributos] = useState([]);

    //Modal
    const [modalActivo, setModalActivo] = useState(false);

    let mostrarModal = () => {
        setModalActivo(true)
        setAtributos(tipoSeleccionado.atributos.map(atributo => `${atributo.posicion}`));
    };
    let ocultarModal = () => {
        setModalActivo(false);
        setAtributos([]);
    };

    let nada = () => {};
    let errores = [];
    if(!permisos.includes(PERMISOS_PROYECTO.VER_TIPO)){
        errores.push((<li key="modificar_tipo">No posee el permiso para modificar tipo de item</li>))
    }
    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Ver Tipo de Ítem</Popover.Title>
            <Popover.Content>
                Ver los datos del tipo de item
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede ver el tipo de item</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )
    return (
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeVer ? popover_info : popover_error}>
            <a href={"#"} className={puedeVer ? "" : styles.isDisabled} onClick={puedeVer ? mostrarModal : nada}>Ver</a>
            </OverlayTrigger>
            <Modal show={modalActivo} onHide={ocultarModal} size="lg">
                <Modal.Header>
                    <Modal.Title>
                        Ver tipo de item
                    </Modal.Title>
                </Modal.Header>
                <Form>
                    <Modal.Body>
                        <CSRFToken/>
                        {/* Nombre del tipo de item */}
                        <Form.Group>
                            <Form.Label>Nombre del tipo de item*</Form.Label>
                            <Form.Control type="text" name="nombre" maxLength="50"
                                          defaultValue={tipoSeleccionado.nombre} required readOnly/>
                        </Form.Group>
                        {/* Descripción del tipo de item */}
                        <Form.Group>
                            <Form.Label>Descripción</Form.Label>
                            <Form.Control as="textarea" name="descripcion" cols="40" rows="10"
                                          defaultValue={tipoSeleccionado.descripcion} readOnly/>
                        </Form.Group>
                        {/* Prefijo del Código */}
                        <Form.Group>
                            <Form.Label>Prefijo del código*</Form.Label>
                            <Form.Control type="text" name="prefijo" maxLength="4" required
                                          defaultValue={tipoSeleccionado.prefijo} readOnly/>
                        </Form.Group>
                        {/* Fase de proyecto */}
                        <Form.Group>
                            <Form.Label>Fase*</Form.Label>
                            <Form.Control type="text" name="fase" defaultValue={
                               faseTipoItem.numero  + ". " + faseTipoItem.nombre} required readOnly>
                            </Form.Control>
                        </Form.Group>
                        {/* Atributos de un tipo de item */}
                        <Form.Group>
                            <Card style={{width: "100%"}}>
                                <Card.Header>
                                    <Row>
                                        <Col xs={12} className="m-auto">Atributos</Col>
                                    </Row>
                                </Card.Header>
                                <ListGroup variant="flush">
                                    {/* Todos los atributos */
                                        atributos.map(uuid => {
                                            let atributo = tipoSeleccionado.atributos.find(attr => `${attr.posicion}` === uuid);
                                            return atributo !== undefined
                                                ? <AtributoDisabledTipoItem key={uuid}
                                                                    defaultNombre={atributo.nombre}
                                                                    defaultTipo={atributo.tipo}
                                                                    defaultObligatorio={atributo.obligatorio ? "True" : "False"}/>
                                                : <AtributoDisabledTipoItem key={uuid}/>
                                        })
                                    }
                                </ListGroup>
                            </Card>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Salir</Button>
                    </Modal.Footer>
                </Form>
            </Modal>

        </>
    );
}
