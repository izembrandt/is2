import React from "react";
import {Col, Form, Row, ListGroup, Button} from "react-bootstrap";
import {TIPO_ATRIBUTO} from "../../../constantes/tipos_de_atributo";

export const AtributoTipoItem = ({eliminar, defaultNombre = "", defaultTipo = TIPO_ATRIBUTO.TEXTO, defaultObligatorio = "False" }) => {
    return (
        <ListGroup.Item>
            {/* Nombre del Atributo */}
            <Form.Group>
                <Form.Label>Nombre de atributo*</Form.Label>
                <Form.Control type="text" name="atributos[nombre]" maxLength="50" defaultValue={defaultNombre} required/>
            </Form.Group>
            {/* Tipo de Atributo */}
            <Form.Group>
                <Form.Label>Tipo*</Form.Label>
                <Form.Control as="select" name="atributos[tipo]" defaultValue={defaultTipo}>
                    <option value={TIPO_ATRIBUTO.TEXTO}>Texto</option>
                    <option value={TIPO_ATRIBUTO.FECHA}>Fecha</option>
                    <option value={TIPO_ATRIBUTO.NUMERO}>Numero</option>
                    <option value={TIPO_ATRIBUTO.ARCHIVO}>Archivo</option>
                </Form.Control>
            </Form.Group>
            {/* Obligatoriedad */}
            <Form.Group as={Row} className="my-4">
                <Form.Label column sm={6}>Obligatorio*</Form.Label>
                <Col sm={6}>
                    <Form.Control as="select" required name="atributos[obligatorio]" defaultValue={defaultObligatorio}>
                        <option value="False">NO</option>
                        <option value="True">SI</option>
                    </Form.Control>
                </Col>
            </Form.Group>
            {/* Eliminar Atributo */}
            <Button variant="danger" className="btn-icon-split">
                <span className="icon text-white-50">
                    <i className="fas fa-trash"/>
                </span>
                <span className="text" onClick={eliminar}>Eliminar atributo</span>
            </Button>
        </ListGroup.Item>
    );
}


export const AtributoDisabledTipoItem = ({defaultNombre = "", defaultTipo = TIPO_ATRIBUTO.TEXTO, defaultObligatorio = "False" }) => {
    return (
        <ListGroup.Item>
            {/* Nombre del Atributo */}
            <Form.Group>
                <Form.Label>Nombre de atributo*</Form.Label>
                <Form.Control type="text" name="atributos[nombre]" maxLength="50" defaultValue={defaultNombre} required disabled/>
            </Form.Group>
            {/* Tipo de Atributo */}
            <Form.Group>
                <Form.Label>Tipo*</Form.Label>
                <Form.Control as="select" name="atributos[tipo]" defaultValue={defaultTipo} disabled>
                    <option value={TIPO_ATRIBUTO.TEXTO}>Texto</option>
                    <option value={TIPO_ATRIBUTO.FECHA}>Fecha</option>
                    <option value={TIPO_ATRIBUTO.NUMERO}>Numero</option>
                    <option value={TIPO_ATRIBUTO.ARCHIVO}>Archivo</option>
                </Form.Control>
            </Form.Group>
            {/* Obligatoriedad */}
            <Form.Group as={Row} className="my-4">
                <Form.Label column sm={6}>Obligatorio*</Form.Label>
                <Col sm={6}>
                    <Form.Control as="select" required name="atributos[obligatorio]" defaultValue={defaultObligatorio} disabled>
                        <option value="False">NO</option>
                        <option value="True">SI</option>
                    </Form.Control>
                </Col>
            </Form.Group>
        </ListGroup.Item>
    );
}