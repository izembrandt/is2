import React, {useEffect, useState} from "react";
import {Button, Col, Form, Modal, Card, Row, ListGroup, Popover, OverlayTrigger} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {useDispatch, useSelector} from "react-redux";
import {crearTipoItem, seleccionarEstadoTiposItem} from "../../../redux/proyecto/tipoItem";
import {fetchFasesProyecto, seleccionarFasesProyecto, vaciarFases} from "../../../redux/proyecto/fases";
import { v4 as uuidv4 } from 'uuid';
import {AtributoTipoItem} from "./AtributoTipoItem";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";

export const AgregarTipoItem = ({idProyecto}) => {
    //States
    let dispatch = useDispatch();
    let estadoTipo = useSelector(seleccionarEstadoTiposItem);
    let [textoBoton, setTextoBoton] = useState("Crear");
    const permisos = useSelector(seleccionarPermisosProyecto);
    let { estado } = useSelector(seleccionarInfoProyecto);
    let puedeCrear = permisos.includes(PERMISOS_PROYECTO.CREAR_TIPO) && estado !== ESTADO_PROYECTO.CANCELADO && estado !== ESTADO_PROYECTO.FINALIZADO;
    //Efecto para tipos
    useEffect(() => {
        switch (estadoTipo) {
            case ESTADO_STATE.COMPLETADO:
                setTextoBoton("Crear");
                ocultarModal();
                break;
            case ESTADO_STATE.FALLADO:
                setTextoBoton("Error.");
                break;
            case ESTADO_STATE.PENDIENTE:
                setTextoBoton("Cargando..");
                break;
            case ESTADO_STATE.VACIO:
                setTextoBoton("No disponible");
                break;
        }
    }, [dispatch, estadoTipo]);

    //Efecto para fases
    let estadoFase = useSelector(state => state.fases.estado);
    let listaFases = useSelector(seleccionarFasesProyecto);
    useEffect(() => {
        switch (estadoFase) {
            case ESTADO_STATE.VACIO:
                dispatch(fetchFasesProyecto(idProyecto));
                break;
            case ESTADO_STATE.COMPLETADO:
                return () => dispatch(vaciarFases());
                break;
        }
    }, [dispatch, listaFases]);

    //Atributos
    let [atributos, setAtributos] = useState([]);
    let agregarAtributo = (_) => setAtributos(prevState => {
        let newState = [...prevState];
        newState.push(uuidv4());
        return newState;
    });
    let eliminarAtributo = uuid => setAtributos(prevState => {
        let newState = [];
        prevState.forEach(value => {
            if (value !== uuid) newState.push(value);
        })
        return newState;
    });


    //Modal
    const [modalActivo, setModalActivo] = useState(false);

    let mostrarModal = () => setModalActivo(true);
    let ocultarModal = () => {
        setModalActivo(false);
        setAtributos([]);
    };

    //Submit
    let submitAgregar = (event) => {
        //Prevenimos la recarga
        event.preventDefault();
        //Recogemos los datos
        let datos = new FormData(event.target);
        //Hacemos dispatch
        dispatch(crearTipoItem({datos: datos, id: idProyecto}));
    }


    let errores = [];
    if(estado === ESTADO_PROYECTO.CANCELADO){
        errores.push((<li key="proyecto_cancelado">El proyecto no debe estar en estado cancelado</li>))
    }
    if(estado === ESTADO_PROYECTO.FINALIZADO){
        errores.push((<li key="proyecto_finalizado">El proyecto no debe estar en estado finalizado</li>))
    }
    if(!permisos.includes(PERMISOS_PROYECTO.CREAR_TIPO)){
        errores.push((<li key="crear_tipo">No posee el permiso para crear tipo de item</li>))
    }

    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Agregar Tipo de Ítem</Popover.Title>
            <Popover.Content>
                Agregar tipo de item al proyecto
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede agregar tipo de item</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    //Render
    return (
        <Col xs={12} xl={2} lg={3} md={4} className="mb-4 m-md-auto align-items-center">
           <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeCrear ? popover_info : popover_error}>
            <Button variant={ puedeCrear ? "primary" : "secondary" } onClick={puedeCrear ? mostrarModal : ocultarModal}>Crear Tipo</Button>
            </OverlayTrigger>
            <Modal show={modalActivo} onHide={ocultarModal} size="lg">
                <Modal.Header>
                    <Modal.Title>
                        Crear nuevo tipo de item
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={submitAgregar}>
                    <Modal.Body>
                        <CSRFToken/>
                        {/* Nombre del tipo de item */}
                        <Form.Group>
                            <Form.Label>Nombre del tipo de item*</Form.Label>
                            <Form.Control type="text" name="nombre" maxLength="50" required/>
                        </Form.Group>
                        {/* Descripción del tipo de item */}
                        <Form.Group>
                            <Form.Label>Descripción</Form.Label>
                            <Form.Control as="textarea" name="descripcion" cols="40" rows="10"/>
                        </Form.Group>
                        {/* Prefijo del Código */}
                        <Form.Group>
                            <Form.Label>Prefijo del código*</Form.Label>
                            <Form.Control type="text" name="prefijo" maxLength="4" required/>
                        </Form.Group>
                        {/* Fase de proyecto */}
                        <Form.Group>
                            <Form.Label>Fase*</Form.Label>
                            <Form.Control as="select" name="fase" defaultValue="" required>
                                {   /* Opciones de fase */
                                    listaFases.map(fase => <option key={fase.id}
                                                                   value={fase.id}>{fase.numero + ". " + fase.nombre}</option>)
                                }
                            </Form.Control>
                        </Form.Group>
                        {/* Atributos de un tipo de item */}
                        <Form.Group>
                            <Card style={{width: "100%"}}>
                                <Card.Header>
                                    <Row>
                                        <Col xs={8} className="m-auto">Atributos</Col>
                                        <Col xs={4}>
                                            <Button variant="success" className="btn-icon-split" onClick={agregarAtributo}>
                                                <span className="icon text-white-50">
                                                    <i className="fas fa-plus"></i>
                                                </span>
                                                <span className="text">Agregar atributo</span>
                                            </Button>
                                        </Col>
                                    </Row>
                                </Card.Header>
                                <ListGroup variant="flush">
                                    {/* Todos los atributos */
                                        atributos.map(uuid => <AtributoTipoItem key={uuid} eliminar={()=>eliminarAtributo(uuid)} />)
                                    }
                                </ListGroup>
                            </Card>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Cancelar</Button>
                        <Button variant="primary" type="submit"
                                disabled={estadoTipo !== ESTADO_STATE.COMPLETADO}>{textoBoton}</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </Col>
    );
}