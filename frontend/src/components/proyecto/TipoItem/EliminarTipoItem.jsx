import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Form, Modal, OverlayTrigger, Popover} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {eliminarTipoItem, seleccionarEstadoTiposItem, seleccionarTipoItemPorID} from "../../../redux/proyecto/tipoItem";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import styles from "../Fases/css/estilo.module.css";

export const EliminarTipoItem = ({id, idProyecto}) => {
    let dispatch = useDispatch();
    let estadoTipos = useSelector(seleccionarEstadoTiposItem);
    let tipoSeleccionado = useSelector(seleccionarTipoItemPorID(id));
    const [modalActivo, setModalActivo] = useState(false);
    const [textoBoton, setTextoBoton] = useState("Eliminar");
    const permisos = useSelector(seleccionarPermisosProyecto);
    let { estado } = useSelector(seleccionarInfoProyecto);
    let puedeEliminar = permisos.includes(PERMISOS_PROYECTO.ELIMINAR_TIPO) && !tipoSeleccionado.usado && estado !== ESTADO_PROYECTO.CANCELADO && estado !== ESTADO_PROYECTO.FINALIZADO;

    let mostrarModal = () => setModalActivo(true);
    let ocultarModal = () => setModalActivo(false);

    let eliminarRolEvento = (event) => {
        //Prevenimos que se refresque la pagina
        event.preventDefault();
        //Recogemos los datos (CSRF)
        let data = new FormData(event.target);
        data.append("pk", id);
        //Enviar al servidor la solicitud
        dispatch(eliminarTipoItem({
                id: idProyecto,
                datos: data
            })
        );
    }

    useEffect(() => {
        switch (estadoTipos) {
            case ESTADO_STATE.COMPLETADO:
                setTextoBoton("Eliminar");
                ocultarModal();
                break;
            case ESTADO_STATE.VACIO:
                setTextoBoton("No disponible");
                break;
            case ESTADO_STATE.FALLADO:
                setTextoBoton("Error.");
                break;
            case ESTADO_STATE.PENDIENTE:
                setTextoBoton("Cargando..");
                break;
        }
    }, [dispatch, estadoTipos]);

    let errores = [];
    if(estado === ESTADO_PROYECTO.CANCELADO){
        errores.push((<li key="proyecto_cancelado">El proyecto no debe estar en estado cancelado</li>))
    }
    if(estado === ESTADO_PROYECTO.FINALIZADO){
        errores.push((<li key="proyecto_finalizado">El proyecto no debe estar en estado finalizado</li>))
    }
    if(!permisos.includes(PERMISOS_PROYECTO.ELIMINAR_TIPO)){
        errores.push((<li key="eliminar_tipo">No posee el permiso para eliminar el tipo de item</li>))
    }
    if(tipoSeleccionado.usado){
        errores.push((<li key="tipo_seleccionado">El tipo de item esta siendo usado por un item</li>))
    }
    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Eliminar Tipo de Ítem</Popover.Title>
            <Popover.Content>
                Eliminar tipo de item del proyecto
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede eliminar tipo de item</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    //Render

    return (
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeEliminar ? popover_info : popover_error}>
            <a href={"#"} className={puedeEliminar ? "" : styles.isDisabled} onClick={puedeEliminar ? mostrarModal : ocultarModal}>Eliminar</a>
            </OverlayTrigger>
            <Modal show={modalActivo} onHide={ocultarModal}>
                <Modal.Header>
                    <Modal.Title>
                        Eliminar {tipoSeleccionado.nombre}
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={eliminarRolEvento}>
                    <CSRFToken/>
                    <Modal.Body>
                        Estas seguro de eliminar el tipo de item "{tipoSeleccionado.nombre}" del proyecto?
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Salir</Button>
                        <Button variant="danger" type="submit"
                                disabled={estadoTipos !== ESTADO_STATE.COMPLETADO}>{textoBoton}</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}