import React from "react";
import {ESTADO_LINEA} from "../../../constantes/estados";
import {useDispatch} from "react-redux";
import {abrirModalLB} from "../../../redux/proyecto/modales";
import {Badge} from "react-bootstrap";

const estado = {
    [ESTADO_LINEA.ABIERTO]: <Badge pill variant="success">Abierto</Badge>,
    [ESTADO_LINEA.CERRADO]: <Badge pill variant="primary">Cerrado</Badge>,
    [ESTADO_LINEA.ROTO]: <Badge pill variant="danger">Roto</Badge>
}

export const LineaBase = ({lineaBase}) => {
    let dispatch = useDispatch();
    const abrirInterfaz = () => {
        dispatch(abrirModalLB({linea: lineaBase.uuid}));
    };

    return (
        <>
            <li className="p-2" onClick={abrirInterfaz}>
                <div className="float-left"><b>Nº {lineaBase.numero}</b></div>
                <div className="float-right">
                    {estado[lineaBase.estado]}
                </div>
                <br/>
                <div className="overflow-auto">{lineaBase.nombre}</div>
            </li>
        </>
    );
}