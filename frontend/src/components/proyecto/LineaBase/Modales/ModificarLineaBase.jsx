import React, {useState} from "react";
import {Modal, Form, Button} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {abrirModal, seleccionarLineaAbierta} from "../../../../redux/proyecto/modales";
import CSRFToken from "../../../CSRFToken";
import {modificarLineaBase, seleccionarLineaBasePorUUID} from "../../../../redux/proyecto/lineasBase";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";
import {INTERFAZ_MODAL} from "../../../../constantes/modales";
import {TwitterPicker} from "react-color";
import {coloresLineaBaseDefault, hallarColorTexto} from "../../../../utils/colores";

const popover = {
    position: 'absolute',
}

const cover = {
    position: 'fixed',
    top: '0px',
    right: '0px',
    bottom: '0px',
    left: '0px',
}

export const ModificarLineaBase = () => {
    let dispatch = useDispatch();
    let proyecto = useSelector(seleccionarInfoProyecto);
    let linea_base_uuid = useSelector(seleccionarLineaAbierta);
    let linea_base = useSelector(seleccionarLineaBasePorUUID(linea_base_uuid));

    //STATES
    let [mostrarColorPicker, setMostrarColorPicker] = useState(false);
    let [colorSeleccionado, setColorSeleccionado] = useState(linea_base.color);

    //Funciones
    const cerrarModal = () => {
        dispatch(abrirModal(INTERFAZ_MODAL.LINEA_BASE));
    };
    const abrirPicker = () => {
        setMostrarColorPicker(true);
    };
    const cerrarPicker = () => {
        setMostrarColorPicker(false);
    };

    let submit = event => {
        event.preventDefault();
        //recogemos datos
        let datos = new FormData(event.target);
        datos.append("fase", linea_base.fase);
        datos.append("linea_base", linea_base.uuid);
        datos.append("color", colorSeleccionado.toString());
        //despachamos
        dispatch(modificarLineaBase({id: proyecto.id, datos}))
            .then(cerrarModal);
    }
    return (
        <>
            <style type="text/css">
                {`
                    .btn-color {
                        background-color: ${colorSeleccionado};
                        color: ${hallarColorTexto(colorSeleccionado)};
                    }   
                `}
            </style>
            <Modal.Header>
                <Modal.Title>Modificar "{linea_base.nombre}"</Modal.Title>
            </Modal.Header>
            <form onSubmit={submit}>
                <Modal.Body>
                    <CSRFToken/>
                    {/* Nombre */}
                    <Form.Group>
                        <Form.Label>Nombre de la línea base*</Form.Label>
                        <Form.Control type="text" name="nombre" maxLength="100" defaultValue={linea_base.nombre}
                                      required/>
                    </Form.Group>
                    {/* Descripción */}
                    <Form.Group>
                        <Form.Label>Descripción de la línea base</Form.Label>
                        <Form.Control as="textarea" name="descripcion" defaultValue={linea_base.descripcion} cols="40"
                                      rows="10"/>
                    </Form.Group>
                    {/* Color */}
                    <Form.Group>
                        <Form.Label>Color de la línea base</Form.Label>
                        <Button variant="color" block onClick={abrirPicker}>{colorSeleccionado}</Button>
                        {mostrarColorPicker
                            ?
                            <div style={popover}>
                                <div style={cover} onClick={cerrarPicker}/>
                                <TwitterPicker color={colorSeleccionado}
                                               onChange={color => setColorSeleccionado(color.hex.toUpperCase())}
                                               colors={ coloresLineaBaseDefault }
                                />
                            </div>
                            : null
                        }
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={cerrarModal}>Cancelar</Button>
                    <Button variant="primary" type="submit">Modificar</Button>
                </Modal.Footer>
            </form>
        </>
    );
}