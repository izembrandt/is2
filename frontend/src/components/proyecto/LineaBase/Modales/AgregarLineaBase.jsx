import React, {useEffect, useState} from "react";
import {Modal, Form, Button, Col, Row, OverlayTrigger, Popover} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {seleccionarFasePorID} from "../../../../redux/proyecto/fases";
import {seleccionarFaseAbierta, cerrarTodo} from "../../../../redux/proyecto/modales";
import CSRFToken from "../../../CSRFToken";
import {crearLineaBase, seleccionarItemsCandidatosLBPorFaseID} from "../../../../redux/proyecto/lineasBase";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";
import {coloresLineaBaseDefault, hallarColorTexto} from "../../../../utils/colores";
import {TwitterPicker} from "react-color";
import {ItemGenerico} from "../ItemGenerico";
import {fetchItemsProyecto} from "../../../../redux/proyecto/items";

const popover = {
    position: 'absolute',
}

const cover = {
    position: 'fixed',
    top: '0px',
    right: '0px',
    bottom: '0px',
    left: '0px',
}

export const AgregarLineaBase = () => {
    let dispatch = useDispatch();
    let proyecto = useSelector(seleccionarInfoProyecto);
    let fase_id = useSelector(seleccionarFaseAbierta);
    let fase = useSelector(seleccionarFasePorID(fase_id));
    let itemsPosibles = useSelector(seleccionarItemsCandidatosLBPorFaseID(fase_id))

    //STATES
    let [mostrarColorPicker, setMostrarColorPicker] = useState(false);
    let [colorSeleccionado, setColorSeleccionado] = useState(coloresLineaBaseDefault[0]);
    let [itemsLinea, setItemsLinea] = useState([]);
    let [itemsCandidatos, setItemsCandidato] = useState([]);

    //FUNCIONES
    const cerrarModal = () => {
        dispatch(cerrarTodo())
    };
    const abrirPicker = () => {
        setMostrarColorPicker(true);
    };
    const cerrarPicker = () => {
        setMostrarColorPicker(false);
    };

    const meterEnLinea = uuid => () => {
        //Primero buscamos el item que se va a meter
        const item_moviendose = itemsCandidatos.find(item => item.uuid === uuid);
        //Removemos de los candidatos
        setItemsCandidato(prevState => {
            return prevState.filter(item => item.uuid !== uuid);
        });
        //Agregamos a los items en linea base
        setItemsLinea(prevState => {
            let nuevoState = JSON.parse(JSON.stringify(prevState));
            nuevoState.push(item_moviendose);
            return nuevoState;
        });
    };

    const sacarDeLinea = uuid => () => {
        //Primero buscamos el item que se va a meter
        const item_moviendose = itemsLinea.find(item => item.uuid === uuid);
        //Removemos de los candidatos
        setItemsLinea(prevState => {
            return prevState.filter(item => item.uuid !== uuid);
        });
        //Agregamos a los items en linea base
        setItemsCandidato(prevState => {
            let nuevoState = JSON.parse(JSON.stringify(prevState));
            nuevoState.push(item_moviendose);
            return nuevoState;
        });
    };

    let submit = event => {
        event.preventDefault();
        //recogemos datos
        let datos = new FormData(event.target);
        datos.append("fase", fase_id);
        datos.append('fase_id', fase_id);
        datos.append("color", colorSeleccionado.toString());
        itemsLinea.forEach( item => datos.append("items", item.uuid))
        //despachamos
        dispatch(crearLineaBase({id: proyecto.id, datos}))
            .then(() => dispatch(fetchItemsProyecto({ id: proyecto.id})))
            .then(cerrarModal);
    }

    useEffect(() => {
        setItemsCandidato(itemsPosibles);
        setItemsLinea([]);
    }, [dispatch]);

    const popover_error = (
         <Popover id="popover-basic">
            <Popover.Title as="h3">No puedes crear esta linea base</Popover.Title>
            <Popover.Content>
                La linea base debe tener al menos un elemento
            </Popover.Content>
        </Popover>
    );

    const popover_info = (
         <Popover id="popover-basic">
            <Popover.Title as="h3">Crear la linea base con los items seleccionados</Popover.Title>
        </Popover>
    );

    return (
        <>
            <style type="text/css">
                {`
                    .btn-color {
                        background-color: ${colorSeleccionado};
                        color: ${hallarColorTexto(colorSeleccionado)};
                    }   
                `}
            </style>
            <Modal.Header>
                <Modal.Title>Agregar LB a "{fase.nombre}"</Modal.Title>
            </Modal.Header>
            <form onSubmit={submit}>
                <Modal.Body >
                    <CSRFToken/>
                    {/* Nombre */}
                    <Form.Group>
                        <Form.Label>Nombre de la línea base*</Form.Label>
                        <Form.Control type="text" name="nombre" maxLength="100" required/>
                    </Form.Group>
                    {/* Color */}
                    <Form.Group>
                        <Form.Label>Color de la línea base</Form.Label>
                        <Button variant="color" block onClick={abrirPicker}>{colorSeleccionado}</Button>
                        {mostrarColorPicker
                            ?
                            <div style={popover}>
                                <div style={cover} onClick={cerrarPicker}/>
                                <TwitterPicker color={colorSeleccionado}
                                               onChange={color => setColorSeleccionado(color.hex.toUpperCase())}
                                               colors={coloresLineaBaseDefault}
                                />
                            </div>
                            : null
                        }
                    </Form.Group>
                    {/* Descripción */}
                    <Form.Group>
                        <Form.Label>Descripción de la línea base</Form.Label>
                        <Form.Control as="textarea" name="descripcion" cols="40" rows="10"/>
                    </Form.Group>
                    {/* Ítems */}
                    <Form.Group>
                        <Form.Label>Ítems de la línea base</Form.Label>
                    </Form.Group>
                    <Row style={{minHeight: "150px"}}>
                        <Col xs={6}>
                            <div className="fase-lista-de-items" style={{width: "auto"}}>
                                <header>Items aprobados</header>
                                <ul>
                                    {
                                        itemsCandidatos.map(item => <ItemGenerico key={item.uuid} item={item}
                                                                                  onClick={meterEnLinea(item.uuid)}/>)
                                    }
                                    <br/>
                                </ul>
                                <footer>Items aprobados</footer>
                            </div>
                        </Col>
                        <Col xs={6}>
                            <div className="fase-lista-de-items" style={{width: "auto"}}>
                                <header>Items en línea base</header>
                                <ul>
                                    {
                                        itemsLinea.map(item => <ItemGenerico key={item.uuid} item={item}
                                                                             onClick={sacarDeLinea(item.uuid)}/>)
                                    }
                                    <br/>
                                </ul>
                                <footer>Items en línea base</footer>
                            </div>
                        </Col>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={cerrarModal}>Cancelar</Button>
                    <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={ itemsLinea.length === 0 ? popover_error : popover_info}>
                    <Button variant={itemsLinea.length === 0 ? "secondary" : "primary"} type={itemsLinea.length  === 0 ? "button" : "submit"}>Crear</Button>
                    </OverlayTrigger>
                </Modal.Footer>
            </form>
        </>
    );
}