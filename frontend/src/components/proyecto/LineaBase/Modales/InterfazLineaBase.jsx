import React from "react";
import {Modal, Button, Row, Col} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {cerrarTodo, seleccionarLineaAbierta, abrirModal} from "../../../../redux/proyecto/modales";
import {seleccionarItemsDeLineaBaseConUUID, seleccionarLineaBasePorUUID} from "../../../../redux/proyecto/lineasBase";
import {BotonGenerico} from "../BotonGenerico";
import {ItemGenerico} from "../ItemGenerico";
import {seleccionarInfoProyecto, verificarPermisoFase} from "../../../../redux/proyecto/proyecto";
import {PERMISOS_FASE} from "../../../../constantes/permisos";
import {saveAs} from 'file-saver';
import {urlGenerarInformeLineaBase} from "../../../../url";
import {ESTADO_LINEA, ESTADO_PROYECTO} from "../../../../constantes/estados";
import {toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {ToastContainer} from "react-toastify";
import Contenido from "../../../Contenido/Contenido";

export const InterfazLineaBase = () => {
    const proyecto = useSelector(seleccionarInfoProyecto);
    let dispatch = useDispatch();
    let linea_base_uuid = useSelector(seleccionarLineaAbierta);
    let linea_base = useSelector(seleccionarLineaBasePorUUID(linea_base_uuid));
    let items = useSelector(seleccionarItemsDeLineaBaseConUUID(linea_base_uuid));
    let {id} = useSelector(seleccionarInfoProyecto);


    const cerrarModal = () => {
        dispatch(cerrarTodo())
    };


    return (
        <>
            <Modal.Header>
                <ToastContainer autoClose={2000} />
                <Modal.Title>Línea Base "{linea_base.nombre}"</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Row style={{minHeight: "400px"}}>
                    {/* Lado Izquierdo */}
                    <Col xs={12} lg={8}>
                        <div className="fase-lista-de-items" style={{width: "auto"}}>
                            <header>Items</header>
                            <ul>
                                {items.length > 0
                                    ? items.map(item => <ItemGenerico onClick={() => {
                                    }} item={item} key={item.uuid}/>)
                                    : <p>No hay items</p>
                                }
                            </ul>
                            <footer>Items</footer>
                        </div>
                    </Col>
                    {/* Lado Derecho */}
                    <Col xs={12} lg={4}>
                        <Col xs={12}>

                            <h5>Informe</h5>

                            {/* Descargar informe */}
                            <BotonGenerico onClick={() => {
                                toast.success("Informe se esta descargando!")
                                saveAs(urlGenerarInformeLineaBase(id, linea_base_uuid), "informe.pdf");
                                }}
                                           variant="success"
                                           icono="fas fa-download">Descargar informe
                            </BotonGenerico>
                        </Col>
                    </Col>
                </Row>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={cerrarModal}>Salir</Button>
            </Modal.Footer>
        </>
    );
}