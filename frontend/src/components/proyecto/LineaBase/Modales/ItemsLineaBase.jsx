import React, {useEffect, useState} from "react";
import {Col, Modal, Row, Button} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {
    agregarItemLineaBase, sacarItemLineaBase,
    seleccionarItemsCandidatosLineaBaseConUUID,
    seleccionarItemsDeLineaBaseConUUID
} from "../../../../redux/proyecto/lineasBase";
import {seleccionarLineaAbierta, abrirModal} from "../../../../redux/proyecto/modales";
import {ItemGenerico} from "../ItemGenerico";
import {INTERFAZ_MODAL} from "../../../../constantes/modales";
import Cookies from "js-cookie";
import {ESTADO_ITEM} from "../../../../constantes/estados";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";
import {fetchItemsProyecto} from "../../../../redux/proyecto/items";

export const ItemsLineaBase = () => {
    let dispatch = useDispatch();
    let proyecto = useSelector(seleccionarInfoProyecto);
    let linea_base_uuid = useSelector(seleccionarLineaAbierta);

    let items_linea_inicial = useSelector(seleccionarItemsDeLineaBaseConUUID(linea_base_uuid));
    let items_candidatos_inicial = useSelector(seleccionarItemsCandidatosLineaBaseConUUID(linea_base_uuid));

    let [itemsLinea, setItemsLinea] = useState([]);
    let [itemsCandidatos, setItemsCandidato] = useState([]);

    useEffect(() => {
        setItemsCandidato(items_candidatos_inicial);
        setItemsLinea(items_linea_inicial);
    }, [dispatch]);

    const meterEnLinea = uuid => () => {
        //Primero buscamos el item que se va a meter
        const item_moviendose = itemsCandidatos.find(item => item.uuid === uuid);
        //Removemos de los candidatos
        setItemsCandidato(prevState => {
            return prevState.filter(item => item.uuid !== uuid);
        });
        //Agregamos a los items en linea base
        setItemsLinea(prevState => {
            let nuevoState = JSON.parse(JSON.stringify(prevState));
            nuevoState.push(item_moviendose);
            return nuevoState;
        });
    };

    const sacarDeLinea = uuid => () => {
        //Primero buscamos el item que se va a meter
        const item_moviendose = itemsLinea.find(item => item.uuid === uuid);
        //Removemos de los candidatos
        setItemsLinea(prevState => {
            return prevState.filter(item => item.uuid !== uuid);
        });
        //Agregamos a los items en linea base
        setItemsCandidato(prevState => {
            let nuevoState = JSON.parse(JSON.stringify(prevState));
            nuevoState.push(item_moviendose);
            return nuevoState;
        });
    };

    const volverInterfaz = () => {
        dispatch(abrirModal(INTERFAZ_MODAL.LINEA_BASE));
    };

    const aceptarCambios = event => {
        let csrftoken = Cookies.get('csrftoken');
        let agregar = false;
        let eliminar = false;
        //Primero agregamos los items aprobados a la línea base
        let datos_agregar = new FormData();
        datos_agregar.append("csrfmiddlewaretoken", csrftoken);
        datos_agregar.append("linea_base", linea_base_uuid);
        itemsLinea.forEach(item => {
            if (item.estado === ESTADO_ITEM.APROBADO) {
                datos_agregar.append("items", item.uuid);
                agregar = true;
            }

        });
        //Y eliminamos lo que ya no estan en linea base
        let datos_eliminar = new FormData();
        datos_eliminar.append("csrfmiddlewaretoken", csrftoken);
        datos_eliminar.append("linea_base", linea_base_uuid);
        itemsCandidatos.forEach(item => {
            if (item.estado === ESTADO_ITEM.EN_LINEA_BASE) {
                datos_eliminar.append("items", item.uuid);
                eliminar = true;
            }

        });
        //Despachamos
        if (agregar && eliminar) {
            dispatch(agregarItemLineaBase({id: proyecto.id, datos: datos_agregar}))
                .then(() => dispatch(sacarItemLineaBase({id: proyecto.id, datos: datos_eliminar})))
                .then(() => dispatch(fetchItemsProyecto({id: proyecto.id})))
                .then(volverInterfaz);
        } else if (!agregar && eliminar) {
            dispatch(sacarItemLineaBase({id: proyecto.id, datos: datos_eliminar}))
                .then(() => dispatch(fetchItemsProyecto({id: proyecto.id})))
                .then(volverInterfaz);
        } else if (agregar && !eliminar) {
            dispatch(agregarItemLineaBase({id: proyecto.id, datos: datos_agregar}))
                .then(() => dispatch(fetchItemsProyecto({id: proyecto.id})))
                .then(volverInterfaz);
        } else {
            dispatch(fetchItemsProyecto({id: proyecto.id}))
                .then(volverInterfaz)
        }
    }

    return (
        <>
            <Modal.Header>Modificar Items</Modal.Header>
            <Modal.Body>
                <Row style={{minHeight: "600px"}}>
                    <Col xs={6}>
                        <div className="fase-lista-de-items" style={{width: "auto"}}>
                            <header>Items aprobados</header>
                            <ul>
                                {
                                    itemsCandidatos.map(item => <ItemGenerico key={item.uuid} item={item}
                                                                              onClick={meterEnLinea(item.uuid)}/>)
                                }
                                <br/>
                            </ul>
                            <footer>Items aprobados</footer>
                        </div>
                    </Col>
                    <Col xs={6}>
                        <div className="fase-lista-de-items" style={{width: "auto"}}>
                            <header>Items en línea base</header>
                            <ul>
                                {
                                    itemsLinea.map(item => <ItemGenerico key={item.uuid} item={item}
                                                                         onClick={sacarDeLinea(item.uuid)}/>)
                                }
                                <br/>
                            </ul>
                            <footer>Items en línea base</footer>
                        </div>
                    </Col>
                </Row>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={volverInterfaz}>Cancelar</Button>
                <Button variant="primary" onClick={aceptarCambios}>Aceptar Cambios</Button>
            </Modal.Footer>
        </>
    );
}