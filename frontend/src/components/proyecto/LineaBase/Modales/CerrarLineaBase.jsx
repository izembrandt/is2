import React, {useEffect} from "react";
import {Modal, Form, Button} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import { abrirModal, seleccionarLineaAbierta} from "../../../../redux/proyecto/modales";
import CSRFToken from "../../../CSRFToken";
import {cerrarLinea, seleccionarLineaBasePorUUID} from "../../../../redux/proyecto/lineasBase";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";
import {INTERFAZ_MODAL} from "../../../../constantes/modales";
export const CerrarLineaBase = () => {
    let dispatch = useDispatch();
    let proyecto = useSelector(seleccionarInfoProyecto);
    let linea_base_uuid = useSelector(seleccionarLineaAbierta);
    let linea_base = useSelector(seleccionarLineaBasePorUUID(linea_base_uuid));

    const cerrarModal = () => {dispatch(abrirModal(INTERFAZ_MODAL.LINEA_BASE))};

    let submit = event => {
        event.preventDefault();
        //recogemos datos
        let datos = new FormData(event.target);
        datos.append("fase", linea_base.fase);
        datos.append("linea_base", linea_base.uuid);
        datos.forEach((value, key) => console.log(`${key} : ${value}`));
        //Enviar al servidor la solicitud
        dispatch(cerrarLinea({ id: proyecto.id, datos}))
            .then(cerrarModal);
    }

    return (
        <>
            <Modal.Header>
                <Modal.Title>Cerrar "{linea_base.nombre}"</Modal.Title>
            </Modal.Header>
            <form onSubmit={submit}>
                <CSRFToken/>
                <Modal.Body>
                    Estas seguro de cerrar la linea base "{linea_base.nombre}"?
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={cerrarModal}>Cancelar</Button>
                    <Button variant="primary" type="submit">Si, Cerrar</Button>
                </Modal.Footer>
            </form>
        </>
    );
}