import React from "react";
import {Modal} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {seleccionarModalAbierto, cerrarTodo, abrirModal} from "../../../redux/proyecto/modales";
import {INTERFAZ_MODAL} from "../../../constantes/modales";
import {AgregarLineaBase} from "./Modales/AgregarLineaBase";
import {InterfazLineaBase} from "./Modales/InterfazLineaBase";
import {ModificarLineaBase} from "./Modales/ModificarLineaBase";
import {CerrarLineaBase} from "./Modales/CerrarLineaBase";
import {ItemsLineaBase} from "./Modales/ItemsLineaBase";

export const ModalesLB = () => {
    let dispatch = useDispatch();
    let modalActivo = useSelector(seleccionarModalAbierto);

    let volverInicio = () => {dispatch(cerrarTodo())}

    let abrir = modal => () => dispatch(abrirModal(modal));

    return (
        <>
            <Modal show={modalActivo === INTERFAZ_MODAL.CREAR_LINEA_BASE} onHide={volverInicio} centered size="lg"
                >
                <AgregarLineaBase/>
            </Modal>
            <Modal show={modalActivo === INTERFAZ_MODAL.LINEA_BASE} onHide={volverInicio} centered size="lg" scrollable>
                <InterfazLineaBase/>
            </Modal>
            <Modal show={modalActivo === INTERFAZ_MODAL.MODIFICAR_LINEA_BASE}
                   onHide={abrir(INTERFAZ_MODAL.LINEA_BASE)} centered>
                <ModificarLineaBase/>
            </Modal>
            <Modal show={modalActivo === INTERFAZ_MODAL.CERRAR_LINEA_BASE}
                   onHide={abrir(INTERFAZ_MODAL.LINEA_BASE)} centered scrollable>
                <CerrarLineaBase/>
            </Modal>
            <Modal show={modalActivo === INTERFAZ_MODAL.AGREGAR_ITEMS_LINEA_BASE} onHide={abrir(INTERFAZ_MODAL.LINEA_BASE)}
                   centered size="lg" scrollable>
                <ItemsLineaBase/>
            </Modal>
        </>
    );
}