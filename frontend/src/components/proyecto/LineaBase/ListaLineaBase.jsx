import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {seleccionarItemsCandidatosLBPorFaseID, seleccionarLineaBasePorFaseID} from "../../../redux/proyecto/lineasBase";
import {LineaBase} from "./LineaBase";
import {setFase, abrirModal} from "../../../redux/proyecto/modales";
import {INTERFAZ_MODAL} from "../../../constantes/modales";
import {seleccionarInfoProyecto, verificarPermisoFase} from "../../../redux/proyecto/proyecto";
import {PERMISOS_FASE} from "../../../constantes/permisos";
import {ESTADO_PROYECTO} from "../../../constantes/estados";
import {OverlayTrigger, Popover} from "react-bootstrap";
import styles from "../Fases/css/estilo.module.css";
export const ListaLineaBase = ({fase}) => {
    let dispatch = useDispatch();
    let lista_lineas = useSelector(seleccionarLineaBasePorFaseID(fase.id));
    let lista_linea_ordenado = JSON.parse(JSON.stringify(lista_lineas)).sort( (a, b) => a.numero - b.numero );
    const proyecto = useSelector(seleccionarInfoProyecto);
    let itemsPosibles = useSelector(seleccionarItemsCandidatosLBPorFaseID(fase.id));

    let puedeCrear = !fase.cerrado
            && proyecto.estado === ESTADO_PROYECTO.EJECUCION
            && useSelector(verificarPermisoFase(PERMISOS_FASE.CREAR_LINEA_BASE, fase.id))
            && itemsPosibles.length > 0;


    let apretarAgregar = (event) => {
        event.preventDefault();
        dispatch(setFase(fase.id));
        dispatch(abrirModal(INTERFAZ_MODAL.CREAR_LINEA_BASE));
    }

    let nada = () => {}
    let errores = [];
    if(fase.cerrado){
        errores.push((<li key="fase_cerrada">La fase ya esta cerrada</li>))
    }
    if(proyecto.estado !== ESTADO_PROYECTO.EJECUCION){
        errores.push((<li key="proyecto_ejecucion">El proyecto no esta en estado de ejecucion</li>))
    }
    if(!useSelector(verificarPermisoFase(PERMISOS_FASE.CREAR_LINEA_BASE, fase.id))){
        errores.push((<li key="permiso_crear">No posee permisos para crear lineas base</li>))
    }
    if(itemsPosibles.length === 0){
        errores.push((<li key="tiene-items">La fase no tiene items aprobados</li>))
    }
    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Agregar Ítem</Popover.Title>
            <Popover.Content>
                Agregue ítems a esta fase del proyecto
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede agregar item</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    return (
        <div className="fase-lista-de-items">
            <header>{fase.nombre}</header>
            <ul>
                {
                    lista_linea_ordenado.map(linea => (<LineaBase key={linea.uuid} lineaBase={linea}/>))
                }
            </ul>
            <footer>
                <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeCrear? popover_info : popover_error}>
                    <a href="#" className={puedeCrear ? "" : styles.isDisabled}  onClick={puedeCrear ? apretarAgregar : nada}>
                        <span className="icon"><i className="fas fa-plus"/></span>
                        <span className="text">{" "}Agregar Linea Base</span>
                    </a>
                </OverlayTrigger>
            </footer>
        </div>
    );
}