import React from "react";
import {Button} from "react-bootstrap";

export const BotonGenerico = ({onClick, variant, disabled=false, colorTexto = "text-white-50", children, icono="fas fa-share-square"}) => {
    return (
        <Button variant={variant} className="btn-block align-items-start"
                onClick={onClick}
                disabled={disabled}>
                                <span className={"icon " + colorTexto}>
                                    <i className={icono}/>
                                </span>
            {" "}
            <span className="text">{children}</span>
        </Button>
    );
}