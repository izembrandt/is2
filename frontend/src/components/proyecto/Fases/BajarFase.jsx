import React, {useEffect, useState} from "react";
import 'react-dual-listbox/lib/react-dual-listbox.css';
import {useDispatch, useSelector} from "react-redux";
import {
    bajarFase, seleccionarFasePorID, seleccionarFasesProyecto,
} from "../../../redux/proyecto/fases";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import Cookies from "js-cookie";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import styles from "./css/estilo.module.css";
import {OverlayTrigger, Popover} from "react-bootstrap";

export const BajarFase = ({id}) => {
    let listaFase = useSelector(seleccionarFasesProyecto);
    let fase = useSelector(seleccionarFasePorID(id));
    let proyecto = useSelector(seleccionarInfoProyecto);
    let idProyecto = proyecto.id;
    let dispatch = useDispatch();
    let permisos = useSelector(seleccionarPermisosProyecto);
    let { estado } = useSelector(seleccionarInfoProyecto);
    let puedeBajar = fase.numero < listaFase.length && permisos.includes(PERMISOS_PROYECTO.MODIFICAR_FASES) && estado === ESTADO_PROYECTO.PENDIENTE;
    let sinEvento = () => {};

    let bajarFaseEvento = (event) => {
        //Prevenimos que se refresque la pagina
        event.preventDefault();
        //Recogemos los datos (CSRF)
        let data = new FormData();
        data.append("fid", id);
        data.append("csrfmiddlewaretoken", Cookies.get('csrftoken'));
        //Enviar al servidor la solicitud
        dispatch(bajarFase({
                id: idProyecto,
                data: data,
            })
        );
    }

    //Texto Boton
    const [textoBoton, setTextoBoton] = useState("Bajar")
    let estadoFases = useSelector(state => state.fases.estado);
    useEffect(() => {
        switch (estadoFases ) {
            case ESTADO_STATE.COMPLETADO:
                setTextoBoton("Bajar");
                break;
            case ESTADO_STATE.PENDIENTE:
                setTextoBoton("Cargando...");
                break;
            case ESTADO_STATE.VACIO:
                setTextoBoton("No disponible.");
                break;
            case ESTADO_STATE.FALLADO:
                setTextoBoton("Error..");
                break;
        }
    }, [estadoFases])


    let errores = [];
    if(estado !== ESTADO_PROYECTO.PENDIENTE){
        errores.push((<li key="proyecto_pendiente">El proyecto no está en estado pendiente</li>))
    }
    if(!permisos.includes(PERMISOS_PROYECTO.MODIFICAR_FASES)){
        errores.push((<li key="modificar_fases">No posee el permiso para modificar la fase</li>))
    }

    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Bajar Fase</Popover.Title>
            <Popover.Content>
                Modifica el orden de las fases
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede mover las fases</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    return (
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeBajar? popover_info : popover_error}>
            <a href={"#"} className={puedeBajar ? "" : styles.isDisabled} onClick={puedeBajar ? bajarFaseEvento : sinEvento}><i className="fas fa-chevron-down"/></a>
            </OverlayTrigger>
        </>
    );
}
