import React, {useEffect, useState} from "react";
import {Popover, OverlayTrigger, Button, Form, Modal} from "react-bootstrap";
import {
    cerrarFase,
    seleccionarFasePorID
} from "../../../redux/proyecto/fases";
import CSRFToken from "../../CSRFToken";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import styles from './css/estilo.module.css';
import {useDispatch, useSelector} from "react-redux";
import {seleccionarSolicitudesPendientesDeFaseID} from "../../../redux/proyecto/comite";

export const CerrarFase = ({id}) => {
    let dispatch = useDispatch();
    let estadoFase = useSelector(state => state.fases.estado);
    let proyecto = useSelector(seleccionarInfoProyecto);
    let idProyecto = proyecto.id;
    let fase = useSelector(seleccionarFasePorID(id));
    const [modalActivo, setModalActivo] = useState(false);
    const [textoBoton, setTextoBoton] = useState("Cerrar Fase");
    let permisos = useSelector(seleccionarPermisosProyecto);
    let {estado} = useSelector(seleccionarInfoProyecto);
    let solicitudes = useSelector(seleccionarSolicitudesPendientesDeFaseID(id));
    let puedeCerrar = permisos.includes(PERMISOS_PROYECTO.CERRAR_FASES)
        && estado === ESTADO_PROYECTO.EJECUCION
        && !fase.cerrado
        && fase.cerrable
        && solicitudes.length === 0;

    let mostrarModal = () => setModalActivo(true);
    let ocultarModal = () => setModalActivo(false);
    let nada = () => {
    };

    let errores = [];
    if (estado !== ESTADO_PROYECTO.EJECUCION) {
        errores.push((<li key="proyecto_ejecucion">El proyecto debe estar en estado ejecucion</li>));
    }
    if (!permisos.includes(PERMISOS_PROYECTO.CERRAR_FASES)) {
        errores.push((<li key="permiso_proyecto">No posee permisos para cerrar fases</li>));
    }
    if (fase.cerrado) {
        errores.push((<li key="fase_cerrada">La fase ya esta cerrada</li>));
    } else if (!fase.cerrable) {
        errores.push((<li key="fase_cerrable">La fase aun no esta lista para ser cerrada</li>));
    }
    if (solicitudes.length > 0) {
        errores.push(<li key="item_pendiente">Esta fase todavía posee solicitudes pendientes</li>);
    }
    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Cerrar {fase.nombre}</Popover.Title>
            <Popover.Content>
                Sirve para bloquear las modificaciones de todos los elementos de esta fase, no se puede revertir.
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede cerrar {fase.nombre}</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )


    let cerrarFaseEvento = (event) => {
        //Prevenimos que se refresque la pagina
        event.preventDefault();
        //Recogemos los datos (CSRF)
        let data = new FormData(event.target);
        data.append("fid", id);
        //Enviar al servidor la solicitud
        dispatch(cerrarFase({
                id: idProyecto,
                data: data
            })
        ).then(() => setModalActivo(false));
    }

    useEffect(() => {
        switch (estadoFase) {
            case ESTADO_STATE.COMPLETADO:
                setTextoBoton("Cerrar");
                ocultarModal();
                break;
            case ESTADO_STATE.VACIO:
                setTextoBoton("No disponible");
                break;
            case ESTADO_STATE.FALLADO:
                setTextoBoton("Error.");
                break;
            case ESTADO_STATE.PENDIENTE:
                setTextoBoton("Cargando..");
                break;
        }
    }, [dispatch, estadoFase]);


    return (
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto"
                            overlay={puedeCerrar ? popover_info : popover_error}>
                <a href={"#"} className={puedeCerrar ? "" : styles.isDisabled}
                   onClick={puedeCerrar ? mostrarModal : nada}>{textoBoton}</a>
            </OverlayTrigger>
            <Modal show={modalActivo} onHide={ocultarModal}>
                <Modal.Header>
                    <Modal.Title>
                        Cerrar {fase.nombre}
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={cerrarFaseEvento}>
                    <CSRFToken/>
                    <Modal.Body>
                        Estas seguro de cerrar la Fase "{fase.nombre}"?
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Salir</Button>
                        <Button variant="danger" type="submit"
                                disabled={estadoFase !== ESTADO_STATE.COMPLETADO}>{textoBoton}</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}