import React from "react";
import {EditarFase} from "../Fases/EditarFase";
import {EliminarFase} from "../Fases/EliminarFase";
import {SubirFase} from "./SubirFase";
import {BajarFase} from "../Fases/BajarFase";
import {CerrarFase} from "../Fases/CerrarFase";
import {InformeFase} from "../Fases/InformeFase";

export const AccionesFase = ({id}) => {
    return (<>
        {/*Editar Fase*/}
        <EditarFase id={id}/>
        <a > / </a>

        {/*Eliminar Fase*/}
        <EliminarFase id={id}/>
        <a > / </a>

        {/*Cerrar Fase*/}
        <CerrarFase id={id}/>

        {/*Subir Fase*/}
        <a > / </a>
        <SubirFase id={id}/>

        {/*Bajar Fase*/}
        <a > / </a>
        < BajarFase id={id}/>

        {/*Descargar informe de fase*/}
        <a > / </a>
        < InformeFase id={id}/>
    </>);
}