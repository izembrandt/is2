import {toast} from "react-toastify";
import {urlGenerarInformeFase} from "../../../url";
import React from "react";
import 'react-dual-listbox/lib/react-dual-listbox.css';
import {useSelector} from "react-redux";
import {seleccionarFasePorID} from "../../../redux/proyecto/fases";
import {seleccionarInfoProyecto} from "../../../redux/proyecto/proyecto";

export const InformeFase = ({id}) => {
    let fase = useSelector(seleccionarFasePorID(id));
    let proyecto = useSelector(seleccionarInfoProyecto);

    return (
        <>
            <a  href={"#"} onClick={() => {
                                toast.success("Informe de fase se esta descargando!")
                                saveAs(urlGenerarInformeFase(proyecto.id, fase.id), "informe.pdf");
                                }}
            >Descargar Informe</a>
        </>
    );
}

