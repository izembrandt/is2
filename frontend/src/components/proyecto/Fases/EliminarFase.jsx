import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
    eliminarFase,
    seleccionarFasePorID
} from "../../../redux/proyecto/fases";
import {Button, Form, Modal, OverlayTrigger, Popover} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import styles from "./css/estilo.module.css";
export const EliminarFase = ({id}) => {
    let dispatch = useDispatch();
    let estadoFase = useSelector(state => state.fases.estado);
    let proyecto = useSelector(seleccionarInfoProyecto);
    let idProyecto = proyecto.id;
    let fase = useSelector(seleccionarFasePorID(id));
    const [modalActivo, setModalActivo] = useState(false);
    const [textoBoton, setTextoBoton] = useState("Eliminar");
    let permisos = useSelector(seleccionarPermisosProyecto);
    let { estado } = useSelector(seleccionarInfoProyecto);
    let puedeEliminar = permisos.includes(PERMISOS_PROYECTO.ELIMINAR_FASES) && !fase.usado && estado === ESTADO_PROYECTO.PENDIENTE;

    let mostrarModal = () => setModalActivo(true);
    let ocultarModal = () => setModalActivo(false);

    let errores = [];
    if(estado !== ESTADO_PROYECTO.PENDIENTE){
        errores.push((<li key="proyecto_pendiente">El proyecto debe estar en estado pendiente</li>))
    }
    if(!permisos.includes(PERMISOS_PROYECTO.MODIFICAR_FASES)){
        errores.push((<li key="permiso_proyecto">No posees el permiso para eliminar fases</li>))
    }
    if(fase.usado) {
        errores.push((<li key="fase_usada">La fase esta asociodo a uno o varios tipos de item</li>))
    }
    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Eliminar {fase.nombre}</Popover.Title>
            <Popover.Content>
                Elimina esta fase del proyecto.
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede eliminar {fase.nombre}</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    let eliminarFaseEvento = (event) => {
        //Prevenimos que se refresque la pagina
        event.preventDefault();
        //Recogemos los datos (CSRF)
        let data = new FormData(event.target);
        data.append("id", id);
        //Enviar al servidor la solicitud
        dispatch(eliminarFase({
                id: idProyecto,
                data: data
            })
        );
    }

    useEffect(() => {
        switch (estadoFase) {
            case ESTADO_STATE.COMPLETADO:
                setTextoBoton("Eliminar");
                ocultarModal();
                break;
            case ESTADO_STATE.VACIO:
                setTextoBoton("No disponible");
                break;
            case ESTADO_STATE.FALLADO:
                setTextoBoton("Error.");
                break;
            case ESTADO_STATE.PENDIENTE:
                setTextoBoton("Cargando..");
                break;
        }
    }, [dispatch, estadoFase]);


    return (
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeEliminar ? popover_info : popover_error}>
                <a href={"#"} className={puedeEliminar ? "" : styles.isDisabled} onClick={puedeEliminar ? mostrarModal : ocultarModal}>Eliminar</a>
            </OverlayTrigger>
                <Modal show={modalActivo} onHide={ocultarModal}>
                <Modal.Header>
                    <Modal.Title>
                        Eliminar {fase.nombre}
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={eliminarFaseEvento}>
                    <CSRFToken/>
                    <Modal.Body>
                        Estas seguro de eliminar la Fase "{fase.nombre}" del proyecto?
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Salir</Button>
                        <Button variant="danger" type="submit" disabled={estadoFase !== ESTADO_STATE.COMPLETADO}>{textoBoton}</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}