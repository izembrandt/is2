import React, {useEffect, useState} from "react";
import {Button, Col, Form, Modal, OverlayTrigger, Popover} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {useDispatch, useSelector} from "react-redux";
import {
    crearFase,
} from "../../../redux/proyecto/fases";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";

const AgregarFase = ({idProyecto}) => {
    //Modal
    const [modalActivo, setModalActivo] = useState(false);
    let mostrarModal = () => setModalActivo(true);
    let ocultarModal = () => setModalActivo(false);
    let nada = () => {};
    //Effect
    let dispatch = useDispatch();

    //Texto Boton
    const [textoBoton, setTextoBoton] = useState("Crear")
    let estadoFases = useSelector(state => state.fases.estado);
    const permisos = useSelector(seleccionarPermisosProyecto);
    const proyecto = useSelector(seleccionarInfoProyecto);
    let puedeCrearFase = permisos.includes(PERMISOS_PROYECTO.CREAR_FASES) && proyecto.estado === ESTADO_PROYECTO.PENDIENTE;

    useEffect(() => {
        switch (estadoFases) {
            case ESTADO_STATE.COMPLETADO:
                setTextoBoton("Crear");
                break;
            case ESTADO_STATE.PENDIENTE:
                setTextoBoton("Cargando...");
                break;
            case ESTADO_STATE.FALLADO:
                setTextoBoton("Error..");
                break;
        }
    }, [estadoFases])

    //Cuando se haga submit
    const manejarSubmit = (event) => {
        //Paramos el evento
        event.preventDefault();
        //Recogemos los datos
        const datos = new FormData(event.target);
        //Pasamos a la función los datos y lo siguiente
        dispatch(crearFase({
                id: idProyecto,
                formData: datos
            })
        ).then(() => setModalActivo(false));
    }


    let errores = [];
    if(!permisos.includes(PERMISOS_PROYECTO.CREAR_FASES)){
        errores.push((<li key="permiso_crear">No posee permisos para agregar fases</li>))
    }
    if(proyecto.estado !== ESTADO_PROYECTO.PENDIENTE){
        errores.push((<li key="proyecto_pendiente">El proyecto no se encuentra en estado pendiente</li>))
    }

    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Agregar Fase</Popover.Title>
            <Popover.Content>
                Agrega una fase mas al proyecto.
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede agregar una fase</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    return (
        <Col xs={12} xl={2} lg={3} md={4} className="mb-4 m-md-auto align-items-center">
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeCrearFase? popover_info : popover_error}>
            <Button variant={puedeCrearFase ? "primary" : "secondary"} onClick={puedeCrearFase ? mostrarModal : nada}>Crear Fase</Button>
            </OverlayTrigger>

            <Modal show={modalActivo} onHide={ocultarModal} size="lg">
                <Modal.Header>
                    <Modal.Title>
                        Crear nueva Fase
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={manejarSubmit}>
                    <Modal.Body>
                        <CSRFToken/>
                        {/* Nombre de la Fase */}
                        <Form.Group>
                            <Form.Label>Nombre del Fase</Form.Label>
                            <Form.Control type="text" name="nombre" maxLength="50" required/>
                        </Form.Group>
                        {/* Descripción de la Fase */}
                        <Form.Group>
                            <Form.Label>Descripción</Form.Label>
                            <Form.Control as="textarea" name="descripcion" cols="40" rows="10"/>
                        </Form.Group>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Cancelar</Button>
                        <Button variant="primary" type="submit"
                                disabled={estadoFases !== ESTADO_STATE.COMPLETADO}>{textoBoton}</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </Col>
    );
}

export default AgregarFase;