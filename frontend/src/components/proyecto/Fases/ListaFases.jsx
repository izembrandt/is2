import React from "react";

//Tablas
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';

//React Bootstrap
import {Card, Button} from "react-bootstrap";
import {AccionesFase} from "./accionesFase";
// import {MoverFases} from "./MoverFase";

const ListaFases = ({fases = []}) => {
    let fases_clon = fases.map(item => item);
    fases_clon.sort((a,b)=> a.numero - b.numero);
    let Columnas = [
        {
            text: 'Fase',
            dataField: 'nombre',
        },
        {
            text: 'Descripcion',
            dataField: 'descripcion',
        },
        {
            text: 'Acciones',
            dataField: 'id',
            formatter: (cell, row) => <AccionesFase id={row.id}/>,
        },
    ]

    return (
        <Card className="shadow mb-4">
            <Card.Header className="py-3 container-fluid">
                <div className="row">
                    <div className="col-12 col-xl-10 col-lg-9 col-md-8 my-auto">
                        <h6 className="m-0 font-weight-bold text-primary">Lista de fases</h6>
                    </div>
                </div>
            </Card.Header>
            <Card.Body>
                <BootstrapTable
                    keyField="id"
                    data={fases_clon}
                    columns={Columnas}
                    noDataIndication="No hay fases existentes"
                    pagination={paginationFactory()}
                />
            </Card.Body>
        </Card>
    );
};

export default ListaFases;