import React, {useEffect, useState} from "react";
import {Popover,OverlayTrigger, Button, Form, Modal} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import 'react-dual-listbox/lib/react-dual-listbox.css';
import {useDispatch, useSelector} from "react-redux";
import {
    editarFase, seleccionarFasePorID,
} from "../../../redux/proyecto/fases";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import styles from "./css/estilo.module.css";

export const EditarFase = ({id}) => {
    let fase = useSelector(seleccionarFasePorID(id));
    let proyecto = useSelector(seleccionarInfoProyecto);
    let idProyecto = proyecto.id;
    //Modal
    const [modalActivo, setModalActivo] = useState(false);

    let mostrarModal = () => setModalActivo(true);
    let ocultarModal = () => setModalActivo(false);


    let permisos = useSelector(seleccionarPermisosProyecto);
    let { estado } = useSelector(seleccionarInfoProyecto);
    let puedeEditar = permisos.includes(PERMISOS_PROYECTO.MODIFICAR_FASES) && !fase.cerrado && estado === ESTADO_PROYECTO.PENDIENTE;

    let dispatch = useDispatch();

    //Texto Boton
    const [textoBoton, setTextoBoton] = useState("Editar")
    let estadoFases = useSelector(state => state.fases.estado);
    useEffect(() => {
        switch (estadoFases ) {
            case ESTADO_STATE.COMPLETADO:
                setTextoBoton("Editar");
                setModalActivo(false);
                break;
            case ESTADO_STATE.PENDIENTE:
                setTextoBoton("Cargando...");
                break;
            case ESTADO_STATE.VACIO:
                setTextoBoton("No disponible.");
                break;
            case ESTADO_STATE.FALLADO:
                setTextoBoton("Error..");
                break;
        }
    }, [estadoFases])

    //Cuando se haga submit
    const manejarSubmit = (event) => {
        //Paramos el evento
        event.preventDefault();
        //Recogemos los datos
        const datos = new FormData(event.target);
        datos.append("fid", id);
        dispatch(editarFase({id: idProyecto, data:datos}));
    }
    let errores = [];
    if(estado !== ESTADO_PROYECTO.PENDIENTE){
        errores.push((<li key="proyecto_pendiente">El proyecto debe estar en estado pendiente</li>))
    }
    if(!permisos.includes(PERMISOS_PROYECTO.MODIFICAR_FASES)){
        errores.push((<li key="permiso_proyecto">No posees el permiso para modificar fases</li>))
    }
    if(fase.cerrado) {
        errores.push((<li key="fase_cerrada">Necesita la fase abierta</li>))
    }
    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Editar {fase.nombre}</Popover.Title>
            <Popover.Content>
                Edita el nombre y la descripcion de esta fase
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede editar {fase.nombre}</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )


    return (
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeEditar ? popover_info : popover_error}>
                <a href={"#"} className={puedeEditar ? "" : styles.isDisabled} onClick={puedeEditar ? mostrarModal : ocultarModal}>Editar</a>
            </OverlayTrigger>
            <Modal show={modalActivo} onHide={ocultarModal} size="lg">
                <Modal.Header>
                    <Modal.Title>
                        Editar {fase.nombre}
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={manejarSubmit}>

                    <Modal.Body>
                        <CSRFToken/>
                        {/* Nombre de la fase */}
                        <Form.Group>
                            <Form.Label>Nombre de la Fase*</Form.Label>
                            <Form.Control type="text" name="nombre" maxLength="50" required defaultValue={fase.nombre}/>
                        </Form.Group>
                        {/* Descripción de la fase */}
                        <Form.Group>
                            <Form.Label>Descripción</Form.Label>
                            <Form.Control as="textarea" name="descripcion" cols="40" rows="10" defaultValue={fase.descripcion}/>
                        </Form.Group>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Cancelar</Button>
                        <Button variant="primary" type="submit"
                                disabled={estadoFases !== ESTADO_STATE.COMPLETADO}>{textoBoton}</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}
