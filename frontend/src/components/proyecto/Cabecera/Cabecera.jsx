import React from "react";
import {Col, Row} from "react-bootstrap";


const Cabecera = ({titulo, descripcion, children}) => (
    <>
        <h1 className="h3 my-2 text-gray-800">{titulo}</h1>
        <p className="mb-4">{descripcion}</p>
        {children}
    </>
);

export default Cabecera;