import React from "react";
import { Card, ListGroup, Form } from "react-bootstrap";

export const InformacionItem = ({ item }) => {

    return(
        <Card>
            <Card.Header>
                Información del Item
            </Card.Header>
            <ListGroup variant="flush">
                <ListGroup.Item>
                    {/* Descripcion */}
                    <Form.Group>
                        <Form.Label>Descripcion</Form.Label>
                        <Form.Control as="textarea" value={item.descripcion} rows={3} disabled/>
                    </Form.Group>
                    {/* Costo de impacto */}
                    <Form.Group>
                        <Form.Label>Costo de impacto</Form.Label>
                        <Form.Control type="number" value={item.costo} disabled/>
                    </Form.Group>
                    {/* Version */}
                    <Form.Group>
                        <Form.Label>Versión</Form.Label>
                        <Form.Control type="number" value={item.version} disabled/>
                    </Form.Group>
                    {/* Código */}
                    <Form.Group>
                        <Form.Label>Código</Form.Label>
                        <Form.Control type="text" value={item.prefijo + ". " + item.numero} disabled/>
                    </Form.Group>
                </ListGroup.Item>
            </ListGroup>
        </Card>
    );
}