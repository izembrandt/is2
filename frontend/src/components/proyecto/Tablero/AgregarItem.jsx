import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
    seleccionarInfoProyecto,
    seleccionarPermisosProyecto,
    verificarPermisoFase
} from "../../../redux/proyecto/proyecto";
import {PERMISOS_PROYECTO, PERMISOS_FASE} from "../../../constantes/permisos";
import {Modal, Form, Button, Card, ListGroup, Popover, OverlayTrigger} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {seleccionarTiposItemPorFase} from "../../../redux/proyecto/tipoItem";
import {AtributoItem} from "./AtributoItem";
import {crearItemProyecto, seleccionarRelacionesCandidatosNuevoItem} from "../../../redux/proyecto/items";
import {seleccionarFasePorID} from "../../../redux/proyecto/fases";
import {ESTADO_PROYECTO} from "../../../constantes/estados";
import styles from "../Fases/css/estilo.module.css";

export const AgregarItem = ({idFase}) => {

    let dispatch = useDispatch()
    let tipos = useSelector(seleccionarTiposItemPorFase(idFase));
    let [modalActivo, setModalActivo] = useState(false);
    let [tipoSeleccionado, setTipoSeleccionado] = useState("");
    let id_proyecto = useSelector(seleccionarInfoProyecto).id;
    let fase = useSelector(seleccionarFasePorID(idFase));
    let candidatos = useSelector(seleccionarRelacionesCandidatosNuevoItem(idFase));

    let { estado } = useSelector(seleccionarInfoProyecto);
    let puedeAgregar = useSelector(verificarPermisoFase(PERMISOS_FASE.CREAR_ITEM, idFase)) && estado === ESTADO_PROYECTO.EJECUCION && !fase.cerrado &&( fase.numero === 1 || candidatos.length > 0 );

    let seleccionarTipo = event => {
        setTipoSeleccionado("" + event.target.value);
    }

    let mostrarModal = evento => {
        evento.preventDefault();
        setModalActivo(true);
    }
    let ocultarModal = () => setModalActivo(false);

    let submitItem = event => {
        event.preventDefault();
        let datos = new FormData(event.target);
        datos.append("fase_id", idFase);
        dispatch(crearItemProyecto({id: id_proyecto, datos: datos}))
            .then(() => ocultarModal());
    }

    let errores = [];
    if(estado !== ESTADO_PROYECTO.EJECUCION){
        errores.push((<li key="proyecto_ejecutado">El proyecto debe estar en ejecución</li>))
    }
    if(!useSelector(verificarPermisoFase(PERMISOS_FASE.CREAR_ITEM, idFase))){
        errores.push((<li key="crear_item">No posee el permiso para agregar un ítem</li>))
    }
    if(fase.cerrado){
        errores.push((<li key="fase_cerrada">La fase ya esta cerrada</li>))
    }
    if(fase.numero !== 1 && candidatos.length === 0){
        errores.push((<li key="fase_anterior">La fase anterior no posee ningun item dentro de una linea base</li>))
    }
    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Agregar Ítem</Popover.Title>
            <Popover.Content>
                Agregue ítems a esta fase del proyecto
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede agregar item</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )


    return (
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeAgregar? popover_info : popover_error}>
                <a href="#" className={puedeAgregar ? "" : styles.isDisabled}  onClick={puedeAgregar ? mostrarModal : ocultarModal}>
                    <span className="icon"><i className="fas fa-plus"/></span>
                    <span className="text">{" "}Agregar Item a fase</span>
                </a>
            </OverlayTrigger>

            <Modal show={modalActivo} onHide={ocultarModal}>
                <Modal.Header>
                    <Modal.Title>
                        Crear nuevo item
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={submitItem}>
                    <Modal.Body>
                        <CSRFToken/>
                        {/* Nombre del item */}
                        <Form.Group>
                            <Form.Label>Nombre del Item*</Form.Label>
                            <Form.Control type="text" name="nombre" maxLength="50" required/>
                        </Form.Group>
                        {/* Descripcion del Item */}
                        <Form.Group>
                            <Form.Label>Descripción del Item</Form.Label>
                            <Form.Control as="textarea" name="descripcion" cols="40" rows="10"/>
                        </Form.Group>
                        {/* Costo de Impacto */}
                        <Form.Group>
                            <Form.Label>Costo de impacto*</Form.Label>
                            <Form.Control type="number" name="costo" required/>
                        </Form.Group>
                        {/* Antecesor o padre */ fase.numero > 1 &&
                        <Form.Group>
                            <Form.Label>Antecesor o Padre</Form.Label>
                            <Form.Control as="select" name="antecesor" required>
                                {/* Opciones */
                                    candidatos.map(opcion =>
                                        <option key={opcion.uuid} value={opcion.uuid}>
                                            {opcion.prefijo + ". " + opcion.numero + " - " + opcion.nombre}
                                        </option>)
                                }
                            </Form.Control>
                        </Form.Group>
                        }
                        {/* Tipo de Item */}
                        <Form.Group>
                            <Form.Label>Tipo de Item*</Form.Label>
                            <Form.Control as="select" name="tipo_de_item" value={tipoSeleccionado}
                                          onChange={seleccionarTipo} required>
                                <option value="">Seleccionar Tipo de Item</option>
                                {/* Opciones */
                                    tipos.map(tipo => (<option key={tipo.id} value={tipo.id}>{tipo.nombre}</option>))
                                }
                            </Form.Control>
                        </Form.Group>
                        {/* Atributos */}
                        <Form.Group>
                            <Card>
                                <Card.Header>
                                    Atributos
                                </Card.Header>
                                <ListGroup>
                                    {tipoSeleccionado !== "" &&/* Por cada atributo */
                                    tipos.find(tipo => "" + tipo.id === "" + tipoSeleccionado)
                                        .atributos.map(attr => <AtributoItem key={attr.posicion} atributo={attr}/>)
                                    }
                                </ListGroup>
                            </Card>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Cancelar</Button>
                        <Button variant="primary" type="submit">Crear</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}