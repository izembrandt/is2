import React from "react";
import {ListGroup, Form} from "react-bootstrap";
import {TIPO_ATRIBUTO} from "../../../constantes/tipos_de_atributo";

const campo = {
    [TIPO_ATRIBUTO.TEXTO]: ({posicion, obligatorio, valor}) => (
        <Form.Control name={"Atributo " + posicion} rows="3" as="textarea" value={valor} disabled={valor !== undefined}
                      required={obligatorio}/>),
    [TIPO_ATRIBUTO.FECHA]: ({posicion, obligatorio, valor}) => (
        <Form.Control name={"Atributo " + posicion} type="date" value={valor} disabled={valor !== undefined}
                      required={obligatorio}/>),
    [TIPO_ATRIBUTO.NUMERO]: ({posicion, obligatorio, valor}) => (
        <Form.Control name={"Atributo " + posicion} type="number" step="0.0001" value={valor} disabled={valor !== undefined}
                      required={obligatorio}/>),
    [TIPO_ATRIBUTO.ARCHIVO]: (attr) => (
        <>
            <Form.Control type="text" disabled value={attr.url} />
            <a className="btn btn-success mt-3"
               href={attr.url} download="" target="_blank">
                <span> Descargar </span>&nbsp;<i className="fa fa-download"/>
            </a>
        </>
    )

}

export const AtributoItemMostrar = ({atributo}) => {
    let campo_renderizar = campo[atributo.tipo](atributo);

    return (
        <ListGroup.Item>
            <Form.Group>
                <Form.Label>{atributo.nombre}</Form.Label>
                {campo_renderizar}
            </Form.Group>
        </ListGroup.Item>
    );
}