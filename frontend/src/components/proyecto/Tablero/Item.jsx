import React, {useState} from "react";
import {ESTADO_ITEM} from "../../../constantes/estados";
import {InterfazItem} from "./InterazItem";
import {Badge} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {abrirInterfazItem} from "../../../redux/proyecto/modales";
import {seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import {seleccionarLineaBasePorUUID} from "../../../redux/proyecto/lineasBase";
import {hallarColorTexto} from "../../../utils/colores";

const estado = {
    [ESTADO_ITEM.PENDIENTE]: "Pendiente",
    [ESTADO_ITEM.DESARROLLO]: "Sin aprobar",
    [ESTADO_ITEM.APROBADO]: "Aprobado",
    [ESTADO_ITEM.EN_LINEA_BASE]: "En línea base",
    [ESTADO_ITEM.DESACTIVADO]: "Desactivado",
    [ESTADO_ITEM.REVISION]: <p style={{color: "red"}}>En revisión</p>,
}

export const Item = ({item}) => {
    let dispatch = useDispatch();

    //Datos
    let lineaBase = useSelector(seleccionarLineaBasePorUUID(item.linea_base));

    //Funciones
    let mostrar = () => dispatch(abrirInterfazItem({uuid: item.uuid}));

    //Estilo
    let estiloLB = {
        "backgroundColor": lineaBase !== undefined ? lineaBase.color : "white",
        "color": lineaBase !== undefined ? hallarColorTexto(lineaBase.color) : "black",
    }


    return (
        <>
            <li className="p-2" onClick={mostrar}>
                <div>{item.nombre}</div>
                {lineaBase !== undefined
                    ? <Badge style={estiloLB} className="my-2">{lineaBase.nombre.toUpperCase()}</Badge>
                    : <br/>
                }
                <div>
                    <div className="float-left" style={{"fontSize": "12px", "color": "gray"}}>
                        {estado[item.estado]}
                    </div>
                    <div className="float-right estado-item">{item.prefijo + ". " + item.numero}</div>
                </div>
                <br/>
            </li>
        </>
    );
}

