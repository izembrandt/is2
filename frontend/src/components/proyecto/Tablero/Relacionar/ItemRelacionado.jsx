import React from "react";
import {ListGroup } from "react-bootstrap";
import {desrelacionarItem, seleccionarItemPorUUID} from "../../../../redux/proyecto/items";
import Cookies from "js-cookie";
import {useDispatch, useSelector} from "react-redux";
import {seleccionarInfoProyecto, verificarPermisoFase} from "../../../../redux/proyecto/proyecto";
import {ESTADO_ITEM} from "../../../../constantes/estados";
import {PERMISOS_FASE} from "../../../../constantes/permisos";
import {seleccionarFasePorID} from "../../../../redux/proyecto/fases";

export const ItemRelacionado = ({ item, uuid_destino }) => {
    let dispatch = useDispatch();
    let proyecto = useSelector(seleccionarInfoProyecto);
    let item_destino = useSelector(seleccionarItemPorUUID(uuid_destino));
    let fase = useSelector(seleccionarFasePorID(item_destino.fase))

    let onDesrelacionar = () => {
        let datos = new FormData();
        datos.append("uuid_origen", item.uuid);
        datos.append("uuid_destino", uuid_destino);
        datos.append("csrfmiddlewaretoken", Cookies.get('csrftoken'));
        //Despachar
        dispatch(desrelacionarItem({ datos, id: proyecto.id}));
    }

    return (
        <ListGroup.Item>
            <div className="form-inline">
                <label>{`${item.prefijo}. ${item.numero} - ${item.nombre}`}</label>
                {item_destino.estado === ESTADO_ITEM.DESARROLLO && useSelector(verificarPermisoFase(PERMISOS_FASE.RELACIONAR_ITEM, fase.id)) &&
                    <button className="btn" onClick={onDesrelacionar} disabled={!(item_destino.antecesores.length > 1) && fase.numero > 1}><i className="fas fa-trash text-danger"/></button>
                }
            </div>
        </ListGroup.Item>
    );
}