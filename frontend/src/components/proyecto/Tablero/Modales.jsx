import React from "react";
import {InterfazItem} from "./InterazItem";
import {useDispatch, useSelector} from "react-redux";
import {seleccionarModalAbierto, cerrarTodo, seleccionarItemAbierto, abrirInterfazItem, abrirRelacionarItem} from "../../../redux/proyecto/modales";
import { Modal } from "react-bootstrap";
import {INTERFAZ_MODAL} from "../../../constantes/modales";
import {SolicitarAprobacion} from "./Item_modales/SolicitarAprobacion";
import {AprobarSolicitud} from "./Item_modales/AprobarSolicitud";
import {CancelarSolicitud} from "./Item_modales/CancelarSolicitud";
import {Desaprobar} from "./Item_modales/Desaprobar";
import {Desactivar} from "./Item_modales/Desactivar";
import {Modificar} from "./Item_modales/Modificar";
import { Historial } from "./Item_modales/Historial";
import {Relacionar} from "./Item_modales/Relacionar";
import {Restaurar} from "./Item_modales/Restaurar";
import {CrearRelacionPadre} from "./Item_modales/CrearRelacionPadre";
import {CrearRelacionAntecesor} from "./Item_modales/CrearRelacionAntecesor";
import {SolicitarCambioModal} from "./Item_modales/SolicitarCambioModal";
import AceptarRevisionModal from "./Item_modales/AceptarRevisionModal";
import RevisarItemModal from "./Item_modales/RevisarItemModal";

export const Modales = () => {
    let dispatch = useDispatch();
    let item_actual_uuid = useSelector(seleccionarItemAbierto);
    let modalAbierto = useSelector(seleccionarModalAbierto);
    let volverTablero = () => dispatch(cerrarTodo());

    let abrirInterfazItemActual = () => dispatch(abrirInterfazItem({ uuid: item_actual_uuid}));
    let abrirRelacionesItem = () => dispatch(abrirRelacionarItem());

    return (
        <>
            <Modal show={modalAbierto === INTERFAZ_MODAL.ITEM} size="lg" onHide={volverTablero} centered>
                <InterfazItem/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.SOLICITAR_APROBACION}
                   size="xs" onHide={abrirInterfazItemActual} centered>
                <SolicitarAprobacion/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.CANCELAR_SOLICITUD} size="xs"
                   onHide={abrirInterfazItemActual} centered>
                <CancelarSolicitud/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.APROBAR_SOLICITUD} size="xs"
                   onHide={abrirInterfazItemActual} centered>
                <AprobarSolicitud/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.DESAPROBAR_ITEM} size="xs"
                   onHide={abrirInterfazItemActual} centered>
                <Desaprobar/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.DESACTIVAR_ITEM} size="xs"
                   onHide={abrirInterfazItemActual} centered>
                <Desactivar/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.MODIFICAR_ITEM}
                   onHide={abrirInterfazItemActual} centered>
                <Modificar/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.RELACIONAR_ITEM} size="lg"
                   onHide={abrirInterfazItemActual} centered>
                <Relacionar/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.HISTORIAL_ITEM} size="lg" scrollable
                   onHide={abrirInterfazItemActual} centered>
                <Historial/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.RESTAURAR_ITEM} size="xs" onHide={abrirInterfazItemActual} centered>
                <Restaurar/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.CREAR_PADRE} onHide={abrirRelacionesItem} centered>
                <CrearRelacionPadre/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.CREAR_ANTECESOR} onHide={abrirRelacionesItem} centered>
                <CrearRelacionAntecesor/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.CREAR_SOLICITUD_CAMBIO} onHide={abrirInterfazItemActual} centered>
                <SolicitarCambioModal/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.ACEPTAR_REVISION} onHide={abrirInterfazItemActual} centered>
                <AceptarRevisionModal/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.REVISAR_ITEM} onHide={abrirInterfazItemActual} centered>
                <RevisarItemModal/>
            </Modal>
        </>
    );
}