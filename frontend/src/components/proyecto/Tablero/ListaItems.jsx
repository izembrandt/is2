import React from "react";
import {useSelector} from "react-redux";

import './Tablero.scss';
import {AgregarItem} from "./AgregarItem";
import {seleccionarItemsPorFase} from "../../../redux/proyecto/items";
import {Item} from "./Item";

export const ListaItems = ({fase}) => {
    //Agarramos los items
    let itemsDeFase = useSelector(seleccionarItemsPorFase(fase.id)).filter(item => item.es_ultimo);
    let itemOrdenados = itemsDeFase.map(x => x).sort((a, b) => {
        if (a.prefijo === b.prefijo) {
            //Desempate
            return a.numero < b.numero;
        }

        return a.prefijo < b.prefijo;
    });
    return (
        <div className="fase-lista-de-items">
            <header>{fase.nombre}</header>
            <ul>
                {/* ACA VAN LOS ITEMS */}
                {
                    itemOrdenados.map(item => (<Item key={item.uuid} item={item}/>))
                }
            </ul>
            <footer><AgregarItem idFase={fase.id}></AgregarItem></footer>
        </div>
    );
}