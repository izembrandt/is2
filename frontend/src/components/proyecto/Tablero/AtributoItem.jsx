import React from "react";
import {ListGroup, Form} from "react-bootstrap";
import {TIPO_ATRIBUTO} from "../../../constantes/tipos_de_atributo";
import {EstirarArchivo} from "./EstirarArchivo";

const campo = {
    [TIPO_ATRIBUTO.TEXTO]: ({posicion, obligatorio, valor}) => (
        <Form.Control name={"Atributo " + posicion} rows="3" as="textarea" defaultValue={valor}
                      required={obligatorio}/>),
    [TIPO_ATRIBUTO.FECHA]: ({posicion, obligatorio, valor}) => (
        <Form.Control name={"Atributo " + posicion} type="date" defaultValue={valor}
                      required={obligatorio}/>),
    [TIPO_ATRIBUTO.NUMERO]: ({posicion, obligatorio, valor}) => (
        <Form.Control name={"Atributo " + posicion} type="number" step="0.0001" defaultValue={valor}
                      required={obligatorio}/>),
    [TIPO_ATRIBUTO.ARCHIVO]: (attr) => (<EstirarArchivo atributo={attr}/>)

}

export const AtributoItem = ({atributo}) => {
    let campo_renderizar = campo[atributo.tipo](atributo);

    return (
        <ListGroup.Item>
            <Form.Group>
                <Form.Label>{atributo.nombre}</Form.Label>
                {campo_renderizar}
            </Form.Group>
        </ListGroup.Item>
    );
}