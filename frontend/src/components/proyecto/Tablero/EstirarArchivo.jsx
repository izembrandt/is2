import React, {useCallback, useEffect, useState} from "react";
import {useDropzone} from "react-dropzone";
import './Tablero.scss';
import {urlSolicitarSubida} from "../../../url";
import {useSelector} from "react-redux";
import {seleccionarInfoProyecto} from "../../../redux/proyecto/proyecto";
import Cookies from "js-cookie";
import {Button} from "react-bootstrap";
import {toast} from "react-toastify";

//TODO: Verificar que suba todo antes de submitear, y corregir lo que celeste borró
export const EstirarArchivo = ({atributo}) => {
    const toastId = React.useRef(null);
    let proyecto = useSelector(seleccionarInfoProyecto);
    let [progreso, setProgreso] = useState(0);
    let [nombreRecurso, setNombreRecurso] = useState("");
    let [nombreArchivo, setNombreArchivo] = useState("");

    let tieneArchivo = nombreArchivo !== "" && nombreArchivo !== "";
    let puedeMandar = !atributo.obligatorio || tieneArchivo;

    useEffect(() => {
        //Verificar si ya esta cargado
        if (atributo.valor !== undefined) {
            setNombreArchivo(atributo.valor);
            setNombreRecurso(atributo.valor);
            setProgreso(100);
        }
    }, []);

    const onDrop = useCallback(async acceptedFiles => {
        //Solicitar al servidor la url
        let archivo = acceptedFiles[0];
        setNombreArchivo(archivo.name);
        let solicitudUrl = new FormData();
        solicitudUrl.append("csrfmiddlewaretoken", Cookies.get('csrftoken'));
        solicitudUrl.append("nombre", archivo.name);
        solicitudUrl.append("content_type", archivo.type);
        let {url, nombre_recurso} = await fetch(urlSolicitarSubida(proyecto.id), {
            method: 'POST',
            body: solicitudUrl
        }).then(response => response.json());
        //Ahora ya tenemos, nos resta subir el archivo
        const xhr = new XMLHttpRequest();
        toastId.current = null; //Tenemos que inicializar el toast
        xhr.upload.onprogress = event => {
            const percentage = parseInt((event.loaded / event.total) * 100);
            setProgreso(percentage); // Update progress here
            if (toastId.current === null) {
                toastId.current = toast.info(
                    `Subiendo ${archivo.name} ${percentage}%`
                    , {
                        progress: event.loaded / event.total,
                        closeOnClick: false,
                        autoClose: false,
                    });
            } else {
                toast.update(toastId.current, {
                    progress: event.loaded / event.total,
                    render: `Subiendo ${archivo.name} ${percentage}%`
                });
            }
        };
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== 4) return;
            if (xhr.status !== 200) {
                console.log('error al subir archivo'); // Handle error here
                toast.update(toastId.current, {
                    type: "error",
                    render: "No se pudo subir el archivo"
                });
                setNombreRecurso("");
            } else {
                setNombreRecurso(nombre_recurso);
                toast.dismiss(toastId.current);
                toastId.current = toast.success("Carga exitosa");
            }
        };
        xhr.open('PUT', url, true);
        xhr.send(archivo)
    }, []);

    const {acceptedFiles, getRootProps, getInputProps} = useDropzone({
        onDrop
    });

    let eliminarArchivo = () => {
        setNombreRecurso("");
        setNombreArchivo("");
    }

    return (
        <div>
            <div {...getRootProps({className: 'dropzone'})}>
                <input {...getInputProps()} />
                <div className="drop-container">
                    <div className="mensaje-drop">
                        {nombreArchivo !== "" ? (<><i
                            className="fas fa-file"/>{" " + nombreArchivo + " - " + progreso + " %"}</>) : "Arrastra y tira el archivo a subir"}
                    </div>
                </div>
            </div>
            <input type="text" name={"Atributo " + atributo.posicion} value={nombreRecurso}
                   required={atributo.obligatorio} onChange={() => setNombreRecurso(prevState => prevState)}
                   style={{opacity: 0, width: 0, height: 0, float: "left"}}/>
            {tieneArchivo && <Button variant="danger" className="m-2" style={{float: "right"}}
                                     onClick={eliminarArchivo}>Eliminar</Button>}
        </div>
    );
}
