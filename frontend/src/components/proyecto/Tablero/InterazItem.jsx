import React from "react";
import {Modal, Button, Row, Col, OverlayTrigger, Popover} from "react-bootstrap";
import {InformacionItem} from "./InformacionItem";
import {AtributosItemInterfaz} from "./AtributosItemInterfaz";

import {useDispatch, useSelector} from "react-redux";
import {
    abrirSolicitarAprobacion,
    abrirAprobarSolicitud,
    abrirDesaprobarItem,
    abrirRestaurarItem,
    abrirHistorialItem,
    abrirModificarItem,
    abrirRelacionarItem,
    abrirDesactivarItem,
    abrirCancelarSolicitud,
    abrirTrazabilidadItem,
    cerrarTodo,
    seleccionarItemAbierto, abrirModal
} from "../../../redux/proyecto/modales";
import {
    seleccionarItemPorUUID,
    verificarSiPuedeDesaprobar, verificarSiPuedeRevisar,
    verificarSiPuedeSoliciar
} from "../../../redux/proyecto/items";
import {seleccionarUltimaVersionDelItem} from "../../../redux/proyecto/items";
import {seleccionarInfoProyecto, verificarPermisoFase} from "../../../redux/proyecto/proyecto";
import {PERMISOS_FASE} from "../../../constantes/permisos";
import {ESTADO_ITEM, ESTADO_PROYECTO} from "../../../constantes/estados";
import ImpactoTotal from "./Impacto/ImpactoTotal";
import {seleccionarSolicitudPendienteDeItemUUID} from "../../../redux/proyecto/comite";
import {seleccionarFasePorID} from "../../../redux/proyecto/fases";
import {INTERFAZ_MODAL} from "../../../constantes/modales";

export const InterfazItem = () => {
    const proyecto = useSelector(seleccionarInfoProyecto);
    let dispatch = useDispatch();
    let item_uuid = useSelector(seleccionarItemAbierto);
    let item = useSelector(seleccionarItemPorUUID(item_uuid));
    let fase = useSelector(seleccionarFasePorID(item.fase));
    let cerrarModal = () => dispatch(cerrarTodo());

    let puedeSolicitar = useSelector(verificarSiPuedeSoliciar(item))
        && useSelector(verificarPermisoFase(PERMISOS_FASE.MODIFICAR_ITEM, item.fase))
        && proyecto.estado === ESTADO_PROYECTO.EJECUCION;

    let puedeAprobar = item.estado === ESTADO_ITEM.PENDIENTE
        && item.es_ultimo
        && (useSelector(verificarPermisoFase(PERMISOS_FASE.APROBAR_ITEM, item.fase)))
        && proyecto.estado !== ESTADO_PROYECTO.CANCELADO
        && proyecto.estado !== ESTADO_PROYECTO.FINALIZADO;


    let puedeCancelar = useSelector(verificarPermisoFase(PERMISOS_FASE.MODIFICAR_ITEM, item.fase))
        && proyecto.estado !== ESTADO_PROYECTO.CANCELADO
        && proyecto.estado !== ESTADO_PROYECTO.FINALIZADO;


    let puedeDesaprobar = useSelector(verificarSiPuedeDesaprobar(item))
        && useSelector(verificarPermisoFase(PERMISOS_FASE.DESAPROBAR_ITEM, item.fase))
        && proyecto.estado !== ESTADO_PROYECTO.CANCELADO
        && proyecto.estado !== ESTADO_PROYECTO.FINALIZADO;


    let puedeModificar = item.estado === ESTADO_ITEM.DESARROLLO
        && useSelector(verificarPermisoFase(PERMISOS_FASE.MODIFICAR_ITEM, item.fase))
        && proyecto.estado !== ESTADO_PROYECTO.CANCELADO
        && proyecto.estado !== ESTADO_PROYECTO.FINALIZADO;


    let puedeDesactivar = useSelector(verificarPermisoFase(PERMISOS_FASE.ELIMINAR_ITEM, item.fase))
        && item.estado === ESTADO_ITEM.DESARROLLO
        && proyecto.estado !== ESTADO_PROYECTO.CANCELADO
        && proyecto.estado !== ESTADO_PROYECTO.FINALIZADO;

    let puedeRestaurar = useSelector(verificarPermisoFase(PERMISOS_FASE.MODIFICAR_ITEM, item.fase))
        && useSelector(seleccionarUltimaVersionDelItem(item)).estado === ESTADO_ITEM.DESARROLLO
        && proyecto.estado !== ESTADO_PROYECTO.CANCELADO
        && proyecto.estado !== ESTADO_PROYECTO.FINALIZADO;
    let puedeHistorial = item.es_ultimo && item.version > 1;

    let puedeSolicitarCambio = useSelector(verificarPermisoFase(PERMISOS_FASE.CREAR_SOLICITUD_CAMBIO, item.fase))
        && item.linea_base !== null
        && useSelector(seleccionarSolicitudPendienteDeItemUUID(item.uuid)) === undefined
        && !fase.cerrado;

    let puedeRevisarItem = item.estado === ESTADO_ITEM.REVISION
        && item.linea_base === null
        && useSelector(verificarSiPuedeRevisar(item))
        && useSelector(verificarPermisoFase(PERMISOS_FASE.DESAPROBAR_ITEM, item.fase));

    let puedeAceptarRevision = item.estado === ESTADO_ITEM.REVISION
        && useSelector(verificarSiPuedeRevisar(item))
        && useSelector(verificarPermisoFase(PERMISOS_FASE.APROBAR_ITEM, item.fase));

    let sinEvento = () => "";


    let erroresSolicitar = [];
    if (proyecto.estado !== ESTADO_PROYECTO.EJECUCION) {
        erroresSolicitar.push((<li key="proyecto_ejecutado">El proyecto debe estar en ejecución</li>))
    }
    if (!useSelector(verificarPermisoFase(PERMISOS_FASE.MODIFICAR_ITEM, item.fase))) {
        erroresSolicitar.push((<li key="modificar_item">No posee el permiso para modificar item</li>))
    }
    if (!useSelector(verificarSiPuedeSoliciar(item))) {
        erroresSolicitar.push((<li key="puede_solicitar">El item no puede ser aprobado</li>))
    }

    let erroresModificar = [];
    if (item.estado !== ESTADO_ITEM.DESARROLLO) {
        erroresModificar.push((<li key="item_desarrollo">El Ítem debe estar en desarrollo </li>))
    }
    if (!useSelector(verificarPermisoFase(PERMISOS_FASE.MODIFICAR_ITEM, item.fase))) {
        erroresModificar.push((<li key="modificar_item">No posee el permiso para modificar item</li>))
    }
    if (proyecto.estado === ESTADO_PROYECTO.CANCELADO) {
        erroresModificar.push((<li key="proyecto_cancelado">El proyecto no debe estar cancelado</li>))
    }
    if (proyecto.estado === ESTADO_PROYECTO.FINALIZADO) {
        erroresModificar.push((<li key="proyecto_finalizado">El proyecto no debe estar finalizado</li>))
    }

    let erroresHistorial = [];
    if (!(item.version > 1)) {
        erroresHistorial.push((<li key="modificar_item">No existen versiones dentro del historial</li>))
    }
    if (!item.es_ultimo) {
        erroresHistorial.push(<li key="version_anterior">Este ítem es una versión anterior</li>)
    }

    let erroresDesactivar = [];
    if (!useSelector(verificarPermisoFase(PERMISOS_FASE.ELIMINAR_ITEM, item.fase))) {
        erroresDesactivar.push((<li key="eliminar_item">No posee permisos para eliminar el ítem </li>))
    }
    if (item.estado !== ESTADO_ITEM.DESARROLLO) {
        erroresDesactivar.push((<li key="item_desarrollo">El Ítem debe estar en desarrollo</li>))
    }
    if (proyecto.estado === ESTADO_PROYECTO.CANCELADO) {
        erroresDesactivar.push((<li key="proyecto_cancelado">El proyecto no debe estar cancelado</li>))
    }
    if (proyecto.estado === ESTADO_PROYECTO.FINALIZADO) {
        erroresDesactivar.push((<li key="proyecto_finalizado">EL proyecto no debe estar finalizado</li>))
    }


    let erroresAprobar = [];
    if (!useSelector(verificarPermisoFase(PERMISOS_FASE.APROBAR_ITEM, item.fase))) {
        erroresAprobar.push((<li key="APROBAR_ITEM">No posee permisos para aprobar el ítem </li>))
    }
    if (item.estado !== ESTADO_ITEM.PENDIENTE) {
        erroresAprobar.push((<li key="item_desarrollo">El Ítem no posee solicitudes pendientes</li>))
    }
    if (proyecto.estado === ESTADO_PROYECTO.CANCELADO) {
        erroresAprobar.push((<li key="proyecto_cancelado">El proyecto no debe estar cancelado</li>))
    }
    if (proyecto.estado === ESTADO_PROYECTO.FINALIZADO) {
        erroresAprobar.push((<li key="proyecto_finalizado">El proyecto no debe estar finalizado</li>))
    }

    let erroresCancelar = [];
    if (!useSelector(verificarPermisoFase(PERMISOS_FASE.MODIFICAR_ITEM, item.fase))) {
        erroresCancelar.push((<li key="APROBAR_ITEM">No posee permisos para modificar el ítem </li>))
    }
    if (proyecto.estado === ESTADO_PROYECTO.CANCELADO) {
        erroresCancelar.push((<li key="proyecto_cancelado">El proyecto no debe estar cancelado</li>))
    }
    if (proyecto.estado === ESTADO_PROYECTO.FINALIZADO) {
        erroresCancelar.push((<li key="proyecto_finalizado">El proyecto no debe estar finalizado</li>))
    }

    let erroresDesaprobar = [];
    if (!useSelector(verificarSiPuedeDesaprobar(item))) {
        erroresDesaprobar.push((<li key="relaciones">El item tiene hijos o sucesores</li>))
    }
    if (!useSelector(verificarPermisoFase(PERMISOS_FASE.DESAPROBAR_ITEM, item.fase))) {
        erroresDesaprobar.push((<li key="permisos">No posee permisos para Desaprobar Item</li>))
    }
    if (proyecto.estado === ESTADO_PROYECTO.CANCELADO) {
        erroresDesaprobar.push((<li key="proyecto_cancelado">El proyecto no debe estar cancelado</li>))
    }
    if (proyecto.estado === ESTADO_PROYECTO.FINALIZADO) {
        erroresDesaprobar.push((<li key="proyecto_finalizado">El proyecto no debe estar finalizado</li>))
    }

    let erroresRestaurar = [];
    if (!useSelector(verificarPermisoFase(PERMISOS_FASE.MODIFICAR_ITEM, item.fase))) {
        erroresRestaurar.push((<li key="modoficar">No posee permisos para modificar el ítem </li>))
    }
    if (useSelector(seleccionarUltimaVersionDelItem(item)).estado !== ESTADO_ITEM.DESARROLLO) {
        erroresRestaurar.push((<li key="item_desarrollo">El Ítem no esta en desarrollo </li>))
    }
    if (proyecto.estado === ESTADO_PROYECTO.CANCELADO) {
        erroresRestaurar.push((<li key="proyecto_cancelado">El proyecto no debe estar cancelado</li>))
    }
    if (proyecto.estado === ESTADO_PROYECTO.FINALIZADO) {
        erroresRestaurar.push((<li key="proyecto_finalizado">El proyecto no debe estar finalizado</li>))
    }

    let erroresSolicitarCambio = [];
    if (!useSelector(verificarPermisoFase(PERMISOS_FASE.CREAR_SOLICITUD_CAMBIO, item.fase))) {
        erroresSolicitarCambio.push((<li key="crear">No posee permisos para crear solicitud</li>))
    }
    if (item.linea_base === null) {
        erroresSolicitarCambio.push((<li key="crear">El item no esta en linea base</li>))
    }
    if (!(useSelector(seleccionarSolicitudPendienteDeItemUUID(item.uuid)) === undefined)){
        erroresSolicitarCambio.push((<li key="solicitud">El item ya tiene una solicitud pendiente</li>))
    }
    if (fase.cerrado) {
        erroresSolicitarCambio.push((<li key="cerrada">La fase ya esta cerrada</li>))
    }

    let erroresRevisarItem = [];
    if (item.estado !== ESTADO_ITEM.REVISION) {
        erroresRevisarItem.push((<li key="revision">El item no esta en revision</li>))
    }
    if (item.linea_base !== null) {
        erroresRevisarItem.push((<li key="crear">El item esta en una linea base, para realizar cambios debes hacer otra solicitud</li>))
    }
    if (!useSelector(verificarSiPuedeRevisar(item))) {
        erroresRevisarItem.push((<li key="puede">El item no debe tener antecesores ni padres fuera de linea de base</li>))
    }
    if (!useSelector(verificarPermisoFase(PERMISOS_FASE.DESAPROBAR_ITEM, item.fase))) {
        erroresRevisarItem.push((<li key="desaprobar">No posee permisos de Desaprobar item</li>))
    }

    let erroresAceptarRevision = [];
    if (item.estado !== ESTADO_ITEM.REVISION) {
        erroresAceptarRevision.push((<li key="revision">El item no esta en revision</li>))
    }
    if (!useSelector(verificarSiPuedeRevisar(item))) {
        erroresAceptarRevision.push((<li key="puede">El item no debe tener antecesores ni padres fuera de linea de base</li>))
    }
    if (!useSelector(verificarPermisoFase(PERMISOS_FASE.APROBAR_ITEM, item.fase))) {
        erroresAceptarRevision.push((<li key="aprobar">No posee permisos de aprobar item</li>))
    }

    const popover_info = (titulo, contenido) => {
        return <Popover id="popover-basic">
            <Popover.Title as="h3">{titulo}</Popover.Title>
            <Popover.Content>
                {contenido}
            </Popover.Content>
        </Popover>;
    };
    const popover_error = (titulo, errores) => {
        return <Popover id="popover-basic">
            <Popover.Title as="h3">{titulo}</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    };

    return (
        <>
            <Modal.Header>
                <Modal.Title>{item.nombre}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Row>
                    {/* Lado izquierdo */}
                    <Col xs={12} lg={8}>
                        <Row>
                            <Col xs={12} className="mb-3">
                                <InformacionItem item={item}/>
                            </Col>
                            <Col xs={12}>
                                <AtributosItemInterfaz item={item}/>
                            </Col>
                        </Row>
                    </Col>
                    {/* Lado derecho */}
                    <Col xs={12} lg={4}><Col xs={12}>

                        {/*SEPARADOR*/}
                        <h5>Acciones</h5>

                        {/* Modificar Item */}
                        {item.es_ultimo && item.estado !== ESTADO_ITEM.DESACTIVADO &&
                        <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeModificar ?
                            popover_info("Modificar Ítem", "Puede modificar los datos del Ítem")
                            : popover_error("No puede Modificar el Ítem", erroresModificar)}>
                            <Button variant={puedeModificar ? "info" : "secondary"}
                                    className="btn-block align-items-start"
                                    onClick={puedeModificar ? () => dispatch(abrirModificarItem()) : sinEvento}>
                                    <span className="icon text-white-50">
                                        <i className="fas fa-edit"/>
                                    </span>
                                {" "}
                                <span className="text">Modificar Item</span>
                            </Button>
                        </OverlayTrigger>
                        }
                        {/* Relacionar Item */}
                        {item.estado !== ESTADO_ITEM.DESACTIVADO &&
                        <OverlayTrigger trigger={["hover", "click"]} placement="auto"
                                        overlay={popover_info("Relaciones de Ítems", "Relaciones existentes del Ítem")}>
                            <Button variant="info" className="btn-block align-items-start"
                                    onClick={() => dispatch(abrirRelacionarItem())}>
                                <span className="icon text-white-50"><i className="fas fa-exchange-alt"/></span>
                                {" "}
                                <span className="text">Relaciones</span>
                            </Button>
                        </OverlayTrigger>
                        }
                        {/* Historial del Item */}
                        <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeHistorial ?
                            popover_info("Historial Ítem", "Puede ver el historial del Ítem seleccionado")
                            : popover_error("No puede ver Historial Ítem", erroresHistorial)}>
                            <Button variant={puedeHistorial ? "info" : "secondary"}
                                    className="btn-block align-items-start"
                                    onClick={puedeHistorial ? () => dispatch(abrirHistorialItem()) : sinEvento}>
                                <span className="icon text-white-50">
                                <i className="fas fa-history"/>
                                </span>
                                {" "}
                                <span className="text">Historial Item</span>
                            </Button>
                        </OverlayTrigger>
                        {/* Desactivar Item */}
                        {item.estado !== ESTADO_ITEM.DESACTIVADO && item.es_ultimo &&
                        <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeDesactivar ?
                            popover_info("Desactivar Ítem", "Puede desactivar el Ítem de la fase")
                            : popover_error("No puede desactivar el Ítem", erroresDesactivar)}>
                            <Button variant={puedeDesactivar ? "info" : "secondary"}
                                    className="btn-block align-items-start"
                                    onClick={puedeDesactivar ? () => dispatch(abrirDesactivarItem()) : sinEvento}>
                                    <span className="icon text-white-50">
                                        <i className="fas fa-minus-circle"/>
                                    </span>
                                {" "}
                                <span className="text">Desactivar Item</span>
                            </Button>
                        </OverlayTrigger>
                        }

                        {/*SEPARADOR*/}
                        {item.estado !== ESTADO_ITEM.EN_LINEA_BASE && item.es_ultimo && item.estado !== ESTADO_ITEM.DESACTIVADO && item.estado !== ESTADO_ITEM.REVISION &&
                        <><br/><h5>Aprobacion</h5></>
                        }

                        {/* Solicitar Aprobación */}
                        {item.estado === ESTADO_ITEM.DESARROLLO && item.es_ultimo &&
                        <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeSolicitar ?
                            popover_info("Solicitar Aprobación", "Puede solicitar la aprobación del Ítem")
                            : popover_error("No puede solicitar aprobación", erroresSolicitar)}>
                            <Button variant={puedeSolicitar ? "info" : "secondary"}
                                    className="btn-block align-items-start"
                                    onClick={puedeSolicitar ? () => dispatch(abrirSolicitarAprobacion()) : sinEvento}>
                                        <span className="icon text-white-50">
                                            <i className="fas fa-share-square"/>
                                        </span>
                                {" "}
                                <span className="text">Solicitar Aprobación</span>
                            </Button>
                        </OverlayTrigger>
                        }
                        {/* Aprobar Solicitud */}
                        {item.estado === ESTADO_ITEM.PENDIENTE && item.es_ultimo &&
                        <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeAprobar ?
                            popover_info("Aprobar Ítem", "Puede aprobar el Ítem")
                            : popover_error("No puede aprobar el Ítem", erroresAprobar)}>
                            <Button variant={puedeAprobar ? "info" : "secondary"}
                                    className="btn-block align-items-start"
                                    onClick={puedeAprobar ? () => dispatch(abrirAprobarSolicitud()) : sinEvento}>
                                    <span className="icon text-white-50">
                                        <i className="fas fa-thumbs-up"/>
                                    </span>
                                {" "}
                                <span className="text">Aprobar Item</span>
                            </Button>
                        </OverlayTrigger>
                        }
                        {/* Cancelar Solicitud */}
                        {item.estado === ESTADO_ITEM.PENDIENTE && item.es_ultimo &&
                        <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeCancelar ?
                            popover_info("Cancelar solicitud", "Puede cancelar la solicitud")
                            : popover_error("No puede cancelar la solicitud", erroresCancelar)}>
                            <Button variant={puedeCancelar ? "info" : "secondary"}
                                    className="btn-block align-items-start"
                                    onClick={puedeCancelar ? () => dispatch(abrirCancelarSolicitud()) : sinEvento}>
                                    <span className="icon text-white-50">
                                        <i className="fas fa-share-square"/>
                                    </span>
                                {" "}
                                <span className="text">Cancelar Solicitud</span>
                            </Button>
                        </OverlayTrigger>
                        }
                        {/* Desaprobar Item */}
                        {item.estado === ESTADO_ITEM.APROBADO && item.es_ultimo &&
                        <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeDesaprobar ?
                            popover_info("Desaprobar Ítem", "Puede desaprobar Ítem")
                            : popover_error("No puede desaprobar Ítem", erroresDesaprobar)}>
                            <Button variant={puedeDesaprobar ? "info" : "secondary"}
                                    className="btn-block align-items-start"
                                    onClick={puedeDesaprobar ? () => dispatch(abrirDesaprobarItem()) : sinEvento}>
                                    <span className="icon text-white-50">
                                        <i className="fas fa-thumbs-down"/>
                                    </span>
                                {" "}
                                <span className="text">Desaprobar Item</span>
                            </Button>
                        </OverlayTrigger>
                        }

                        {/*SEPARADOR*/}
                        {item.es_ultimo && (item.linea_base !== null || item.estado === ESTADO_ITEM.REVISION) &&
                        <><br/><h5>Revisión</h5></>
                        }

                        {/* Solicitud de cambio */}
                        {item.es_ultimo && item.linea_base !== null &&
                        <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeSolicitarCambio ?
                            popover_info("Solicitar cambio de Ítem", "Puede romper la linea de base si se acepta la solicitud")
                            : popover_error("No puede solcitar cambio del Ítem", erroresSolicitarCambio)}>
                            <Button variant={puedeSolicitarCambio ? "info" : "secondary"}
                                    className="btn-block align-items-start"
                                    onClick={puedeSolicitarCambio ? () => dispatch(abrirModal(INTERFAZ_MODAL.CREAR_SOLICITUD_CAMBIO)) : sinEvento}>
                                    <span className="icon text-white-50">
                                        <i className="fas fa-edit"/>
                                    </span>
                                {" "}
                                <span className="text">Solicitar Cambio</span>
                            </Button>
                        </OverlayTrigger>
                        }
                        {/* Revisar Item */}
                        {item.es_ultimo && item.estado === ESTADO_ITEM.REVISION &&
                        <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeRevisarItem ?
                            popover_info("Revisar el Ítem", "Puede agregar cambios al item")
                            : popover_error("No puede revisar el Ítem", erroresRevisarItem)}>
                            <Button variant={puedeRevisarItem ? "info" : "secondary"}
                                    className="btn-block align-items-start"
                                    onClick={puedeRevisarItem ? () => dispatch(abrirModal(INTERFAZ_MODAL.REVISAR_ITEM)) : sinEvento}>
                                    <span className="icon text-white-50">
                                        <i className="fas fa-edit"/>
                                    </span>
                                {" "}
                                <span className="text">Revisar Item</span>
                            </Button>
                        </OverlayTrigger>}
                        {/* Aceptar revisión */}
                        {item.es_ultimo && item.estado === ESTADO_ITEM.REVISION &&
                        <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeAceptarRevision ?
                            popover_info("Aceptar la revision del Ítem", "Puede aceptar los cambios realizados al item")
                            : popover_error("No puede aceptar la revision el Ítem", erroresAceptarRevision)}>
                            <Button variant={puedeAceptarRevision ? "info" : "secondary"}
                                    className="btn-block align-items-start"
                                    onClick={puedeAceptarRevision ? () => dispatch(abrirModal(INTERFAZ_MODAL.ACEPTAR_REVISION)) : sinEvento}>
                                    <span className="icon text-white-50">
                                        <i className="fas fa-edit"/>
                                    </span>
                                {" "}
                                <span className="text">Aceptar revisión</span>
                            </Button>
                        </OverlayTrigger>}

                        {/*SEPARADOR*/}
                        {item.es_ultimo && item.estado !== ESTADO_ITEM.DESACTIVADO &&
                        <><br/><h5>Impacto</h5></>
                        }

                        {/* Trazabilidad */}
                        {item.estado !== ESTADO_ITEM.DESACTIVADO && item.es_ultimo &&
                        <>
                            <ImpactoTotal item={item}/>
                            <OverlayTrigger trigger={["hover", "click"]} placement="auto"
                                            overlay={popover_info("Graficar Trazabilidad", "Muestra la gráfica de la trazabilidad")}>
                                <Button variant="info" className="btn-block align-items-start"
                                        onClick={() => dispatch(abrirTrazabilidadItem({uuid: item_uuid}))}>
                                    <span className="icon text-white-50"><i
                                        className="fas fa-project-diagram"/></span>{" "}<span className="text">Graficar Trazabilidad</span>
                                </Button>
                            </OverlayTrigger>
                        </>
                        }
                        {/* Restaurar Item */}
                        {!item.es_ultimo &&
                        <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeRestaurar ?
                            popover_info("Restaurar Ítem", "Puede restaurar el Ítem")
                            : popover_error("No puede restaurar Ítem", erroresRestaurar)}>
                            <Button variant={puedeRestaurar ? "info" : "secondary"}
                                    className="btn-block align-items-start"
                                    onClick={puedeRestaurar ? () => dispatch(abrirRestaurarItem()) : sinEvento}>
                                <span className="icon text-white-50"><i className="fas fa-history"/></span>{" "}
                                <span className="text">Restaurar Item</span>
                            </Button>
                        </OverlayTrigger>
                        }
                    </Col>
                    </Col>
                </Row>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={cerrarModal}>Salir</Button>
            </Modal.Footer>
        </>
    );
}
