import ReactDOMServer from 'react-dom/server';
import React from "react";
import * as d3_base from "d3";
import * as d3_zoom from "d3-zoom";
import {ItemTrazable} from "./ItemTrazable";
import {hallarColorTexto} from "../../../../utils/colores";

let dagre = require("dagre");
let Graph = require("graphlib").Graph;
let HashMap = require('hashmap');
let Queue = require('queue-fifo');
const d3 = Object.assign({}, d3_base, d3_zoom);

//TODO: Evitar que constrasten los colores de las lineas base con el titulo
const crearGrafo = ({item_actual, items_proyecto}) => { //Asume que todos los items son las ultimas versiones
    //Cada nivel es una fase
    const Candidatos = new Set();
    //Cargamos los items en un hash map
    let items_proyecto_map = new HashMap();
    items_proyecto.forEach(item => items_proyecto_map.set(item.uuid, item));

    //Calculamos el arbol izquierdo preprocesando los items
    let ColaIzquierda = new Queue();
    ColaIzquierda.enqueue(item_actual.uuid); //Encolamos el item actual
    while (!ColaIzquierda.isEmpty()) {
        let uuid = ColaIzquierda.dequeue();
        let item = items_proyecto_map.get(uuid);

        if (item.marcado === undefined) {
            Candidatos.add(item);
            item.marcado = true;
            items_proyecto_map.set(uuid, item);
            //Por cada antecesor encolamos
            item.antecesores.forEach(antecesor_uuid => {
                ColaIzquierda.enqueue(antecesor_uuid);
            });
        }
    }

    let tmp = items_proyecto_map.get(item_actual.uuid);
    tmp.marcado = undefined;
    items_proyecto_map.set(item_actual.uuid, tmp);

    //Calculamos arbol derecho
    let ColaDerecha = new Queue();
    ColaDerecha.enqueue(item_actual.uuid);
    while (!ColaDerecha.isEmpty()) {
        let uuid = ColaDerecha.dequeue();
        let item = items_proyecto_map.get(uuid);

        if (item.marcado === undefined) {
            Candidatos.add(item);
            item.marcado = true;
            items_proyecto_map.set(uuid, item);
            item.relaciones.forEach(sucesor_uuid => {
                ColaDerecha.enqueue(sucesor_uuid);
            });
        }
    }

    let grafo = [];

    //Tengo ambos arboles, crear niveles
    for (let item of Candidatos) {
        let nodo = {
            id: item.uuid,
            item
        };
        let padres = [];
        item.antecesores.forEach(antecesor => {
            let item_antecesor = items_proyecto_map.get(antecesor);
            if (Candidatos.has(item_antecesor)) {
                //Esta dentro del grafo
                padres.push(antecesor);
            }
        });
        if (padres.length > 0) {
            nodo.parentIds = padres;
        }
        grafo.push(nodo);
    }

    return grafo;
}

export const layout_grafo = ({
                                 item_actual = {},
                                 items_proyecto = [],
                                 rankdir = "LR",
                                 curve = d3.curveBasis,
                                 mostrarLB = false,
                                 lista_fases = [],
                                 lista_lb = []
                             }) => {
    let preproceso = crearGrafo({item_actual, items_proyecto});

    let g = new Graph({
        compound: true,
    });

    g.setGraph({
        rankdir
    });

    g.setDefaultEdgeLabel(function () {
        return {};
    });

    const fases_candidatos = new Set();
    const lb_candidatos = new Set();

    const colorItemSeleccionado = "#e9e9e9";

    //Procesar items
    preproceso.forEach(({id, item}) => {
        let componente = <ItemTrazable item={item} key={id} actual={item.uuid === item_actual.uuid}/>;
        let html = ReactDOMServer.renderToString(componente);

        g.setNode(id, {
            labelType: "html",
            label: html,
            width: 230,
            height: 36,
            rx: 5,
            ry: 5,
            style: "stroke-opacity: 0;",
        });
        //Ver si es de una linea base
        if (mostrarLB && item.linea_base !== null) {
            g.setParent(id, item.linea_base);
        } else {
            g.setParent(id, item.fase);
        }

        if (item.linea_base !== null) lb_candidatos.add(item.linea_base);
        fases_candidatos.add(item.fase);
    });

    //Agregar nodos fase
    for (let fase_cand of fases_candidatos) {
        let datos_fase = lista_fases.find(fase => fase.id === fase_cand);
        g.setNode(fase_cand, {
            label: `${datos_fase.numero}. ${datos_fase.nombre}`,
            clusterLabelPos: "top",
        });
    }

    preproceso.forEach(nodo => {
        if (nodo.parentIds !== undefined) {
            nodo.parentIds.forEach(padre => {
                g.setEdge(padre, nodo.id, {artificial: false, curve, arrowhead: "vee"});
            });
        }
    });

    //Agregar nodos de línea base
    //let indexColor = 0;
    if (mostrarLB) {
        for (let lb_cand of lb_candidatos) {
            let lb_datos = lista_lb.find(lb => lb.uuid === lb_cand);

            const color = lb_datos.color;
            g.setNode(lb_cand, {
                labelType: "html",
                label: `<p style="color: ${hallarColorTexto(lb_datos.color)};">${"LB. " + lb_datos.numero + " - " + lb_datos.nombre}</p>`,
                clusterLabelPos: "top",
                rx: 5,
                ry: 5,
                style: `fill: ${color};`,
            });
            g.setParent(lb_cand, lb_datos.fase);
        }
    }

    dagre.layout(g, {
        rankdir: "LR",
    });

    return (g);
};
