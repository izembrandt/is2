import React from "react";
import {ESTADO_ITEM} from "../../../../constantes/estados";

const estado = {
    [ESTADO_ITEM.PENDIENTE]: <span className="badge badge-pill bg-gradient-warning text-gray-100">Pendiente</span>,
    [ESTADO_ITEM.DESARROLLO]: <span className="badge badge-pill bg-gradient-primary text-gray-100">En desarrollo</span>,
    [ESTADO_ITEM.APROBADO]: <span className="badge badge-pill bg-gradient-success text-gray-100">Aprobado</span>,
    [ESTADO_ITEM.EN_LINEA_BASE]: <span className="badge badge-pill bg-gradient-info text-gray-100">En línea base</span>,
    [ESTADO_ITEM.DESACTIVADO]: <span className="badge badge-pill bg-gray-800 text-gray-100">Desactivado</span>,
    [ESTADO_ITEM.REVISION]: <span className="badge badge-pill bg-gradient-danger bg-gray-800 text-gray-100">En revisión</span>,
}

export const ItemTrazable = ({item, actual = false}) => {
    return (
        <div className={"p-2 item-trazable " + (actual ? "item-seleccionado" : "item-no-seleccionado")}>
            <div className="float-left"><b>{item.prefijo + ". " + item.numero}</b></div>
            <div className="float-right">
                {estado[item.estado]}
            </div>
            <br/>
            <div className="overflow-auto">{item.nombre}</div>
        </div>
    );
}