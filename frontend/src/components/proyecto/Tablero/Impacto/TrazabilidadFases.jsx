import React, {useEffect, useRef, useState} from "react";
import {layout_grafo} from "./TrazabilidadLayout";
import {Button, Col, Form} from "react-bootstrap";
import {cerrarTodo, seleccionarItemAbierto, seleccionarModalAbierto} from "../../../../redux/proyecto/modales";
import {useDispatch, useSelector} from "react-redux";
import {INTERFAZ_MODAL} from "../../../../constantes/modales";
import * as d3_base from 'd3';
import * as d3_zoom from 'd3-zoom';

const d3 = Object.assign({}, d3_base, d3_zoom);
const dagreD3 = require("dagre-d3");

import './Trazabilidad.scss';
import {seleccionarItemPorUUID, seleccionarUltimosItems} from "../../../../redux/proyecto/items";
import {seleccionarFasesProyecto} from "../../../../redux/proyecto/fases";
import {seleccionarLineasBaseDelProyecto} from "../../../../redux/proyecto/lineasBase";

export const TrazabilidadFases = () => {
    let dispatch = useDispatch();
    let d3Container = useRef(null);
    //DATOS
    let modalActivo = useSelector(seleccionarModalAbierto);
    let item_actual_uuid = useSelector(seleccionarItemAbierto);
    let item_actual = useSelector(seleccionarItemPorUUID(item_actual_uuid));
    let items_proyecto = JSON.parse(JSON.stringify(useSelector(seleccionarUltimosItems)));
    let lista_fases = useSelector(seleccionarFasesProyecto);
    let lista_lb = useSelector(seleccionarLineasBaseDelProyecto);


    //Opciones de configuración
    let [rankdir, setRankdir] = useState("LR");
    let [mostrarLB, setMostrarLB] = useState(false);


    useEffect(() => {
        if (modalActivo === INTERFAZ_MODAL.TRAZABILIDAD && d3Container.current) {
            let grafo = layout_grafo({
                item_actual,
                items_proyecto,
                rankdir,
                mostrarLB,
                lista_fases,
                lista_lb
            });

            console.log(grafo);

            d3Container.current.innerHTML = '';

            let svg = d3.select(d3Container.current)
                .append("svg")
                .attr("width", "100%")
                .attr("height", "95%")
                .attr("style", "border: 1px solid #ccc!important; border-radius: 14px; cursor: grab;");

            let inner = svg.append("g");


            let zoom = d3.zoom().on("zoom", ({transform}) => {
                inner.attr("transform", transform);
            });

            svg.call(zoom);

            let render = new dagreD3.render();

            render(inner, grafo);

            zoom.translateBy(svg, 14, 14);
            zoom.scaleBy(svg, 1);

        }
    }, [d3Container.current, rankdir, mostrarLB]);

    return (
        <div style={{height: "83vh"}}>
            <Form.Row className="mb-4">
                <Col>
                    <Button variant="primary" onClick={() => dispatch(cerrarTodo())}>Volver al tablero</Button>
                </Col>
                <Col sm={2}>
                    <Form.Check
                        type="checkbox"
                        label="Mostrar líneas bases"
                        inline
                        checked={mostrarLB}
                        onChange={({target})=> setMostrarLB(target.checked)}
                    />
                </Col>
                <Col sm={2}>
                    <Form.Control as="select" value={rankdir} onChange={({target}) => {
                        setRankdir(target.value)
                    }}>
                        <option value="LR">Izquierda a Derecha</option>
                        <option value="RL">Derecha a Izquierda</option>
                        <option value="TB">Arriba a abajo</option>
                        <option value="BT">Abajo a Arriba</option>
                    </Form.Control>
                </Col>
            </Form.Row>
            <div ref={d3Container} style={{height: "95%"}}>

            </div>
        </div>
    );
};