import React, {useState} from "react";
import {useSelector} from "react-redux";
import {
    seleccionarImpactoDeItem, seleccionarImpactoTotal,
} from "../../../../redux/proyecto/items";
import {Button, OverlayTrigger, Popover} from "react-bootstrap";

const ImpactoTotal = ({item}) => {
    let [mostrarCalculo, setMostrarCalculo] = useState(false);
    let arriba = useSelector(seleccionarImpactoDeItem(item)) * 100;
    let abajo = useSelector(seleccionarImpactoTotal);
    let texto = abajo !== 0 ? (" " + (arriba/abajo).toFixed(2) + "%") : "0";


    const popover_info = (titulo, contenido) => {
     return <Popover id="popover-basic">
            <Popover.Title as="h3">{titulo}</Popover.Title>
            <Popover.Content>
                {contenido}
            </Popover.Content>
        </Popover>;
    };
    return (
        <>
            {mostrarCalculo
                ? <Button variant="light" type="button" className="btn-block align-items-start" onClick={()=> setMostrarCalculo(false)}>
                    <span className="text">{texto}</span>
                  </Button>
                :
                <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={popover_info ("Calculo de Impacto", "Muestra el cálculo de impacto")}>
                    <Button variant="info" type="button" className="btn-block align-items-start" onClick={()=> setMostrarCalculo(true)}>
                        <span className="icon text-white-50"><i className="fas fa-share-square"/></span>
                        <span className="text">{" "}Mostrar Impacto</span>
                    </Button>
                </OverlayTrigger>
            }
        </>
    )
}

export default ImpactoTotal;