import React from "react";
import {Card, ListGroup} from "react-bootstrap";
import {AtributoItemMostrar} from "./AtributoItemMostrar";

export const AtributosItemInterfaz = ({ item }) => {

    return(
        <Card>
            <Card.Header>
                Atributos
            </Card.Header>
            <ListGroup variant="flush">
                    {
                        item.atributos.map( attr => (<AtributoItemMostrar key={attr.posicion} atributo={attr}/>))
                    }
            </ListGroup>
        </Card>
    );
}