import React, {useState} from "react";
import {Button, Modal} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {abrirInterfazItem, seleccionarItemAbierto} from "../../../../redux/proyecto/modales";
import {seleccionarItemPorUUID, cancelarSolicitudItem} from "../../../../redux/proyecto/items";
import CSRFToken from "../../../CSRFToken";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";

export const CancelarSolicitud = () => {
    let dispatch = useDispatch();
    let item_uuid = useSelector(seleccionarItemAbierto);
    let item = useSelector(seleccionarItemPorUUID(item_uuid));
    let proyecto = useSelector(seleccionarInfoProyecto);

    const volverInterfaz = () => dispatch(abrirInterfazItem({uuid: item_uuid}));

    const cancelarSolicitud = evento => {
        evento.preventDefault();
        let datos = new FormData(evento.target);
        datos.append("uuid", item_uuid);
        dispatch(cancelarSolicitudItem({datos, id: proyecto.id}))
            .then(()=>dispatch(abrirInterfazItem({ uuid: item_uuid})));
    }

    return (
        <>
            <Modal.Header>
                <Modal.Title>Cancelar Solicitud de "{item.nombre}"</Modal.Title>
            </Modal.Header>
            <form onSubmit={cancelarSolicitud}>
                <CSRFToken/>
                <Modal.Body>
                    Desea cancelar la solicitud de aprobacion de este item?
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={volverInterfaz}>No, Salir</Button>
                    <Button variant="primary" type="submit">Si, Cancelar</Button>
                </Modal.Footer>
            </form>
        </>
    );
}

