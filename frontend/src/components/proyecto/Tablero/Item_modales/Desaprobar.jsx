import React, {useState} from "react";
import {Button, Modal} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {abrirInterfazItem, seleccionarItemAbierto} from "../../../../redux/proyecto/modales";
import {seleccionarItemPorUUID, desaprobarItem as desaprobar} from "../../../../redux/proyecto/items";
import CSRFToken from "../../../CSRFToken";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";

export const Desaprobar = () => {
    let dispatch = useDispatch();
    let item_uuid = useSelector(seleccionarItemAbierto);
    let item = useSelector(seleccionarItemPorUUID(item_uuid));
    let proyecto = useSelector(seleccionarInfoProyecto);

    const volverInterfaz = () => dispatch(abrirInterfazItem({uuid: item_uuid}));

    const desaprobarItem = event => {
        event.preventDefault();
        let datos = new FormData(event.target);
        datos.append("uuid", item_uuid);
        dispatch(desaprobar({ id: proyecto.id, datos}))
            .then(volverInterfaz);
    }

    return (
        <>
            <Modal.Header>
                <Modal.Title>Desaprobar el item {item.nombre}</Modal.Title>
            </Modal.Header>
            <form onSubmit={desaprobarItem}>
                <CSRFToken/>
                <Modal.Body>
                    Desea desaprobar este item?
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={volverInterfaz}>Cancelar</Button>
                    <Button variant="danger" type="submit">Desaprobar</Button>
                </Modal.Footer>
            </form>
        </>
    );
}

