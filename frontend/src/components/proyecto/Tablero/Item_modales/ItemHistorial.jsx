import React from "react";
import {Card, ListGroup, Form} from "react-bootstrap";
import {useSelector} from "react-redux";
import {seleccionarItemPorUUID} from "../../../../redux/proyecto/items";


export const ItemHistorial = ({uuid, onClick}) => {
    let item = useSelector(seleccionarItemPorUUID(uuid));

    return (
        <>
            <Card onClick={onClick}>
                <Card.Header>
                    Version: {item.version}
                </Card.Header>
                <ListGroup variant="flush">
                    <ListGroup.Item>
                        <Form.Group>
                            <Form.Label>Nombre:</Form.Label>
                            <Form.Control type="text" value={item.nombre} disabled/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Descripcion:</Form.Label>
                            <Form.Control as="textarea" value={item.nombre} disabled/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Modificado:</Form.Label>
                            <Form.Control type="text" value={item.fecha} disabled/>
                        </Form.Group>
                    </ListGroup.Item>
                </ListGroup>
            </Card>
            <br/>
        </>
    );
}
