import React from "react";
import {Button, Modal } from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {abrirInterfazItem, seleccionarItemAbierto} from "../../../../redux/proyecto/modales";
import {seleccionarItemPorUUID} from "../../../../redux/proyecto/items";
import {ItemHistorial} from "./ItemHistorial";



export const Historial = () => {
    let dispatch = useDispatch();
    let item_uuid = useSelector(seleccionarItemAbierto);
    let item = useSelector(seleccionarItemPorUUID(item_uuid));

    const volverInterfaz = ()=> dispatch(abrirInterfazItem({uuid: item_uuid}));

    const clickVersion = uuid => _ => dispatch(abrirInterfazItem({ uuid }));

    return (
        <>
            <Modal.Header>
                <Modal.Title>Historial del Item: {item.nombre}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {
                    item.versiones_anteriores.map( version => <ItemHistorial key={version} uuid={version} onClick={clickVersion(version)}/>)
                }
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={volverInterfaz}>Cancelar</Button>
            </Modal.Footer>
        </>
    );
}

