import React, {useState} from "react";
import {Button, Modal, Form, Card, ListGroup} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {abrirInterfazItem, seleccionarItemAbierto} from "../../../../redux/proyecto/modales";
import {seleccionarItemPorUUID, modificarItem} from "../../../../redux/proyecto/items";
import CSRFToken from "../../../CSRFToken";
import {AtributoItem} from "../AtributoItem";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";

export const Modificar = () => {
    let dispatch = useDispatch();
    let item_uuid = useSelector(seleccionarItemAbierto);
    let item = useSelector(seleccionarItemPorUUID(item_uuid));
    let proyecto = useSelector(seleccionarInfoProyecto);

    const volverInterfaz = () => dispatch(abrirInterfazItem({uuid: item_uuid}));

    const submitModificar = evento => {
        evento.preventDefault();
        let datos = new FormData(evento.target);
        datos.append("uuid", item_uuid);
        dispatch(modificarItem({datos, id: proyecto.id}))
            .then(volverInterfaz);

    }

    return (
        <>
            <Modal.Header>
                <Modal.Title>Modificar el item "{item.nombre}"</Modal.Title>
            </Modal.Header>
            <form onSubmit={submitModificar}>
                <Modal.Body>
                    <CSRFToken/>
                    {/* Nombre del item */}
                    <Form.Group>
                        <Form.Label>Nombre del Item*</Form.Label>
                        <Form.Control type="text" name="nombre" maxLength="50" defaultValue={item.nombre} required/>
                    </Form.Group>
                    {/* Descripcion del Item */}
                    <Form.Group>
                        <Form.Label>Descripción del Item</Form.Label>
                        <Form.Control as="textarea" name="descripcion" cols="40" rows="10" defaultValue={item.descripcion}/>
                    </Form.Group>
                    {/* Costo de Impacto */}
                    <Form.Group>
                        <Form.Label>Costo de impacto*</Form.Label>
                        <Form.Control type="number" name="costo" required defaultValue={item.costo}/>
                    </Form.Group>
                    {/* Atributos */}
                    <Form.Group>
                        <Card>
                            <Card.Header>
                                Atributos
                            </Card.Header>
                            <ListGroup>
                                {
                                    item.atributos.map( attr => <AtributoItem atributo={attr} key={attr.posicion}/>)
                                }
                            </ListGroup>
                        </Card>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={volverInterfaz}>Cancelar</Button>
                    <Button variant="primary" type="submit">Modificar Item</Button>
                </Modal.Footer>
            </form>
        </>
    );
}

