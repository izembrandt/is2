import React from "react";
import {Form, Modal, Button} from "react-bootstrap";
import {abrirInterfazItem, seleccionarItemAbierto} from "../../../../redux/proyecto/modales";
import {useDispatch, useSelector} from "react-redux";
import {seleccionarItemPorUUID} from "../../../../redux/proyecto/items";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";
import CSRFToken from "../../../CSRFToken";
import {crearSolicitudCambio} from "../../../../redux/proyecto/comite";

export const SolicitarCambioModal = () => {
    let dispatch = useDispatch();
    let item_uuid = useSelector(seleccionarItemAbierto);
    let item = useSelector(seleccionarItemPorUUID(item_uuid));
    let proyecto = useSelector(seleccionarInfoProyecto);

    const volverInterfaz = () => dispatch(abrirInterfazItem({uuid: item_uuid}));

    const submit = event => {
        event.preventDefault();
        let datos = new FormData(event.target);
        datos.append("item_solicitante", item_uuid);
        dispatch(crearSolicitudCambio({id: proyecto.id, formData: datos}))
            .then(volverInterfaz);
    }

    return (
        <>
            <Modal.Header>
                <Modal.Title>Crear una solicitud de cambio</Modal.Title>
            </Modal.Header>
            <form onSubmit={submit}>
                <CSRFToken/>
                <Modal.Body>
                    <Form.Group>
                        <Form.Label>Estas seguro que deseas solicitar el cambio de "{item.nombre}"? Ingrese en el cuadro
                            de
                            texto la palabra "<b>SOLICITAR</b>" en mayusculas y sin comillas para confirmar la
                            solicitud. Una solicitud aprobada implica que la línea base del ítem se romperá y los ítem
                            contenidos en ella estarán en revisión.
                        </Form.Label>
                        <Form.Control type="text" name="confirmacion" maxLength="50"
                                      required pattern="SOLICITAR" placeholder="SOLICITAR"/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Mótivo de la solicitud (opcional)</Form.Label>
                        <Form.Control as="textarea" name="motivo" cols="40" rows="5" defaultValue="Sin motivo"/>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={volverInterfaz}>Cancelar</Button>
                    <Button type="submit" variant="warning">Solicitar cambio</Button>
                </Modal.Footer>
            </form>
        </>
    );
}