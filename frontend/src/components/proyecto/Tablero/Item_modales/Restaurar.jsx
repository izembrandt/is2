import React, {useState} from "react";
import {Button, Modal} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {abrirInterfazItem, seleccionarItemAbierto, cerrarTodo} from "../../../../redux/proyecto/modales";
import {seleccionarItemPorUUID, restaurarItem} from "../../../../redux/proyecto/items";
import CSRFToken from "../../../CSRFToken";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";

export const Restaurar = () => {
    let dispatch = useDispatch();
    let item_uuid = useSelector(seleccionarItemAbierto);
    let item = useSelector(seleccionarItemPorUUID(item_uuid));
    let proyecto = useSelector(seleccionarInfoProyecto);

    const volverInterfaz = () => dispatch(abrirInterfazItem({uuid: item_uuid}));

    const submitRestaurar = event => {
        event.preventDefault();
        let datos = new FormData(event.target);
        datos.append("uuid", item_uuid);
        dispatch(restaurarItem({ id: proyecto.id, datos}))
            .then(dispatch(cerrarTodo()));
    }

    return (
        <>
            <Modal.Header>
                <Modal.Title>Restaurar item "{item.nombre}" </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                Seguro que desea restaurar el item "{item.nombre}" a la version "{item.version}"?
            </Modal.Body>
            <Modal.Footer>
                <form onSubmit={submitRestaurar}>
                    <CSRFToken/>
                    <Button variant="secondary" onClick={volverInterfaz}>Cancelar</Button>
                    {" "}
                    <Button variant="primary" type="submit">Restaurar</Button>
                </form>
            </Modal.Footer>
        </>
    );
}

