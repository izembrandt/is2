import React from "react";
import {Button, Modal, Row, Col, Card, ListGroup} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {abrirRelacionarItem, seleccionarItemAbierto} from "../../../../redux/proyecto/modales";
import {
    seleccionarItemPorUUID,
    relacionarItem, seleccionarAntecesoresCandidatosPorUUID
} from "../../../../redux/proyecto/items";
import {seleccionarFasePorID, seleccionarFasePorNumero} from "../../../../redux/proyecto/fases";
import {ItemCandidato} from "../Relacionar/ItemCandidato";
import Cookies from "js-cookie";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";

export const CrearRelacionAntecesor = () => {
    let dispatch = useDispatch();
    let item_uuid = useSelector(seleccionarItemAbierto);
    let item = useSelector(seleccionarItemPorUUID(item_uuid));
    let fase_item = useSelector(seleccionarFasePorID(item.fase));
    let { nombre } = useSelector(seleccionarFasePorNumero(fase_item.numero - 1));
    let candidatos = useSelector(seleccionarAntecesoresCandidatosPorUUID(item_uuid));
    let proyecto = useSelector(seleccionarInfoProyecto);

    const volverInterfaz = () => dispatch(abrirRelacionarItem());

    const relacionarClick = uuid => () => {
        //Recogemos datos
        let datos = new FormData();
        datos.append("uuid_origen", uuid);
        datos.append("uuid_destino", item_uuid);
        datos.append("csrfmiddlewaretoken", Cookies.get('csrftoken'));
        dispatch(relacionarItem({ datos, id: proyecto.id}))
            .then(volverInterfaz);
    };

    return (
        <>
            <Modal.Header>
                <Modal.Title>Crear relación padre para "{item.nombre}"</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="fase-lista-de-items" style={{width: "auto"}}>
                    <header>{ nombre }</header>
                    <ul>
                        {
                         candidatos.map( cand => <ItemCandidato item={cand} key={cand.uuid} onClick={relacionarClick(cand.uuid)}/>)
                        }
                    </ul>
                    <footer>{ nombre }</footer>
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={volverInterfaz}>Cancelar</Button>
            </Modal.Footer>
        </>
    );
}
