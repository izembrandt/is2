import React, {useState} from "react";
import {Button, Modal} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {abrirInterfazItem, seleccionarItemAbierto} from "../../../../redux/proyecto/modales";
import {aprobarItem, seleccionarItemPorUUID} from "../../../../redux/proyecto/items";
import CSRFToken from "../../../CSRFToken";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";

export const AprobarSolicitud = () => {
    let dispatch = useDispatch();
    let item_uuid = useSelector(seleccionarItemAbierto);
    let item = useSelector(seleccionarItemPorUUID(item_uuid));
    let proyecto = useSelector(seleccionarInfoProyecto);

    const volverInterfaz = () => dispatch(abrirInterfazItem({uuid: item_uuid}));

    const aprobarItemSubmit = event => {
        event.preventDefault();
        let datos = new FormData(event.target);
        datos.append("uuid", item_uuid);
        dispatch(aprobarItem({ id: proyecto.id, datos}))
            .then(volverInterfaz);
    }

    return (
        <>
            <Modal.Header>
                <Modal.Title>Aprobar Solicitud de {item.nombre}</Modal.Title>
            </Modal.Header>
            <form onSubmit={aprobarItemSubmit}>
                <CSRFToken/>
                <Modal.Body>
                    Desea aprobar este item?
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={volverInterfaz}>Cancelar</Button>
                    <Button variant="primary" type="submit">Aprobar</Button>
                </Modal.Footer>
            </form>
        </>
    );
}

