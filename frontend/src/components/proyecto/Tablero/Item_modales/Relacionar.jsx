import React from "react";
import {Button, Modal, Row, Col, Card, ListGroup, OverlayTrigger, Popover} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {abrirCrearPadre, abrirInterfazItem, seleccionarItemAbierto, abrirCrearAntecesor} from "../../../../redux/proyecto/modales";
import {
    seleccionarItemPorUUID,
    seleccionarPadresPorUUID,
    seleccionarAntecesoresPorUUID,
    verificarSiPuedeDesaprobar
} from "../../../../redux/proyecto/items";
import {ItemRelacionado} from "../Relacionar/ItemRelacionado";
import {seleccionarFasePorID} from "../../../../redux/proyecto/fases";
import {seleccionarInfoProyecto, verificarPermisoFase} from "../../../../redux/proyecto/proyecto";
import {PERMISOS_FASE} from "../../../../constantes/permisos";
import {ESTADO_ITEM, ESTADO_PROYECTO} from "../../../../constantes/estados";

export const Relacionar = () => {
    const proyecto = useSelector(seleccionarInfoProyecto);
    let dispatch = useDispatch();
    let item_uuid = useSelector(seleccionarItemAbierto);
    let item = useSelector(seleccionarItemPorUUID(item_uuid));
    let fase_item = useSelector(seleccionarFasePorID(item.fase));
    let padres = useSelector(seleccionarPadresPorUUID(item_uuid));
    let antecesores = useSelector(seleccionarAntecesoresPorUUID(item_uuid));
    let puedeCrearPadre = useSelector(verificarPermisoFase(PERMISOS_FASE.RELACIONAR_ITEM, item.fase))
        && item.estado === ESTADO_ITEM.DESARROLLO
        && item.es_ultimo
        && proyecto.estado !== ESTADO_PROYECTO.CANCELADO
        && proyecto.estado !== ESTADO_PROYECTO.FINALIZADO;
    let puedeCrearAntecesor = useSelector(verificarPermisoFase(PERMISOS_FASE.RELACIONAR_ITEM, item.fase))
        && fase_item.numero > 1
        && item.estado === ESTADO_ITEM.DESARROLLO
        && item.es_ultimo
        && proyecto.estado !== ESTADO_PROYECTO.CANCELADO
        && proyecto.estado !== ESTADO_PROYECTO.FINALIZADO;
    let nada = () => {};

    const volverInterfaz = () => dispatch(abrirInterfazItem({uuid: item_uuid}));
    const botonCrearPadre = () => dispatch(abrirCrearPadre());
    const botonCrearAntecesor = () => dispatch(abrirCrearAntecesor());

    let erroresPadre = [];
    if(item.estado !== ESTADO_ITEM.DESARROLLO){
        erroresPadre.push((<li key="desarrollo">El item debe estar en Desarrollo</li>))
    }
    if(proyecto.estado === ESTADO_PROYECTO.CANCELADO){
        erroresPadre.push((<li key="proyecto_cancelado">El proyecto no debe estar cancelado</li>))
    }
    if(proyecto.estado === ESTADO_PROYECTO.FINALIZADO){
        erroresPadre.push((<li key="proyecto_finalizado">El proyecto no debe estar finalizado</li>))
    }

    let erroresAntecesor = [];
    if(fase_item.numero === 1){
        erroresAntecesor.push((<li key="uno">El item esta en la primera Fase</li>))
    }
    if(item.estado !== ESTADO_ITEM.DESARROLLO){
        erroresAntecesor.push((<li key="desarrollo">El item debe estar en Desarrollo</li>))
    }
    if(proyecto.estado === ESTADO_PROYECTO.CANCELADO){
        erroresAntecesor.push((<li key="proyecto_cancelado">El proyecto no debe estar cancelado</li>))
    }
    if(proyecto.estado === ESTADO_PROYECTO.FINALIZADO){
        erroresAntecesor.push((<li key="proyecto_finalizado">El proyecto no debe estar finalizado</li>))
    }

    if (!item.es_ultimo) {
        const versionAnteriorError = <li key="version_anterior">Este ítem es una versión anterior</li>;
        erroresPadre.push(versionAnteriorError);
        erroresAntecesor.push(versionAnteriorError);
    }


   const popover_info = (titulo, contenido) => {
         return <Popover id="popover-basic">
                <Popover.Title as="h3">{titulo}</Popover.Title>
                <Popover.Content>
                    {contenido}
                </Popover.Content>
         </Popover>;
    };
   const popover_error = (titulo, errores) =>
    {
        return <Popover id="popover-basic">
            <Popover.Title as="h3">{titulo}</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    };


    return (
        <>
            <Modal.Header>
                <Modal.Title>Relaciones del item "{item.nombre}"</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Row>
                    {/* Lado izquierdo */}
                    <Col xs={12} lg={8}>
                        {/* Padres */}
                        <Col xs={12}>
                            <Card>
                                <Card.Header>
                                    <h6 className="m-0 font-weight-bold text-primary">Padres</h6>
                                </Card.Header>
                                <ListGroup variant="flush">
                                    {padres.length > 0
                                        ? padres.map(padre => <ItemRelacionado item={padre} key={padre.uuid} uuid_destino={item_uuid}/>)
                                        : <ListGroup.Item>No posee padres</ListGroup.Item>
                                    }
                                </ListGroup>
                            </Card>
                        </Col>
                        {/* Antecesores */}
                        <Col xs={12} className="my-2">
                            <Card>
                                <Card.Header>
                                    <h6 className="m-0 font-weight-bold text-primary">Antecesores</h6>
                                </Card.Header>
                                <ListGroup variant="flush">
                                    {antecesores.length > 0
                                        ? antecesores.map( antecesor => <ItemRelacionado item={antecesor} key={antecesor.uuid} uuid_destino={item_uuid}/>)
                                        : <ListGroup.Item>No posee antecesores</ListGroup.Item>
                                    }
                                </ListGroup>
                            </Card>
                        </Col>
                    </Col>
                    {/* Lado Derecho */}
                    <Col xs={12} lg={4}>
                        <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeCrearPadre?
                                     popover_info ("Agregar padre", "Puede agregar una relacion padre al item")
                                     : popover_error ("No puede agregar un padre al item",erroresPadre)}>
                            <Button variant={puedeCrearPadre ? "info" : "secondary"} className="btn-block" onClick={puedeCrearPadre ? botonCrearPadre : nada}>
                            Crear Padre
                        </Button>
                        </OverlayTrigger>

                        <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeCrearAntecesor?
                                     popover_info ("Agregar antecesor", "Puede agregar una relacion antecesor al item")
                                     : popover_error ("No puede agregar un antecesor al item",erroresAntecesor)}>
                            <Button variant={puedeCrearAntecesor ? "info" : "secondary"} className="btn-block" onClick={puedeCrearAntecesor ? botonCrearAntecesor : nada}>
                                    Crear Antecesor
                            </Button>
                        </OverlayTrigger>
                    </Col>
                </Row>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={volverInterfaz}>Atras</Button>
            </Modal.Footer>
        </>
    );
}

