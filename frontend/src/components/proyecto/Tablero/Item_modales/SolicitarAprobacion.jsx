import React, {useState} from "react";
import {Button, Modal} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {abrirInterfazItem, seleccionarItemAbierto} from "../../../../redux/proyecto/modales";
import {seleccionarItemPorUUID, solicitarAprobacionItem} from "../../../../redux/proyecto/items";
import CSRFToken from "../../../CSRFToken";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";

export const SolicitarAprobacion = () => {
    let dispatch = useDispatch();
    let item_uuid = useSelector(seleccionarItemAbierto);
    let item = useSelector(seleccionarItemPorUUID(item_uuid));
    let proyecto = useSelector(seleccionarInfoProyecto);

    const volverInterfaz = () => dispatch(abrirInterfazItem({uuid: item_uuid}));

    let solicitarAprobacion = event => {
        event.preventDefault();
        let datos = new FormData(event.target);
        datos.append("uuid", item_uuid);
        dispatch(solicitarAprobacionItem({ datos, id: proyecto.id}))
            .then(()=>dispatch(abrirInterfazItem({ uuid: item_uuid })));
    }

    return (
        <>
            <Modal.Header>
                <Modal.Title>Solicitar Aprobación de "{item.nombre}"</Modal.Title>
            </Modal.Header>
            <form onSubmit={solicitarAprobacion}>
                <CSRFToken/>
                <Modal.Body>
                    Desea solicitar la aprobación de este item?
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={volverInterfaz}>Cancelar</Button>
                    <Button variant="primary" type="submit">Solicitar</Button>
                </Modal.Footer>
            </form>
        </>
    );
}

