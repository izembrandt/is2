import React, {useState} from "react";
import {Button, Modal, Form} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {abrirInterfazItem, seleccionarItemAbierto} from "../../../../redux/proyecto/modales";
import {desactivarItem as desactivar, seleccionarItemPorUUID} from "../../../../redux/proyecto/items";
import CSRFToken from "../../../CSRFToken";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";

export const Desactivar = () => {
    let dispatch = useDispatch();
    let item_uuid = useSelector(seleccionarItemAbierto);
    let item = useSelector(seleccionarItemPorUUID(item_uuid));
    let proyecto = useSelector(seleccionarInfoProyecto);


    const volverInterfaz = () => dispatch(abrirInterfazItem({uuid: item_uuid}));

    const desactivarItem = event => {
        event.preventDefault();
        let datos = new FormData(event.target);
        datos.append("uuid", item_uuid);
        dispatch(desactivar({id: proyecto.id, datos}))
            .then(volverInterfaz);
    }

    return (
        <>
            <Modal.Header>
                <Modal.Title>Desactivar el item "{item.nombre}"</Modal.Title>
            </Modal.Header>
            <form onSubmit={desactivarItem}>
                <CSRFToken/>
                <Modal.Body>
                    <Form.Label>Estas seguro que deseas eliminar "{item.nombre}"? Ingrese en el cuadro de texto la
                    palabra "<b>DESACTIVAR</b>" en en mayusculas y sin comillas para confirmar la desactivación.
                    </Form.Label>
                    <Form.Control type="text" name="confirmacion" maxLength="50"
                           required pattern="DESACTIVAR"/>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={volverInterfaz}>Cancelar</Button>
                    <Button variant="danger" type="submit">Desactivar</Button>
                </Modal.Footer>
            </form>
        </>

);
}

