import React from "react";
import {Modal, Form, Button} from "react-bootstrap";
import CSRFToken from "../../../CSRFToken";
import {useDispatch, useSelector} from "react-redux";
import {abrirInterfazItem, cerrarTodo, seleccionarItemAbierto} from "../../../../redux/proyecto/modales";
import {revisarItem} from "../../../../redux/proyecto/items";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";

const RevisarItemModal = () => {
    let dispatch = useDispatch();
    let item_uuid = useSelector(seleccionarItemAbierto);
    let proyecto = useSelector(seleccionarInfoProyecto);

    const volverInterfaz = () => dispatch(abrirInterfazItem({uuid: item_uuid}));

    const submit = event => {
        event.preventDefault();
        const datos = new FormData(event.target);
        datos.append("item", item_uuid);
        dispatch(revisarItem({ id: proyecto.id, datos}))
            .then( () => dispatch(cerrarTodo()));
    }

    return (
        <>
            <Modal.Header>
                <Modal.Title>Revisar ítem</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Group>
                    <Form.Label>¿Estás seguro que deseas revisar este ítem? Todos los
                        hijos/sucesores del item que esten en línea base se marcarán como
                        que necesitarán revisión
                    </Form.Label>
                </Form.Group>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={volverInterfaz}>Cancelar</Button>
                <form onSubmit={submit}>
                    <CSRFToken/>
                    <Button variant="primary"
                            type="submit">
                        Revisar ítem
                    </Button>
                </form>
            </Modal.Footer>
        </>
    );
}


export default RevisarItemModal;