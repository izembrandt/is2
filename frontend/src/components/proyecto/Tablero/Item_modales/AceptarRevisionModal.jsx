import React from "react";
import {Modal, Form, Button} from "react-bootstrap";
import CSRFToken from "../../../CSRFToken";
import {useDispatch, useSelector} from "react-redux";
import {abrirInterfazItem, cerrarTodo, seleccionarItemAbierto} from "../../../../redux/proyecto/modales";
import {aceptarRevisionItem} from "../../../../redux/proyecto/items";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";

const AceptarRevisionModal = () => {
    let dispatch = useDispatch();
    let item_uuid = useSelector(seleccionarItemAbierto);
    let proyecto = useSelector(seleccionarInfoProyecto);

    const volverInterfaz = () => dispatch(abrirInterfazItem({uuid: item_uuid}));

    const submit = event => {
        event.preventDefault();
        const datos = new FormData(event.target);
        datos.append("item", item_uuid);
        dispatch(aceptarRevisionItem({id: proyecto.id, datos: datos}))
            .then(()=>dispatch(cerrarTodo()));
    }

    return (
        <>
            <Modal.Header>
                <Modal.Title>Aceptar revisión de item</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Group>
                    <Form.Label>Se aceptará la revisión del item y pasará a su estado anterior(En linea base o aprobado)</Form.Label>
                </Form.Group>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={volverInterfaz}>Cancelar</Button>
                <form onSubmit={submit}>
                    <CSRFToken/>
                    <Button
                        type="submit"
                        variant="primary">Aceptar Revisión</Button>
                </form>
            </Modal.Footer>
        </>
    );
}

export default AceptarRevisionModal;