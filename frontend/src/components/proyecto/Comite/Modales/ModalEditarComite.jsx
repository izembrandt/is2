import React, {useEffect, useState} from "react";
import {Button, Modal, Form} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import CSRFToken from "../../../CSRFToken";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";
import {cerrarTodo} from "../../../../redux/proyecto/modales";
import {fetchListaParticipantes, seleccionarListaParticipantesProyecto} from "../../../../redux/proyecto/participantes";
import DualListBox from "react-dual-listbox";
import {asignarComite, seleccionarComiteProyecto} from "../../../../redux/proyecto/comite";

export const ModalEditarComponente = () => {
    let dispatch = useDispatch();
    let proyecto = useSelector(seleccionarInfoProyecto);
    let comite = useSelector(seleccionarComiteProyecto);

    let participantes = useSelector(seleccionarListaParticipantesProyecto)
        .map(persona => ({
            value: persona.email,
            label: `${persona.first_name} ${persona.last_name} - ${persona.email}`
        }));

    let [miembros, setMiembros] = useState(comite.integrantes.map(persona => (persona.correo)));

    const numeroInvalido = miembros.length < 3 || miembros.length % 2 === 0;

    const cerrarModal = () => dispatch(cerrarTodo());

    const ModificarComiteSubmit = event => {
        event.preventDefault();
        const datos = new FormData(event.target)
        miembros.forEach(email => datos.append("integrantes", email));
        dispatch(asignarComite({id: proyecto.id, formData: datos}))
            .then(() => dispatch(cerrarTodo()));
    }

    //Descargar lista de participantes
    useEffect(() => {
        if (proyecto.id !== 0) {
            dispatch(fetchListaParticipantes(proyecto.id))
        }
    }, [dispatch, proyecto]);

    let Contador = () => {
        const estiloContador = {
            color: numeroInvalido ? "red" : "green",
        }

        return (
            <Form.Label>Numero de integrantes: <h1 style={estiloContador}>{miembros.length}</h1></Form.Label>
        );
    }

    return (
        <>
            <Modal.Header>
                <Modal.Title>Modificar integrantes cómite de cambios</Modal.Title>
            </Modal.Header>
            <form onSubmit={ModificarComiteSubmit}>
                <CSRFToken/>
                <Modal.Body>
                    <Form.Group>
                        <Form.Label>Seleccione los miembros que conformaran el cómite:</Form.Label>
                        <DualListBox options={participantes}
                                     onChange={miembros => setMiembros(miembros)}
                                     selected={miembros}
                                     canFilter
                                     filterPlaceholder="Buscar..."
                        />
                    </Form.Group>
                    <div style={{textAlign: "right"}}>
                        <Contador/>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={cerrarModal}>Cancelar</Button>
                    <Button variant="primary" type="submit" disabled={numeroInvalido}>Modificar</Button>
                </Modal.Footer>
            </form>
        </>
    );
}

