import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {cerrarTodo, seleccionarModalAbierto} from "../../../redux/proyecto/modales";
import {Modal} from "react-bootstrap";
import {INTERFAZ_MODAL} from "../../../constantes/modales";
import {ModalCrearComite} from "./Modales/ModalCrearComite";
import {ModalEditarComponente} from "./Modales/ModalEditarComite";

export const ModalesComite = () => {
    let dispatch = useDispatch();
    let modalActivo = useSelector(seleccionarModalAbierto);

    const salirModal = () => dispatch(cerrarTodo());

    return(
      <div>
          <Modal show={modalActivo === INTERFAZ_MODAL.CREAR_COMITE} onHide={salirModal} centered size="lg">
            <ModalCrearComite/>
          </Modal>
          <Modal show={modalActivo === INTERFAZ_MODAL.MODIFICAR_COMITE} onHide={salirModal} centered size="lg">
            <ModalEditarComponente/>
          </Modal>
      </div>
    );
}
