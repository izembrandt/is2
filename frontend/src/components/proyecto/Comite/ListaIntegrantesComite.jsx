import React from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import {Card, Button, Popover} from "react-bootstrap";
import Cabecera from "../Cabecera/Cabecera";
import {useDispatch, useSelector} from "react-redux";
import {abrirModal} from "../../../redux/proyecto/modales";
import {INTERFAZ_MODAL} from "../../../constantes/modales";
import {seleccionarInfoProyecto, verificarPermisoProyecto} from "../../../redux/proyecto/proyecto";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import {ESTADO_PROYECTO} from "../../../constantes/estados";
import {OverlayTrigger} from "react-bootstrap";

const columnas = [
    {
        text: "Nombre completo",
        dataField: 'nombre',
        sort: true,
        formatter: (cell, row) => `${row.nombre} ${row.apellido}`

    },
    {
        text: "Correo electrónico",
        dataField: 'correo',
        sort: true
    }
]

export const ListaIntegrantesComite = ({comite}) => {
    let dispatch = useDispatch();
    let proyecto = useSelector(seleccionarInfoProyecto);
    let puedeModificar = useSelector(verificarPermisoProyecto(PERMISOS_PROYECTO.ASIGNAR_COMITE))
        && (proyecto.estado === ESTADO_PROYECTO.EJECUCION || proyecto.estado === ESTADO_PROYECTO.PENDIENTE);
    let sinEventoLista = () => {};

    let erroresModificar = [];
    if(!useSelector(verificarPermisoProyecto(PERMISOS_PROYECTO.ASIGNAR_COMITE))){
        erroresModificar.push((<li key="modificar_comite">No posee el permiso para modificar comité</li>))
    }
    if(proyecto.estado === ESTADO_PROYECTO.CANCELADO){
        erroresModificar.push((<li key="proyecto_cancelado">EL proyecto ya esta cancelado y no puede ser modificado</li>))
    }
    if(proyecto.estado === ESTADO_PROYECTO.FINALIZADO){
        erroresModificar.push((<li key="proyecto_finalizado">EL proyecto ya esta finalizado y no puede ser modificado</li>))
    }
     const popover_info = (titulo, contenido) => {
     return <Popover id="popover-basic">
            <Popover.Title as="h3">{titulo}</Popover.Title>
            <Popover.Content>
                {contenido}
            </Popover.Content>
        </Popover>;
    };
   const popover_error = (titulo, errores) =>
    {
        return <Popover id="popover-basic">
            <Popover.Title as="h3">{titulo}</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    };

    return (
        <>
            <Cabecera titulo="Comité de cambios"
                      descripcion="Se encarga de aprobar/rechazar las solicitudes de cambio en el proyecto"/>

            <Card className="shadow mb-4">
                <Card.Header className="py-3 container-fluid">
                    <div className="row">
                        <div className="col-12 col-xl-10 col-lg-9 col-md-8 my-auto">
                            <h6 className="m-0 font-weight-bold text-primary">Miembros del comité</h6>
                        </div>
                    </div>
                </Card.Header>
                <Card.Body>
                    <BootstrapTable
                        keyField="correo"
                        data={comite.integrantes}
                        columns={columnas}
                        noDataIndication="No hay integrantes"
                        pagination={paginationFactory()}
                    />
                </Card.Body>
            </Card>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeModificar?
                popover_info ("Modificar Comité", "Puede modificar el comité de cambio")
                : popover_error ("No puede modificar el comité de cambio",erroresModificar)}>
                <Button variant={puedeModificar? "primary" : "secondary" } onClick={puedeModificar? () => dispatch(abrirModal(INTERFAZ_MODAL.MODIFICAR_COMITE)) : sinEventoLista}>Modificar comité</Button>
            </OverlayTrigger>
        </>
    );
}
