import React from "react";
import {Modal} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {cerrarTodo, seleccionarModalAbierto} from "../../../redux/proyecto/modales";
import {INTERFAZ_MODAL} from "../../../constantes/modales";
import VerSolicitudModal from "./Modales/VerSolicitudModal";
import VotarModal from "./Modales/VotarModal";
import ReporteSolicitudModal from "./Modales/ReporteSolicitudModal";

const ModalesSolicitud = () => {
    let dispatch = useDispatch();
    let modalAbierto = useSelector(seleccionarModalAbierto);

    let cerrar = () => dispatch(cerrarTodo());

    return (
        <>
            <Modal show={modalAbierto === INTERFAZ_MODAL.VER_SOLICITUD} size="lg" onHide={cerrar} centered>
                <VerSolicitudModal/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.VOTAR_SOLICITUD} onHide={cerrar} centered>
                <VotarModal/>
            </Modal>
            <Modal show={modalAbierto === INTERFAZ_MODAL.REPORTE_SOLICITUD} onHide={cerrar} centered>
                <ReporteSolicitudModal/>
            </Modal>
        </>
    )
}

export default ModalesSolicitud;
