import React from "react";
import {Card, Button} from "react-bootstrap";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import {useDispatch, useSelector} from "react-redux";
import {seleccionarSolicitudes} from "../../../redux/proyecto/comite";
import ItemSolicitud from "./ItemSolicitud";
import {AccionesSolicitud} from "./AccionesSolicitud";
import {abrirModal} from "../../../redux/proyecto/modales";
import {INTERFAZ_MODAL} from "../../../constantes/modales";

const columnaSolicitudes = [
    {
        text: 'Solicitante',
        dataField: 'usuario_solicitante'
    },
    {
        text: 'Item',
        dataField: 'item_solicitante',
        formatter: (cell, _) => <ItemSolicitud item_uuid={cell}/>
    },
    {
        text: 'Motivo',
        dataField: 'motivo',
        formatter: (cell, _) => (cell.length === 0 ? 'Sin motivos' : cell)
    },
    {
        text: 'Fecha',
        dataField: 'fecha_solicitud'
    },
    {
        text: 'Acciones',
        dataField: 'uuid',
        formatter: (cell, row) => <AccionesSolicitud solicitud={row}/>
    }
]

export const ListaSolicitudes = () => {
    let dispatch = useDispatch();
    let solicitudes = useSelector(seleccionarSolicitudes);

    const abrirReporte = () => {
        dispatch(abrirModal(INTERFAZ_MODAL.REPORTE_SOLICITUD));
    }

    return(
        <Card className="shadow mb-4">
            <Card.Header className="py-3 container-fluid">
                <div className="row">
                    <div className="col-12 col-xl-10 col-lg-9 col-md-8 my-auto">
                        <h6 className="m-0 font-weight-bold text-primary">Solicitudes de cambio</h6>
                    </div>
                    <div className="my-auto">
                        <Button variant="primary" onClick={abrirReporte}>
                            Reporte de solicitudes
                        </Button>
                    </div>
                </div>
            </Card.Header>
            <Card.Body>
                <BootstrapTable
                    keyField="uuid"
                    data={solicitudes}
                    columns={columnaSolicitudes}
                    noDataIndication="No hay solicitudes"
                    pagination={paginationFactory()}
                />
            </Card.Body>
        </Card>
    );
}