import React from "react";
import {hallarColorTexto} from "../../../utils/colores";
import {useSelector} from "react-redux";
import {seleccionarItemPorUUID} from "../../../redux/proyecto/items";
import {seleccionarLineaBasePorUUID} from "../../../redux/proyecto/lineasBase";
import {Badge} from "react-bootstrap";


const ItemSolicitud = ({item_uuid}) => {
    let item = useSelector(seleccionarItemPorUUID(item_uuid));
    let lineaBase = useSelector(seleccionarLineaBasePorUUID(item.linea_base));


    //Estilo
    let estiloLB = {
        "backgroundColor": lineaBase !== undefined ? lineaBase.color : "white",
        "color": lineaBase !== undefined ? hallarColorTexto(lineaBase.color) : "black",
    }

    return (
        <div className="m-auto overflow-auto">
            <div style={{color: "black"}}>{`${item.prefijo}. ${item.numero} - ${item.nombre}`}</div>
            {lineaBase !== undefined
                ? <Badge style={estiloLB} className="my-2">{lineaBase.nombre.toUpperCase()}</Badge>
                : <br/>
            }
        </div>
    );
}

export default ItemSolicitud;