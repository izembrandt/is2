import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {abrirModal, setSolicitud} from "../../../redux/proyecto/modales";
import {INTERFAZ_MODAL} from "../../../constantes/modales";
import {seleccionarPerfilUsuario} from "../../../redux/perfil";
import {seleccionarComiteProyecto} from "../../../redux/proyecto/comite";
import styles from "../Fases/css/estilo.module.css";
import {OverlayTrigger, Popover} from "react-bootstrap";

export const AccionesSolicitud = ({solicitud}) => {
    let dispatch = useDispatch();
    let usuario = useSelector(seleccionarPerfilUsuario);
    let comite = useSelector(seleccionarComiteProyecto);
    const uuid = solicitud.uuid;
    let correos = [];
    let puedeVotar;

    if(comite === null) {
        puedeVotar = false;
    }else{
        correos = comite.integrantes.map(valor => valor.correo);
        puedeVotar = correos.includes(usuario.correo);
    }
    let nada = () => {};

    const abrirModalSolicitud = modal => () => {
        dispatch(setSolicitud(uuid));
        dispatch(abrirModal(modal));
    }
    const popover_info = (titulo, contenido) => {
         return <Popover id="popover-basic">
                <Popover.Title as="h3">{titulo}</Popover.Title>
                <Popover.Content>
                    {contenido}
                </Popover.Content>
         </Popover>;
    };

    const AccionVer = () => (<OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={
                            popover_info ("Ver Solicitud", "Ver la informacion sobre la votacion de la solicitud de cambio")}>
                                <a href="#"
                                onClick={abrirModalSolicitud(INTERFAZ_MODAL.VER_SOLICITUD)}>Ver</a>
                            </OverlayTrigger>);


    const AccionVotar = () => (<OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={ puedeVotar
                                    ? popover_info ("Votar esta solicitud", "Votar para aprobar o rechazar la solicitud de cambio")
                                    : popover_info ("No puedes votar", "Debe ser miembro del comite para votar")}>
                                        <a href="#" className={puedeVotar ? "" : styles.isDisabled}
                                        onClick={puedeVotar ? abrirModalSolicitud(INTERFAZ_MODAL.VOTAR_SOLICITUD) : nada}>Votar</a>
                                </OverlayTrigger>);
    return (
        <p>
            <AccionVer/>{" / "}<AccionVotar/>
        </p>
    );
};