import React from "react";
import {Row, Modal, Button, Col, Form} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {cerrarTodo, seleccionarSolicitudAbierta} from "../../../../redux/proyecto/modales";
import {seleccionarSolicitudUUID} from "../../../../redux/proyecto/comite";
import ItemSolicitud from "../ItemSolicitud";
import { Doughnut } from 'react-chartjs-2';

const VerSolicitudModal = () => {
    let dispatch = useDispatch();
    let solicitudUUID = useSelector(seleccionarSolicitudAbierta);
    let solicitud = useSelector(seleccionarSolicitudUUID(solicitudUUID));

    let votosFavor = solicitud.votos.filter( voto =>  voto.a_favor );
    let votosContra = solicitud.votos.filter( voto => !voto.a_favor );

    let cerrar = () => dispatch(cerrarTodo());

    let datos = {
        labels: [
            'A favor',
            'En contra'
        ],
        datasets: [{
            data: [votosFavor.length, votosContra.length],
            backgroundColor: ['#1cc88a','#e74a3b'],

        }]
    }

    return (
        <>
            <Modal.Header>
                <Modal.Title>Solicitud de cambio</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Row>
                    <Col>
                        <Row className="p-2">
                            <Form.Label>Item:</Form.Label>
                            <ItemSolicitud item_uuid={solicitud.item_solicitante}/>
                        </Row>
                    </Col>
                    <Col>
                        <Doughnut data={datos}/>
                    </Col>
                </Row>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={cerrar}>Salir</Button>
            </Modal.Footer>
        </>
    );
}

export default VerSolicitudModal;