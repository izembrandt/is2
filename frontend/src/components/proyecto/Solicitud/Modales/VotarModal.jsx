import React from "react";
import {Row, Modal, Button, Form, Col} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {cerrarTodo, seleccionarSolicitudAbierta} from "../../../../redux/proyecto/modales";
import {ejercerVotoSolicitud, seleccionarSolicitudUUID} from "../../../../redux/proyecto/comite";
import ItemSolicitud from "../ItemSolicitud";
import Cookies from "js-cookie";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";
import {seleccionarPerfilUsuario} from "../../../../redux/perfil";

const VotarModal = () => {
    let dispatch = useDispatch();
    let solicitudUUID = useSelector(seleccionarSolicitudAbierta);
    let solicitud = useSelector(seleccionarSolicitudUUID(solicitudUUID));
    let proyecto = useSelector(seleccionarInfoProyecto);
    let usuario = useSelector(seleccionarPerfilUsuario);

    const votoHecho = solicitud.votos.find(voto => voto.votante === usuario.correo);


    let cerrar = () => dispatch(cerrarTodo());

    const ejercerVoto = veredicto => event => {
        const datos = new FormData();
        datos.append("solicitud", solicitudUUID);
        datos.append("a_favor", veredicto);
        datos.append("csrfmiddlewaretoken", Cookies.get('csrftoken'));
        datos.forEach((value, key) => console.log(`${key}: ${value}`));
        dispatch(ejercerVotoSolicitud({id: proyecto.id, formData: datos}));

    }

    return (
        <>
            <Modal.Header>
                <Modal.Title>Votar por la aprobación de la solicitud de cambio</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Group>
                    <Form.Label>Votando por la aprobación de:</Form.Label>
                </Form.Group>
                <Row className="p-2">
                    <ItemSolicitud item_uuid={solicitud.item_solicitante}/>
                </Row>
                <Form.Group>
                    <Form.Label>Motivo: </Form.Label>
                    <Form.Control as="textarea" name="motivo" cols="40" rows="5" value={solicitud.motivo} disabled/>
                </Form.Group>
                {votoHecho !== undefined
                    ?
                    <Form.Row>
                        <Col>
                            <Button variant={!votoHecho.a_favor ? "danger" : "secondary"} block>Rechazar</Button>
                        </Col>
                        <Col>
                            <Button variant={votoHecho.a_favor ? "success" : "secondary"} block>Aceptar</Button>
                        </Col>
                    </Form.Row>
                    :
                    <Form.Row>
                        <Col>
                            <Button variant="danger" block onClick={ejercerVoto("false")}>Rechazar</Button>
                        </Col>
                        <Col>
                            <Button variant="success" block onClick={ejercerVoto("true")}>Aceptar</Button>
                        </Col>
                    </Form.Row>
                }
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={cerrar}>Salir</Button>
            </Modal.Footer>
        </>
    );
}

export default VotarModal;