import React from "react";
import {Modal, Button, Form} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {cerrarTodo} from "../../../../redux/proyecto/modales";
import {saveAs} from "file-saver";
import {urlReporteSolicitud} from "../../../../url";
import {seleccionarInfoProyecto} from "../../../../redux/proyecto/proyecto";
import {toast} from "react-toastify";


const ReporteSolicitudModal = () => {
    let dispatch = useDispatch();
    let proyecto = useSelector(seleccionarInfoProyecto);

    let cerrar = () => dispatch(cerrarTodo());

    const onSubmit = event => {
        event.preventDefault();
        const formData = new FormData(event.target);
        const data = [...formData.entries()];
        const asString = data
            .map(x => `${encodeURIComponent(x[0])}=${encodeURIComponent(x[1].replace("T", " "))}`)
            .join('&');


        const inicio = new Date(formData.get("inicio"));
        const fin = new Date(formData.get("fin"));

        const urlArchivo = urlReporteSolicitud(proyecto.id) + "?" + asString;

        if (inicio >= fin) {
            toast.error("La fecha de inicio tiene que ser menor a la fecha de fin")
        } else {
            saveAs(urlArchivo);
            cerrar();
        }

    }

    return (
        <>
            <Modal.Header>
                <Modal.Title>Reporte de solicitudes de cambio</Modal.Title>
            </Modal.Header>
            <form onSubmit={onSubmit}>
                <Modal.Body>
                    <Form.Group>
                        <Form.Label>Fecha de inicio</Form.Label>
                        <Form.Control type="datetime-local" required name="inicio"/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Fecha de fin</Form.Label>
                        <Form.Control type="datetime-local" required name="fin"/>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={cerrar}>
                        Salir
                    </Button>
                    <Button variant="success" type="submit">
                        Descargar
                    </Button>
                </Modal.Footer>
            </form>
        </>
    );
}

export default ReporteSolicitudModal;