import React from "react";
import {VerRol} from "./VerRol";
import {EliminarRol} from "./EliminarRol";
import {EditarRol} from "./EditarRol";


export const AccionesRol = ({ uuid, idProyecto }) => {
    return(<>
            {/*Se muestra el boton para ver roles*/}
                <VerRol uuid={uuid}/>
            {/*Separador de botones*/}
            <a > / </a>

            {/*Se muestra el boton para modificar roles*/}
                <EditarRol uuid={uuid} idProyecto={idProyecto}/>
            {/*Separador de botones*/}
            <a > / </a>

            {/*Se muestra la opcion de eliminar Rol*/}
                <EliminarRol uuid={uuid} idProyecto={idProyecto}/>
        </>);
}