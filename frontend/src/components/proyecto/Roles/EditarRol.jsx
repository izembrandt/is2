import React, {useEffect, useState} from "react";
import {Button, Col, Form, Modal, OverlayTrigger, Popover} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import DualListBox from "react-dual-listbox";
import 'react-dual-listbox/lib/react-dual-listbox.css';
import {useDispatch, useSelector} from "react-redux";
import {
    editarRol,
    seleccionarPermisosFaseRoles,
    seleccionarPermisosProyectoRoles, seleccionarRolPorUUID
} from "../../../redux/proyecto/roles";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {fetchFasesProyecto, seleccionarFasesProyecto, vaciarFases} from "../../../redux/proyecto/fases";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import styles from "../Fases/css/estilo.module.css";

export const EditarRol = ({uuid, idProyecto}) => {
    let rol = useSelector(seleccionarRolPorUUID(uuid));
    //Modal
    const [modalActivo, setModalActivo] = useState(false);

    let mostrarModal = () => setModalActivo(true);
    let ocultarModal = () => setModalActivo(false);


    let permisos = useSelector(seleccionarPermisosProyecto);
    let { estado } = useSelector(seleccionarInfoProyecto);
    let puedeEditar = permisos.includes(PERMISOS_PROYECTO.MODIFICAR_ROLES) && estado !== ESTADO_PROYECTO.CANCELADO && estado !== ESTADO_PROYECTO.FINALIZADO;

    //Effect
    let estadoFase = useSelector(state => state.fases.estado);
    let listaFases = useSelector(seleccionarFasesProyecto);
    let dispatch = useDispatch();

    //Opciones
    const opcionesPermisoProyecto = useSelector(seleccionarPermisosProyectoRoles).map(valor => {
        return {value: valor.codigo, label: valor.nombre};
    });
    const opcionesPermisosFase = useSelector(seleccionarPermisosFaseRoles).map(valor => {
        return {value: valor.codigo, label: valor.nombre};
    });
    const opcionesFase = listaFases.map(valor => {
        return {value: valor.id, label: valor.nombre};
    });

    let nada = () => {};

    //Items del formulario
    const [permisosProyectoSeleccionados, setPermisosProyectoSeleccionados] = useState(rol.permisos_proyecto);
    const [permisosFaseSeleccionados, setPermisosFaseSeleccionados] = useState(rol.permisos_fases);
    const [fasesSeleccionadas, setFasesSeleccionadas] = useState(rol.fases);

    //Texto Boton
    const [textoBoton, setTextoBoton] = useState("Editar")
    let estadoRoles = useSelector(state => state.roles.estado);
    useEffect(() => {
        switch (estadoRoles) {
            case ESTADO_STATE.COMPLETADO:
                setTextoBoton("Editar");
                setModalActivo(false);
                break;
            case ESTADO_STATE.PENDIENTE:
                setTextoBoton("Cargando...");
                break;
            case ESTADO_STATE.VACIO:
                setTextoBoton("No disponible.");
                break;
            case ESTADO_STATE.FALLADO:
                setTextoBoton("Error..");
                break;
        }
    }, [estadoRoles])

    //Cuando se haga submit
    const manejarSubmit = (event) => {
        //Paramos el evento
        event.preventDefault();
        //Recogemos los datos
        const datos = new FormData(event.target);
        datos.append("uuid", uuid);
        permisosProyectoSeleccionados.forEach(value => datos.append("permisos_proyecto", value));
        permisosFaseSeleccionados.forEach(value => datos.append("permisos_fase", value));
        fasesSeleccionadas.forEach(value => datos.append("fases", value));
        //Pasamos a la función los datos y lo siguiente
        dispatch(editarRol({id: idProyecto, data:datos}));
    }

    let errores = [];
    if(estado === ESTADO_PROYECTO.CANCELADO){
        errores.push((<li key="proyecto_cancelado">El proyecto no debe estar en estado cancelado</li>))
    }
    if(estado === ESTADO_PROYECTO.FINALIZADO){
        errores.push((<li key="proyecto_finalizado">El proyecto no debe estar finalizado</li>))
    }
    if(!permisos.includes(PERMISOS_PROYECTO.MODIFICAR_ROLES)){
        errores.push((<li key="modificar_roles">No posee el permiso para modificar roles</li>))
    }

    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Editar rol</Popover.Title>
            <Popover.Content>
                Edita los detalles del rol
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede editar rol</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    return (
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeEditar ? popover_info : popover_error}>
            <a href={"#"} className={puedeEditar ? "" : styles.isDisabled} onClick={puedeEditar ? mostrarModal : nada}>Editar</a>
            </OverlayTrigger>
            <Modal show={modalActivo} onHide={ocultarModal} size="lg">
                <Modal.Header>
                    <Modal.Title>
                        Editar {rol.nombre}
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={manejarSubmit}>
                    <Modal.Body>
                        <CSRFToken/>
                        {/* Nombre del Rol */}
                        <Form.Group>
                            <Form.Label>Nombre del Rol*</Form.Label>
                            <Form.Control type="text" name="nombre" maxLength="50" required defaultValue={rol.nombre}/>
                        </Form.Group>
                        {/* Descripción del Rol */}
                        <Form.Group>
                            <Form.Label>Descripción</Form.Label>
                            <Form.Control as="textarea" name="descripcion" cols="40" rows="10" defaultValue={rol.descripcion}/>
                        </Form.Group>
                        {/* Permisos del Proyecto */}
                        <Form.Group>
                            <Form.Label>Permisos del Proyecto</Form.Label>
                            <DualListBox
                                options={opcionesPermisoProyecto}
                                selected={permisosProyectoSeleccionados}
                                onChange={(permisos) => setPermisosProyectoSeleccionados(permisos)}
                            />
                        </Form.Group>
                        {/* Fases del Proyecto */}
                        <Form.Group>
                            <Form.Label>Fases del Proyecto</Form.Label>
                            <DualListBox
                                options={opcionesFase}
                                selected={fasesSeleccionadas}
                                onChange={(fases) => setFasesSeleccionadas(fases)}
                            />
                        </Form.Group>
                        {/* Permisos de Fase */}
                        <Form.Group>
                            <Form.Label>Permisos de Fase</Form.Label>
                            <DualListBox
                                options={opcionesPermisosFase}
                                selected={permisosFaseSeleccionados}
                                onChange={(permisos) => setPermisosFaseSeleccionados(permisos)}
                            />
                        </Form.Group>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Cancelar</Button>
                        <Button variant="primary" type="submit"
                                disabled={estadoRoles !== ESTADO_STATE.COMPLETADO}>{textoBoton}</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}
