import React from "react";

//Tablas
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';

//React Bootstrap
import {Card, Button} from "react-bootstrap";
import {AccionesRol} from "./AccionesRol";


const ListaRoles = ({roles = [], idProyecto}) => {
    let Columnas = [
        {
            text: 'Rol',
            dataField: 'nombre',
            sort: true,
        },
        {
            text: 'Acciones',
            dataField: 'uuid',
            sort: true,
            formatter: (cell, row) => <AccionesRol uuid={row.uuid} idProyecto={idProyecto}/>,
        },
    ]


    return (
        <Card className="shadow mb-4">
            <Card.Header className="py-3 container-fluid">
                <div className="row">
                    <div className="col-12 col-xl-10 col-lg-9 col-md-8 my-auto">
                        <h6 className="m-0 font-weight-bold text-primary">Lista de roles</h6>
                    </div>
                </div>
            </Card.Header>
            <Card.Body>
                <BootstrapTable
                    keyField="uuid"
                    data={roles}
                    columns={Columnas}
                    noDataIndication="No hay roles creados"
                    pagination={paginationFactory()}
                />
            </Card.Body>
        </Card>
    );
};

export default ListaRoles;