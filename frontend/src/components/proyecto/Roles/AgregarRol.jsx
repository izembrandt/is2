import React, {useEffect, useState} from "react";
import {Button, Col, Form, Modal, OverlayTrigger, Popover, Row, DropdownButton, Dropdown} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import DualListBox from "react-dual-listbox";
import 'react-dual-listbox/lib/react-dual-listbox.css';
import {useDispatch, useSelector} from "react-redux";
import {
    crearRol,
    seleccionarPermisosFaseRoles,
    seleccionarPermisosProyectoRoles, seleccionarRolPresets
} from "../../../redux/proyecto/roles";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {fetchFasesProyecto, seleccionarFasesProyecto, vaciarFases} from "../../../redux/proyecto/fases";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";

const AgregarRol = ({idProyecto}) => {
    //Modal
    const [modalActivo, setModalActivo] = useState(false);

    let mostrarModal = () => {
        dispatch(fetchFasesProyecto(idProyecto));
        setModalActivo(true);
    };
    let ocultarModal = () => {
        setModalActivo(false);
    };
    let permisos = useSelector(seleccionarPermisosProyecto);
    let {estado} = useSelector(seleccionarInfoProyecto);
    let puedeAgregar = permisos.includes(PERMISOS_PROYECTO.ASIGNAR_ROLES) && estado !== ESTADO_PROYECTO.CANCELADO;
    let presets = useSelector(seleccionarRolPresets);

    //Effect
    let estadoFase = useSelector(state => state.fases.estado);
    let listaFases = useSelector(seleccionarFasesProyecto);
    let dispatch = useDispatch();

    //Opciones
    const opcionesPermisoProyecto = useSelector(seleccionarPermisosProyectoRoles).map(valor => {
        return {value: valor.codigo, label: valor.nombre};
    });
    const opcionesPermisosFase = useSelector(seleccionarPermisosFaseRoles).map(valor => {
        return {value: valor.codigo, label: valor.nombre};
    });
    const opcionesFase = listaFases.map(valor => {
        return {value: valor.id, label: valor.nombre};
    });

    //Items del formulario
    const [permisosProyectoSeleccionados, setPermisosProyectoSeleccionados] = useState([]);
    const [permisosFaseSeleccionados, setPermisosFaseSeleccionados] = useState([]);
    const [fasesSeleccionadas, setFasesSeleccionadas] = useState([]);

    //Texto Boton
    const [textoBoton, setTextoBoton] = useState("Crear")
    let estadoRoles = useSelector(state => state.roles.estado);

    useEffect(() => {
        switch (estadoRoles) {
            case ESTADO_STATE.COMPLETADO:
                setTextoBoton("Crear");
                setModalActivo(false);
                break;
            case ESTADO_STATE.PENDIENTE:
                setTextoBoton("Cargando...");
                break;
            case ESTADO_STATE.VACIO:
                setTextoBoton("No disponible.");
                break;
            case ESTADO_STATE.FALLADO:
                setTextoBoton("Error..");
                break;
        }
    }, [estadoRoles])

    //Cuando se haga submit
    const manejarSubmit = (event) => {
        //Paramos el evento
        event.preventDefault();
        //Recogemos los datos
        const datos = new FormData(event.target);
        permisosProyectoSeleccionados.forEach(value => datos.append("permisos_proyecto", value));
        permisosFaseSeleccionados.forEach(value => datos.append("permisos_fase", value));
        fasesSeleccionadas.forEach(value => datos.append("fases", value));
        //Pasamos a la función los datos y lo siguiente
        dispatch(crearRol({
                id: idProyecto,
                formData: datos
            })
        );
    }

    //Cargar preset
    const cargarPreset = presetElegido => event => {
        setPermisosFaseSeleccionados(presetElegido.permisos_fases);
        setPermisosProyectoSeleccionados(presetElegido.permisos_proyecto);
        setFasesSeleccionadas(listaFases.map( fase => fase.id));
    }

    let errores = [];
    if(estado === ESTADO_PROYECTO.CANCELADO){
        errores.push((<li key="proyecto_cancelado">El proyecto no debe estar en estado cancelado</li>))
    }
    if(!permisos.includes(PERMISOS_PROYECTO.ASIGNAR_ROLES)){
        errores.push((<li key="asignar_roles">No posee el permiso para agregar rol</li>))
    }

    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Agregar Rol</Popover.Title>
            <Popover.Content>
                Agrega Roles al proyecto
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede agregar rol</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    return (
        <Col xs={12} xl={2} lg={3} md={4} className="mb-4 m-md-auto align-items-center">
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeAgregar? popover_info : popover_error}>
            <Button variant={puedeAgregar ? "primary" : "secondary" } onClick={puedeAgregar ? mostrarModal : ocultarModal}>Crear Rol</Button>
             </OverlayTrigger>
            <Modal show={modalActivo} onHide={ocultarModal} size="lg">
                <Modal.Header>
                    <Modal.Title>
                        Crear nuevo rol
                    </Modal.Title>
                    <DropdownButton id="dropdown-basic-button" title="Roles predefinidos">
                        {
                            presets.map( preset =>
                                <Dropdown.Item
                                    onClick={cargarPreset(preset)}
                                    key={preset.nombre}>{preset.nombre}</Dropdown.Item>
                            )
                        }
                    </DropdownButton>
                </Modal.Header>
                <Form onSubmit={manejarSubmit}>
                    <Modal.Body>
                        <CSRFToken/>
                        {/* Nombre del Rol */}
                        <Form.Group>
                            <Form.Label>Nombre del Rol*</Form.Label>
                            <Form.Control type="text" name="nombre" maxLength="50" required/>
                        </Form.Group>
                        {/* Descripción del Rol */}
                        <Form.Group>
                            <Form.Label>Descripción</Form.Label>
                            <Form.Control as="textarea" name="descripcion" cols="40" rows="10"/>
                        </Form.Group>
                        {/* Permisos del Proyecto */}
                        <Form.Group>
                            <Form.Label>Permisos del Proyecto</Form.Label>
                            <DualListBox
                                canFilter
                                filterPlaceholder="Buscar.."
                                options={opcionesPermisoProyecto}
                                selected={permisosProyectoSeleccionados}
                                onChange={(permisos) => setPermisosProyectoSeleccionados(permisos)}
                            />
                        </Form.Group>
                        {/* Fases del Proyecto */}
                        <Form.Group>
                            <Form.Label>Fases del Proyecto</Form.Label>
                            <DualListBox
                                canFilter
                                filterPlaceholder="Buscar.."
                                options={opcionesFase}
                                selected={fasesSeleccionadas}
                                onChange={(fases) => setFasesSeleccionadas(fases)}
                            />
                        </Form.Group>
                        {/* Permisos de Fase */}
                        <Form.Group>
                            <Form.Label>Permisos de Fase</Form.Label>
                            <DualListBox
                                canFilter
                                filterPlaceholder="Buscar.."
                                options={opcionesPermisosFase}
                                selected={permisosFaseSeleccionados}
                                onChange={(permisos) => setPermisosFaseSeleccionados(permisos)}
                            />
                        </Form.Group>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Cancelar</Button>
                        <Button variant="primary" type="submit"
                                disabled={estadoRoles !== ESTADO_STATE.COMPLETADO}>{textoBoton}</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </Col>
    );
}

export default AgregarRol;