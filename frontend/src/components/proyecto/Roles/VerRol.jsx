import React, {useState} from "react";
import {useSelector} from "react-redux";
import {
    seleccionarPermisosFaseRoles,
    seleccionarPermisosProyectoRoles,
    seleccionarRolPorUUID
} from "../../../redux/proyecto/roles";
import {Button, Form, Modal, OverlayTrigger, Popover} from "react-bootstrap";
import {seleccionarFasePorID, seleccionarFasesProyecto,} from "../../../redux/proyecto/fases";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import {seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import styles from "../Fases/css/estilo.module.css";
import {ESTADO_PROYECTO} from "../../../constantes/estados";

export const VerRol = ({uuid}) => {
    let rol = useSelector(seleccionarRolPorUUID(uuid));
    const [modalActivo, setModalActivo] = useState(false);
    let permisos = useSelector(seleccionarPermisosProyecto);
    let puedeVer = permisos.includes(PERMISOS_PROYECTO.VER_ROLES);


    let mostrarModal = () => setModalActivo(true);
    let ocultarModal = () => setModalActivo(false);
    let nada = () => {
    };

    //Datos
    const fases = useSelector(seleccionarFasesProyecto);
    const permisosProyecto = useSelector(seleccionarPermisosProyectoRoles);
    const permisosFase = useSelector(seleccionarPermisosFaseRoles);

    let errores = [];
    if (!permisos.includes(PERMISOS_PROYECTO.VER_ROLES)) {
        errores.push((<li key="ver_roles">No posee el permiso para ver roles</li>))
    }

    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Mirar rol</Popover.Title>
            <Popover.Content>
                Mirar los detalles del rol
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede ver rol</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    return (
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto"
                            overlay={puedeVer ? popover_info : popover_error}>
                <a href={"#"} className={puedeVer ? "" : styles.isDisabled}
                   onClick={puedeVer ? mostrarModal : nada}>Ver</a>
            </OverlayTrigger>
            <Modal show={modalActivo} onHide={ocultarModal} size="lg">
                <Modal.Header>
                    <Modal.Title>
                        {rol.nombre}
                    </Modal.Title>
                </Modal.Header>
                <Form>
                    <Modal.Body>
                        {/* Nombre del Rol */}
                        <Form.Group>
                            <Form.Label>Nombre del Rol*</Form.Label>
                            <Form.Control type="text" name="nombre" maxLength="50" value={rol.nombre} required
                                          readOnly/>
                        </Form.Group>
                        {/* Descripción del Rol */}
                        <Form.Group>
                            <Form.Label>Descripción</Form.Label>
                            <Form.Control as="textarea" name="descripcion" value={rol.descripcion} cols="40" rows="10"
                                          readOnly/>
                        </Form.Group>
                        {/* Permisos del Proyecto */}
                        <Form.Group>
                            <Form.Label>Permisos del Proyecto</Form.Label>
                            {permisosProyecto.map(permisoProyecto =>
                                rol.permisos_proyecto.includes(permisoProyecto['codigo']) &&
                                <li key={permisoProyecto['codigo']}>
                                    {
                                        permisoProyecto['nombre']
                                    }
                                </li>
                            )}
                        </Form.Group>
                        {/* Fases del Proyecto */}
                        <Form.Group>
                            <Form.Label>Fases del Proyecto</Form.Label>
                            {rol.fases.map(faseId =>
                                <li key={faseId}>
                                    {
                                        fases.find(f => f.id === faseId).nombre
                                    }
                                </li>
                            )}
                        </Form.Group>
                        {/* Permisos de Fase */}
                        <Form.Group>
                            <Form.Label>Permisos de Fase</Form.Label>
                            {permisosFase.map(permisoFase =>
                                rol.permisos_fases.includes(permisoFase['codigo']) &&
                                <li key={permisoFase['codigo']}>{
                                    permisoFase['nombre']
                                }</li>
                            )}
                        </Form.Group>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Salir</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}