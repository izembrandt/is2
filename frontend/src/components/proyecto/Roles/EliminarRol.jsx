import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
    eliminarRol,
    seleccionarErrorRoles,
    seleccionarRolPorUUID
} from "../../../redux/proyecto/roles";
import {Button, Form, Modal, OverlayTrigger, Popover} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import styles from "../Fases/css/estilo.module.css";

export const EliminarRol = ({uuid, idProyecto}) => {
    let dispatch = useDispatch();
    let estadoRoles = useSelector(state => state.roles.estado);
    let rol = useSelector(seleccionarRolPorUUID(uuid));
    const [modalActivo, setModalActivo] = useState(false);
    const [textoBoton, setTextoBoton] = useState("Eliminar");

    let mostrarModal = () => setModalActivo(true);
    let ocultarModal = () => setModalActivo(false);
    let permisos = useSelector(seleccionarPermisosProyecto);
    let { estado } = useSelector(seleccionarInfoProyecto);
    let puedeEliminar = permisos.includes(PERMISOS_PROYECTO.ELIMINAR_ROLES) && estado !== ESTADO_PROYECTO.CANCELADO && estado !== ESTADO_PROYECTO.FINALIZADO;


    let eliminarRolEvento = (event) => {
        //Prevenimos que se refresque la pagina
        event.preventDefault();
        //Recogemos los datos (CSRF)
        let data = new FormData(event.target);
        data.append("uuid", uuid);
        //Enviar al servidor la solicitud
        dispatch(eliminarRol({
                id: idProyecto,
                data: data
            })
        );
    }

    useEffect(() => {
        switch (estadoRoles) {
            case ESTADO_STATE.COMPLETADO:
                setTextoBoton("Eliminar");
                ocultarModal();
                break;
            case ESTADO_STATE.VACIO:
                setTextoBoton("No disponible");
                break;
            case ESTADO_STATE.FALLADO:
                setTextoBoton("Error.");
                break;
            case ESTADO_STATE.PENDIENTE:
                setTextoBoton("Cargando..");
                break;
        }
    }, [dispatch, estadoRoles]);



    let errores = [];
    if(estado === ESTADO_PROYECTO.CANCELADO){
        errores.push((<li key="proyecto_cancelado">El proyecto no debe estar en estado cancelado</li>))
    }
    if(estado === ESTADO_PROYECTO.FINALIZADO){
        errores.push((<li key="proyecto_finalizado">El proyecto no debe estar en estado finalizado</li>))
    }
    if(!permisos.includes(PERMISOS_PROYECTO.ELIMINAR_ROLES)){
        errores.push((<li key="modificar_roles">No posee el permiso para eliminar roles</li>))
    }

    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Eliminar rol</Popover.Title>
            <Popover.Content>
                Elimina el rol de la lista de roles
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede eliminar rol</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )


    return (
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeEliminar? popover_info : popover_error}>
            <a href={"#"} className={puedeEliminar ? "" : styles.isDisabled} onClick={puedeEliminar ? mostrarModal : ocultarModal}>Eliminar</a>
            </OverlayTrigger>
            <Modal show={modalActivo} onHide={ocultarModal}>
                <Modal.Header>
                    <Modal.Title>
                        Eliminar {rol.nombre}
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={eliminarRolEvento}>
                    <CSRFToken/>
                    <Modal.Body>
                        Estas seguro de eliminar el Rol "{rol.nombre}" del proyecto? Los usuarios que posean este rol
                        serán desasignados del rol y sus permisos
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Salir</Button>
                        <Button variant="danger" type="submit" disabled={estadoRoles !== ESTADO_STATE.COMPLETADO}>{textoBoton}</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}