    import React, {useEffect, useState} from "react";
import {Col, Button, Modal, Form, Row, Popover, OverlayTrigger} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {useDispatch, useSelector} from "react-redux";
import {
    asignarRolParticipante,
    seleccionarEstadoParticipantes, seleccionarParticipantePorCorreo
} from "../../../redux/proyecto/participantes";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import DualListBox from "react-dual-listbox";
import 'react-dual-listbox/lib/react-dual-listbox.css';
import {fetchListaRoles, seleccionarListaRolesProyecto, vaciarRoles} from "../../../redux/proyecto/roles";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
    import styles from "../Fases/css/estilo.module.css";
    import {seleccionarFasePorID} from "../../../redux/proyecto/fases";

export const VerParticipante = ({ correo }) => {
    //States
    let [textoBoton, setTextoBoton] = useState("Guardar");
    let [rolesSeleccionados, setRolesSeleccionados] = useState([]);
    let roles = useSelector(seleccionarListaRolesProyecto).map(rol => ({value: rol.uuid, label: rol.nombre}));
    let estadoParticipantes = useSelector(seleccionarEstadoParticipantes);
    let participante = useSelector(seleccionarParticipantePorCorreo(correo));
    let proyecto = useSelector(seleccionarInfoProyecto);
    let dispatch = useDispatch();
    let permisos = useSelector(seleccionarPermisosProyecto);
    let puedeVer = permisos.includes(PERMISOS_PROYECTO.VER_PARTICIPANTES)
    //Modal
    const [modalActivo, setModalActivo] = useState(false);

    let mostrarModal = () => {
        dispatch(fetchListaRoles(proyecto.id)); //Actualizamos la lista de roles
        setRolesSeleccionados(participante.roles.map( rol => rol.uuid));
        setModalActivo(true);
    };
    let ocultarModal = () => {
        if (modalActivo) dispatch(vaciarRoles());
        setModalActivo(false)
    };


    let errores = [];
    if(!permisos.includes(PERMISOS_PROYECTO.VER_PARTICIPANTES)){
        errores.push((<li key="ver_participantes">No posee el permiso para ver roles</li>))
    }

    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Ver Roles</Popover.Title>
            <Popover.Content>
                Posibilita ver los roles del participante en el proyecto
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede ver los roles del participantes</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    return(
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeVer ? popover_info : popover_error}>
            <a href={"#"} className={puedeVer ? "" : styles.isDisabled} onClick={puedeVer ? mostrarModal : ocultarModal}>Ver Roles</a>
            </OverlayTrigger>
            <Modal show={modalActivo} onHide={ocultarModal} size="lg">
                <Modal.Header>
                    <Modal.Title>
                        Roles
                    </Modal.Title>
                </Modal.Header>
                <Form>
                    <Modal.Body>
                        <CSRFToken/>
                        {participante.roles.length > 0
                            ? participante.roles.map(roles =>
                                    <li key={roles.uuid}>{roles.nombre}</li>
                            )
                            : <a>"No tiene roles asignados"</a>
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Salir</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}