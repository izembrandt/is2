import React, {useEffect, useState} from "react";
import {Col, Button, Modal, Form, Row, OverlayTrigger, Popover} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {useDispatch, useSelector} from "react-redux";
import {
    eliminarParticipante,
    seleccionarEstadoParticipantes,
    seleccionarParticipantePorCorreo
} from "../../../redux/proyecto/participantes";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import styles from "../Fases/css/estilo.module.css";
import {verificarEsComite} from "../../../redux/proyecto/comite";

export const EliminarParticipante = ({correo}) => {
    //States
    let [textoBoton, setTextoBoton] = useState("Eliminar");
    let estadoParticipantes = useSelector(seleccionarEstadoParticipantes);
    let participante = useSelector(seleccionarParticipantePorCorreo(correo));
    let proyecto = useSelector(seleccionarInfoProyecto);
    let dispatch = useDispatch();
    let permisos = useSelector(seleccionarPermisosProyecto);
    let {estado} = useSelector(seleccionarInfoProyecto);
    const esComite = useSelector(verificarEsComite(correo));
    let puedeEliminar = permisos.includes(PERMISOS_PROYECTO.QUITAR_PARTICIPANTES)
        && estado !== ESTADO_PROYECTO.CANCELADO
        && estado !== ESTADO_PROYECTO.FINALIZADO
        && !esComite;

    //Modal
    const [modalActivo, setModalActivo] = useState(false);

    let mostrarModal = () => setModalActivo(true);
    let ocultarModal = () => setModalActivo(false);


    //Efecto
    useEffect(() => {
        switch (estadoParticipantes) {
            case ESTADO_STATE.COMPLETADO:
                setTextoBoton("Eliminar");
                ocultarModal();
                break;
            case ESTADO_STATE.VACIO:
                setTextoBoton("No disponible");
                break;
            case ESTADO_STATE.PENDIENTE:
                setTextoBoton("Cargando..");
                break;
            case ESTADO_STATE.FALLADO:
                setTextoBoton("Error");
                break;
        }
    }, [dispatch, estadoParticipantes]);

    //Handle
    let submitParticipante = event => {
        event.preventDefault();
        let datos = new FormData(event.target);
        datos.append("email", correo);
        dispatch(eliminarParticipante({id: proyecto.id, formData: datos}))
    }


    let errores = [];
    if (estado === ESTADO_PROYECTO.CANCELADO) {
        errores.push((<li key="proyecto_cancelado">El proyecto no debe estar en estado cancelado</li>))
    }
    if (estado === ESTADO_PROYECTO.FINALIZADO) {
        errores.push((<li key="proyecto_finalizado">El proyecto no debe estar en estado finalizado</li>))
    }
    if (!permisos.includes(PERMISOS_PROYECTO.QUITAR_PARTICIPANTES)) {
        errores.push((<li key="quitar_participante">No posee el permiso para eliminar participantes</li>))
    }
    if (esComite) {
        errores.push(<li key="es_comite">Este usuario pertenece al comité</li>)
    }

    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Eliminar a {participante.first_name + " " + participante.last_name}</Popover.Title>
            <Popover.Content>
                Elimina participantes del proyecto
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede eliminar a este participante del proyecto</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    return (
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto"
                            overlay={puedeEliminar ? popover_info : popover_error}>
                <a href={"#"} className={puedeEliminar ? "" : styles.isDisabled}
                   onClick={puedeEliminar ? mostrarModal : ocultarModal}>Eliminar</a>
            </OverlayTrigger>
            <Modal show={modalActivo} onHide={ocultarModal} size="lg">
                <Modal.Header>
                    <Modal.Title>
                        Eliminar participante
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={submitParticipante}>
                    <Modal.Body>
                        <CSRFToken/>
                        <p>Estas seguro de eliminar a "{participante.first_name + " " + participante.last_name}" del
                            proyecto</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Cancelar</Button>
                        <Button variant="primary" type="submit"
                                disabled={estadoParticipantes !== ESTADO_STATE.COMPLETADO}>{textoBoton}</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}