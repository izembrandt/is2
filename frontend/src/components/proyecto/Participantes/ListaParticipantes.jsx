import React from "react";

//Tablas
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';

//React Bootstrap
import {Card, Button, Row, Col} from "react-bootstrap";
import {AccionesParticipante} from "./AccionesParticipante";


const ListaParticipantes = ({participantes = []}) => {
    let Columnas = [
        {
            text: 'Nombre',
            dataField: 'first_name',
            sort: true,
            formatter: (cell, row) => (<p>{row.first_name + " " + row.last_name}</p>)
        },
        {
            text: 'Correo',
            dataField: 'email',
            sort: true,
            formatter: (cell, row) => (<p>{row.email}</p>)
        },

        {
            text: 'Acciones',
            dataField: 'id',
            sort: true,
            formatter: (col, row) => <AccionesParticipante email={row.email}/>,
        },
    ]


    return (
        <Card className="shadow mb-4">
            <Card.Header className="py-3 container-fluid">
                <div className="row">
                    <div className="col-12 col-xl-10 col-lg-9 col-md-8 my-auto">
                        <h6 className="m-0 font-weight-bold text-primary">Lista de Participantes</h6>
                    </div>
                </div>
            </Card.Header>
            <Card.Body>
                <BootstrapTable
                    keyField="email"
                    data={participantes}
                    columns={Columnas}
                    noDataIndication="No hay participantes agregados"
                    pagination={paginationFactory()}
                />
            </Card.Body>
        </Card>
    );
};

export default ListaParticipantes;