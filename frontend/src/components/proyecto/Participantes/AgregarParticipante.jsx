import React, {useEffect, useState} from "react";
import {Col, Button, Modal, Form, Popover, OverlayTrigger} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {useDispatch, useSelector} from "react-redux";
import {agregarParticipante, seleccionarEstadoParticipantes} from "../../../redux/proyecto/participantes";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import {seleccionarPerfilUsuario} from "../../../redux/perfil";

export const AgregarParticipante = () => {
    //States
    let [textoBoton, setTextoBoton] = useState("Agregar");
    let [emailTexto, setEmailTexto] = useState("");
    let estadoParticipantes = useSelector(seleccionarEstadoParticipantes);
    let proyecto = useSelector(seleccionarInfoProyecto);
    let dispatch = useDispatch();
    let permisos = useSelector(seleccionarPermisosProyecto);
    let { estado } = useSelector(seleccionarInfoProyecto);
    let usuario =  useSelector(seleccionarPerfilUsuario);
    let puedeAgregar = permisos.includes(PERMISOS_PROYECTO.AGREGAR_PARTICIPANTES)
        && usuario.correo !== emailTexto;

    //Modal
    const [modalActivo, setModalActivo] = useState(false);

    let mostrarModal = () => setModalActivo(true);
    let ocultarModal = () => {
        setModalActivo(false);
        setEmailTexto("");
    }

    //Efecto
    useEffect(()=>{
        switch (estadoParticipantes){
            case ESTADO_STATE.COMPLETADO:
                setTextoBoton("Agregar");
                ocultarModal();
                break;
            case ESTADO_STATE.VACIO:
                setTextoBoton("No disponible");
                break;
            case ESTADO_STATE.PENDIENTE:
                setTextoBoton("Cargando..");
                break;
            case ESTADO_STATE.FALLADO:

                setTextoBoton("Error");
                break;
        }
    }, [dispatch, estadoParticipantes]);

    //Handle
    let submitParticipante = event => {
        event.preventDefault();
        let datos = new FormData(event.target);
        dispatch(agregarParticipante({id: proyecto.id, formData:datos }))
    }


    let errores = [];
    if(estado === ESTADO_PROYECTO.CANCELADO){
        errores.push((<li key="proyecto_cancelado">El proyecto no debe estar en estado cancelado</li>))
    }
    if(!permisos.includes(PERMISOS_PROYECTO.AGREGAR_PARTICIPANTES)){
        errores.push((<li key="agregar_participantes">No posee el permiso para agregar participantes</li>))
    }

    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Agregar Participantes</Popover.Title>
            <Popover.Content>
                Agrega participantes al proyecto
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede agregar participantes</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    const popover_error_aceptar = (
         <Popover id="popover-basic">
            <Popover.Title as="h3">No puedes volver a agregarte al proyecto</Popover.Title>
        </Popover>
    );

    const popover_info_aceptar = (
         <Popover id="popover-basic">
            <Popover.Title as="h3">Agrega al usuario seleccionado</Popover.Title>
        </Popover>
    );

    return(
        <Col xs={12} xl={3} lg={3} md={4} className="mb-4 m-md-auto align-items-center">
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeAgregar? popover_info : popover_error}>
            <Button variant={puedeAgregar ? "primary" : "secondary" } onClick={puedeAgregar ? mostrarModal : ocultarModal}>Agregar Participante</Button>
            </OverlayTrigger>
                <Modal show={modalActivo} onHide={ocultarModal} size="lg">
                <Modal.Header>
                    <Modal.Title>
                        Agregar participante
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={submitParticipante}>
                    <Modal.Body>
                        <CSRFToken/>
                        <Form.Group>
                            <Form.Label>Email del participante*</Form.Label>
                            <Form.Control type="email" name="email" required
                                          onChange={(event)=>setEmailTexto(event.target.value)}/>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Cancelar</Button>
                        <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={estadoParticipantes === ESTADO_STATE.PENDIENTE || !puedeAgregar ? popover_error_aceptar : popover_info_aceptar}>
                            <Button variant={estadoParticipantes === ESTADO_STATE.PENDIENTE || !puedeAgregar ? "secondary" : "primary"}
                                    type={estadoParticipantes === ESTADO_STATE.PENDIENTE || !puedeAgregar ? "button" : "submit"}>{textoBoton}</Button>
                        </OverlayTrigger>
                    </Modal.Footer>
                </Form>
            </Modal>
        </Col>
    );
}