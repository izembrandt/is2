import React from "react";
import {AsignarRolParticipante} from "./AsignarRolParticipante";
import {EliminarParticipante} from "./EliminarParticipante";
import {VerParticipante} from "./VerParticipante";



export const AccionesParticipante = ({ email }) => {
    return (
        <>

            {/*Boton para ver los roles de un participante del prpyecto*/}
            <VerParticipante correo={email}/>
            {/*Separador*/}
            <a > / </a>

            {/*Boton para asignar roles a un participante*/}
            <AsignarRolParticipante correo={email}/>
            {/*Separador*/}
            <a > / </a>

            {/*Boton para eliminar a un participante del prpyecto*/}
            <EliminarParticipante correo={email}/>
        </>
    );
}