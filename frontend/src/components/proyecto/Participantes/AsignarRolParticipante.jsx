import React, {useEffect, useState} from "react";
import {Col, Button, Modal, Form, Row, Popover, OverlayTrigger} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {useDispatch, useSelector} from "react-redux";
import {
    asignarRolParticipante,
    seleccionarEstadoParticipantes, seleccionarParticipantePorCorreo
} from "../../../redux/proyecto/participantes";
import {seleccionarInfoProyecto, seleccionarPermisosProyecto} from "../../../redux/proyecto/proyecto";
import DualListBox from "react-dual-listbox";
import 'react-dual-listbox/lib/react-dual-listbox.css';
import {fetchListaRoles, seleccionarListaRolesProyecto, vaciarRoles} from "../../../redux/proyecto/roles";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import styles from "../Fases/css/estilo.module.css";

export const AsignarRolParticipante = ({ correo }) => {
    //States
    let [textoBoton, setTextoBoton] = useState("Guardar");
    let [rolesSeleccionados, setRolesSeleccionados] = useState([]);
    let roles = useSelector(seleccionarListaRolesProyecto).map(rol => ({value: rol.uuid, label: rol.nombre}));
    let estadoParticipantes = useSelector(seleccionarEstadoParticipantes);
    let participante = useSelector(seleccionarParticipantePorCorreo(correo));
    let proyecto = useSelector(seleccionarInfoProyecto);
    let dispatch = useDispatch();
    let permisos = useSelector(seleccionarPermisosProyecto);
    let { estado } = useSelector(seleccionarInfoProyecto);
    let puedeAsignarRol = permisos.includes(PERMISOS_PROYECTO.ASIGNAR_ROLES) && estado !== ESTADO_PROYECTO.CANCELADO;

    //Modal
    const [modalActivo, setModalActivo] = useState(false);

    let mostrarModal = () => {
        dispatch(fetchListaRoles(proyecto.id)); //Actualizamos la lista de roles
        setRolesSeleccionados(participante.roles.map( rol => rol.uuid));
        setModalActivo(true);
    };
    let ocultarModal = () => {
        if (modalActivo) dispatch(vaciarRoles());
        setModalActivo(false)
    };


    //Efecto
    useEffect(()=>{
        switch (estadoParticipantes){
            case ESTADO_STATE.COMPLETADO:
                setTextoBoton("Guardar");
                ocultarModal();
                break;
            case ESTADO_STATE.VACIO:
                setTextoBoton("No disponible");
                break;
            case ESTADO_STATE.PENDIENTE:
                setTextoBoton("Cargando..");
                break;
            case ESTADO_STATE.FALLADO:
                setTextoBoton("Error");
                break;
        }
    }, [dispatch, estadoParticipantes]);

    //Handle
    let submitParticipante = event => {
        event.preventDefault();
        let datos = new FormData(event.target);
        datos.append("usuario_correo", correo);
        rolesSeleccionados.forEach(rol => datos.append("roles_asignados", rol));
        dispatch(asignarRolParticipante({id: proyecto.id, formData:datos }))
    }


    let errores = [];
    if(estado === ESTADO_PROYECTO.CANCELADO){
        errores.push((<li key="proyecto_cancelado">El proyecto no debe estar en estado cancelado</li>))
    }
    if(!permisos.includes(PERMISOS_PROYECTO.ASIGNAR_ROLES)){
        errores.push((<li key="asignar_roles">No posee el permiso para asignar roles</li>))
    }

    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Asignar roles</Popover.Title>
            <Popover.Content>
                Asigna roles al participante
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede asignar roles</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    return(
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeAsignarRol? popover_info : popover_error}>
            <a href={"#"} className={puedeAsignarRol ? "" : styles.isDisabled} onClick={puedeAsignarRol ? mostrarModal : ocultarModal}>Asignar Roles</a>
            </OverlayTrigger>
                <Modal show={modalActivo} onHide={ocultarModal} size="lg">
                <Modal.Header>
                    <Modal.Title>
                        Asignar Rol
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={submitParticipante}>
                    <Modal.Body>
                        <CSRFToken/>
                        <DualListBox options={roles} selected={rolesSeleccionados} onChange={roles => setRolesSeleccionados(roles)} />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={ocultarModal}>Cancelar</Button>
                        <Button variant="primary" type="submit"
                                disabled={estadoParticipantes !== ESTADO_STATE.COMPLETADO}>{textoBoton}</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}