import React from "react";
import {Row, Col, Card, Form, Button} from "react-bootstrap";
import ContenedorFluido from "../../Contenido/ContenedorFluido";
import {useSelector} from "react-redux";
import {seleccionarInfoProyecto} from "../../../redux/proyecto/proyecto";
import {ESTADO_PROYECTO} from "../../../constantes/estados";
const estados_informativo = {
    [ESTADO_PROYECTO.CANCELADO]: 'Cancelado',
    [ESTADO_PROYECTO.EJECUCION]: 'En ejecución',
    [ESTADO_PROYECTO.FINALIZADO]: 'Finalizado',
    [ESTADO_PROYECTO.PENDIENTE]: 'Pendiente'
}

const InformacionProyecto = () => {
    let proyectoSeleccionado = useSelector(seleccionarInfoProyecto);

    let nombre_proy = proyectoSeleccionado.nombre,
        descripcion_proy = proyectoSeleccionado.descripcion,
        fecha_proy = proyectoSeleccionado.fecha_creacion,
        estado_proy = estados_informativo[proyectoSeleccionado.estado]

    return (

        <div className="col-lg-6">
            <div className="card shadow mb-4">

                <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 className="m-0 font-weight-bold text-primary">Información del Proyecto</h6>
                </div>
                <Card.Body>
                    <p><strong>Nombre:</strong>{' '}{nombre_proy}</p>
                    <p><strong>Descripción:</strong>{' '}{descripcion_proy}</p>
                    <p><strong>Fecha de creación:</strong>{' '}{fecha_proy}</p>
                    <p><strong>Estado:</strong>{' '}{estado_proy}</p>
                </Card.Body>


            </div>
        </div>

    );
}


export default InformacionProyecto;