import React, {useEffect} from "react";
import {Form } from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {actualizarGerente, seleccionarInfoProyecto} from "../../../redux/proyecto/proyecto";
import CSRFToken from "../../CSRFToken";

const GerenteProyecto = () => {
    let dispatch = useDispatch();
    let proyecto = useSelector(seleccionarInfoProyecto);

    let handleAsignar = event => {
        //Prevenimos el evento
        event.preventDefault();
        //Recogemos datos
        let datos = new FormData(event.target);
        //Despachamos
        dispatch(actualizarGerente({id: proyecto.id, datos: datos}))
    }
    return (
        <div className="col-lg-6" id="card-seleccionar-gerente">

            <div className="card shadow mb-4">
                <div className="card-header py-3">

                    <h6 className="m-0 font-weight-bold text-primary">Gerente del Proyecto</h6>

                </div>
                <div className="card-body">
                    <form onSubmit={handleAsignar}>
                        <CSRFToken/>
                        <Form.Group>
                            <Form.Label>Email del gerente</Form.Label>
                            <Form.Control type="email" name="email_gerente" required/>
                        </Form.Group>
                        <button type="submit" className="btn btn-primary mb-2">Seleccionar Gerente</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default GerenteProyecto;