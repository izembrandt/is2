import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
    finalizarProyecto,
    seleccionarInfoProyecto,
    seleccionarPermisosProyecto, verificarPermisoProyecto
} from "../../../redux/proyecto/proyecto";
import {Button, Modal, OverlayTrigger, Popover} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {ESTADO_PROYECTO} from "../../../constantes/estados";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";

const FinalizarProyectoComp = () => {
    let dispatch = useDispatch();
    let proyecto = useSelector(seleccionarInfoProyecto);

    const [modalAbierto, setModalAbierto] = useState(false);

    //POPOVER
    const puedeFinalizar = useSelector(verificarPermisoProyecto(PERMISOS_PROYECTO.FINALIZAR_PROYECTO))
    && proyecto.estado === ESTADO_PROYECTO.EJECUCION
    && proyecto.finalizable;

    //FUNCIONES
    const abrirModal = () => {
        setModalAbierto(true)
    };
    const cerrarModal = () => {
        setModalAbierto(false)
    };

    const onSubmit = event => {
        event.preventDefault();
        let datos = new FormData(event.target);
        dispatch(finalizarProyecto({id: proyecto.id, datos}))
            .then(cerrarModal);
    }

    //POPOVER ERRORES
    let nada = () => {};
    let errores = [];
    if(!(useSelector(verificarPermisoProyecto(PERMISOS_PROYECTO.FINALIZAR_PROYECTO)))){
        errores.push((<li key="permiso">No posee permisos para finalizar el proyecto</li>));
    }
    if(proyecto.estado !== ESTADO_PROYECTO.EJECUCION){
        errores.push((<li key="ejecucion">El proyecto no esta en estado ejecucion</li>));
    }
    if(!proyecto.finalizable){
        errores.push((<li key="pendiente">El proyecto no esta preparado para ser finalizado</li>));
    }

    //POPOVER MENSAJES
    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Finalizar el Proyecto!</Popover.Title>
            <Popover.Content>
                Finaliza definitivamente el proyecto para su cierre.
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede finalizar el Proyecto.</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )
    return (
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeFinalizar
                ? popover_info : popover_error}>
                <Button variant={puedeFinalizar ? "primary" : "secondary"} block
                        onClick={puedeFinalizar ? abrirModal : nada}>
                    Finalizar proyecto
                </Button>
            </OverlayTrigger>
            <Modal show={modalAbierto} size="xs" onHide={cerrarModal}>
                <Modal.Header>
                    <Modal.Title>
                        Finalizar el proyecto
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    ¿Desea finaliza este proyecto? Despues de esta acción no se podrá cancelar el proyecto y será
                    marcado como finalizado.
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={cerrarModal}>Cancelar</Button>
                    <form onSubmit={onSubmit}>
                        <CSRFToken/>
                        <Button variant="primary" type="submit">Finalizar el proyecto</Button>
                    </form>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default FinalizarProyectoComp;