import React, {useEffect} from "react";
import {Button, Form, OverlayTrigger, Popover} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {urlCancelarProyecto, urlEjecutarProyecto, urlGenerarInformeItems} from "../../../url";
import {useDispatch, useSelector} from "react-redux";
import {
    seleccionarEstadoProyecto,
    seleccionarInfoProyecto,
    seleccionarPermisosProyecto, verificarPermisoProyecto
} from "../../../redux/proyecto/proyecto";
import {ESTADO_PROYECTO, ESTADO_STATE} from "../../../constantes/estados";
import {fetchFasesProyecto, seleccionarFasesProyecto, vaciarFases} from "../../../redux/proyecto/fases";
import FinalizarProyectoComp from "./FinalizarProyectoComp";
import EditarProyecto from "./EditarProyecto";
import {toast} from "react-toastify";
import {fetchItemsProyecto, seleccionarItemsProyecto} from "../../../redux/proyecto/items";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";

const AccionesProyecto = () => {
    let {id, estado} = useSelector(seleccionarInfoProyecto);
    let dispatch = useDispatch()
    let permisosProyecto = useSelector(seleccionarPermisosProyecto);
    let estadoProyecto = useSelector(seleccionarEstadoProyecto);
    let hayFases = useSelector(seleccionarFasesProyecto).length > 0;
    let hayItems = useSelector(seleccionarItemsProyecto).length > 0;
    let puedeDescargar = hayFases && hayItems;
    let puedeEjecutar = hayFases
        && estado === ESTADO_PROYECTO.PENDIENTE
        && permisosProyecto.includes(PERMISOS_PROYECTO.EJECUTAR_PROYECTO);
    let puedeCancelar = estado !== ESTADO_PROYECTO.CANCELADO
        && estado !== ESTADO_PROYECTO.FINALIZADO
        && permisosProyecto.includes(PERMISOS_PROYECTO.CANCELAR_PROYECTO) ;
    //Despachar items
    useEffect(() => {
        if (id !== 0) {
            dispatch(fetchFasesProyecto(id));
            dispatch(fetchItemsProyecto({id:id}));
        }
    }, [dispatch, estadoProyecto]);

    let sinEvento = () => {};
    let erroresInforme = [];
    if(!hayFases){
        erroresInforme.push((<li key="fases">El proyecto no tiene fases</li>));
    }
    if(!hayItems){
        erroresInforme.push((<li key="item">El proyecto no tiene items</li>));
    }
    let erroresEjecutar = [];
    if(!hayFases){
        erroresEjecutar.push((<li key="fases">El proyecto no tiene fases</li>));
    }
    if(estado !== ESTADO_PROYECTO.PENDIENTE){
        erroresEjecutar.push((<li key="pendiente">El proyecto no esta en estado pendiente</li>));
    }
    if(!(useSelector(verificarPermisoProyecto(PERMISOS_PROYECTO.EJECUTAR_PROYECTO)))){
        erroresEjecutar.push((<li key="permiso">No posee permisos para ejecutar el proyecto</li>));
    }
    let erroresCancelar = [];
    if(estado === ESTADO_PROYECTO.CANCELADO){
        erroresCancelar.push((<li key="cancelado">El proyecto ya esta cancelado</li>));
    }
    if(estado === ESTADO_PROYECTO.FINALIZADO){
        erroresCancelar.push((<li key="finalizado">El proyecto ya no puede cancelarse porque ya esta Finalizado</li>));
    }
    if(!(useSelector(verificarPermisoProyecto(PERMISOS_PROYECTO.CANCELAR_PROYECTO)))){
        erroresCancelar.push((<li key="permiso">No posee permisos para cancelar el proyecto</li>));
    }

   const popover_info = (titulo, contenido) => {
         return <Popover id="popover-basic">
                <Popover.Title as="h3">{titulo}</Popover.Title>
                <Popover.Content>
                    {contenido}
                </Popover.Content>
         </Popover>;
    };
   const popover_error = (titulo, errores) =>
    {
        return <Popover id="popover-basic">
            <Popover.Title as="h3">{titulo}</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    };


    return (

        <div className="col-lg-6">
            <div className="card shadow mb-4">
                <div className="card-header py-3">
                    <h6 className="m-0 font-weight-bold text-primary">Acciones del proyecto</h6>
                </div>
                <div className="card-body align-items-center">
                    <div className="m-auto text-center">
                        <Form method='post' action={urlEjecutarProyecto(id)}>
                            <CSRFToken/>
                            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeEjecutar?
                                     popover_info ("Ejecutar el Proyecto", "Puede cambiar el estado del proyecto a en Ejecucion")
                                     : popover_error ("No puede Ejecutar el proyecto", erroresEjecutar)}>
                                <input type = {puedeEjecutar ? "submit" : "button"} className={puedeEjecutar ? "btn btn-primary btn-block" : "btn btn-secondary btn-block" } value="Ejecutar Proyecto"/>
                            </OverlayTrigger>
                        </Form>
                        <p></p>
                        <Form method='post' action={urlCancelarProyecto(id)}>
                            <CSRFToken/>
                            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeCancelar?
                                     popover_info ("Cancelar el Proyecto", "Cancela el proyecto, una vez cancelado ya no podran realizarse cambios")
                                     : popover_error ("No puede Ejecutar el proyecto", erroresCancelar)}>
                                <input type = {puedeCancelar ? "submit" : "button"} className={puedeCancelar ? "btn btn-danger btn-block" : "btn btn-secondary btn-block" } value="Cancelar Proyecto"/>
                            </OverlayTrigger>
                        </Form>
                        <p></p>
                        <EditarProyecto/>
                        <p></p>
                        <FinalizarProyectoComp/>
                        <p></p>
                            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeDescargar
                                                ? popover_info ("Descarga el informe de los Items del Proyecto", "El informe contiene una lista de todos los items con estado en Desarrollo y Pendiente")
                                                : popover_error("No se puede realizar el informe de items", erroresInforme)}>
                                <Button variant={puedeDescargar ? "success" : "secondary"} block onClick={puedeDescargar ? () => {
                                    toast.success("Informe se esta descargando!")
                                    saveAs(urlGenerarInformeItems(id), "informe.pdf");}
                                    : sinEvento}>
                                    Descargar Informe de Items
                                </Button>
                            </OverlayTrigger>
                    </div>
                </div>
            </div>
        </div>
    );
}


export default AccionesProyecto;