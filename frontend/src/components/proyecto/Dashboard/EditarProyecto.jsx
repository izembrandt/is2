import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
    seleccionarInfoProyecto,
    seleccionarPermisosProyecto, verificarPermisoProyecto
} from "../../../redux/proyecto/proyecto";
import {ESTADO_PROYECTO} from "../../../constantes/estados";
import {PERMISOS_PROYECTO} from "../../../constantes/permisos";
import {Button, Modal, Form, OverlayTrigger, Popover} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {urlModificarProyecto} from "../../../url";


const EditarProyecto = () => {
    let dispatch = useDispatch();
    let proyecto = useSelector(seleccionarInfoProyecto);

    const [modalAbierto, setModalAbierto] = useState(false);
    //POPOVER CONDICION
    const puedeEditar =
        useSelector(verificarPermisoProyecto(PERMISOS_PROYECTO.EJECUTAR_PROYECTO))
        && proyecto.estado === ESTADO_PROYECTO.PENDIENTE;

    //FUNCIONES
    const abrirModal = () => {
        setModalAbierto(true)
    };
    const cerrarModal = () => {
        setModalAbierto(false)
    };

    //POPOVER ERRORES
    let nada = () => {
    };
    let errores = [];
    if (!(useSelector(verificarPermisoProyecto(PERMISOS_PROYECTO.EJECUTAR_PROYECTO)))) {
        errores.push((<li key="permiso">No posee permisos para editar informacion del proyecto</li>));
    }
    if (proyecto.estado !== ESTADO_PROYECTO.PENDIENTE) {
        errores.push((<li key="pendiente">El proyecto no esta en estado pendiente</li>));
    }

    //POPOVER MENSAJES
    const popover_info = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Editar la informacion de Proyecto!</Popover.Title>
            <Popover.Content>
                Modifica el nombre y la descripcion de este proyecto
            </Popover.Content>
        </Popover>
    );
    const popover_error = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">No se puede editar la informacion del Proyecto.</Popover.Title>
            <Popover.Content>
                <ul>
                    {errores}
                </ul>
            </Popover.Content>
        </Popover>
    )

    return (
        <>
            <OverlayTrigger trigger={["hover", "click"]} placement="auto" overlay={puedeEditar
                ? popover_info : popover_error}>
                <Button variant={puedeEditar ? "primary" : "secondary"} block onClick={puedeEditar ? abrirModal : nada}>
                    Editar detalles del proyecto
                </Button>
            </OverlayTrigger>
            <Modal show={modalAbierto} size="xs" onHide={cerrarModal}>
                <Modal.Header>
                    <Modal.Title>
                        Editar el proyecto
                    </Modal.Title>
                </Modal.Header>
                <form method="post" action={urlModificarProyecto(proyecto.id)}>
                    <CSRFToken/>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Label>Nombre del proyecto*</Form.Label>
                            <Form.Control type="text" name="nombre" maxLength="50"
                                          required defaultValue={proyecto.nombre}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Descripción del proyecto</Form.Label>
                            <Form.Control as="textarea" cols="40" rows="10" name="descripcion"
                                          defaultValue={proyecto.descripcion}/>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={cerrarModal}>Cancelar</Button>
                        <Button variant="primary" type="submit">Editar</Button>
                    </Modal.Footer>
                </form>
            </Modal>
        </>
    );
}

export default EditarProyecto;