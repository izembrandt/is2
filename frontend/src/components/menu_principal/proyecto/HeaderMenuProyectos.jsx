import React, {useState} from "react";
import {Row, Col, Modal, Form} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import CSRFToken from "../../CSRFToken";
import { urlCrearProyecto } from "../../../url";


const HeaderMenuProyectos = ({puedeAgregarProyectos = true}) => (
    <Row>
        <Col xs={12} xl={10} lg={9} md={8}>
            <h1 className="h3 my-2 text-gray-800">Proyectos</h1>
            <p className="mb-4">Lista de proyectos donde eres participante.</p>
        </Col>
        {puedeAgregarProyectos &&
        <OpcionCrearProyecto/>
        }
    </Row>
);

const OpcionCrearProyecto = () => {
    const [mostrarCrearProyecto, setCrearProyecto] = useState(false);

    const abrirModalCrear = () => setCrearProyecto(true);
    const cerrarModalCrear = () => setCrearProyecto(false);

    return (
        <Col xs={12} xl={2} lg={3} md={4} className="mb-4 m-md-auto align-items-center">
            <Button type="button" variant="primary" onClick={abrirModalCrear}>
                Crear nuevo proyecto
            </Button>
            <Modal show={mostrarCrearProyecto} onHide={cerrarModalCrear}>
                <Modal.Header>
                    <Modal.Title>
                        Crear nuevo proyecto
                    </Modal.Title>
                </Modal.Header>
                <Form method="post" action={urlCrearProyecto}>
                    <Modal.Body>
                        <CSRFToken/>
                        <Form.Group controlId="formNombreProyecto"> {/* Nombre del Proyecto */}
                            <Form.Label>Nombre de Proyecto*</Form.Label>
                            <Form.Control type="text" name="nombre" maxLength="50" required/>
                            <Form.Text>El nombre solo puede contener hasta 50 caracteres de largo.</Form.Text>
                        </Form.Group>
                        <Form.Group controlId="formDescripcionProyecto">
                            <Form.Label>Descripción</Form.Label>
                            <Form.Control as="textarea" cols="40" rows="10" name="descripcion"/>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={cerrarModalCrear}>Cancelar</Button>
                        <Button variant="primary" type="submit">Crear Proyecto</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </Col>
    );
}


export default HeaderMenuProyectos;