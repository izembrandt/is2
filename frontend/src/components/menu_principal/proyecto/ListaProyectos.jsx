import React, {Component} from "react";
import {Row, Col, Card, Nav, Badge} from "react-bootstrap";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from 'react-bootstrap-table2-paginator';
import { Link } from "react-router-dom";

import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import {urlProyecto} from "../../../url";

const BadgesEstadoProyecto = {
    C: <Badge variant="danger" pill>Cancelado</Badge>,
    P: <Badge variant="secondary" pill>Pendiente</Badge>,
    E: <Badge variant="primary" pill>En Ejecución</Badge>,
    F: <Badge variant="success" pill>Finalizado</Badge>,
}


const Columnas = [
    {
        text: 'Nombre',
        dataField: 'nombre',
        formatter: (cell, row) => (<Link to={urlProyecto + row.id}>{cell}</Link>),
        sort: true,
    },
    {
        text: 'Gerente',
        dataField: 'nombre_gerente',
        sort: true,
    },
    {
        text: 'Estado',
        dataField: 'estado',
        formatter: (cell) => (BadgesEstadoProyecto[cell]),
        sort: true,
    }
]

const defaultOrdenamiento = [
    {
        dataField: 'estado',
        order: 'desc'
    },
    {
        dataField: 'nombre',
        order: 'asc'
    }
]

class ListaProyectos extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filtrarPor: ['P', 'E']
        }
    }

    render() {
        let {proyectos} = this.props;
        let proyectosFiltrados = proyectos.filter(({estado}) => (this.state.filtrarPor.includes(estado)));

        return (
            <Row>
                <Col>
                    <Card className="text-center">
                        <Card.Header> { /* Pestañas*/}
                            <Nav variant="tabs" defaultActiveKey="#TusProyectos">
                                <Nav.Item>
                                    <Nav.Link href="#TusProyectos"
                                              onClick={() => this.setState({
                                                      filtrarPor: ['P', 'E']
                                                  }
                                              )}
                                    >Tus Proyectos</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link href="#Cancelados"
                                              tabIndex="-1"
                                              aria-disabled="true"
                                              onClick={() => this.setState({
                                                      filtrarPor: ['C']
                                                  }
                                              )}
                                    >Cancelados
                                    </Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link href="#Finalizados"
                                              onClick={() => this.setState({
                                                      filtrarPor: ['F']
                                                  }
                                              )}
                                    >Finalizados</Nav.Link>
                                </Nav.Item>
                            </Nav>
                        </Card.Header>
                        <Card.Body> {/* Tabla de proyectos */}
                            <BootstrapTable
                                keyField="id"
                                data={proyectosFiltrados}
                                columns={Columnas}
                                noDataIndication="No tienes proyectos"
                                defaultSorted={defaultOrdenamiento}
                                pagination={ paginationFactory() }
                            />
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        );
    }

}

export default ListaProyectos;
