import React from 'react';
import CasitaMenuPrincipal from "../CasitaMenuPrincipal/CasitaMenuPrincipal";
import InformacionUsuario from "../InformacionUsuario/InformacionUsuario";
import {urlMenuPrincipal} from "../../../url";
import {useSelector} from "react-redux";
import {seleccionarPerfilUsuario} from "../../../redux/perfil";

const TopbarNavbar = ({children}) => {

    const usuario = useSelector(seleccionarPerfilUsuario);

    return (
        <ul className="navbar-nav ml-auto">
            {children}
            <CasitaMenuPrincipal urlMenuPrincipal={urlMenuPrincipal}/>
            <InformacionUsuario nombre={usuario.nombre} apellido={usuario.apellido} email={usuario.correo}/>
        </ul>
    );
}

export default TopbarNavbar;