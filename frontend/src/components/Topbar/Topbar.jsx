import React from 'react';

const Topbar = ({children}) => {
    return (
        <nav className="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
            {children}
        </nav>
    );
}

export default Topbar;