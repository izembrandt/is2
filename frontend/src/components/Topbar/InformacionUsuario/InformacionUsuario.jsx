import React, {useState} from 'react';
import {Link} from "react-router-dom";
import {Modal, Button} from "react-bootstrap";
import CSRFToken from "../../CSRFToken";
import {urlAjustesPerfil, urlCerrarSesion} from "../../../url";

const InformacionUsuario = ({nombre = "Nombre", apellido = "Apellido", email = "Correo"}) => {
    const [mostrarLogout, setMostrarLogout] = useState(false);

    const cerrarLogout = () => setMostrarLogout(false);
    const abrirLogout = () => setMostrarLogout(true);

    return (
        <li className="nav-item dropdown no-arrow">
            <a className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span className="mr-2 d-none d-lg-inline text-gray-600 small">{nombre} {apellido} - {email}</span>
                <img className="img-profile rounded-circle" src="https://i5.walmartimages.ca/images/Large/094/514/6000200094514.jpg"/>
            </a>
            <div className="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                 aria-labelledby="userDropdown">
                <Link className="dropdown-item" to={ urlAjustesPerfil }>
                    <i className="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"/>
                    Ajustes
                </Link>
                <div className="dropdown-divider"/>
                <a className="dropdown-item" onClick={abrirLogout}>
                    <i className="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"/>
                    Cerrar Sesión
                </a>
            </div>
            <Modal show={mostrarLogout} onHide={cerrarLogout}>
                <Modal.Header closeButton>
                    <Modal.Title>Cerrar Sesión</Modal.Title>
                </Modal.Header>
                <Modal.Body>Haz click en "Cerrar sesión" si deseas terminar tu sesión actual y volver al
                    inicio.</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={cerrarLogout}>
                        Cancelar
                    </Button>
                    <form method="post" action={urlCerrarSesion}>
                        <CSRFToken/>
                        <Button variant="primary" type="submit">
                            Cerrar Sesión
                        </Button>
                    </form>

                </Modal.Footer>
            </Modal>
        </li>
    );
}

export default InformacionUsuario;