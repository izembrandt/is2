import React from 'react';

const CasitaMenuPrincipal = ({urlMenuPrincipal}) => {
    return(
        <li className="nav-item no-arrow">
                <a className="nav-link" href={urlMenuPrincipal}>
                    <i className="fas fa-home"></i>
                </a>
        </li>
    );
};

export default CasitaMenuPrincipal;
