import React from "react";
import Cookies from "js-cookie";

const CSRFToken = () => {
    const csrfToken = Cookies.get('csrftoken');

    return(
        <input type="hidden" name="csrfmiddlewaretoken" value={csrfToken}/>
    );
}

export default CSRFToken;