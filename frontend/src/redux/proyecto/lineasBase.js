import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import {ESTADO_ITEM, ESTADO_STATE} from "../../constantes/estados";
import {
    urlAgregarItemLineaBase, urlCerrarLineaBase,
    urlCrearLineaBase,
    urlEliminarItemLineaBase,
    urlModificarLineaBase,
    urlObtenerLineasBase
} from "../../url";
import {seleccionarUltimosItems} from "./items";
import {toast} from "react-toastify";

//Estado Inicial
const estadoInicial = {
    lineas: [],
    estado: ESTADO_STATE.VACIO,
    error: ""
}


//Asíncronos
export const fetchLineasBase = createAsyncThunk("lineas/fetchLineas", async ({id}, {rejectWithValue}) => {
    let response = await fetch(urlObtenerLineasBase(id));
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const crearLineaBase = createAsyncThunk("lineas/crearLinea", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlCrearLineaBase(id), {
        method: 'POST',
        body: datos
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const modificarLineaBase = createAsyncThunk("lineas/modificarLineas", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlModificarLineaBase(id), {
        method: 'POST',
        body: datos
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const agregarItemLineaBase = createAsyncThunk("lineas/agregarItem", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlAgregarItemLineaBase(id), {
        method: 'POST',
        body: datos
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const sacarItemLineaBase = createAsyncThunk("lineas/sacarItem", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlEliminarItemLineaBase(id), {
        method: 'POST',
        body: datos
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const cerrarLinea = createAsyncThunk("lineas/cerrarLinea", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlCerrarLineaBase(id), {
        method: 'POST',
        body: datos
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

//Atajos
const actualizarLineas = mensaje => (state, action) => {
    state.estado = ESTADO_STATE.COMPLETADO;
    state.lineas = action.payload.lineas;
    toast.success(mensaje);
}

const handleError = mensaje => (state, action) => {
    state.estado = ESTADO_STATE.FALLADO;
    state.error = action.payload.error;
    toast.error(mensaje);
}

const pendiente = (state, _) => {
    state.estado = ESTADO_STATE.PENDIENTE;
};


//Slice
const lineasSlice = createSlice({
    name: 'lineas',
    initialState: estadoInicial,
    reducers: {},
    extraReducers: {
        //FULFILLED
        [fetchLineasBase.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.lineas = action.payload.lineas;
        },
        [crearLineaBase.fulfilled]: actualizarLineas("Se creo la linea base correctamente!"),
        [modificarLineaBase.fulfilled]: actualizarLineas("Se modifico la linea base correctamente!"),
        [agregarItemLineaBase.fulfilled]: actualizarLineas("Se agrego el item a la linea base correctamente!"),
        [sacarItemLineaBase.fulfilled]: actualizarLineas("Se removio el item de la linea base correctamente!"),
        [cerrarLinea.fulfilled]: actualizarLineas("Se cerro la linea de base correctamente!"),
        //PENDING
        [fetchLineasBase.pending]: pendiente,
        [crearLineaBase.pending]: pendiente,
        [modificarLineaBase.pending]: pendiente,
        [agregarItemLineaBase.pending]: pendiente,
        [sacarItemLineaBase.pending]: pendiente,
        [cerrarLinea.pending]: pendiente,
        //REJECTED
            [fetchLineasBase.rejected]: handleError("No se pudo obtener la lista de lineas base."),
        [crearLineaBase.rejected]: handleError("No se pudo crear la linea base."),
        [modificarLineaBase.rejected]: handleError("No se pudo modificar la linea base."),
        [agregarItemLineaBase.rejected]: handleError("No se pudo agregar el item a la linea base."),
        [sacarItemLineaBase.rejected]: handleError("No se pudo remover el item de la linea base."),
        [cerrarLinea.rejected]: handleError("No se pudo cerrar la linea base."),
    }
});

//Reducer
export default lineasSlice.reducer;

//Acciones
export const {} = lineasSlice.actions;

//Selectores
export const seleccionarLineasBaseDelProyecto = state => state.lineas.lineas;
export const seleccionarLineaBasePorFaseID = fase_id => state => seleccionarLineasBaseDelProyecto(state).filter(linea => linea.fase === fase_id);
export const seleccionarLineaBasePorUUID = uuid => state => seleccionarLineasBaseDelProyecto(state).find( linea => linea.uuid === uuid);
export const seleccionarItemsDeLineaBaseConUUID = uuid => state => seleccionarUltimosItems(state).filter( item => item.linea_base === uuid );
export const seleccionarItemsCandidatosLineaBaseConUUID = uuid => state => {
    let linea_base = seleccionarLineaBasePorUUID(uuid)(state);
    let candidatos = seleccionarUltimosItems(state).filter(item => item.fase === linea_base.fase && item.estado === ESTADO_ITEM.APROBADO);
    return candidatos;
}

export const seleccionarItemsCandidatosLBPorFaseID = faseID => state => {
    return seleccionarUltimosItems(state)
        .filter(item => item.fase === faseID && item.estado === ESTADO_ITEM.APROBADO)
}

export const seleccionarEstadoLB = state => state.lineas.estado;