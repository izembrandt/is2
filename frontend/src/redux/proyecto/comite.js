//Estado Inicial
import {ESTADO_SOLICITUD, ESTADO_STATE} from "../../constantes/estados";
import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {
    urlAsignarComite,
    urlCrearComite,
    urlCrearSolicitudCambio,
    urlEjercerVoto,
    urlObtenerComite,
    urlObtenerSolicitudes
} from "../../url";
import {toast} from "react-toastify";

const estadoInicial = {
    comite: null,
    solicitudes: [],
    estado: ESTADO_STATE.VACIO,
    error: ""
}

//Asíncronos
export const fetchComiteProyecto = createAsyncThunk("comite/fetchComite", async ({id}, {rejectWithValue}) => {
    let response = await fetch(urlObtenerComite(id));
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const crearComite = createAsyncThunk("comite/crearComite", async ({id, formData}, {rejectWithValue}) => {
    let response = await fetch(urlCrearComite(id), {
        method: 'POST',
        body: formData,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const asignarComite = createAsyncThunk("comite/crearAsignar",
    async ({id, formData}, {rejectWithValue}) => {
        let response = await fetch(urlAsignarComite(id), {
            method: 'POST',
            body: formData,
        });
        let cuerpo = await response.json();
        return response.ok ? cuerpo : rejectWithValue(cuerpo);
    });

export const fetchSolicitudCambio = createAsyncThunk("comite/fetchSolicitud", async ({id}, {rejectWithValue}) => {
    let response = await fetch(urlObtenerSolicitudes(id));
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});


export const crearSolicitudCambio = createAsyncThunk("comite/crearSolicitud",
    async ({id, formData}, {rejectWithValue}) => {
        let response = await fetch(urlCrearSolicitudCambio(id), {
            method: 'POST',
            body: formData,
        });
        let cuerpo = await response.json();
        return response.ok ? cuerpo : rejectWithValue(cuerpo);
    });

export const ejercerVotoSolicitud = createAsyncThunk("comite/ejercerVoto",
    async ({id, formData}, {rejectWithValue}) => {
        let response = await fetch(urlEjercerVoto(id), {
            method: 'POST',
            body: formData,
        });
        let cuerpo = await response.json();
        return response.ok ? cuerpo : rejectWithValue(cuerpo);
    });


//Slice
const comiteSlice = createSlice({
    name: 'comite',
    initialState: estadoInicial,
    reducers: {
        vaciarComite(state, _) {
            state.estado = ESTADO_STATE.VACIO;
        }
    },
    extraReducers: {
        [fetchComiteProyecto.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [fetchComiteProyecto.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.comite = action.payload.comite;
        },
        [fetchComiteProyecto.rejected]: (state, action) => {
            state.estado = ESTADO_STATE.FALLADO;
            state.error = action.payload.error;
            toast.error("No se pudo obtener la lista de comite de Proyecto");
        },
        [crearComite.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [crearComite.fulfilled]: (state, action) => {
            state.comite = action.payload.comite;
            state.estado = ESTADO_STATE.COMPLETADO;
            toast.success("Comite creada exitosamente!");
        },
        [crearComite.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("No se pudo crear el Comite");
        },
        [asignarComite.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [asignarComite.fulfilled]: (state, action) => {
            state.comite = action.payload.comite;
            state.estado = ESTADO_STATE.COMPLETADO;
            toast.success("Se asigno exitosamente el Comite!");
        },
        [asignarComite.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("No se pudo asignar el Comite");
        },
        [fetchSolicitudCambio.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [fetchSolicitudCambio.fulfilled]: (state, action) => {
            state.solicitudes = action.payload.solicitudes;
            state.estado = ESTADO_STATE.COMPLETADO;
        },
        [fetchSolicitudCambio.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("No se pudo obtener las Solicitudes de Cambio");
        },
        [crearSolicitudCambio.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [crearSolicitudCambio.fulfilled]: (state, action) => {
            state.solicitudes = action.payload.solicitudes;
            state.estado = ESTADO_STATE.COMPLETADO;
            toast.success("La solicitud de cambio fue creada exitosamente!");
        },
        [crearSolicitudCambio.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("No se pudo crear la solicitud ce cambio");
        },
        [ejercerVotoSolicitud.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [ejercerVotoSolicitud.fulfilled]: (state, action) => {
            state.solicitudes = action.payload.solicitudes;
            state.estado = ESTADO_STATE.COMPLETADO;
            toast.success("Voto registrado!");
        },
        [ejercerVotoSolicitud.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("No se pudo registrar el voto");
        },
    },
});

//Reducer
export default comiteSlice.reducer;

//Acciones
export const {vaciarComite} = comiteSlice.actions;

//Seleccionador
export const seleccionarComiteProyecto = state => state.comite.comite;
export const seleccionarSolicitudes = state => state.comite.solicitudes;
export const seleccionarSolicitudUUID = solicitud_uuid =>
        state => seleccionarSolicitudes(state).find(solicitud => solicitud.uuid === solicitud_uuid)
export const seleccionarSolicitudesPendientes = state => seleccionarSolicitudes(state)
    .filter(solicitud => solicitud.estado === ESTADO_SOLICITUD.PENDIENTE);
export const seleccionarSolicitudPendienteDeItemUUID = item_uuid => state => seleccionarSolicitudesPendientes(state)
    .find(solicitud => solicitud.item_solicitante === item_uuid)
export const seleccionarSolicitudesPendientesDeFaseID = fase_id => state => {
    return seleccionarSolicitudesPendientes(state)
        .filter(solicitud => solicitud.fase === fase_id);
}

export const verificarEsComite = email => state => {
    const comite = seleccionarComiteProyecto(state);
    // Si no se tiene comite
    if ( comite === null ) return false;
    // Si tiene entonces vemos si pertenece a los integrantes
    return comite.integrantes.find( user => user.correo === email ) !== undefined;
}