import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import {ESTADO_STATE} from "../../constantes/estados";
import {
    urlCrearTipoItem,
    urlEditarTipoItem,
    urlEliminarTipoItem,
    urlImportarTipoItem,
    urlListarTipoItem
} from "../../url";
import {toast} from "react-toastify";

//Estado Inicial
const estadoInicial = {
    tipos: [],
    tipos_importables: [],
    estado: ESTADO_STATE.VACIO,
    error: "",
}

//Asíncronos
export const fetchTiposItem = createAsyncThunk("tipoItem/fetchTipos", async ({id}, {rejectWithValue}) => {
    let response = await fetch(urlListarTipoItem(id));
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const crearTipoItem = createAsyncThunk("tipoItem/crearTipo", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlCrearTipoItem(id), {
        method: 'POST',
        body: datos
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const editarTipoItem = createAsyncThunk("tipoItem/editarTipo", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlEditarTipoItem(id), {
        method: 'POST',
        body: datos
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const eliminarTipoItem = createAsyncThunk("tipoItem/eliminarTipo", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlEliminarTipoItem(id), {
        method: 'POST',
        body: datos
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const importarTipoItem = createAsyncThunk("tipoItem/importarTipo", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlImportarTipoItem(id), {
        method: 'POST',
        body: datos
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});


//Slice
const tipoItemSlice = createSlice({
    name: "tipoItem",
    initialState: estadoInicial,
    reducers: {
        vaciarTipos(state, _) {
            state = estadoInicial;
        }
    },
    extraReducers: {
        [fetchTiposItem.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [fetchTiposItem.fulfilled]: (state, action) => {
            state.tipos = action.payload.tipos;
            state.tipos_importables = action.payload.tipos_importables;
            state.estado = ESTADO_STATE.COMPLETADO;
        },
        [fetchTiposItem.rejected]: (state, action) => {
            state.estado = ESTADO_STATE.FALLADO;
            state.error = action.payload.error;
            toast.error("No se pudo obtener a lista de Tipos items del proyecto");
        },
        [crearTipoItem.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [crearTipoItem.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.tipos = action.payload.tipos;
            toast.success("El Tipo de item fue creado correctamente!");
        },
        [crearTipoItem.rejected]: (state, action) => {
            state.estado = ESTADO_STATE.FALLADO;
            state.error = action.payload.error;
            toast.error("No pudo crearse el tipo de item.");
        },
        [editarTipoItem.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [editarTipoItem.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.tipos = action.payload.tipos;
            toast.success("El Tipo de item fue editado correctamente!");
        },
        [editarTipoItem.rejected]: (state, action) => {
            state.estado = ESTADO_STATE.FALLADO;
            state.error = action.payload.error;
        },
        [eliminarTipoItem.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [eliminarTipoItem.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.tipos = action.payload.tipos;
            toast.success("El Tipo de item fue eliminado correctamente!");
        },
        [eliminarTipoItem.rejected]: (state, action) => {
            state.estado = ESTADO_STATE.FALLADO;
            state.error = action.payload.error;
            toast.error("No pudo eliminarse el tipo de item.");
        },
        [importarTipoItem.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [importarTipoItem.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.tipos = action.payload.tipos;
            toast.success("El Tipo de item fue importado correctamente!");
        },
        [importarTipoItem.rejected]: (state, action) => {
            state.estado = ESTADO_STATE.FALLADO;
            state.error = action.payload.error;
            toast.error("No pudo importarse el tipo de item.");
        }
    }
});

//Reducer
export default tipoItemSlice.reducer;

//Acciones
export const {vaciarTipos} = tipoItemSlice.actions;

//Selectores
export const seleccionarTiposItem = state => state.tipoItem.tipos;
export const seleccionarTipoItemPorID = id => state => state.tipoItem.tipos.find(tipo => tipo.id === id);
export const seleccionarTiposItemPorFase = fase_id => state => state.tipoItem.tipos.filter( tipo => tipo.fase === fase_id );
export const seleccionarEstadoTiposItem = state => state.tipoItem.estado;
export const seleccionarImportables = state => state.tipoItem.tipos_importables;