import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import {ESTADO_STATE} from "../../constantes/estados";
import {urlCrearRol, urlEditarRol, urlEliminarRol, urlListaRoles} from "../../url";
import {toast} from "react-toastify";

//Estado inicial
const estadoInicial = {
    roles: [],
    permisos_proyecto: [],
    permisos_fase: [],
    presets: [],
    estado: ESTADO_STATE.VACIO,
    error: ""
}

//Asíncronos
export const fetchListaRoles = createAsyncThunk("roles/fetchListaRoles", async (id, {rejectWithValue}) => {
    let response = await fetch(urlListaRoles(id));
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const crearRol = createAsyncThunk("roles/crearRol", async ({id, formData}, {rejectWithValue}) => {
    let response = await fetch(urlCrearRol(id), {
        method: 'POST',
        body: formData,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const editarRol = createAsyncThunk("roles/editarRol", async ({id, data}, { rejectWithValue }) => {
    let response = await fetch(urlEditarRol(id), {
       method: 'POST',
       body: data
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const eliminarRol = createAsyncThunk("roles/eliminarRol", async ({id, data}, {rejectWithValue}) => {
    let response = await fetch(urlEliminarRol(id), {
        method: 'POST',
        body: data,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});


//Slice
const rolesSlice = createSlice({
    name: 'roles',
    initialState: estadoInicial,
    reducers: {
        vaciarRoles(state, _) {
            state.roles = [];
            state.estado = ESTADO_STATE.VACIO;
        }
    },
    extraReducers: {
        [fetchListaRoles.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [fetchListaRoles.fulfilled]: (state, action) => {
            state.roles = action.payload.roles;
            state.permisos_proyecto = action.payload.permisos_proyecto;
            state.permisos_fase = action.payload.permisos_fase;
            state.presets = action.payload.presets;
            state.estado = ESTADO_STATE.COMPLETADO;
        },
        [fetchListaRoles.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("No se pudo obtener la lista de los roles del proyecto.");
        },
        [crearRol.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [crearRol.fulfilled]: (state, action) => {
            state.roles = action.payload.roles;
            state.estado = ESTADO_STATE.COMPLETADO;
            toast.success("El rol fue creado correctamente!");
        },
        [crearRol.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("No se pudo crear el rol.");
        },
        [eliminarRol.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [eliminarRol.fulfilled]: (state, action) => {
            state.roles = action.payload.roles;
            state.estado = ESTADO_STATE.COMPLETADO;
            toast.success("El rol fue eliminado correctamente!");
        },
        [eliminarRol.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("No se pudo eliminar el rol.");
        },
        [editarRol.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [editarRol.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("No se pudo editar el rol.");
        },
        [editarRol.fulfilled]: (state, action) => {
            state.roles = action.payload.roles;
            state.estado = ESTADO_STATE.COMPLETADO;
            toast.success("El rol fue editado correctamente!");
        }
    },
});

//Reducer
export default rolesSlice.reducer;

//Acciones
export const {vaciarRoles} = rolesSlice.actions;

//Selectores
export const seleccionarListaRolesProyecto = state => state.roles.roles;
export const seleccionarPermisosProyectoRoles = state => state.roles.permisos_proyecto;
export const seleccionarPermisosFaseRoles = state => state.roles.permisos_fase;
export const seleccionarRolPorUUID = UUID => state => state.roles.roles.find(rol => rol.uuid === UUID);
export const seleccionarErrorRoles = state => state.roles.error;
export const seleccionarRolPresets = state => state.roles.presets;