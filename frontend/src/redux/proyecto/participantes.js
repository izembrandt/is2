import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import {urlAgregarParticipantes, urlAsignarRol, urlEliminarParticipante, urlListaParticipantes} from "../../url";
import {ESTADO_STATE} from "../../constantes/estados";
import {toast} from "react-toastify";

//Estado Inicial
const estadoInicial = {
    estado: ESTADO_STATE.VACIO,
    error: "",
    participantes: []
}

//Asíncronos
export const fetchListaParticipantes = createAsyncThunk("participantes/fetchListaParticipantes", async (id, {rejectWithValue}) => {
    let response = await fetch(urlListaParticipantes(id));
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const agregarParticipante = createAsyncThunk("participantes/agregarParticipante", async ({id, formData}, {rejectWithValue}) => {
    let response = await fetch(urlAgregarParticipantes(id), {
        method: 'POST',
        body: formData
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const asignarRolParticipante = createAsyncThunk("participantes/asignarRol", async ({id, formData}, {rejectWithValue}) => {
    let response = await fetch(urlAsignarRol(id), {
        method: 'POST',
        body: formData
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const eliminarParticipante = createAsyncThunk("participantes/eliminarParticipante", async ({id, formData}, {rejectWithValue})=>{
    let response = await fetch(urlEliminarParticipante(id), {
        method: 'POST',
        body: formData
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

//Slice
const participantesSlice = createSlice({
    name: 'participantes',
    initialState: estadoInicial,
    reducers: {
        vaciarParticipantes(state, _) {
            state = estadoInicial;
        }
    },
    extraReducers: {
        [fetchListaParticipantes.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [fetchListaParticipantes.fulfilled]: (state, action) => {
            state.participantes = action.payload.participantes;
            state.estado = ESTADO_STATE.COMPLETADO;
        },
        [fetchListaParticipantes.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("No se pudo obtener la lista de participantes.")
        },
        [agregarParticipante.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [agregarParticipante.fulfilled]: (state, action) => {
            state.participantes = action.payload.participantes;
            state.estado = ESTADO_STATE.COMPLETADO;
            toast.success("Se agrego el usuario correctamente al proyecto!");
        },
        [agregarParticipante.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("No se pudo agregar el usuario al proyecto.");
        },
        [asignarRolParticipante.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [asignarRolParticipante.fulfilled]: (state, action) => {
            state.participantes = action.payload.participantes;
            state.estado = ESTADO_STATE.COMPLETADO;
            toast.success("Se asigno el rol correctamente al participante!");
        },
        [asignarRolParticipante.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("No se pudo asignar el rol al participante.");
        },
        [eliminarParticipante.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [eliminarParticipante.fulfilled]: (state, action) => {
            state.participantes = action.payload.participantes;
            state.estado = ESTADO_STATE.COMPLETADO;
            toast.success("Se elimino al participante correctamente del proyecto!");
        },
        [eliminarParticipante.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("No se pudo eliminar al participante del proyecto.");
        },
    },
});


//Reducer
export default participantesSlice.reducer;

//Acciones
export const {vaciarParticipantes} = participantesSlice.actions

//Selectores
export const seleccionarListaParticipantesProyecto = state => state.participantes.participantes;
export const seleccionarParticipantePorCorreo = correo => state => state.participantes.participantes.find(value => value.email === correo);
export const seleccionarEstadoParticipantes = state => state.participantes.estado;
