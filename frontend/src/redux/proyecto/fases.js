import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import {ESTADO_STATE} from "../../constantes/estados";
import {urlCrearFase, urlEditarFase, urlCerrarFase, urlEliminarFase, urlListarFases, urlSubirFase, urlBajarFase} from "../../url";
import { toast } from "react-toastify";

//Estado Inicial
const estadoInicial = {
    fases: [],
    estado: ESTADO_STATE.VACIO,
    error: ""
}

//Asíncronos
export const fetchFasesProyecto = createAsyncThunk("fases/fetchFases", async (id, { rejectWithValue }) => {
    let response = await fetch(urlListarFases(id));
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const crearFase = createAsyncThunk("fases/crearFase", async ({id, formData}, {rejectWithValue}) => {
    let response = await fetch(urlCrearFase(id), {
        method: 'POST',
        body: formData,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const editarFase = createAsyncThunk("fases/editarFase", async ({id, data}, { rejectWithValue }) => {
        let response = await fetch(urlEditarFase(id), {
       method: 'POST',
       body: data
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const eliminarFase = createAsyncThunk("fases/eliminarFase", async ({id, data}, {rejectWithValue}) => {
    let response = await fetch(urlEliminarFase(id), {
        method: 'POST',
        body: data,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const cerrarFase = createAsyncThunk("fases/cerrarFase", async ({id, data}, {rejectWithValue}) => {
    let response = await fetch(urlCerrarFase(id), {
        method: 'POST',
        body: data,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const subirFase = createAsyncThunk("fases/subirFase", async ({id, data}, {rejectWithValue}) => {
    let response = await fetch(urlSubirFase(id), {
        method: 'POST',
        body: data,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const bajarFase = createAsyncThunk("fases/bajarFase", async ({id, data}, {rejectWithValue}) => {
    let response = await fetch(urlBajarFase(id), {
        method: 'POST',
        body: data,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});




//Slice
const fasesSlice = createSlice({
    name: 'fases',
    initialState: estadoInicial,
    reducers: {
        vaciarFases(state, _){
            state.estado = ESTADO_STATE.VACIO;
        }
    },
    extraReducers: {
        [fetchFasesProyecto.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [fetchFasesProyecto.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.fases = action.payload.fases;
        },
        [fetchFasesProyecto.rejected]: (state, action) => {
            state.estado = ESTADO_STATE.FALLADO;
            state.error = action.payload.error;
            toast.error("No se pudo obtener la lista de fases del proyecto");
        },
        [crearFase.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [crearFase.fulfilled]: (state, action) => {
            state.fases = action.payload.fases;
            state.estado = ESTADO_STATE.COMPLETADO;
            // console.log(action.payload);
            toast.success("Fase creada exitosamente!");
        },
        [crearFase.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("La fase "+state.fases.fases.nombre +" no pudo ser creada.");
        },
        [editarFase.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [editarFase.fulfilled]: (state, action) => {
            state.fases = action.payload.fases;
            state.estado = ESTADO_STATE.COMPLETADO;
            toast.success("Fase actualizada exitosamente!");
        },
        [editarFase.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("La fase no pudo ser actualizada.");
        },
        [eliminarFase.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [eliminarFase.fulfilled]: (state, action) => {
            state.fases = action.payload.fases;
            state.estado = ESTADO_STATE.COMPLETADO;
            toast.success("Fase eliminada exitosamente!");
        },
        [eliminarFase.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("La fase no pudo ser eliminada.");
        },
        [cerrarFase.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [cerrarFase.fulfilled]: (state, action) => {
            state.fases = action.payload.fases;
            state.estado = ESTADO_STATE.COMPLETADO;
            toast.success("Fase cerrada exitosamente!");
        },
        [cerrarFase.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("La fase no pudo ser cerrada.");
        },
        [subirFase.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [subirFase.fulfilled]: (state, action) => {
            state.fases = action.payload.fases;
            state.estado = ESTADO_STATE.COMPLETADO;
            toast.success("La fase ha subido!");
        },
        [subirFase.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("La fase no pudo subir");
        },
        [bajarFase.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [bajarFase.fulfilled]: (state, action) => {
            state.fases = action.payload.fases;
            state.estado = ESTADO_STATE.COMPLETADO;
            toast.success("La fase ha bajado!");
        },
        [bajarFase.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
            toast.error("La fase no pudo bajar.");
        },
    },
});

//Reducer
export default fasesSlice.reducer;

//Acciones
export const { vaciarFases } = fasesSlice.actions;

//Selectores
export const seleccionarFasesProyecto = state => state.fases.fases;
export const seleccionarFasePorID = ID => state => state.fases.fases.find(fase => fase.id === ID);
export const seleccionarFasePorNumero = numero => state => state.fases.fases.find( fase => fase.numero === numero );