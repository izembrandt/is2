import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import {ESTADO_STATE} from "../../constantes/estados";
import {urlAsignarGerente, urlFinalizarProyecto, urlProyectoObtenerInfo} from "../../url";
import {toast} from "react-toastify";

//Estado Inicial
const estadoInicial = {
    estado: ESTADO_STATE.VACIO,
    error: "",
    proyecto: {
        id: 0,
        nombre: "",
        descripcion: "",
        fecha_creacion: "",
        estado: "",
        gerente: ""
    },
    permisos_proyecto: [],
    roles: [],
}

//Asíncronos
export const fetchInfoProyecto = createAsyncThunk("proyecto/fetchInfo",
    async ({id}, {rejectWithValue}) => {
        let response = await fetch(urlProyectoObtenerInfo(id));
        let cuerpo = await response.json();
        return response.ok ? cuerpo : rejectWithValue(cuerpo);
    });

export const actualizarGerente = createAsyncThunk("proyecto/actualizarGerente", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlAsignarGerente(id), {
        method: 'POST',
        body: datos
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const finalizarProyecto = createAsyncThunk("proyecto/finalizar", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlFinalizarProyecto(id), {
        method: 'POST',
        body: datos
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

//Slice
const proyectoSlice = createSlice({
    name: 'proyecto',
    initialState: estadoInicial,
    reducers: {
        vaciarProyecto(state, _) {
            state = estadoInicial;
        }
    },
    extraReducers: {
        [fetchInfoProyecto.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [fetchInfoProyecto.rejected]: (state, action) => {
            state.estado = ESTADO_STATE.FALLADO;
            state.error = action.payload.error;
            toast.error("No se pudo obtener la informacion del proyecto.");
        },
        [fetchInfoProyecto.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.proyecto = action.payload.proyecto;
            state.permisos_proyecto = action.payload.permisos_proyecto;
            state.roles = action.payload.roles;
        },
        [actualizarGerente.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [actualizarGerente.rejected]: (state, action) => {
            state.estado = ESTADO_STATE.FALLADO;
            state.error = action.payload.error;
            toast.error("No se pudo actualizar el gerente del proyecto.");
        },
        [actualizarGerente.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.proyecto = action.payload.proyecto;
            state.permisos_proyecto = action.payload.permisos_proyecto;
            toast.success("Se actualizo el gerente del proyecto correctamente!");
        },
        [finalizarProyecto.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [finalizarProyecto.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.proyecto = action.payload.proyecto;
            state.permisos_proyecto = action.payload.permisos_proyecto;
            state.roles = action.payload.roles;
            toast.success("Se finalizó el proyecto correctamente.")
        },
        [finalizarProyecto.rejected]: (state, action) => {
            state.estado = ESTADO_STATE.FALLADO;
            state.error = action.payload.error;
            toast.error("No se pudo finalizar el proyecto");
        }
    }
});


//Reducer
export default proyectoSlice.reducer;

//Acciones
export const {vaciarProyecto} = proyectoSlice.actions

//Selectores
export const seleccionarInfoProyecto = state => state.proyecto.proyecto;
export const seleccionarPermisosProyecto = state => state.proyecto.permisos_proyecto;
export const seleccionarEstadoProyecto = state => state.proyecto.estado;

//Funciones
export const verificarPermisoProyecto = permiso => state => seleccionarPermisosProyecto(state).includes(permiso);
export const verificarPermisoFase = (permiso, fase_uuid) => state => {
    let roles = state.proyecto.roles;
    let tiene = false;
    roles.forEach(rol => {
        if (rol.fases.includes(fase_uuid) && rol.permisos_fases.includes(permiso)) tiene = true;
    });

    return tiene;
}