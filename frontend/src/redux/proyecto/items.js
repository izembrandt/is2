import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {ESTADO_ITEM, ESTADO_LINEA, ESTADO_STATE} from "../../constantes/estados";
import {
    urlAceptarRevision,
    urlAprobarItem,
    urlCancelarSolicitudAprobarItem,
    urlCrearItemProyecto,
    urlDesactivarItem,
    urlDesaprobarItem,
    urlDesrelacionarItem,
    urlModificarItemProyecto,
    urlObtenerItemsProyecto,
    urlRelacionarItem,
    urlRestaurarItem, urlRevisarItem,
    urlSolicitarAprobarItem
} from "../../url";
import {seleccionarFasePorID, seleccionarFasePorNumero} from "./fases";
import {toast} from "react-toastify";

const estadoInicial = {
    items: [],
    estado: ESTADO_STATE.VACIO,
    error: "",
}

//Asincronos
export const fetchItemsProyecto = createAsyncThunk("items/fetchItems", async ({id}, {rejectWithValue}) => {
    let response = await fetch(urlObtenerItemsProyecto(id));
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const crearItemProyecto = createAsyncThunk("items/crearItem", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlCrearItemProyecto(id), {
        method: 'POST',
        body: datos,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const solicitarAprobacionItem = createAsyncThunk("items/solicitarAprobacion", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlSolicitarAprobarItem(id), {
        method: 'POST',
        body: datos,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const aprobarItem = createAsyncThunk("items/aprobarItem", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlAprobarItem(id), {
        method: 'POST',
        body: datos,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const cancelarSolicitudItem = createAsyncThunk("items/cancelarSolicitud", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlCancelarSolicitudAprobarItem(id), {
        method: 'POST',
        body: datos,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const desaprobarItem = createAsyncThunk("items/desaprobarItem", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlDesaprobarItem(id), {
        method: 'POST',
        body: datos,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const desactivarItem = createAsyncThunk("items/desactivarItem", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlDesactivarItem(id), {
        method: 'POST',
        body: datos,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const relacionarItem = createAsyncThunk("items/relacionarItem", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlRelacionarItem(id), {
        method: 'POST',
        body: datos,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const desrelacionarItem = createAsyncThunk("items/desrelacionarItem", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlDesrelacionarItem(id), {
        method: 'POST',
        body: datos,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const modificarItem = createAsyncThunk("items/modificarItem", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlModificarItemProyecto(id), {
        method: 'POST',
        body: datos,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const restaurarItem = createAsyncThunk("items/restaurarItem", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlRestaurarItem(id), {
        method: 'POST',
        body: datos,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const aceptarRevisionItem = createAsyncThunk("items/aceptarRevision", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlAceptarRevision(id), {
        method: 'POST',
        body: datos,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const revisarItem = createAsyncThunk("items/revisarItem", async ({id, datos}, {rejectWithValue}) => {
    let response = await fetch(urlRevisarItem(id), {
        method: 'POST',
        body: datos,
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});


//Atajos
const pendiente = (state, _) => {
    state.estado = ESTADO_STATE.PENDIENTE;
}

const error = mensaje => (state, action) => {
    state.estado = ESTADO_STATE.FALLADO;
    state.error = action.payload.error;
    console.log(state.error);
    toast.error(mensaje);
}

const reemplazarItem = mensaje => (state, action) => {
    state.estado = ESTADO_STATE.COMPLETADO;
    let nuevoItem = action.payload.item;
    let index = state.items.findIndex(item => item.uuid === nuevoItem.uuid);
    state.items[index] = nuevoItem;
    toast.success(mensaje);
}

//Slice
const itemsSlice = createSlice({
    name: 'items',
    initialState: estadoInicial,
    reducers: {
        vaciarItems(state, _) {
            state.estado = ESTADO_STATE.VACIO;
        }
    },
    extraReducers: {
        [fetchItemsProyecto.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.items = action.payload; //Recibe un vector de los items
        },
        [aceptarRevisionItem.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.items = action.payload; //Recibe un vector de los items
            toast.success("La revisión se aceptó correctamente!");
        },
        [revisarItem.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.items = action.payload; //Recibe un vector de los items
            toast.success("El item se puede modificar ahora!");
        },
        [crearItemProyecto.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.items = action.payload; //Recibe el nuevo item nomas
            toast.success("Item creado exitosamente!");
        },
        [solicitarAprobacionItem.fulfilled]: reemplazarItem("Se creo la solicitud de aprobacion!"),
        [aprobarItem.fulfilled]: reemplazarItem("El item fue aprobado correctamente!"),
        [cancelarSolicitudItem.fulfilled]: reemplazarItem("Se cancelo la solicitud de aprobacion!"),
        [desaprobarItem.fulfilled]: reemplazarItem("El item fue desaprobado correctamente!"),
        [desactivarItem.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.items = action.payload; //Recibe un vector de los items
            toast.success("El item fue desactivado correctamente!")
        },
        [relacionarItem.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            const {item_origen, item_destino, item_versionado} = action.payload;
            //Reemplazamos el origen
            let index_origen = state.items.findIndex(item => item.uuid === item_origen.uuid);
            state.items[index_origen] = item_origen;
            //Reemplazamos el item destino
            let index_destino = state.items.findIndex(item => item.uuid === item_destino.uuid);
            state.items[index_destino] = item_destino;
            //Agregamos el item versionado
            state.items.push(item_versionado);
            toast.success("El item fue relacionado correctamente!")
        },
        [desrelacionarItem.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            const {item_origen, item_destino, item_versionado} = action.payload;
            //Reemplazamos el origen
            let index_origen = state.items.findIndex(item => item.uuid === item_origen.uuid);
            state.items[index_origen] = item_origen;
            //Reemplazamos el item destino
            let index_destino = state.items.findIndex(item => item.uuid === item_destino.uuid);
            state.items[index_destino] = item_destino;
            //Agregamos el item versionado
            state.items.push(item_versionado);
            toast.success("El item fue desrelacionado correctamente!")
        },
        [modificarItem.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            const {item_nuevo, item_viejo} = action.payload;
            //Reemplazar el nuevo item
            let index_nuevo = state.items.findIndex(item => item.uuid === item_nuevo.uuid);
            state.items[index_nuevo] = item_nuevo;
            //Agregamos la vieja versión al state
            state.items.push(item_viejo);
            toast.success("El item fue modificado correctamente!")
        },
        [restaurarItem.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.items = action.payload; //Recibe un vector de los items
            toast.success("El item fue restaurado correctamente!")
        },
        //ERRORES Y PENDIENTES
        [fetchItemsProyecto.pending]: pendiente,
        [fetchItemsProyecto.rejected]: error("No se pudo obtener la lista de items del proyecto."),
        [crearItemProyecto.pending]: pendiente,
        [crearItemProyecto.rejected]: error("No se pudo crear el item."),
        [solicitarAprobacionItem.pending]: pendiente,
        [solicitarAprobacionItem.rejected]: error("No se pudo crear la solicitud."),
        [aprobarItem.pending]: pendiente,
        [aprobarItem.rejected]: error("No se pudo aprobar el item."),
        [cancelarSolicitudItem.pending]: pendiente,
        [cancelarSolicitudItem.rejected]: error("No se pudo cancelar la solicitud."),
        [desaprobarItem.pending]: pendiente,
        [desaprobarItem.rejected]: error("No se pudo desaprobar el item."),
        [desactivarItem.pending]: pendiente,
        [desactivarItem.rejected]: error("No se puedo desactivar el item."),
        [relacionarItem.pending]: pendiente,
        [relacionarItem.rejected]: error("No se pudo relacionar el item."),
        [desrelacionarItem.pending]: pendiente,
        [desrelacionarItem.rejected]: error("No se pudo desrelacionar el item"),
        [modificarItem.pending]: pendiente,
        [modificarItem.rejected]: error("No se pudo modificar el item."),
        [aceptarRevisionItem.pending]: pendiente,
        [aceptarRevisionItem.rejected]: error("No se pudo aceptar la revisión del item"),
        [revisarItem.pending]: pendiente,
        [revisarItem.rejected]: error("No se pudo revisar el item"),
    }
});

//Reducers
export default itemsSlice.reducer;

//Acciones
export const {vaciarItems} = itemsSlice.actions

//Selectores
export const seleccionarItemsProyecto = state => state.items.items;
export const seleccionarItemPorUUID = uuid => state => state.items.items.find(item => item.uuid === uuid);
export const seleccionarItemsPorFase = fase_id => state => state.items.items.filter(item => item.fase === fase_id);
export const seleccionarEstadoItems = state => state.items.estado;
export const seleccionarUltimosItems = state => state.items.items.filter(item => item.es_ultimo);
export const seleccionarPadresAntecesoresPorUUID = uuid => state => {
    const item_actual = seleccionarItemPorUUID(uuid)(state);
    if (item_actual.es_ultimo) {
        return seleccionarUltimosItems(state)
            .filter(item => item.relaciones.includes(uuid));
    } else {
        return seleccionarUltimosItems(state)
            .filter(item => item_actual.relaciones.includes(item.uuid));
    }

}
export const seleccionarPadresPorUUID = uuid => state => {
    const item = seleccionarItemPorUUID(uuid)(state);
    return seleccionarPadresAntecesoresPorUUID(uuid)(state).filter(padre => item.fase === padre.fase);
}
export const seleccionarAntecesoresPorUUID = uuid => state => {
    const item = seleccionarItemPorUUID(uuid)(state);
    if (item.fase === 1) {
        return [];
    }
    return seleccionarPadresAntecesoresPorUUID(uuid)(state).filter(antecesor => item.fase === antecesor.fase + 1);
}
export const seleccionarPadresCandidatosPorUUID = uuid => state => {
    const {fase} = seleccionarItemPorUUID(uuid)(state);
    return seleccionarUltimosItems(state)
        .filter(candidato => candidato.fase === fase
            && !candidato.relaciones.includes(uuid)
            && [ESTADO_ITEM.APROBADO, ESTADO_ITEM.EN_LINEA_BASE].includes(candidato.estado)
            && candidato.uuid !== uuid);
}
export const seleccionarAntecesoresCandidatosPorUUID = uuid => state => {
    const {fase} = seleccionarItemPorUUID(uuid)(state);
    return seleccionarUltimosItems(state)
        .filter(candidato => candidato.fase + 1 === fase
            && !candidato.relaciones.includes(uuid)
            && candidato.estado === ESTADO_ITEM.EN_LINEA_BASE);
}
export const seleccionarUltimaVersionDelItem = item => state => {
    if (item.es_ultimo) return item;
    return seleccionarItemsProyecto(state)
        .find(cand => cand.prefijo === item.prefijo && cand.numero === item.numero && cand.es_ultimo);
}
export const seleccionarImpactoDeItem = item => state => {
    if (!item.es_ultimo) return 0;
    let peso = item.costo;
    item.relaciones.forEach(relacion => {
        let sucesor = seleccionarItemPorUUID(relacion)(state);
        peso = peso + seleccionarImpactoDeItem(sucesor)(state);
    });
    return peso;
}
export const seleccionarImpactoTotal = state => state.items.items
    .filter(item => item.es_ultimo && item.estado !== ESTADO_ITEM.DESACTIVADO)
    .reduce((suma, item) => (suma + item.costo), 0);

export const seleccionarImpactoRatioDeITEM = item => state => (seleccionarImpactoDeItem(item)(state) / seleccionarImpactoTotal(state));

export const verificarSiPuedeSoliciar = item_a_aprobar => state => {
    if (!item_a_aprobar.es_ultimo) return false;
    if (item_a_aprobar.estado !== ESTADO_ITEM.DESARROLLO) return false;
    let puede = true;
    let padres = seleccionarPadresAntecesoresPorUUID(item_a_aprobar.uuid)(state);
    let fase = seleccionarFasePorID(item_a_aprobar.fase)(state);
    if (padres.length === 0 && fase.numero > 1) return false;
    padres.forEach(item => {
        if ([ESTADO_ITEM.DESARROLLO, ESTADO_ITEM.DESACTIVADO, ESTADO_ITEM.PENDIENTE].includes(item.estado)) {
            puede = false;
        }
    });
    return puede;
};

export const verificarSiPuedeDesaprobar = item_a_aprobar => state => {
    //Verificar que todos sus relaciones esten en desarrollo
    let puede = true;
    item_a_aprobar.relaciones.forEach(item_uuid => {
        let item = seleccionarItemPorUUID(item_uuid)(state);
        if (item.estado !== ESTADO_ITEM.DESARROLLO) {
            puede = false;
        }
    });
    return puede;
}

export const seleccionarRelacionesCandidatosNuevoItem = fase_id => state => {
    const fase = seleccionarFasePorID(fase_id)(state);
    let padresCandidatos = seleccionarItemsPorFase(fase_id)(state)
        .filter(item => [ESTADO_ITEM.APROBADO, ESTADO_ITEM.EN_LINEA_BASE].includes(item.estado));

    if (fase.numero > 1) {
        const fase_anterior = seleccionarFasePorNumero(fase.numero - 1)(state);
        let antecesoresCandidatos = seleccionarItemsPorFase(fase_anterior.id)(state)
            .filter(item => item.estado === ESTADO_ITEM.EN_LINEA_BASE);
        padresCandidatos = padresCandidatos.concat(antecesoresCandidatos);
    }

    return padresCandidatos;
}

export const verificarSiPuedeRevisar = item_a_revisar => state => {
    if (!item_a_revisar.es_ultimo) return false;
    if (item_a_revisar.estado !== ESTADO_ITEM.REVISION) return false;
    let puede = true;
    let padres = seleccionarPadresAntecesoresPorUUID(item_a_revisar.uuid)(state);
    let fase = seleccionarFasePorID(item_a_revisar.fase)(state);
    if (padres.length === 0 && fase.numero > 1) return false;
    padres.forEach(item => {
        if ([ESTADO_ITEM.DESARROLLO, ESTADO_ITEM.DESACTIVADO, ESTADO_ITEM.PENDIENTE, ESTADO_ITEM.REVISION]
            .includes(item.estado)) {
            puede = false;
        }
    });
    return puede;
};