import {createSlice} from "@reduxjs/toolkit";
import {INTERFAZ_MODAL} from "../../constantes/modales";

const estadoInicial = {
    modalActivo: INTERFAZ_MODAL.NINGUNO,
    itemActual: "",
    lineaActual: "",
    solicitudActual: "",
    faseActual: 0,
    stackItem: [],
}

const modalesSlice = createSlice({
    initialState: estadoInicial,
    name: 'modales',
    reducers: {
        cerrarTodo(state, _) {
            state.modalActivo = INTERFAZ_MODAL.NINGUNO;
        },
        abrirInterfazItem(state, action) {
            let {uuid} = action.payload; //Agarramos uuid del item
            state.itemActual = uuid;
            state.modalActivo = INTERFAZ_MODAL.ITEM;
        },
        abrirSolicitarAprobacion(state, _) {
            state.modalActivo = INTERFAZ_MODAL.SOLICITAR_APROBACION;
        },
        abrirCancelarSolicitud(state, _) {
            state.modalActivo = INTERFAZ_MODAL.CANCELAR_SOLICITUD;
        },
        abrirAprobarSolicitud(state, _) {
            state.modalActivo = INTERFAZ_MODAL.APROBAR_SOLICITUD;
        },
        abrirDesactivarItem(state, _) {
            state.modalActivo = INTERFAZ_MODAL.DESACTIVAR_ITEM;
        },
        abrirDesaprobarItem(state, _) {
            state.modalActivo = INTERFAZ_MODAL.DESAPROBAR_ITEM;
        },
        abrirModificarItem(state, _) {
            state.modalActivo = INTERFAZ_MODAL.MODIFICAR_ITEM;
        },
        abrirRelacionarItem(state, _) {
            state.modalActivo = INTERFAZ_MODAL.RELACIONAR_ITEM;
        },
        abrirHistorialItem(state, _) {
            state.modalActivo = INTERFAZ_MODAL.HISTORIAL_ITEM;
        },
        abrirRestaurarItem(state, _) {
            state.modalActivo = INTERFAZ_MODAL.RESTAURAR_ITEM;
        },
        abrirCrearPadre(state, _) {
            state.modalActivo = INTERFAZ_MODAL.CREAR_PADRE;
        },
        abrirCrearAntecesor(state, _) {
            state.modalActivo = INTERFAZ_MODAL.CREAR_ANTECESOR;
        },
        abrirTrazabilidadItem(state, action) {
            state.modalActivo = INTERFAZ_MODAL.TRAZABILIDAD;
            let {uuid} = action.payload; //Agarramos uuid del item
            state.itemActual = uuid;
        },
        abrirModalLB(state, action) {
            let { linea } = action.payload
            state.lineaActual = linea;
            state.modalActivo = INTERFAZ_MODAL.LINEA_BASE;
        },
        abrirModal(state, action) {
            state.modalActivo = action.payload;
        },
        setFase(state, action) {
            state.faseActual = action.payload;
        },
        setSolicitud(state, action) {
            state.solicitudActual = action.payload;
        }
    }
});

export default modalesSlice.reducer;

export const {
    abrirInterfazItem, abrirDesaprobarItem, abrirModificarItem,
    abrirRestaurarItem, abrirRelacionarItem, abrirHistorialItem,
    abrirCancelarSolicitud, abrirDesactivarItem, abrirAprobarSolicitud,
    cerrarTodo, abrirSolicitarAprobacion, abrirCrearAntecesor, abrirCrearPadre,
    abrirTrazabilidadItem, abrirModalLB, abrirModal, setFase, setSolicitud
} = modalesSlice.actions;

export const seleccionarItemAbierto = state => state.modales.itemActual;
export const seleccionarModalAbierto = state => state.modales.modalActivo;
export const seleccionarLineaAbierta = state => state.modales.lineaActual;
export const seleccionarFaseAbierta = state => state.modales.faseActual;
export const seleccionarSolicitudAbierta = state => state.modales.solicitudActual;