import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import {urlActualizarPerfil, urlFetchUsuario} from "../url";
import {ESTADO_STATE} from "../constantes/estados";

//Estado Inicial
const estadoInicial = {
    usuario: {
        nombre: "",
        apellido: "",
        correo: "",
        administrador: false,
    },
    estado: ESTADO_STATE.VACIO,
    error: "",
    permisos: []
}

//Reducers Asíncronos
export const fetchUsuario = createAsyncThunk("perfil/fetchUsuario", async (_, {rejectWithValue}) => {
    let response = await fetch(urlFetchUsuario)
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

export const actualizarPerfil = createAsyncThunk("perfil/actualizarPerfil", async (perfilNuevo, {rejectWithValue}) => {
    //Recibe un form data para mandar
    let response = await fetch(urlActualizarPerfil, {
        method: 'POST',
        body: perfilNuevo
    });
    let cuerpo = await response.json();
    return response.ok ? cuerpo : rejectWithValue(cuerpo);
});

//Slice
const perfilSlice = createSlice({
    name: "perfil",
    initialState: estadoInicial,
    reducers: {},
    extraReducers: {
        [fetchUsuario.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [fetchUsuario.fulfilled]: (state, action) => {
            state.usuario = action.payload.usuario;
            state.estado = ESTADO_STATE.COMPLETADO;
        },
        [fetchUsuario.rejected]: (state, action) => {
            state.estado = ESTADO_STATE.FALLADO;
            state.error = action.payload.error;
        },
        [actualizarPerfil.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [actualizarPerfil.fulfilled]: (state, action) => {
            state.usuario = action.payload.usuario;
            state.estado = ESTADO_STATE.COMPLETADO;
        },
        [actualizarPerfil.rejected]: (state, action) => {
            state.estado = ESTADO_STATE.FALLADO;
            state.error = action.payload.error;
        },
    }
});


export default perfilSlice.reducer; //Exportamos su reducer

//Selectores
export const seleccionarPerfilUsuario = state => state.usuario.usuario;
export const esAdministrador = state => state.usuario.usuario.administrador;
