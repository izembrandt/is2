import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import {urlFetchListaProyectos} from "../url";
import {ESTADO_STATE} from "../constantes/estados";

const estadoInicial = {
    estado: ESTADO_STATE.VACIO,
    error: "",
    lista: []
}

export const fetchListaProyectos = createAsyncThunk("listaProyectos/fetchListaProyectos",
    async (_, {rejectWithValue}) => {
        let response = await fetch(urlFetchListaProyectos);
        let cuerpo = await response.json();
        return response.ok ? cuerpo : rejectWithValue(cuerpo);
    });

const listaProyectosSlice = createSlice({
    name: "listaProyectos",
    initialState: estadoInicial,
    reducers: {
        vaciarLista(state, _) {
            state.estado = ESTADO_STATE.VACIO; //Lo utilizamos para que refresque la próxima vez que entre a proyectos
        }
    },
    extraReducers: {
        [fetchListaProyectos.fulfilled]: (state, action) => {
            state.lista = action.payload.proyectos;
            state.estado = ESTADO_STATE.COMPLETADO;
        },
        [fetchListaProyectos.rejected]: (state, action) => {
            state.error = action.payload.error;
            state.estado = ESTADO_STATE.FALLADO;
        },
        [fetchListaProyectos.pending]: (state, action) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        }
    }
});

//Reducer
export default listaProyectosSlice.reducer;

//Acciones
export const { vaciarLista } = listaProyectosSlice.actions;

//Selector
export const seleccionarListaProyectos = ({ listaProyectos }) => listaProyectos.lista;