import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import {urlAutorizarUsuarioPOST, urlDesautorizarUsuarioPOST, urlObtenerUsuariosAutorizados} from "../url";
import {ESTADO_STATE} from "../constantes/estados";

//Estado Inicial
const estadoInicial = {
    autorizados: [],
    noAutorizados: [],
    estado: ESTADO_STATE.VACIO,
    error: ""
}

//Asíncronos
export const fetchUsuariosAutorizados = createAsyncThunk("usuariosAutorizados/fetchUsuarios",
    async (_, {rejectWithValue}) => {
        let response = await fetch(urlObtenerUsuariosAutorizados);
        let cuerpo = await response.json();
        return response.ok ? cuerpo : rejectWithValue(cuerpo);
    });

export const autorizarUsuarioPOST = createAsyncThunk("usuariosAutorizados/autorizar",
    async (formData, {rejectWithValue}) => {
        let response = await fetch(urlAutorizarUsuarioPOST, {
            method: 'POST',
            body: formData
        });
        let cuerpo = await response.json();
        console.log(cuerpo);
        return response.ok ? cuerpo : rejectWithValue(cuerpo);
    });

export const desautorizarUsuarioPOST = createAsyncThunk("usuariosAutorizados/desautorizar",
    async (formData, {rejectWithValue}) => {
        let response = await fetch(urlDesautorizarUsuarioPOST, {
            method: 'POST',
            body: formData
        });
        let cuerpo = await response.json();
        console.log(cuerpo);
        return response.ok ? cuerpo : rejectWithValue(cuerpo);
    });

//Slice
const usuariosAutorizadosSlice = createSlice({
    name: "usuariosAutorizados",
    initialState: estadoInicial,
    reducers: {
        vaciarUsuarios(state, _) {
            state.estado = ESTADO_STATE.VACIO;
        }
    },
    extraReducers: {
        [fetchUsuariosAutorizados.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [fetchUsuariosAutorizados.rejected]: (state, action) => {
            state.estado = ESTADO_STATE.FALLADO;
            state.error = action.payload.error;
        },
        [fetchUsuariosAutorizados.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.autorizados = action.payload.autorizados;
            state.noAutorizados = action.payload.noAutorizados;
        },
        [autorizarUsuarioPOST.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [autorizarUsuarioPOST.rejected]: (state, action) => {
            state.estado = ESTADO_STATE.FALLADO;
            state.error = action.payload.error;
        },
        [autorizarUsuarioPOST.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.autorizados = action.payload.autorizados;
            state.noAutorizados = action.payload.noAutorizados;
        },
        [desautorizarUsuarioPOST.pending]: (state, _) => {
            state.estado = ESTADO_STATE.PENDIENTE;
        },
        [desautorizarUsuarioPOST.rejected]: (state, action) => {
            state.estado = ESTADO_STATE.FALLADO;
            state.error = action.payload.error;
        },
        [desautorizarUsuarioPOST.fulfilled]: (state, action) => {
            state.estado = ESTADO_STATE.COMPLETADO;
            state.autorizados = action.payload.autorizados;
            state.noAutorizados = action.payload.noAutorizados;
        }
    }
});


//Reducer
export default usuariosAutorizadosSlice.reducer;

//Acciones
export const {vaciarUsuarios} = usuariosAutorizadosSlice.actions;

//Selectores
export const seleccionarUsuariosAutorizados = state => state.usuariosAutorizados.autorizados;
export const seleccionarUsuariosDesautorizados = state => state.usuariosAutorizados.noAutorizados;