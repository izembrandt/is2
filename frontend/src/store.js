import { configureStore } from "@reduxjs/toolkit";
import perfilReducer from "./redux/perfil";
import listaProyectosReducer from "./redux/listaProyectos";
import usuariosAutorizadosReducer from "./redux/usuariosAutorizados";
import proyectoReducer from "./redux/proyecto/proyecto";
import participantesReducer from "./redux/proyecto/participantes";
import rolesReducer from "./redux/proyecto/roles";
import fasesReducer from "./redux/proyecto/fases";
import tipoItemReducer from "./redux/proyecto/tipoItem";
import itemsReducer from "./redux/proyecto/items"
import modalesReducer from "./redux/proyecto/modales"
import lineasReducer from "./redux/proyecto/lineasBase";
import comiteReducer from "./redux/proyecto/comite";
export default configureStore({
  reducer: {
    usuario: perfilReducer,
    listaProyectos: listaProyectosReducer,
    usuariosAutorizados: usuariosAutorizadosReducer,
    proyecto: proyectoReducer,
    participantes: participantesReducer,
    roles: rolesReducer,
    fases: fasesReducer,
    tipoItem: tipoItemReducer,
    items: itemsReducer,
    modales: modalesReducer,
    lineas: lineasReducer,
    comite: comiteReducer,
  }
});
