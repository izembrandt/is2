from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def index(request):
    """Renderiza la aplicación react si es que el usuario posee esta autenticado y posee los permisos
    para utilizar el sistema.

    :param request: HttpRequest del usuario
    :return: HttpResponse con la aplicación React
    """
    return render(request, 'frontend/index.html')
