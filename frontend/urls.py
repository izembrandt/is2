from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.index, name='Frontend App'),
    re_path(r'(?:.*)/?$', views.index, name='Frontend App route'),
    path('proyecto/<int:proyecto_id>', views.index, name='React Proyecto Dashboard')
]
