from typing import List

import pytest
from django.contrib.auth.models import User, Group
import uuid as uuid
from django.core.management import call_command
from django.db.models import QuerySet

from fases.models import Fase
from fases.permisos import PermisoFase
from item.models import TipoItem, Atributo, Item
from linea_base.models import LineaBase
from proyecto.models import Proyecto
from proyecto.permisos import PermisoProyecto
from roles.funciones import crear_grupo_de_rol
from roles.models import Rol

ROL_DESARROLLADOR_NOMBRE = "DESARROLLADOR TEST"
ROL_QA_NOMBRE = "QA TEST"


@pytest.fixture(scope='session')
def django_db_setup(django_db_setup, django_db_blocker):
    with django_db_blocker.unblock():
        call_command('actualizar_config')


@pytest.fixture
def grupo_usuario(db) -> Group:
    return Group.objects.get(name='Usuario')


@pytest.fixture
def user_factory(db, django_user_model: User, grupo_usuario: Group):
    def create_user(username: str = None):
        if username is None:
            username = uuid.uuid4().__str__()

        usuario: User = django_user_model.objects.create_user(username=username, email=username + "@test.com",
                                                              password="test")
        usuario.groups.add(grupo_usuario)

        return usuario

    return create_user


@pytest.fixture
def usuario_gerente(db, user_factory) -> User:
    return user_factory(username="gerente")


@pytest.fixture
def usuario_qa(db, user_factory) -> User:
    return user_factory(username="qa")


@pytest.fixture
def usuario_desarrollador(db, user_factory) -> User:
    return user_factory(username="desarrollador")


@pytest.fixture
def proyecto_factory(db, usuario_gerente: User, usuario_desarrollador: User, usuario_qa: User, rol_factory):
    def crear_proyecto(nombre: str = "Test", descripcion: str = "Test") -> Proyecto:
        # Creamos el proyecto
        proyecto: Proyecto = Proyecto.crear_proyecto(nombre=nombre, descripcion=descripcion)
        proyecto.asignar_gerente(usuario_gerente)
        proyecto.agregar_participante(usuario_desarrollador)
        proyecto.agregar_participante(usuario_qa)

        # Asignamos rol desarrollador
        rol_desarrollador = rol_factory(proyecto=proyecto, fases=Fase.objects.none(), nombre=ROL_DESARROLLADOR_NOMBRE,
                                        permisos_proyecto=PermisoProyecto.permisos_usuario(),
                                        permisos_fases=PermisoFase.permiso_desarrollador(), desc="TEST")

        rol_desarrollador.asignar_usuario(usuario_desarrollador)

        # Asignamos rol qa
        rol_qa = rol_factory(proyecto=proyecto, fases=Fase.objects.none(), nombre=ROL_QA_NOMBRE,
                             permisos_proyecto=PermisoProyecto.permisos_qa(),
                             permisos_fases=PermisoFase.permiso_qa(), desc="TEST")

        rol_qa.asignar_usuario(usuario_qa)

        # Retornamos el proyecto
        return proyecto

    return crear_proyecto


@pytest.fixture
def rol_factory(db):
    def crear_rol(proyecto: Proyecto, fases: QuerySet, nombre: str, permisos_proyecto: List[str],
                  permisos_fases: List[str],
                  desc: str) -> Rol:
        assert isinstance(proyecto, Proyecto), "No se pasó proyecto al crear rol"
        assert isinstance(fases, QuerySet), "Las fases deben ser un QuerySet de Fases"
        assert isinstance(permisos_proyecto, List), "Los permisos deben ser una lista de cadenas"
        assert isinstance(permisos_fases, List), "Los permisos deben ser una lista de cadenas"

        grupo = crear_grupo_de_rol(permisos_proyecto, proyecto, permisos_fases, fases)

        rol = Rol(nombre=nombre, descripcion=desc, proyecto=proyecto, grupo=grupo,
                  permisos_proyecto=permisos_proyecto, permisos_fases=permisos_fases)

        rol.save()
        rol.fases.set(fases)

        return rol

    return crear_rol


@pytest.fixture
def fase_factory(db):
    def crear_fase(proyecto: Proyecto, nombre: str = "TEST", descripcion: str = "TEST") -> Fase:
        assert isinstance(proyecto, Proyecto), "Se requiere un proyecto"
        assert proyecto.en_configuracion(), "El proyecto debe estar en configuración"

        fase: Fase = proyecto.crear_fase(nombre, descripcion)

        rol_desarrollador = proyecto.rol_set.get(nombre=ROL_DESARROLLADOR_NOMBRE)

        rol_desarrollador.agregar_fase(fase)

        rol_qa = proyecto.rol_set.get(nombre=ROL_QA_NOMBRE)

        rol_qa.agregar_fase(fase)

        return fase

    return crear_fase


@pytest.fixture
def tipo_item_factory(db):
    def crear_tipo_item(proyecto: Proyecto, fase: Fase, prefijo: str, nombre: str = "TEST",
                        descripcion: str = "TEST", atributos: List[dict] = []) -> TipoItem:
        assert isinstance(proyecto, Proyecto), "Se requiere proyecto"
        assert isinstance(fase, Fase), "Se requiere fase"
        assert isinstance(atributos, List), "Se requiere lista de atributos"
        assert isinstance(atributos, List), "Se requiere diccionario"

        tipo = TipoItem(nombre=nombre, descripcion=descripcion, prefijo=prefijo, atributos=atributos,
                        proyecto=proyecto, fase=fase)

        tipo.save()

        return tipo

    return crear_tipo_item


@pytest.fixture
def item_factory(db):
    def crear_item(tipo_item: TipoItem, atributos: List[Atributo], nombre: str = "TEST", desc: str = "TEST",
                   costo: int = 0) -> Item:
        assert isinstance(tipo_item, TipoItem)
        assert isinstance(atributos, List)

        item = Item.crear(nombre=nombre, descripcion=desc, costo=costo, tipo_de_item=tipo_item, atributos=atributos)

        return item

    return crear_item


@pytest.fixture
def linea_base_factory(db):
    def crear_lb(fase: Fase, creador: User, nombre: str = "TEST", desc: str = "TEST") -> LineaBase:
        assert isinstance(fase, Fase), "Se requiere fase"
        assert isinstance(creador, User), "Se requiere usuario"
        lb = LineaBase.crear(nombre=nombre, descripcion=desc, fase=fase, creador=creador)
        return lb

    return crear_lb


@pytest.fixture
def estructura_atributo():
    def crear_estructura(posicion: int, nombre: str = "Test", tipo: str = "T", obligatorio: bool = False) -> dict:
        return {
            "posicion": posicion,
            "nombre": nombre,
            "tipo": tipo,
            "obligatorio": obligatorio
        }

    return crear_estructura
