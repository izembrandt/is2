class PermisoFase:
    # ITEMS
    CREAR_ITEM = 'crear_item'
    MODIFICAR_ITEM = 'modificar_item'
    APROBAR_ITEM = 'aprobar_item'
    DESAPROBAR_ITEM = 'desaprobar_item'
    ELIMINAR_ITEM = 'eliminar_item'
    RELACIONAR_ITEM = 'relacionar_item'
    # LINEA BASE
    CREAR_LINEA_BASE = 'crear_linea_base'
    MODIFICAR_LINEA_BASE = 'modificar_linea_base'
    ELIMINAR_LINEA_BASE = 'eliminar_linea_base'
    CERRAR_LINEA_BASE = 'cerrar_linea_base'
    # SOLICITUD DE CAMBIO
    SOLICITAR_CAMBIO = 'solicitar_cambio'

    @classmethod
    def permisos_usuario(cls):
        return [
            # Items
            cls.CREAR_ITEM,
            cls.MODIFICAR_ITEM,
            cls.APROBAR_ITEM,
            cls.DESAPROBAR_ITEM,
            cls.ELIMINAR_ITEM,
            cls.RELACIONAR_ITEM,
            # Linea Base
            cls.CREAR_LINEA_BASE,
            # cls.MODIFICAR_LINEA_BASE, Ya no hay más abierto
            cls.CERRAR_LINEA_BASE,
            # Solicitud de cambio
            cls.SOLICITAR_CAMBIO,
        ]

    @classmethod
    def permiso_gerente(cls):
        return []

    @classmethod
    def permiso_desarrollador(cls):
        return [
            cls.CREAR_ITEM,
            cls.MODIFICAR_ITEM,
            cls.ELIMINAR_ITEM,
            cls.RELACIONAR_ITEM,
            cls.SOLICITAR_CAMBIO,
        ]

    @classmethod
    def permiso_qa(cls):
        return [
            cls.APROBAR_ITEM,
            cls.DESAPROBAR_ITEM,
            cls.CREAR_LINEA_BASE,
            # cls.MODIFICAR_LINEA_BASE, Ya no hay más abierto
            cls.CERRAR_LINEA_BASE,
        ]
