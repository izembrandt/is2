import pytest
from django.test import RequestFactory
from django.urls import reverse
from django.contrib.auth.models import User, Group
from .views import *
from django.utils import timezone
from roles.views import *


@pytest.mark.django_db
def test_ajax_crear_fase():
    """Verifica si que la funcion crear proyectos responda correctamente al request
    """
    # Creamos una fecha y asignamos como descripcion de la fase
    fecha = timezone.now()
    p = Proyecto(nombre="Test", descripcion="test", estado='P', fecha_creacion=fecha)
    p.save()
    # Buscamos el path de lal funcion
    path = reverse('crear_fases', args=[p.id])
    request = RequestFactory().post(
        path,
        {
            'nombre': 'Test',
            'descripcion': fecha,
        },
        HTTP_X_REQUESTED_WITH='XMLHttpRequest'
    )
    # Creamos un usuario que pueda acceder al proyecto
    request.user = User.objects.create_user(username='tester', email='tester@gmail.com', password='password')
    grupo = Group.objects.get(name='Usuario')
    request.user.groups.add(grupo)
    # permisos
    grupo_rol = crear_grupo_de_rol(['listar_proyectos', 'crear_fases'], p, [], [])
    rol = Rol(nombre='test', descripcion='test', proyecto=p, grupo=grupo_rol,
              permisos_proyecto=['listar_proyectos', 'crear_fases'], permisos_fases='')
    rol.save()
    rol.asignar_usuario(request.user)
    p.asignar_gerente(request.user)
    # Creamos la fase
    response = ajax_crear_fase(request, proyecto_id=p.id)
    # comprobamos que fue un exito si se consigue instanciar la fase
    assert Fase.objects.get(nombre='Test',
                            descripcion=fecha), "La funcion deberia obtener una aparicion del objeto creado"


@pytest.mark.django_db
def test_ajax_editar_fase():
    """Verifica si que la funcion crear proyectos responda correctamente al request
    """
    # Creamos una fecha y asignamos como descripcion de la fase
    fecha = timezone.now()
    p = Proyecto(nombre="Test", descripcion="test", estado='P', fecha_creacion=fecha)
    p.save()
    # Creamos una fase
    f = Fase(nombre="faseTest", descripcion="bla bla", proyecto=p, numero=1, estado='A')
    f.save()
    # Buscamos el path de lal funcion
    path = reverse('editar_fases', args=[p.id])
    request = RequestFactory().post(
        path,
        {
            'fid': f.id,
            'nombre': 'Nuevo nombre',
            'descripcion': 'Nueva descripcion',
        }
    )
    # Creamos un usuario que pueda acceder al proyecto
    request.user = User.objects.create_user(username='tester', email='tester@gmail.com', password='password')
    grupo = Group.objects.get(name='Usuario')
    request.user.groups.add(grupo)
    # permisos
    grupo_rol = crear_grupo_de_rol(['listar_proyectos', 'modificar_fases'], p, [], [])
    rol = Rol(nombre='test', descripcion='test', proyecto=p, grupo=grupo_rol,
              permisos_proyecto=['listar_proyectos', 'modificar_fases'], permisos_fases='')
    rol.save()
    rol.asignar_usuario(request.user)
    # Actualizamos la fase
    ajax_editar_fase(request, proyecto_id=p.id)
    # comprobamos que fue un exito si se consigue la nueva descripcion de la fase
    fase_editada = Fase.objects.get(id=f.id)
    assert fase_editada.nombre == 'Nuevo nombre', "La fase tiene el nombre editado"
    assert fase_editada.descripcion == 'Nueva descripcion', "La fase deberia tener la descripcion actualizada"


@pytest.mark.django_db
def test_ajax_eliminar_fase():
    """Verifica si que la funcion crear proyectos responda correctamente al request
    """
    # Creamos una fecha y asignamos como descripcion de la fase
    fecha = timezone.now()
    p = Proyecto(nombre="Test", descripcion="test", estado='P', fecha_creacion=fecha)
    p.save()
    # Creamos una fase
    f = Fase(nombre="faseTest", descripcion="bla bla", proyecto=p, numero=1, estado='A')
    f.save()
    cant_fases = Fase.objects.filter(proyecto_id=p.id).values().count()
    assert cant_fases == 1, "El proyecto deberia tener 1 fase para testear la funcion elminar"
    # Buscamos el path de lal funcion
    path = reverse('eliminar_fases', args=[p.id])
    request = RequestFactory().post(
        path,
        {
            'id': f.id,
        }
    )
    # Creamos un usuario que pueda acceder al proyecto
    request.user = User.objects.create_user(username='tester', email='tester@gmail.com', password='password')
    grupo = Group.objects.get(name='Usuario')
    request.user.groups.add(grupo)
    # permisos
    grupo_rol = crear_grupo_de_rol(['listar_proyectos', 'eliminar_fases'], p, [], [])
    rol = Rol(nombre='test', descripcion='test', proyecto=p, grupo=grupo_rol,
              permisos_proyecto=['listar_proyectos', 'eliminar_fases'], permisos_fases='')
    rol.save()
    rol.asignar_usuario(request.user)
    # Eliminamos la fase
    ajax_eliminar_fase(request, proyecto_id=p.id)
    # comprobamos que fue un exito si no se encuentra una instancia de la fase
    cant_fases = Fase.objects.filter(proyecto_id=p.id).values().count()
    assert cant_fases == 0, "El proyecto deberia tener 0 fases"
