from django.urls import path
from . import views

urlpatterns = [
    #AJAX
    path('agregar/', views.ajax_crear_fase, name='crear_fases'),
    path('listar/', views.ajax_listar_fases, name="listar_fases"),
    path('eliminar/', views.ajax_eliminar_fase, name='eliminar_fases'),
    path('editar/', views.ajax_editar_fase, name='editar_fases'),
    path('ver/', views.ajax_ver_fase, name='ver_fases'),
    path('subir/', views.ajax_subir_fase, name='subir_fases'),
    path('bajar/', views.ajax_bajar_fase, name='bajar_fases'),
    path('cerrar/', views.cerrar_fase, name='cerrar_fase'),
    path('generarInformeFase/<int:fase_id>', views.informe_fase, name="informe_fase"),

    # AJAX Tabla de trabajo
    path('obtenerFase/', views.ajax_obtener_fases_del_proyecto, name='AJAX Obtener Fases del Proyecto'),
]