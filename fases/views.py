from datetime import datetime

from django.http import JsonResponse, HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required, permission_required
from guardian.decorators import permission_required_or_403
from .forms import *
from proyecto.models import *
from .models import Fase, Item
from .utils import cerrar_fase_de_proyecto
import logging

# Instancia del logger
logger = logging.getLogger(__name__)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.crear_fases', (Proyecto, 'id', 'proyecto_id'))
def ajax_crear_fase(request, proyecto_id):
    """AJAX que permite la creación de fases dentro del proyecto.

    Requiere que el usuario posea el permiso ``proyecto.crear_fases`` en el proyecto.

    :param request: AJAX Request del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: La fase que se creó en el proyecto
    :rtype: JsonResponse

    """
    if request.method == 'POST':
        # Verificar si el proyecto existe
        try:
            proyecto = Proyecto.objects.get(pk=proyecto_id)
        except Proyecto.DoesNotExist:
            return JsonResponse({"error": "No existe el proyecto"}, status=400)

        # Verificar que el proyecto este en configuración
        if not proyecto.en_configuracion():
            return JsonResponse({"error": "Ya no se pueden modificar las fases."}, status=400)

        # Obtener el nuevo numero para la fase a ser creada
        num = proyecto.fases.count() + 1
        # Crear un formulario de CrearFase
        form = Form_crear_fase(request.POST)
        # Verificar si el formulario es valido
        if form.is_valid():
            # Crear fase
            nueva_fase = proyecto.crear_fase(nombre=form.cleaned_data['nombre'],
                                             descripcion=form.cleaned_data['descripcion'])

            # Logging
            logger.info("{} creó la fase [{}] en el proyecto {}".format(request.user.email, nueva_fase, proyecto))

            # Asociar gerente a la fase
            proyecto.rol_gerente.agregar_fase(nueva_fase)

            # Respondemos el request
            return JsonResponse({"fases": Fase.listar_fases_proyecto_json(proyecto_id)}, status=200)

        else:
            # Respondemos si no pudo procesar el formulario
            return JsonResponse({"error": form.errors}, status=400)

    # Error desconocido
    return JsonResponse({"error": "No es POST"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_listar_fases(request, proyecto_id):
    """AJAX que devuelve la lista de fases del proyecto

    Requiere el permiso ``proyecto.ver_fases`` dentro del proyecto.

    :param request: AJAX Request
    :param proyecto_id: Identificador del Proyecto
    :return: Lista de roles del proyecto
    :rtype: JsonResponse

    """
    # Verificar metodo valido
    if request.method == 'GET':
        # Buscamos las fases del proyecto
        proyecto = get_object_or_404(Proyecto, pk=proyecto_id)

        # Enviamos el response
        return JsonResponse({"fases": Fase.listar_fases_proyecto_json(proyecto_id)}, status=200)

    # Error desconocido
    return JsonResponse({"error": "No es AJAX ó GET"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.eliminar_fases', (Proyecto, 'id', 'proyecto_id'))
def ajax_eliminar_fase(request, proyecto_id):
    """AJAX que permite eliminar una fase de un proyecto, si es que esta fase no tiene
    ningún tipo de item asociado.

    Requiere el permiso ``proyecto.eliminar_fases`` dentro del proyecto.

    :param request: AJAX Request del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: Lista de fases del proyecto
    :rtype: JsonResponse

    """

    # Verificar método valido
    if request.method == 'POST':
        # Obtener la fase
        fase = Fase.objects.get(id=request.POST['id'])

        # Buscar proyecto
        proyecto = get_object_or_404(Proyecto, pk=proyecto_id)

        # Verificar que el proyecto este en configuración
        if not proyecto.en_configuracion():
            return JsonResponse({"error": "Ya no se pueden modificar las fases."}, status=400)

        # Verificar que no se este usando
        if fase.usado:
            return JsonResponse({"error": "Hay tipos de item con esta fase"}, status=400)

        # Reenumerar
        for fase_candidato in proyecto.fases:
            if fase_candidato.numero > fase.numero:
                fase_candidato.numero = fase_candidato.numero - 1
                fase_candidato.save()

        # Logging
        logger.info("{} eliminó la fase [{}] en el proyecto {}".format(request.user.email, fase, proyecto))

        # Eliminar la fase
        fase.delete()

        # Enviamos el response
        return JsonResponse({"fases": Fase.listar_fases_proyecto_json(proyecto_id)}, status=200)

    # Error desconocido
    return JsonResponse({"error": "No es POST"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.modificar_fases', (Proyecto, 'id', 'proyecto_id'))
def ajax_editar_fase(request, proyecto_id):
    """AJAX que edita roles dentro del proyecto y retorna la lista de roles caso sea exitoso.

    Requiere el permiso ``proyecto.modificar_fases`` dentro del proyecto.

    :param request: AJAX Request del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: Lista de fases del proyecto
    :rtype: JsonResponse

    """
    # Verificar metodo valido
    if request.method == 'POST':
        # Buscamos el proyecto
        try:
            proyecto = Proyecto.objects.get(pk=proyecto_id)
        except Proyecto.DoesNotExist:
            return JsonResponse({"error": "No existe el proyecto"}, status=400)

        # Verificar que el proyecto sea modificable
        if not (proyecto.en_ejecucion() or proyecto.en_configuracion()):
            return JsonResponse({"error": "No se puede modificar el proyecto"}, status=400)

        # Obtener datos del form
        form = Form_crear_fase(request.POST)

        if form.is_valid():
            # Buscamos la fase
            fase = proyecto.fases.get(id=request.POST['fid'])

            # Extraemos los datos
            fase.nombre = form.cleaned_data['nombre']
            fase.descripcion = form.cleaned_data['descripcion']

            # Guardamos
            fase.save()

            # Logging
            logger.info("{} modificó la fase '{}' en el proyecto {}".format(request.user.email, fase, proyecto))

            # Respondemos el request
            return JsonResponse({"fases": Fase.listar_fases_proyecto_json(proyecto_id)}, status=200)

        else:
            # Respondemos si no pudo procesar el formulario
            return JsonResponse({"error": form.errors}, status=400)

    # Error desconocido
    return JsonResponse({"error": ""}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.modificar_fases', (Proyecto, 'id', 'proyecto_id'))
def ajax_ver_fase(request, proyecto_id):
    """AJAX que permite visualizar la fase que va a ser editado en un proyecto.

    Requiere permiso ``proyecto.modificar_fases`` en el proyecto

    :param request: AJAX Request del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: JsonResponse con la visualización de la fase
    """
    # Verificar método valido
    if request.is_ajax and request.method == 'GET':
        # Buscamos el proyecto
        try:
            proyecto = Proyecto.objects.get(pk=proyecto_id)
        except Proyecto.DoesNotExist:
            return JsonResponse({"error": "No existe el proyecto"}, status=400)

        # Buscamos Fase
        try:
            fase = proyecto.fases.get(id=request.GET['fid'])
        except Fase.DoesNotExist:
            return JsonResponse({"error": "No existe la fase"}, status=400)

        return JsonResponse({"fase": fase.a_dict_json()}, status=200)

    # Error desconocido
    return JsonResponse({"error": "No es AJAX ó GET"}, status=400)


# TODO: Mover subir fase al modelo
@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.modificar_fases', (Proyecto, 'id', 'proyecto_id'))
def ajax_subir_fase(request, proyecto_id):
    """AJAX que permite desplazar una fase hacia arriba

    Requiere permiso ``proyecto.modificar_fases`` en el proyecto

    :param request: AJAX Request del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: Lista de fases del proyecto
    :rtype: JsonResponse

    """

    # Verificar método valido
    if request.method == 'POST':
        # Obtener la fase y proyecto
        proyecto = get_object_or_404(Proyecto, pk=proyecto_id)

        # Verificar que el proyecto este en configuración
        if not proyecto.en_configuracion():
            return JsonResponse({"error": "Ya no se pueden modificar las fases."}, status=400)

        fase_actual = proyecto.fases.get(id=request.POST['fid'])

        if fase_actual.numero > 1:
            numero_arriba = fase_actual.numero - 1
            fase_arriba = proyecto.fases.get(numero=numero_arriba)

            # Actualizar la numeracion
            fase_actual.numero = fase_actual.numero - 1
            fase_arriba.numero = fase_arriba.numero + 1

            # Guardar las fases nuevas
            fase_actual.save()
            fase_arriba.save()

            # Logging
            logger.info("{} subió la fase '{}' en el proyecto {}".format(request.user.email, fase_actual, proyecto))
        else:
            return JsonResponse({"error": "El primero no puede subir"}, status=400)
        # Enviamos el response
        return JsonResponse({"fases": Fase.listar_fases_proyecto_json(proyecto_id)}, status=200)

    # Error desconocido
    return JsonResponse({"error": "No es POST"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@permission_required_or_403('proyecto.modificar_fases', (Proyecto, 'id', 'proyecto_id'))
def ajax_bajar_fase(request, proyecto_id):
    """AJAX que permite desplazar una fase hacia abajo.

    Requiere permiso ``proyecto.modificar_fases`` en el proyecto

    :param request: AJAX Request del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: Lista de fases del proyecto
    :rtype: JsonResponse

    """
    # Verificar método valido
    if request.method == 'POST':
        # Obtener la fase y proyecto
        proyecto = get_object_or_404(Proyecto, pk=proyecto_id)

        # Verificar que el proyecto este en configuración
        if not proyecto.en_configuracion():
            return JsonResponse({"error": "Ya no se pueden modificar las fases."}, status=400)

        try:
            fase_actual = proyecto.fases.get(id=request.POST['fid'])
        except Fase.DoesNotExist:
            return JsonResponse({"error": "No existe la fase"}, status=400)

        numero_fases = proyecto.fases.count()

        # TODO: Mover Lógica al Modelo
        if fase_actual.numero < numero_fases:
            fase_abajo = Fase.objects.get(numero=fase_actual.numero + 1, proyecto=proyecto)

            # Actualizar la numeracion
            fase_actual.numero = fase_actual.numero + 1
            fase_abajo.numero = fase_abajo.numero - 1

            # Guardar las fases nuevas
            fase_actual.save()
            fase_abajo.save()

            # Logging
            logger.info("{} bajó la fase '{}' en el proyecto {}".format(request.user.email, fase_actual, proyecto))

        else:
            return JsonResponse({"error": "No se puede bajar el ultimo"}, status=400)

        # Enviamos el response
        return JsonResponse({"fases": Fase.listar_fases_proyecto_json(proyecto_id)}, status=200)

    # Error desconocido
    return JsonResponse({"error": "No es POST"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_obtener_fases_del_proyecto(request, proyecto_id):
    """Retorna un JsonResponse con un Array de las
    :meth:`representaciones JSON de las fases <fases.models.Fase.a_dict_json>` del proyecto.

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: Lista de fases del proyecto
    :rtype: JsonResponse

    """
    # Verificar que el metodo sea valido
    if request.method != 'GET':
        return JsonResponse({"error": "Método invalido"}, status=400)

    # Buscar el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Verificar si es participante
    if request.user not in proyecto.participantes.all():
        return JsonResponse({"error": "No eres participante del proyecto"}, status=403)

    # Buscar las fases del proyecto
    return JsonResponse(Fase.listar_fases_proyecto_json(proyecto_id), safe=False, status=200)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def cerrar_fase(request, proyecto_id):
    """VIEW que permite cerrar una fase.

    Requiere permiso ``proyecto.modificar_fases`` en el proyecto

    :param request: Request del Usuario
    :param proyecto_id: Identificador del Proyecto
    :return: Lista de fases del proyecto
    :rtype: JsonResponse

    """
    # Verificar método valido
    if request.method == 'POST':
        # Obtener la fase y proyecto
        proyecto = get_object_or_404(Proyecto, pk=proyecto_id)

        # Verificar que el proyecto este en desarrollo
        if not proyecto.en_ejecucion():
            return JsonResponse({"error": "No se pueden cerrar las fases."}, status=400)

        try:
            fase = proyecto.fases.get(id=request.POST['fid'])
        except Fase.DoesNotExist:
            return JsonResponse({"error": "No existe la fase"}, status=400)

        if cerrar_fase_de_proyecto(fase=fase):
            # Logging
            logger.info("{} cerró la fase '{}' en el proyecto {}".format(request.user.email, fase, proyecto))
            return JsonResponse({"fases": Fase.listar_fases_proyecto_json(proyecto_id)}, status=200)
        else:
            return JsonResponse({"error": "No se puede cerrar la fase"}, status=400)

    # Error desconocido
    return JsonResponse({"error": "No es POST"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def informe_fase(request: HttpRequest, proyecto_id: int, fase_id):
    """View que genera un informe de los items de una fase

    :param request:
    :param proyecto_id:
    :param fase_id:
    :return: PDF del informe de fase
    """
    from io import BytesIO
    from django.template.loader import get_template
    from xhtml2pdf import pisa
    import pytz
    # Obtener el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Obtener la línea base
    try:
        fase = Fase.objects.get(id=fase_id)
    except Fase.DoesNotExist:
        return JsonResponse({"error": "No existe la Fase"}, status=400)

    items = Item.objects.filter(fase_id=fase_id)
    # Generamos el pdf
    template = get_template('pdf_templates/InformeFase.html')
    asu = pytz.timezone('America/Asuncion')
    html = template.render(
        {"fase": fase, "items": items, "fecha": datetime.date(datetime.now(asu)),
         "hora": datetime.time(datetime.now(asu))})
    resultado = BytesIO()
    pisa.pisaDocument(BytesIO(html.encode("UTF-8")), resultado)

    response = HttpResponse(resultado.getvalue(), content_type="application/pdf")
    response['Content-Disposition'] = 'attachment; filename="informe_fase.pdf"'

    # Logging
    logger.info("{} generó el informe de la fase '{}' en el proyecto {}".format(request.user.email, fase, proyecto))

    return response
