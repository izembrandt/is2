from django.db.models import QuerySet

from fases.models import Fase, EstadoFase
from item.models import Item
from item.constantes import EstadosItem
from comite_de_cambio.models import SolicitudCambio
from comite_de_cambio.constantes import EstadoSolicitud


def se_puede_cerrar_fase(fase: Fase) -> bool:
    """Verifica si una fase cumple los requisitos para cerrarse.

        Estas condiciones son:

        1. Debe tener al menos 1 ítem.
        2. Todos sus ítem tiene que estar en una línea base cerrada o desactivados.
        3. Todas las líneas bases deben estar cerradas o rotas.
        4. La fase debe estar abierta.
        5. La fase anterior debe estar cerrado.
        6. No debe poseer solicitudes pendientes de aprobación.

        :param fase: Fase que se desea cerrar
        :type fase: Fase
        :return: True si es que se puede cerrar, False caso contrario.
        :rtype: bool
    """
    # Revisa que tenga items
    if fase.item_set.count() == 0:
        return False

    # Revisa que todos los items esten desactivado o en linea base
    if Item.listar_items_de_fase_queryset(fase.id) \
            .exclude(es_ultimo=False) \
            .exclude(estado__in=[EstadosItem.LINEA_BASE, EstadosItem.DESACTIVADO]).count() > 0:
        return False

    # Verificar que todas las LB esten cerradas o rotas
    if fase.lineabase_set.filter(estado='A').count() > 0:
        return False

    # Verificar si la fase esta abierta
    if fase.estado != EstadoFase.ABIERTO:
        return False

    # Revisa que la fase anterior este cerrada, si es que existe
    if fase.numero > 1:
        fase_anterior = Fase.objects.get(proyecto_id=fase.proyecto_id, numero=fase.numero - 1)
        if not fase_anterior.esta_cerrado():
            return False

    # Verificar que no haya solicitudes pendientes
    solicitudes: QuerySet = SolicitudCambio.objects.filter(item_solicitante__fase=fase,
                                                           estado=EstadoSolicitud.PENDIENTE)

    if solicitudes.exists():
        # Existe una solicitud pendiente en la fase
        return False

    return True


def cerrar_fase_de_proyecto(fase: Fase) -> bool:
    """Cierra la fase del proyecto verificando primero si cumple la condiciones para cerrar.

    Estas condiciones son:

    1. Debe tener al menos 1 ítem.
    2. Todos sus ítem tiene que estar en una línea base cerrada o desactivados.
    3. Todas las líneas bases deben estar cerradas o rotas.
    4. La fase debe estar abierta.
    5. La fase anterior debe estar cerrado.
    6. No debe poseer solicitudes pendientes de aprobación.

    :param fase: Fase que se desea cerrar
    :type fase: Fase
    :return: True si es que se pudo cerrar, False caso contrario.
    :rtype: bool
    """
    # Cerramos si puede cerrar
    if se_puede_cerrar_fase(fase=fase):
        fase.cerrar()
        return True

    # No se pudo cerrar
    return False

