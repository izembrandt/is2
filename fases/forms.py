from django import forms

class Form_crear_fase(forms.Form):
    '''
    Formulario utilizado para crear fases dentro de un proyecto

    :var nombre: Nombre de la fase con longitud máxima de 50 caracteres
    :var descripcion: Descripción de la fase sin longitud máxima

    '''
    nombre = forms.CharField(label='Nombre de la Fase', max_length=50)
    descripcion = forms.CharField(label="Descripción de la fase",widget=forms.Textarea, required=False)


