from typing import List

from django.db import models
from django.db.models import QuerySet
from item.models import Item
from proyecto.models import Proyecto
from .permisos import PermisoFase


class EstadoFase:
    ABIERTO = 'A'
    CERRADO = 'C'


class Fase(models.Model):
    """Representa a una fase que almacena nombre, descripción, proyecto, numero y estado.

    :meth:`Representación JSON <fases.models.Fase.a_dict_json>` de la Fase.

    :var nombre: Nombre de la fase. Tiene una longitud máxima de 50 caracteres.
    :var descripcion: Descripción de la fase. No tiene longitud máxima.
    :var proyecto: Proyecto al que pertenece una fase.
    :var numero: Identifica el número de fase.
    :var estado: Representa el estado de la fase, que puede ser abierto o cerrado.
    :var usado: True si es que hay tipos de item con la fase, False caso contrario.

    """
    ESTADOS_FASE = (
        ('A', "Abierto"),
        ('C', "Cerrado"),
    )
    nombre = models.CharField("Nombre de la fase", max_length=50)
    descripcion = models.TextField("Descripción de la fase")
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, null=True)
    numero = models.IntegerField("Numero de la fase", default=-1)
    estado = models.CharField("Estado de la fase", max_length=1, choices=ESTADOS_FASE)

    def cerrar(self) -> bool:
        """Cierra una Fase

        :return: Verdadero si se pudo cerrar la fase, Falso caso contrario
        """
        self.estado = EstadoFase.CERRADO
        self.save()
        return True

    @property
    def usado(self) -> bool:
        """Retorna True si la fase se usa en algún tipo de item, caso contrario, Retorna False

        :return: Si es que la fase se usa en algún item.
        :rtype: bool
        """
        return self.tipos_de_item.exists()

    @property
    def tiene_item_para_informe(self):
        """Retorna True si la fase tiene items en desarrollo o pendientes de aprobacion, caso contrario, Retorna False

        :return: Si es que la fase tiene items en desarrollo o pendiente de aprobacion.
        :rtype: bool
        """
        return Item.listar_items_de_fase_queryset(self.id) \
                   .exclude(es_ultimo=False) \
                   .exclude(estado__in=['LB', 'IA', 'DA']).count() > 0

    def esta_cerrado(self) -> bool:
        """Retorna True si la fase esta cerrada, caso contrario, retorna False

        :return: Si es que la fase esta cerrada
        :rtype: bool

        """
        return self.estado == EstadoFase.CERRADO

    def a_dict_json(self) -> dict:
        """Retorna un diccionario en la representación json de la fase.

        La representación JSON es:

        .. code-block:: python

           [{
           "id": ID_DE_FASE,
           "nombre": "NOMBRE_DE_FASE",
           "descripcion": "DESCRIPCION_DE_FASE",
           "proyecto": PROYECTO_ID,
           "numero": NUMERO_DE_FASE,
           "cerrado": TRUE_SI_ESTA_CERRADO_FALSE_SI_ABIERTO,
           "usado": SI_TIENE_TIPOS_DE_ITEM,
           "cerrable": SI_SE_PUEDE_CERRAR,
           }]

        :return: Diccionario de la fase

        """
        from .utils import se_puede_cerrar_fase
        return {
            "id": self.id,
            "nombre": self.nombre,
            "descripcion": self.descripcion,
            "proyecto": self.proyecto_id,
            "numero": self.numero,
            "cerrado": self.esta_cerrado(),
            "usado": self.usado,
            "cerrable": se_puede_cerrar_fase(self),
        }

    @staticmethod
    def listar_fases_proyecto_queryset(proyecto_id: int) -> QuerySet:
        """Retorna la lista de fases de un proyecto como queryset.

        :param proyecto_id: Identificador del Proyecto
        :type proyecto_id: int
        :return: Lista de fases del proyecto
        :rtype: QuerySet<Fase>

        """
        return Fase.objects.filter(proyecto_id=proyecto_id).order_by('numero')

    @staticmethod
    def listar_fases_proyecto_json(proyecto_id) -> List[dict]:
        """Retorna una lista de diccionarios de las fases ordenado por la numeración de la fase
        dentro del proyecto

        Los diccionarios tienen la estructuras de la :meth:`Representación JSON <fases.models.Fase.a_dict_json>` de la
        fase.

        :param proyecto_id: Identificador del Proyecto
        :type proyecto_id: int
        :return: Lista de diccionarios de fases
        :rtype: list

        """
        return [fase.a_dict_json() for fase in Fase.listar_fases_proyecto_queryset(proyecto_id)]

    def __str__(self):
        return "{numero}. {nombre}".format(numero=self.numero, nombre=self.nombre)

    class Meta:
        permissions = [
            # Items
            (PermisoFase.CREAR_ITEM, 'Puede crear items en la fase'),
            (PermisoFase.MODIFICAR_ITEM, 'Puede modificar items en la fase'),
            (PermisoFase.APROBAR_ITEM, 'Puede aprobar items en la fase'),
            (PermisoFase.DESAPROBAR_ITEM, 'Puede desaprobar items en la fase'),
            (PermisoFase.ELIMINAR_ITEM, 'Puede eliminar items de la fase'),
            (PermisoFase.RELACIONAR_ITEM, 'Puede relacionar items en la fase'),
            # Linea Base
            (PermisoFase.CREAR_LINEA_BASE, 'Puede crear lineas base en la fase'),
            (PermisoFase.MODIFICAR_LINEA_BASE, 'Puede modificar los atributos de la linea base'),
            (PermisoFase.ELIMINAR_LINEA_BASE, 'Puede eliminar lineas bases abiertas en la fase'),
            (PermisoFase.CERRAR_LINEA_BASE, 'Puede cerrar lineas bases en la fase'),
            # Solicitud de cambio
            (PermisoFase.SOLICITAR_CAMBIO, 'Puede pedir solicitud de cambio en la fase'),
        ]
