from storages.backends.gcloud import GoogleCloudStorage
import datetime


class AlmacenamientoCloud(GoogleCloudStorage):

    def obtener_url_subida(self, nombre: str, content_type: str):
        # Creamos una url temporal de subida
        blob = self.bucket.blob(nombre)
        url = blob.generate_signed_url(version='v4', expiration=datetime.timedelta(days=1), method='PUT', content_type=content_type)
        return url
