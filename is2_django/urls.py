
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

urlpatterns = [
    path('', TemplateView.as_view(template_name="index.html"), name='Home'),
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('menu/',include('menu_principal.urls')),
    path('proyecto/', include('proyecto.urls')),
    path('sin_acceso/', TemplateView.as_view(template_name='acceso_pendiente.html'), name='Acceso Pendiente'),
    # Login
    path('usuarios/', include('login_social.urls')),
    # Front End
    path('app/', include('frontend.urls')),
]
