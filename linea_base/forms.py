from django import forms
from django.db.models import QuerySet

from fases.models import Fase
from item.constantes import EstadosItem
from .models import LineaBase
from item.models import Item


class FormCrearLineaBase(forms.Form):
    nombre = forms.CharField(label="Nombre", required=True, max_length=100)
    descripcion = forms.CharField(label="Descripción", widget=forms.Textarea, required=False)
    fase = forms.ModelChoiceField(label="Fase", queryset=None, required=True)
    color = forms.CharField(label="Color", required=True, max_length=7)
    items = forms.ModelMultipleChoiceField(label="Items que se van a agregar", queryset=None, required=True)

    def __init__(self, *args, **kwargs):
        self.proyecto_id = kwargs.pop('proyecto_id', None)
        super().__init__(*args, **kwargs)
        if self.proyecto_id is not None:
            self.fields['fase'].queryset = Fase.listar_fases_proyecto_queryset(self.proyecto_id)
            # Items válidos
            item_queryset: QuerySet = Item.listar_items_de_un_proyecto_queryset(self.proyecto_id)
            self.fields['items'].queryset = item_queryset\
                .filter(es_ultimo=True, estado=EstadosItem.APROBADO)


class FormModificarLineaBase(forms.Form):
    nombre = forms.CharField(label="Nombre", required=True, max_length=100)
    descripcion = forms.CharField(label="Descripción", widget=forms.Textarea, required=False)
    linea_base = forms.ModelChoiceField(label="Linea Base", queryset=None, required=True)
    color = forms.CharField(label="Color", required=True, max_length=7)

    def __init__(self, *args, **kwargs):
        self.proyecto_id = kwargs.pop('proyecto_id', None)
        super().__init__(*args, **kwargs)
        if self.proyecto_id is not None:
            self.fields['linea_base'].queryset = LineaBase.lineas_base_de_proyecto_queryset(
                proyecto_id=self.proyecto_id)


class FormAgregarItemLineaBase(forms.Form):
    items = forms.ModelMultipleChoiceField(label="Items que se van a agregar", queryset=None, required=True)

    def __init__(self, *args, **kwargs):
        self.linea_base: LineaBase = kwargs.pop('linea_base', None)
        super().__init__(*args, **kwargs)
        if self.linea_base is not None:
            self.fields['items'].queryset = Item.listar_items_de_fase_queryset(self.linea_base.fase.id)\
                .filter(estado='IA', es_ultimo=True)


class FormEliminarItemLineaBase(forms.Form):
    items = forms.ModelMultipleChoiceField(label="Items que se van a agregar", queryset=None, required=True)

    def __init__(self, *args, **kwargs):
        self.linea_base: LineaBase = kwargs.pop('linea_base', None)
        super().__init__(*args, **kwargs)
        if self.linea_base is not None:
            self.fields['items'].queryset = self.linea_base.items.all()
