import json
import pytest
from django.test import RequestFactory
from django.urls import reverse
from django.utils import timezone

from .models import *
from django.urls import reverse, resolve
from django.contrib.auth.models import User, Group
from .views import *
from roles.views import *
from item.models import Item, TipoItem, Atributo, Archivo


# Create your tests here.

@pytest.mark.django_db
def test_crear_linea_base(proyecto_factory, item_factory, tipo_item_factory, fase_factory, usuario_qa):
    """Verifica se creé correctamente las lineas Base

    CU-012-01. Crear Linea Base
    """
    # Creamos una fecha y asignamos como descripcion de la fase
    identificador = "{}".format(uuid.uuid4())
    # creamos proyecto
    p = proyecto_factory()
    # Creamos una fase
    f = fase_factory(proyecto=p)
    # Ejecutamos
    p.ejecutar()
    cant_fases = Fase.objects.filter(proyecto_id=p.id).values().count()
    assert cant_fases == 1, "El proyecto deberia tener 1 fase para testear la funcion crear linea base"
    tipo_item: TipoItem = tipo_item_factory(proyecto=p, fase=f, prefijo="TEST")
    item: Item = item_factory(tipo_item=tipo_item, atributos=[])
    item.solicitar_aprobacion()
    item.aprobar()
    # Buscamos el path de lal funcion
    path = reverse('Crear Linea Base', args=[p.id])
    request = RequestFactory().post(
        path,
        {
            'fase': f.pk,
            'fase_id': f.pk,
            'nombre': identificador,
            'color': "#AFF05B",
            'descripcion': identificador,
            'items': [item.uuid]
        },
        HTTP_X_REQUESTED_WITH='XMLHttpRequest'
    )
    # Creamos un usuario que pueda acceder al proyecto
    request.user = usuario_qa

    # Crear linea base
    response = crear_linea_base(request, proyecto_id=p.pk)

    print(json.loads(response.content))
    assert response.status_code == 200, "No se pudo crear la linea base"
    item.refresh_from_db()

    assert item.linea_base.esta_cerrado(), "La línea base no esta cerrada"


@pytest.mark.django_db
def test_listar_lineas_base():
    """Verifica se creé correctamente las lineas Base

    """
    # Creamos una fecha y asignamos como descripcion de la fase
    identificador = "{}".format(uuid.uuid4())
    # creamos proyecto
    p = crear_proyecto(identificador)
    # Creamos una fase
    f = crear_fase(identificador, p)
    # Ejecutamos
    p.ejecutar()
    cant_fases = Fase.objects.filter(proyecto_id=p.id).values().count()
    assert cant_fases == 1, "El proyecto deberia tener 1 fase para testear la funcion crear linea base"
    # Buscamos el path de lal funcion
    path = reverse('Obtener las Lineas Base de un Proyecto', args=[p.id])
    request = RequestFactory().get(
        path,
        HTTP_X_REQUESTED_WITH='XMLHttpRequest'
    )
    # Creamos un usuario que pueda acceder al proyecto
    request.user = User.objects.create_user(username=identificador, email=identificador, password=identificador)
    grupo = Group.objects.get(name='Usuario')
    request.user.groups.add(grupo)
    p.participantes.add(request.user)
    # Listar las lineas base
    response = obtener_lineas_base_de_proyecto(request, proyecto_id=p.pk)
    print(response.content)
    assert response.status_code == 200, "No se pudo obtener las lineas base"


def crear_proyecto(id):
    fecha = timezone.now()
    proy = Proyecto(nombre=id,
                    descripcion=id,
                    estado='P',
                    fecha_creacion=fecha)
    proy.save()
    return proy


def crear_fase(id, proy):
    fas = Fase(nombre=id,
               descripcion=id,
               proyecto=proy,
               numero=1,
               estado='A')
    fas.save()
    return fas


def crear_tipo_item(id, fas, proy):
    tip = TipoItem(nombre=id,
                   descripcion=id,
                   prefijo='TEST',
                   atributos=[],
                   proyecto_id=proy.pk,
                   fase=fas
                   )
    tip.save()
    return tip


def crear_item(id, fas, tip, cod):
    ite = Item(fase_id=fas.pk,
               nombre=id,
               descripcion=id,
               costo=1,
               tipo_de_item=tip,
               numero=10,
               estado=cod
               )
    ite.save()
    return ite


def crear_lb(id, fas, cod, usr):
    lineab = LineaBase(fase_id=fas.pk,
                       fase=fas,
                       nombre=id,
                       descripcion=id,
                       estado=cod,
                       numero=1,
                       creador=usr
                       )
    lineab.save()
    return lineab
