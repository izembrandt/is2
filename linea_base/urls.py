from django.urls import path
from .views import *

urlpatterns = [
    path('obtenerLineas/', obtener_lineas_base_de_proyecto, name="Obtener las Lineas Base de un Proyecto"),
    path('crearLB/', crear_linea_base, name='Crear Linea Base'),
    path('modificarLB/', modificar_linea_base, name='Modificar Linea Base'),
    path('cerrarLB/', cerrar_linea_base, name='Cerrar Linea Base'),
    path('agregarItem/', agregar_item_a_linea_base, name="Agregar Item a Linea Base"),
    path('eliminarItem/', sacar_item_de_linea_base, name="Eliminar Item de la Linea Base"),
    path('generarInforme/<uuid:linea_uuid>', informe_linea_base, name="Informe linea base"),
]
