from datetime import datetime

from django.contrib.auth.decorators import login_required, permission_required
from django.http import JsonResponse, HttpRequest, FileResponse, HttpResponse
from django.views.decorators.http import require_http_methods
from proyecto.models import Proyecto
from .forms import *
from fases.permisos import PermisoFase
import logging

logger = logging.getLogger(__name__)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["GET"])
def obtener_lineas_base_de_proyecto(request: HttpRequest, proyecto_id: int) -> JsonResponse:
    """View que obtiene todas las líneas base del proyecto

    :param request: Request del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: Una lista json de todos las líneas base del proyecto
    """
    # Obtener el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    if request.user in proyecto.participantes.all():
        return _json_response_de_lineas_base_de_proyecto(proyecto_id)

    return JsonResponse({"error": "No eres participante del proyecto"}, status=400)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["POST"])
def crear_linea_base(request: HttpRequest, proyecto_id: int) -> JsonResponse:
    """View para crear una línea base dentro de una fase del proyecto

    :param request: Request del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: Una lista json de todos las líneas base del proyecto
    """
    # Datos adicionales
    fase_id = request.POST['fase_id']

    # Obtener el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Obtener la fase
    try:
        fase = Fase.objects.get(pk=fase_id, proyecto=proyecto)
    except Fase.DoesNotExist:
        return JsonResponse({"error": "No existe la fase"}, status=400)

    # Crear la línea base
    if request.user.has_perm(PermisoFase.CREAR_LINEA_BASE, fase):
        # Ahora podemos verificar datos
        form = FormCrearLineaBase(request.POST, proyecto_id=proyecto_id)

        if form.is_valid():
            nombre: str = form.cleaned_data['nombre']
            descripcion: str = form.cleaned_data['descripcion']
            fase: Fase = form.cleaned_data['fase']
            color: str = form.cleaned_data['color']
            items: QuerySet = form.cleaned_data['items']

            linea_base = LineaBase.crear(nombre=nombre, descripcion=descripcion, fase=fase, creador=request.user,
                                         color=color)

            for item_lb in items:
                linea_base.agregarItem(item=item_lb)

            linea_base.cerrar()

            # Logging
            logger.info("{} creó la linea base '{}' en el proyecto {}".format(request.user.email, linea_base, proyecto))

            return _json_response_de_lineas_base_de_proyecto(proyecto_id)

        return JsonResponse({"error": "Los datos proveidos son invalidos"}, status=400)

    return JsonResponse({"error": "No tiene permisos para crear línea base en esta fase"}, status=403)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["POST"])
def modificar_linea_base(request: HttpRequest, proyecto_id: int) -> JsonResponse:
    """View para modificar una línea base dentro de una fase del proyecto

    :param request: Request del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: Una lista json de todos las líneas base del proyecto
    """
    # Obtener el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Validar el formulario
    form = FormModificarLineaBase(request.POST, proyecto_id=proyecto_id)

    if form.is_valid():
        nombre: str = form.cleaned_data['nombre']
        descripcion: str = form.cleaned_data['descripcion']
        linea_base: LineaBase = form.cleaned_data['linea_base']
        color: str = form.cleaned_data['color']

        # Verificar permisos
        if request.user.has_perm(PermisoFase.MODIFICAR_LINEA_BASE, linea_base.fase):
            # Verificar si se puede modificar
            if linea_base.modificar(nombre=nombre, descripcion=descripcion, color=color):
                # Logging
                logger.info(
                    "{} modificó la linea base '{}' en el proyecto {}".format(request.user.email, linea_base, proyecto))
                return _json_response_de_lineas_base_de_proyecto(proyecto_id)
            else:
                return JsonResponse({"error": "No se puede modificar"}, status=400)

        return JsonResponse({"error": "No tienes permisos para modificar lineas bases en esta fase"}, status=403)

    return JsonResponse({"error": "Los datos proveidos son invalidos"}, status=400)


# TODO: Quitar porque ya no se usa
@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["POST"])
def agregar_item_a_linea_base(request: HttpRequest, proyecto_id: int) -> JsonResponse:
    """View para agrager un item a una línea base dentro de una fase del proyecto

    :param request: Request del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: Una lista json de todos las líneas base del proyecto
    """
    # Datos adicionales
    linea_base_uuid = request.POST['linea_base']

    # Obtener el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Obtener la línea base
    try:
        linea_base = LineaBase.objects.get(uuid=linea_base_uuid, fase__proyecto=proyecto)
    except LineaBase.DoesNotExist:
        return JsonResponse({"error": "No existe la linea base"}, status=400)

    # Si tiene permiso procedemos
    if request.user.has_perm(PermisoFase.MODIFICAR_LINEA_BASE, linea_base.fase):
        # Validamos los items
        form = FormAgregarItemLineaBase(request.POST, linea_base=linea_base)
        print(linea_base, request.POST)
        if form.is_valid():
            items = form.cleaned_data['items']

            for item in items:
                linea_base.agregarItem(item)

            # Retornamos las lineas
            return _json_response_de_lineas_base_de_proyecto(proyecto_id)

        return JsonResponse({"error": "Los datos proveidos son invalidos"}, status=400)

    return JsonResponse({"error": "No tienes permisos para modificar lineas bases en esta fase"}, status=403)


# TODO: YA no se usa
@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["POST"])
def sacar_item_de_linea_base(request: HttpRequest, proyecto_id: int) -> JsonResponse:
    """View para sacar un item de una línea base dentro de una fase del proyecto

    :param request: Request del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: Una lista json de todos las líneas base del proyecto
    """
    # Datos adicionales
    linea_base_uuid = request.POST['linea_base']

    # Obtener el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Obtener la línea base
    try:
        linea_base = LineaBase.objects.get(uuid=linea_base_uuid, fase__proyecto=proyecto)
    except LineaBase.DoesNotExist:
        return JsonResponse({"error": "No existe la linea base"}, status=400)

    # Si tiene permiso procedemos
    if request.user.has_perm(PermisoFase.MODIFICAR_LINEA_BASE, linea_base.fase):
        # Validamos los items
        form = FormEliminarItemLineaBase(request.POST, linea_base=linea_base)

        if form.is_valid():
            items = form.cleaned_data['items']

            for item in items:
                item.sacar_de_linea_base()

            # Retornamos las lineas
            return _json_response_de_lineas_base_de_proyecto(proyecto_id)

        return JsonResponse({"error": "Los datos proveidos son invalidos"}, status=400)

    return JsonResponse({"error": "No tienes permisos para modificar lineas bases en esta fase"}, status=403)


# TODO: Ya no se usa
@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["POST"])
def cerrar_linea_base(request: HttpRequest, proyecto_id: int) -> JsonResponse:
    """View para cerrar una línea base dentro de una fase del proyecto

    :param request: Request del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: Una lista json de todos las líneas base del proyecto
    """
    # Datos adicionales
    linea_base_uuid = request.POST['linea_base']

    # Obtener el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Obtener la línea base
    try:
        linea_base = LineaBase.objects.get(uuid=linea_base_uuid, fase__proyecto=proyecto)
    except LineaBase.DoesNotExist:
        return JsonResponse({"error": "No existe la linea base"}, status=400)

    # Si tiene permiso procedemos
    if request.user.has_perm(PermisoFase.CERRAR_LINEA_BASE, linea_base.fase):
        # Probamos cerrar la linea base
        if linea_base.cerrar():
            return _json_response_de_lineas_base_de_proyecto(proyecto_id)

        return JsonResponse({"error": "No se puede cerrar la línea base"}, status=400)

    return JsonResponse({"error": "No tienes permisos para cerrar lineas bases en esta fase"}, status=403)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def informe_linea_base(request: HttpRequest, proyecto_id: int, linea_uuid):
    """View que genera un informe de linea base

    :param request:
    :param proyecto_id:
    :param linea_uuid:
    :return: PDF del informe
    """
    from io import BytesIO
    from django.template.loader import get_template
    from xhtml2pdf import pisa
    import pytz
    # Obtener el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=400)

    # Obtener la línea base
    try:
        linea_base = LineaBase.objects.get(uuid=linea_uuid, fase__proyecto=proyecto)
    except LineaBase.DoesNotExist:
        return JsonResponse({"error": "No existe la linea base"}, status=400)

    # Generamos el pdf
    template = get_template('pdf_templates/InformeLineaBase.html')
    asu = pytz.timezone('America/Asuncion')
    html = template.render(
        {"linea_base": linea_base, "fecha": datetime.date(datetime.now(asu)), "hora": datetime.time(datetime.now(asu))})
    resultado = BytesIO()
    pisa.pisaDocument(BytesIO(html.encode("UTF-8")), resultado)

    response = HttpResponse(resultado.getvalue(), content_type="application/pdf")
    response['Content-Disposition'] = 'attachment; filename="informe.pdf"'

    # Logging
    logger.info(
        "{} generó un informe de la linea base '{}' en el proyecto {}".format(request.user.email, linea_base, proyecto))

    return response


def _json_response_de_lineas_base_de_proyecto(proyecto_id: int):
    return JsonResponse({"lineas": LineaBase.lineas_base_de_proyecto_json(proyecto_id=proyecto_id)}, status=200)
