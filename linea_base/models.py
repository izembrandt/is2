from __future__ import annotations
import uuid as uuid
from item.models import Item
from fases.models import *
from django.conf import settings


# Create your models here.
class LineaBase(models.Model):
    """Modelo para representar una Linea Base de un proyecto.

    :var uuid: Identificador UUID de la Linea Base
    :var nombre: Nombre de la Línea Base
    :var descripcion: Descripción de la Línea Base
    :var estado: Estado de la Línea Base
    :var fase: Fase a la que esta asociado la línea base
    :var items: Items dentro de la línea base
    :var color: Color en hexadecimal de la línea base y empieza con un '#'

    """
    ESTADOS_LB = [
        ('A', 'Abierto'),
        ('R', 'Roto'),
        ('C', 'Cerrado'),
    ]
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nombre = models.CharField("Nombre de Línea Base", max_length=100)
    descripcion = models.TextField("Descripción de la Línea Base")
    estado = models.CharField("Estado de la Línea Base", max_length=1, choices=ESTADOS_LB, default='A')
    fase = models.ForeignKey('fases.Fase', on_delete=models.PROTECT)
    numero = models.IntegerField("Número de Línea base en el proyecto", default=-1)
    creador = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    color = models.CharField("Color de la línea base", max_length=7, default="#AFF05B")

    @staticmethod
    def crear(nombre: str, descripcion: str, fase: Fase, creador: settings.AUTH_USER_MODEL,
              color: str = "#AFF05B") -> LineaBase:
        """Crea una línea base especificada por un nombre, descripción y fase

        :param color: Color de la línea base
        :param nombre: Nombre de la Línea Base
        :param descripcion: Descripción de la Línea Base
        :param fase: Fase asociada a la Línea Base
        :param creador: Usuario que lo creó
        :return: La linea base recien creada
        :rtype: LineaBase
        """
        # Buscamos el número conveniente
        numero = LineaBase.objects.filter(fase__proyecto=fase.proyecto).count() + 1

        # Creamos y retornamos
        nueva_linea = LineaBase(nombre=nombre, descripcion=descripcion, fase=fase, numero=numero, creador=creador,
                                color=color)
        nueva_linea.save()
        return nueva_linea

    def agregarItem(self, item: Item) -> bool:
        """Agrega un Item a la Línea Base

        :param item: Item que se desea agregar a la Linea Base
        :return: Verdadero si se pudo agregar el item, Falso caso contrario
        """
        assert isinstance(item, Item), "El item no es del tipo correcto"
        if self.estado == 'C':
            return False

        if item.cambiar_estado_a_en_linea_base():
            # Agregar el item a la linea base
            self.items.add(item)
            return True

        return False

    def eliminarItem(self, item: Item) -> bool:
        """Elimina un Item de la Línea Base

        :param item: Item que se desea eliminar de la Linea Base
        :return: Verdadero si se pudo eliminar el item, Falso caso contrario
        """
        assert isinstance(item, Item), "El item no es del tipo correcto"
        return item.sacar_de_linea_base()  # El item verifica que la linea base no este cerrada

    def _puede_cerrar(self) -> bool:
        """Indica si la Línea Base se puede Cerrar

        :return: Verdadero si se puede, Falso caso contrario
        """
        if self.items.all().count() == 0:
            return False  # No se puede cerrar una linea base vacia
        if self.estado == 'C':
            return False  # Ya esta cerrado
        if self.fase.numero == 1:
            return True  # La primera fase puede cerrar sin problemas
        # Agarrar todos los items de lineas bases cerradas de la fase anterior/actual y ver si alguna esta relacionada
        # a algun item de la linea base
        items_fase_anterior = Item.objects.filter(fase__proyecto_id=self.fase.proyecto_id,  # Mismo proyecto
                                                  # Agarramos los items de la fase anterior
                                                  fase__numero=self.fase.numero - 1, linea_base__estado='C',
                                                  # Que esten en linea base cerrada
                                                  relaciones__linea_base=self)  # Este al menos una relacionada a
        # un item de esta linea base
        items_misma_fase = Item.objects.filter(fase=self.fase,  # Misma fase
                                               linea_base__estado='C',  # Con linea base cerrada
                                               relaciones__linea_base=self)  # Y que al menos uno este relacionado a
        # un item de esta linea base

        return items_fase_anterior.count() + items_misma_fase.count() > 0  # Si hay al menos un item que cumpla

    def cerrar(self) -> bool:
        """Cierra una línea Base

        :return: Verdadero si se pudo cerrar la línea base, Falso caso contrario
        """
        if self._puede_cerrar():
            self.estado = 'C'
            self.save()
            return True

        return False

    def aJSON(self) -> dict:
        """Convierte la linea base en un diccionario que se puede utilizar como JSON

        .. code-block:: python

           [{
           "uuid": "UUID_DE_LA_LINEA_BASE",
           "nombre": "NOMBRE_DE_LA_LINEA_BASE",
           "descripcion": "DESCRIPCION_DE_LA_LINEA_BASE",
           "estado": 'ESTADO_DE_LA_LINEA_BASE',
           "fase": FASE_DE_LA_LINEA_BASE,
           "numero": NUMERO_DE_LINEA_BASE,
           "creador": {INFO_DEL_CREADOR},
           "cerrable": SI_ES_QUE_SE_PUEDE_CERRAR,
           "color": "#COLOR_EN_HEXADECIMAL",
           }]

        :return: Un diccionario en el formato JSON
        """
        return {
            "uuid": self.uuid,
            "nombre": self.nombre,
            "descripcion": self.descripcion,
            "estado": self.estado,
            "fase": self.fase.pk,
            "numero": self.numero,
            "creador": self.info_creador,
            "cerrable": self._puede_cerrar(),
            "color": self.color,
        }

    @property
    def info_creador(self) -> dict:
        """Retorna un diccionario con la información del creador de la línea base

        :return: La información del creador
        :rtype: dict
        """
        creador = self.creador
        return {
            "nombre": creador.first_name,
            "apellido": creador.last_name,
            "correo": creador.email
        }

    @staticmethod
    def lineas_base_de_proyecto_queryset(proyecto_id: int) -> QuerySet[LineaBase]:
        return LineaBase.objects.filter(fase__proyecto_id=proyecto_id)

    @staticmethod
    def lineas_base_de_proyecto_json(proyecto_id: int) -> list:
        """Retorna la lineas bases de un proyecto en un diccionario

        :param proyecto_id: Identificador del proyecto
        :return: Lista de lineas base del proyecto
        """
        return [linea_base.aJSON() for linea_base in
                LineaBase.lineas_base_de_proyecto_queryset(proyecto_id=proyecto_id)]

    def modificar(self, nombre: str, descripcion: str, color: str = "#AFF05B") -> bool:
        """Modifica el nombre y la descripción de la Línea Base

        :param color: Color de la línea base
        :param nombre: Nombre nuevo
        :param descripcion: Descripción Nueva
        :return: Si se pudo realizar la modificación
        """
        # Solo se puede modificar si esta abierto
        if self.estado != 'A':
            return False

        self.nombre = nombre
        self.descripcion = descripcion
        self.color = color

        self.save()

        return True

    def romper(self):
        """Rompe la linea base
        """
        self.estado = "R"
        self.save()
        for item in self.items.all():
            item.sacar_de_linea_base()
            item.poner_en_revision()

    def esta_cerrado(self) -> bool:
        """Retorna si es que la base esta cerrada

        :return: Verdadero si esta cerrado, Falso si no lo esta
        """
        return self.estado == 'C'

    def esta_roto(self) -> bool:
        return self.estado == 'R'

    def esta_abierto(self) -> bool:
        return self.estado == 'A'
