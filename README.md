# Proyecto de IS2

## Entorno

Sistema Operativo: Linux

IDE: PyCharm Professional Edition

SCC: git (GitLab)

PUN: PyTest

PDO: Sphinx

CI/CD: Gitlab CI/CD

AMB: 
- django (framework)
- apache2 (web server)
- python3.7 (lenguaje)
- PostgreSQL (BD)


## Integrantes

- Ruben Izembrandt @izembrandt
- Mateo Fidabel @mfidabel
- Celeste Mallorquín @celes3223