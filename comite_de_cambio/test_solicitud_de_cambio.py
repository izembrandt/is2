import json
from math import floor
from typing import List

import pytest
from django.contrib.auth.models import User
from django.urls import reverse

from comite_de_cambio.constantes import EstadoSolicitud
from comite_de_cambio.models import Comite, SolicitudCambio
from fases.models import Fase
from item.constantes import EstadosItem
from item.models import Item, TipoItem
from linea_base.models import LineaBase
from proyecto.models import Proyecto
from comite_de_cambio.views import crear_solicitud_de_cambio, votar_solicitud_de_cambio


@pytest.fixture
def proyecto_caso_solicitud(db, proyecto_factory, user_factory, fase_factory, tipo_item_factory, item_factory,
                            linea_base_factory) -> Proyecto:
    """Fixture de un proyecto de una sola fase

    :return: Proyecto
    """
    proyecto: Proyecto = proyecto_factory()
    # Usuarios de test
    usuarios = [user_factory() for _ in range(5)]
    for usuario in usuarios:
        proyecto.agregar_participante(usuario)
    # Cómite de test
    Comite.crear(proyecto=proyecto, integrantes=usuarios)

    # Creamos una fase
    fase: Fase = fase_factory(proyecto=proyecto)

    # Creamos un tipo de ítem
    tipo_item: TipoItem = tipo_item_factory(proyecto=proyecto, fase=fase, prefijo="TEST")

    # Creamos otra fase 2
    fase2: Fase = fase_factory(proyecto=proyecto)

    # Creamos otro tipo de item
    tipo_item2: TipoItem = tipo_item_factory(proyecto=proyecto, fase=fase2, prefijo="TES2")

    # Empezamos el proyecto
    proyecto.ejecutar()

    # Creamos un ítem en la primera fase
    item = item_factory(tipo_item=tipo_item, atributos=[])

    # Aprobamos el ítem
    item.solicitar_aprobacion()
    item.aprobar()

    # Creamos una línea base
    lb: LineaBase = linea_base_factory(fase=fase, creador=usuarios[0])

    # Agregamos el ítem a la línea base
    lb.agregarItem(item)

    # Cerramos la lb
    lb.cerrar()

    # Creamos varios ítems dummies en la siguiente fase
    items_dummies: List[Item] = [item_factory(tipo_item=tipo_item2, atributos=[]) for _ in range(5)]

    for item_dummy in items_dummies:
        item.relacionar_a_item(itemDestino=item_dummy)

    return proyecto


@pytest.fixture
def solicitud_item(db, usuario_desarrollador: User, proyecto_caso_solicitud: Proyecto) -> SolicitudCambio:
    # Creamos una solicitud sobre el ítem
    item = Item.objects.filter(fase__proyecto=proyecto_caso_solicitud, linea_base__isnull=False).first()
    solicitud = SolicitudCambio.crear(usuario_solicitante=usuario_desarrollador, item_solicitante=item, motivo="TEST")
    return solicitud


@pytest.fixture
def comite_integrantes_caso_solicitud(db, proyecto_caso_solicitud: Proyecto) -> List[User]:
    return [*proyecto_caso_solicitud.comite.integrantes.all()]


def test_crear_solicitud_de_cambio(db, proyecto_caso_solicitud: Proyecto, usuario_desarrollador: User):
    """Testea la creación de una solicitud de cambio"""
    # Creamos una solicitud sobre el ítem
    item = Item.objects.filter(fase__proyecto=proyecto_caso_solicitud, linea_base__isnull=False).first()
    _ = SolicitudCambio.crear(usuario_solicitante=usuario_desarrollador, item_solicitante=item, motivo="TEST")


def test_form_crear_solicitud_de_cambio(db, rf, proyecto_caso_solicitud: Proyecto, usuario_desarrollador: User):
    """Testea la creación de una solicitud de cambio con validación de forms"""
    # Creamos una solicitud sobre el ítem
    item = Item.objects.filter(fase__proyecto=proyecto_caso_solicitud, linea_base__isnull=False).first()

    request = rf.post(
        path=reverse("Crear solicitud de cambio", args=[proyecto_caso_solicitud.id]),
        data={
            "item_solicitante": item.uuid,
        }
    )

    request.user = usuario_desarrollador

    response = crear_solicitud_de_cambio(request=request, proyecto_id=proyecto_caso_solicitud.id)

    print(json.loads(response.content))

    assert response.status_code == 200, "No fue exitosa la creación del cómite"


def test_ejercer_voto_solicitud_a_favor(db, solicitud_item: SolicitudCambio,
                                        comite_integrantes_caso_solicitud: List[User]):
    """Testea la votación a favor de una solicitud"""
    # Guardamos una lista de los ítems que estaban antes
    item_linea_base: List[Item] = [*solicitud_item.item_solicitante.linea_base.items.all()]

    # Votar para aprobar
    for miembro in comite_integrantes_caso_solicitud:
        voto = solicitud_item.ejercer_voto(votante=miembro, a_favor=True)
        print(voto)

    solicitud_item.refresh_from_db()

    assert solicitud_item.estado == EstadoSolicitud.APROBADO, "No se aprobó la solicitud despues " \
                                                              "de que voten todos a favor"

    print(solicitud_item.item_solicitante.estado)

    # Verificar que todos los items esten en revisión
    assert solicitud_item.item_solicitante.es_modificable(), "El item con solicitud no se puede modificar"
    assert solicitud_item.item_solicitante.linea_base is None, "El ítem sigue en la línea base"

    for item in solicitud_item.item_solicitante.obtener_sucesores_queryset()\
            .filter(estado__in=[EstadosItem.APROBADO, EstadosItem.LINEA_BASE]):
        assert item.estado == EstadosItem.REVISION, "Su sucesores no estan en revisión"

    for item in solicitud_item.item_solicitante.obtener_hijos_queryset()\
            .filter(estado__in=[EstadosItem.APROBADO, EstadosItem.LINEA_BASE]):
        assert item.estado == EstadosItem.REVISION, "Su hijos no estan en revisión"


def test_ejercer_voto_solicitud_rechazar(db, solicitud_item: SolicitudCambio,
                                         comite_integrantes_caso_solicitud: List[User]):
    """Testea la votación en contra de una solicitud"""
    for miembro in comite_integrantes_caso_solicitud:
        voto = solicitud_item.ejercer_voto(votante=miembro, a_favor=False)
        print(voto)

    assert solicitud_item.estado == EstadoSolicitud.RECHAZADO, "No se rechazó la solicitud despues " \
                                                               "de que voten todos en contra"


def test_ejercer_voto_solicitud_a_favor_mitad_mas_uno(db, solicitud_item: SolicitudCambio,
                                                      comite_integrantes_caso_solicitud: List[User]):
    """Testea la votación a favor de la mitad más 1 de una solicitud"""
    total: int = len(comite_integrantes_caso_solicitud)
    mitad: int = floor(total / 2)

    for i in range(mitad):
        miembro = comite_integrantes_caso_solicitud[i]
        voto = solicitud_item.ejercer_voto(votante=miembro, a_favor=False)
        print(voto)

    for i in range(mitad, total):
        miembro = comite_integrantes_caso_solicitud[i]
        voto = solicitud_item.ejercer_voto(votante=miembro, a_favor=True)
        print(voto)

    assert solicitud_item.estado == EstadoSolicitud.APROBADO, "No se aprobó la solicitud despues " \
                                                              "de que voten mitad + 1 a favor"


def test_ejercer_voto_solicitud_rechazar_mitad_mas_uno(db, solicitud_item: SolicitudCambio,
                                                       comite_integrantes_caso_solicitud: List[User]):
    """Testea la votación en contra de la mitad + 1 de una solicitud"""
    total: int = len(comite_integrantes_caso_solicitud)
    mitad: int = floor(total / 2)

    for i in range(mitad):
        miembro = comite_integrantes_caso_solicitud[i]
        voto = solicitud_item.ejercer_voto(votante=miembro, a_favor=True)
        print(voto)

    for i in range(mitad, total):
        miembro = comite_integrantes_caso_solicitud[i]
        voto = solicitud_item.ejercer_voto(votante=miembro, a_favor=False)
        print(voto)

    assert solicitud_item.estado == EstadoSolicitud.RECHAZADO, "No se rechazó la solicitud despues " \
                                                               "de que voten mitad + 1 en contra"


def test_form_ejercer_voto_solicitud(db, rf, solicitud_item: SolicitudCambio,
                                     proyecto_caso_solicitud: Proyecto,
                                     comite_integrantes_caso_solicitud: List[User]):
    """Testea la votación de una solicitud a través de la validación de un form"""
    for miembro in comite_integrantes_caso_solicitud:
        request = rf.post(
            path=reverse("Ejercer Voto de solicitud de cambio", args=[proyecto_caso_solicitud.id]),
            data={
                "solicitud": solicitud_item.uuid,
                "a_favor": True
            }
        )

        request.user = miembro

        response = votar_solicitud_de_cambio(request=request, proyecto_id=proyecto_caso_solicitud.id)

        print(json.loads(response.content))

    solicitud_item.refresh_from_db()

    assert solicitud_item.estado == EstadoSolicitud.APROBADO, "No se aprobó la solicitud despues " \
                                                              "de que voten todos a favor"
