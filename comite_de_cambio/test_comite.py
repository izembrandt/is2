import json

import pytest
from django.contrib.auth.models import User
from django.urls import reverse

from comite_de_cambio.models import Comite
from proyecto.models import Proyecto
from comite_de_cambio.views import crear_comite_de_cambio, asignar_integrantes_comite_de_cambio


def test_crear_asignar_comite(db, proyecto_factory, usuario_gerente: User, user_factory) -> None:
    """Testea la creación y asignación del comite de un proyecto
    """
    # Creamos un nuevo proyecto
    proyecto: Proyecto = proyecto_factory()

    # Probamos que no tenga cómite
    assert not Comite.objects.filter(proyecto=proyecto).exists(), "Tiene un cómite definido el proyecto creado"

    # Creamos usuarios de ejemplo
    usuario1: User = user_factory(username="test_1")
    usuario2: User = user_factory(username="test_2")
    proyecto.agregar_participante(usuario1)
    proyecto.agregar_participante(usuario2)

    # Probamos crear un cómite con el gerente, y 2 usuarios
    integrantes = [usuario1, usuario_gerente, usuario2]
    comite = Comite.crear(proyecto=proyecto, integrantes=integrantes)

    # Probamos que coincidan
    assert comite.uuid == proyecto.comite.uuid, "Los identificadores del cómite no coincide con el del proyecto"

    # Probamos que todos los integrantes esten
    integrantes_comite = proyecto.comite.integrantes.all()
    for miembro in integrantes:
        assert miembro in integrantes_comite, "Falta un miembro entre los integrantes del cómite"

    # Probamos reasignar los integranre
    usuario3: User = user_factory(username="test_3")
    usuario4: User = user_factory(username="test_4")

    proyecto.agregar_participante(usuario3)
    proyecto.agregar_participante(usuario4)

    integrantes += [usuario3, usuario4]

    comite.asignar_integrantes(integrantes=integrantes)

    # Probamos que todos los integrantes esten
    integrantes_comite = proyecto.comite.integrantes.all()
    for miembro in integrantes:
        assert miembro in integrantes_comite, "Falta un miembro entre los integrantes del cómite"


def test_form_comite(db, rf, proyecto_factory, user_factory, usuario_gerente) -> None:
    """Testea el uso del form para validar la creación/asignación de comité
    """
    # Creamos proyecto de ejemplo
    proyecto: Proyecto = proyecto_factory()

    # Creamos usuarios de ejemplo
    integrantes = [user_factory() for _ in range(5)]

    for miembro in integrantes:
        proyecto.agregar_participante(miembro)

    # Probamos el formulario crear cómite
    request = rf.post(
        path=reverse("Crear Cómite de Cambio", args=[proyecto.id]),
        data={
            "integrantes": [usuario.email for usuario in integrantes]
        },
    )

    request.user = usuario_gerente

    response = crear_comite_de_cambio(request=request, proyecto_id=proyecto.id)

    print(json.loads(response.content))

    assert response.status_code == 200, "No fue exitosa la creación del cómite"

    # Probar asignando integrantes

    # Creamos usuarios de ejemplo
    integrantes = [user_factory() for _ in range(7)]

    for miembro in integrantes:
        proyecto.agregar_participante(miembro)

    # Probamos el formulario crear cómite
    request = rf.post(
        path=reverse("Asignar Cómite de Cambio", args=[proyecto.id]),
        data={
            "integrantes": [usuario.email for usuario in integrantes]
        },
    )

    request.user = usuario_gerente

    response = asignar_integrantes_comite_de_cambio(request=request, proyecto_id=proyecto.id)

    print(json.loads(response.content))

    assert response.status_code == 200, "No fue exitosa la creación del cómite"