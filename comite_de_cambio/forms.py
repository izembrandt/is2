from datetime import date, time, datetime

from django import forms
from django.db.models import QuerySet

from comite_de_cambio.constantes import EstadoSolicitud
from comite_de_cambio.models import SolicitudCambio
from item.constantes import EstadosItem
from item.models import Item
from proyecto.models import Proyecto


class IntegranteComiteFormField(forms.ModelMultipleChoiceField):
    """Representa a los integrantes, esto se usa en los forms"""
    pass


class FormCrearComite(forms.Form):
    """Formulario para la creación de un comité"""
    integrantes = IntegranteComiteFormField(label="Integrantes del Cómite", queryset=None, to_field_name="email",
                                            required=True)

    def __init__(self, *args, **kwargs):
        self.proyecto = kwargs.pop('proyecto', None)
        super().__init__(*args, **kwargs)
        if self.proyecto is not None:
            assert isinstance(self.proyecto, Proyecto)
            self.fields['integrantes'].queryset = self.proyecto.participantes.all()

    def clean(self):
        cleaned_data = super().clean()
        integrantes = cleaned_data.get("integrantes")

        if integrantes:
            if integrantes.count() < 3:
                self.add_error('integrantes', 'Debe haber minimo 3 integrantes')
            if integrantes.count() % 2 == 0:
                self.add_error('integrantes', 'Debe haber un numero impar de integrantes')


class FormAsignarIntegrantesComite(forms.Form):
    """Formulario para reasignar los integrantes de un comité"""
    integrantes = IntegranteComiteFormField(label="Integrantes del Cómite", queryset=None, to_field_name="email",
                                            required=True)

    def __init__(self, *args, **kwargs):
        self.proyecto = kwargs.pop('proyecto', None)
        super().__init__(*args, **kwargs)
        if self.proyecto is not None:
            assert isinstance(self.proyecto, Proyecto)
            self.fields['integrantes'].queryset = self.proyecto.participantes.all()

    def clean(self):
        cleaned_data = super().clean()
        integrantes = cleaned_data.get("integrantes")

        if integrantes:
            if integrantes.count() < 3:
                self.add_error('integrantes', 'Debe haber minimo 3 integrantes')
            if integrantes.count() % 2 == 0:
                self.add_error('integrantes', 'Debe haber un numero impar de integrantes')


class FormCrearSolicitudCambio(forms.Form):
    """Formulario para crear una solicitud de cambio sobre un ítem"""
    item_solicitante = forms.ModelChoiceField(label="Item solicitado", queryset=None, required=True)
    motivo = forms.CharField(label="Motivo de la solicitud", widget=forms.Textarea, required=False)

    def __init__(self, *args, **kwargs):
        self.proyecto = kwargs.pop('proyecto', None)
        super().__init__(*args, **kwargs)
        if self.proyecto is not None:
            assert isinstance(self.proyecto, Proyecto)
            self.fields['item_solicitante'].queryset = Item.listar_items_de_un_proyecto_queryset(
                proyecto_id=self.proyecto.id) \
                .filter(es_ultimo=True, linea_base__isnull=False, fase__estado='A')

    def clean(self):
        cleaned_data = super().clean()
        item_solicitante = cleaned_data.get("item_solicitante")

        solicitud_repetida: QuerySet = SolicitudCambio.objects.filter(item_solicitante=item_solicitante,
                                                                      estado=EstadoSolicitud.PENDIENTE)

        if solicitud_repetida.exists():
            self.add_error("item_solicitante", "Este ítem ya cuenta con una solicitud pendiente")


class FormEjercerVoto(forms.Form):
    """Formulario para ejercer un voto sobre una solicitud de cambio"""
    solicitud = forms.ModelChoiceField(label="Solicitud a votar", queryset=None, required=True)
    a_favor = forms.BooleanField(label="Está a favor?", required=False)

    def __init__(self, *args, **kwargs):
        self.proyecto = kwargs.pop('proyecto', None)
        super().__init__(*args, **kwargs)
        if self.proyecto is not None:
            assert isinstance(self.proyecto, Proyecto)
            self.fields['solicitud'].queryset = SolicitudCambio.objects.filter(
                item_solicitante__fase__proyecto_id=self.proyecto.id, estado=EstadoSolicitud.PENDIENTE)


class FormAceptarRevision(forms.Form):
    """Formulario para aceptar la revisión de un ítem"""
    item = forms.ModelChoiceField(label="Item", queryset=None, required=True)

    def __init__(self, *args, **kwargs):
        self.proyecto = kwargs.pop('proyecto', None)
        super().__init__(*args, **kwargs)
        if self.proyecto is not None:
            assert isinstance(self.proyecto, Proyecto)
            self.fields['item'].queryset = Item.listar_items_de_un_proyecto_queryset(
                proyecto_id=self.proyecto.id) \
                .filter(es_ultimo=True, estado__in=[EstadosItem.REVISION])


class FormRevisarItem(forms.Form):
    """Formulario para indicar que se va a revisar el ítem"""
    item = forms.ModelChoiceField(label="Item", queryset=None, required=True)

    def __init__(self, *args, **kwargs):
        self.proyecto = kwargs.pop('proyecto', None)
        super().__init__(*args, **kwargs)
        if self.proyecto is not None:
            assert isinstance(self.proyecto, Proyecto)
            self.fields['item'].queryset = Item.listar_items_de_un_proyecto_queryset(
                proyecto_id=self.proyecto.id) \
                .filter(es_ultimo=True, estado__in=[EstadosItem.REVISION], linea_base__isnull=True)


class FormReporteSolicitudes(forms.Form):
    """Formulario para solicitar el reporte de solicitudes de cambio en un periodo de tiempo"""
    inicio = forms.DateTimeField(label="Inicio", required=True)
    fin = forms.DateTimeField(label="Fin", required=True)

    def clean(self):
        """Validaciones de los datos
        """
        cleaned_data = super().clean()

        fecha_inicio: datetime = cleaned_data.get("inicio")
        fecha_fin: datetime = cleaned_data.get("fin")

        if fecha_inicio is not None and fecha_fin is not None:
            # Si queda feo, pero así se hace
            if fecha_inicio > fecha_fin:
                self.add_error("inicio", "La fecha de inicio es mayor que el del fín")
