from django.urls import path
from .views import *

urlpatterns = [
    # Cómite
    path("obtenerComite/", obtener_comite_de_cambio, name="Obtener Cómite de Cambio"),
    path("crearComite/", crear_comite_de_cambio, name="Crear Cómite de Cambio"),
    path("asignarComite/", asignar_integrantes_comite_de_cambio, name="Asignar Cómite de Cambio"),
    # Solicitud de Cambio
    path("obtenerSolicitud/", obtener_solicitudes_de_cambio, name="Obtener solicitudes de cambio"),
    path("crearSolicitud/", crear_solicitud_de_cambio, name="Crear solicitud de cambio"),
    path("ejercerVoto/", votar_solicitud_de_cambio, name="Ejercer Voto de solicitud de cambio"),
    path("cantidadSolicitud/", cantidad_solicitudes_en_periodo, name="Cantidad solicitudes de cambio"),
]
