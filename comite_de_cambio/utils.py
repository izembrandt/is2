from datetime import date, datetime
from io import BytesIO
from typing import List

from django.db.models import QuerySet
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from django.utils import timezone

from comite_de_cambio.models import SolicitudCambio
from proyecto.models import Proyecto


def generar_informe_solicitudes_periodo(proyecto: Proyecto, inicio: datetime, fin: datetime) -> HttpResponse:
    """Genera un informe con las solicitudes de cambio que hubo en un proyecto, dado una fecha
    de inicio y una fecha de fin. Se mostrarán todos las solicitudes cuya fecha de solicitud
    esten comprendidas en ese rango.

    :param proyecto: Proyecto donde se buscarán las solicitudes
    :param inicio: Fecha de inicio del periodo
    :param fin: Fecha de fin del periodo
    :return: Respuesta del servidor con el PDF generado
    :rtype: HttpResponse
    :type proyecto: Proyecto
    :type inicio: date
    :type fin: date
    """
    # Seleccionar las solicitudes
    solicitudes: QuerySet = SolicitudCambio.objects.filter(fecha_solicitud__range=[inicio, fin],
                                                           item_solicitante__fase__proyecto=proyecto)

    # Generar PDF
    datetime_reporte = timezone.now()

    contexto = {
        "proyecto": proyecto,
        "fecha": datetime.date(datetime_reporte),
        "hora": datetime.time(datetime_reporte),
        "solicitudes": solicitudes,
        "inicio": inicio,
        "fin": fin,
    }

    # Obtener el template
    template = get_template("pdf_templates/ReporteSolicitud.html")

    # Renderizar el templaye
    html = template.render(contexto)

    # Convertir el html a un flujo de bytes
    resultado = BytesIO()
    pisa.pisaDocument(BytesIO(html.encode("UTF-8")), resultado)

    # Crear el response con el archivo
    response = HttpResponse(resultado.getvalue(), content_type="application/pdf")
    response['Content-Disposition'] = 'attachment; filename="informe_solicitud_{}.pdf"'.format(proyecto.nombre)

    return response
