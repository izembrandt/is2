class EstadoSolicitud:
    PENDIENTE = 'P'
    APROBADO = 'A'
    RECHAZADO = 'R'
    CANCELADO = 'C'
