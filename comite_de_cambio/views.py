import json
from typing import List
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import QuerySet
from django.http import *
from django.contrib.auth.models import User
from django.views.decorators.http import require_http_methods
from comite_de_cambio.constantes import EstadoSolicitud
from comite_de_cambio.forms import FormCrearComite, FormAsignarIntegrantesComite, \
    FormCrearSolicitudCambio, FormEjercerVoto, FormReporteSolicitudes
from comite_de_cambio.models import Comite, SolicitudCambio
from comite_de_cambio.tasks import correo_comite_asignado, correo_solicitud_de_cambio_creado, \
    correo_solicitud_de_cambio_resultado
from comite_de_cambio.utils import generar_informe_solicitudes_periodo
from proyecto.models import Proyecto
from fases.permisos import PermisoFase
import logging

# Instancia del logger
logger = logging.getLogger(__name__)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["GET"])
def obtener_comite_de_cambio(request: HttpRequest, proyecto_id: int) -> JsonResponse:
    """Obtiene el cómite de cambio del proyecto

    :param request: HttpRequest del Usuario
    :param proyecto_id: Id del proyecto
    :return: JsonResponse con el Cómite
    """
    # Obtener el proyecto
    try:
        proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=HttpResponseBadRequest.status_code)

    # Obtener el cómite
    try:
        comite = Comite.objects.get(proyecto=proyecto)
        return JsonResponse({"comite": comite.aJSON()}, status=HttpResponse.status_code)
    except Comite.DoesNotExist:
        return JsonResponse({"comite": None}, status=HttpResponse.status_code)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["POST"])
def crear_comite_de_cambio(request: HttpRequest, proyecto_id: int) -> JsonResponse:
    """Crea el cómite de cambio del proyecto

    :param request: HttpRequest del Usuario
    :param proyecto_id: Id del proyecto
    :return: JsonResponse con el Cómite
    """
    # Obtener el proyecto
    try:
        proyecto: Proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=HttpResponseBadRequest.status_code)

    # Crear el cómite
    if request.user.has_perm('asignar_comite', proyecto):

        # Verificamos los datos
        form: FormCrearComite = FormCrearComite(request.POST, proyecto=proyecto)

        if form.is_valid():
            integrantes: List[User] = [*form.cleaned_data['integrantes']]

            comite = Comite.crear(proyecto, integrantes)

            # Logging
            logger.info("{} creó un comite en el proyecto {}".format(request.user.email, proyecto))

            correo_comite_asignado.delay(emails=[user.email for user in integrantes], proyecto_pk=proyecto_id)

            return JsonResponse({"comite": comite.aJSON()}, status=HttpResponse.status_code)
        else:
            return JsonResponse({"error": json.loads(form.errors.as_json())}, status=HttpResponseBadRequest.status_code)

    return JsonResponse({"error": "No tienes permisos para crear cómite"}, status=HttpResponseForbidden.status_code)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["POST"])
def asignar_integrantes_comite_de_cambio(request: HttpRequest, proyecto_id: int) -> JsonResponse:
    """Reasigna los integrantes de un comite de cambio

    :param request: HttpRequest del Usuario
    :param proyecto_id: Id del proyecto
    :return: JsonResponse con el Cómite
    """
    # Obtener el proyecto
    try:
        proyecto: Proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=HttpResponseBadRequest.status_code)

    try:
        comite: Comite = Comite.objects.get(proyecto=proyecto)
    except Comite.DoesNotExist:
        return JsonResponse({"error": "No existe el comite"}, status=HttpResponseBadRequest.status_code)

    # Crear el cómite
    if request.user.has_perm('asignar_comite', proyecto):

        # Verificamos los datos
        form: FormAsignarIntegrantesComite = FormAsignarIntegrantesComite(request.POST, proyecto=proyecto)

        if form.is_valid():
            integrantes: [User] = [*form.cleaned_data['integrantes']]

            comite.asignar_integrantes(integrantes)

            # Logging
            logger.info("{} asignó los integrantes del cómite del proyecto {}".format(request.user.email, proyecto))

            correo_comite_asignado.delay(emails=[user.email for user in integrantes], proyecto_pk=proyecto_id)

            return JsonResponse({"comite": comite.aJSON()}, status=HttpResponse.status_code)
        else:
            return JsonResponse({"error": json.loads(form.errors.as_json())}, status=HttpResponseBadRequest.status_code)

    return JsonResponse({"error": "No tienes permisos para crear cómite"}, status=HttpResponseForbidden.status_code)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["GET"])
def obtener_solicitudes_de_cambio(request: HttpRequest, proyecto_id: int) -> JsonResponse:
    """Obtiene la lista de solicitudes de cambio

    :param request: HttpRequest del Usuario
    :param proyecto_id: Id del proyecto
    :return: JsonResponse con la lista de las solicitudes
    """
    # Obtener el proyecto
    try:
        proyecto: Proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=HttpResponseBadRequest.status_code)

    solicitudes: QuerySet[SolicitudCambio] = SolicitudCambio.objects.filter(
        item_solicitante__fase__proyecto=proyecto)

    return JsonResponse({"solicitudes": [solicitud.aJSON() for solicitud in solicitudes]},
                        status=HttpResponse.status_code)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["POST"])
def crear_solicitud_de_cambio(request: HttpRequest, proyecto_id: int) -> JsonResponse:
    """Crea una solicitud de cambio en el proyecto

    :param request: HttpRequest del Usuario
    :param proyecto_id: Id del proyecto
    :return: JsonResponse con la lista de solicitudes de cambio
    """
    # Obtener el proyecto
    try:
        proyecto: Proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=HttpResponseBadRequest.status_code)

    # Verificar que el proyecto este en ejecucion
    if not proyecto.en_ejecucion():
        return JsonResponse({"error": "El proyecto no esta en ejecución"},
                            status=HttpResponseBadRequest.status_code)

    # Verificamos los datos
    form: FormCrearSolicitudCambio = FormCrearSolicitudCambio(request.POST, proyecto=proyecto)

    if form.is_valid():
        item = form.cleaned_data['item_solicitante']

        # Tiene que tener permiso para solicitar cambio en la fase
        if not request.user.has_perm(PermisoFase.SOLICITAR_CAMBIO, item.fase):
            return JsonResponse({"error": "No posee los permisos para crear una solicitud de cambio"},
                                status=HttpResponseForbidden.status_code)

        # Tiene que estar abierta la fase
        if item.fase.esta_cerrado():
            return JsonResponse({"error": "La fase esta cerrada"},
                                status=HttpResponseBadRequest.status_code)

        motivo = form.cleaned_data['motivo']

        solicitud = SolicitudCambio.crear(item_solicitante=item, usuario_solicitante=request.user, motivo=motivo)

        # Enviar el correo
        correo_solicitud_de_cambio_creado.delay(solicitud_uuid=solicitud.uuid)

        # Logging
        logger.info("'{}' creó una solicitud de cambio sobre el ítem '{}'".format(request.user.email, item.uuid))

        solicitudes: QuerySet[SolicitudCambio] = SolicitudCambio.objects.filter(
            item_solicitante__fase__proyecto=proyecto)

        return JsonResponse({"solicitudes": [solicitud.aJSON() for solicitud in solicitudes]},
                            status=HttpResponse.status_code)
    else:
        return JsonResponse({"error": json.loads(form.errors.as_json())}, status=HttpResponseBadRequest.status_code)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["POST"])
def votar_solicitud_de_cambio(request: HttpRequest, proyecto_id: int) -> JsonResponse:
    """Ejerce un voto sobre una solicitud de cambio de un ítem

    :param request: HttpRequest del Usuario
    :param proyecto_id: Id del proyecto
    :return: JsonResponse con la lista de solicitudes de cambio
    """
    # Obtener el proyecto
    try:
        proyecto: Proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=HttpResponseBadRequest.status_code)

    form: FormEjercerVoto = FormEjercerVoto(request.POST, proyecto=proyecto)

    # Validamos
    if form.is_valid():
        solicitud: SolicitudCambio = form.cleaned_data['solicitud']
        a_favor: bool = form.cleaned_data['a_favor']

        if not (request.user in solicitud.comite.integrantes.all()):
            return JsonResponse({"error": "No posee los permisos ejercer un voto sobre esta solicitud"},
                                status=HttpResponseForbidden.status_code)

        solicitud.ejercer_voto(votante=request.user, a_favor=a_favor)

        # Mandar correo si es que se llegó a una decisión
        solicitud.refresh_from_db()

        if solicitud.estado != EstadoSolicitud.PENDIENTE:
            # Se llegó a veredicto
            correo_solicitud_de_cambio_resultado.delay(solicitud_uuid=solicitud.uuid)

        solicitudes: QuerySet[SolicitudCambio] = SolicitudCambio.objects.filter(
            item_solicitante__fase__proyecto=proyecto)

        # Logging
        logger.info("{} votó por la solicitud de cambio {}".format(request.user.email, solicitud.uuid))

        return JsonResponse({"solicitudes": [solicitud.aJSON() for solicitud in solicitudes]},
                            status=HttpResponse.status_code)
    else:
        return JsonResponse({"error": json.loads(form.errors.as_json())}, status=HttpResponseBadRequest.status_code)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
@require_http_methods(["GET"])
def cantidad_solicitudes_en_periodo(request, proyecto_id) -> HttpResponse:
    """Responde con un pdf del reporte sobre la cantidad de solicitudes de cambio en un periodo
    de tiempo en el proyecto.

    :param request: HttpRequest del Usuario
    :param proyecto_id: Identificador del proyecto
    :return: HttpResponse con el archivo si es válido, JsonResponse con el error caso contrario
    """
    # Obtener el proyecto
    try:
        proyecto: Proyecto = Proyecto.objects.get(pk=proyecto_id)
    except Proyecto.DoesNotExist:
        return JsonResponse({"error": "No existe el proyecto"}, status=HttpResponseBadRequest.status_code)

    # Verificar que existe en el proyecto
    if request.user in proyecto.participantes.all():
        form: FormReporteSolicitudes = FormReporteSolicitudes(request.GET)

        if form.is_valid():
            fecha_inicio = form.cleaned_data['inicio']
            fecha_fin = form.cleaned_data['fin']

            # Logging
            logger.info(
                "{} solicitó un informe de las solicitudes de cambio del proyecto: {}".format(request.user.email,
                                                                                              proyecto))

            return generar_informe_solicitudes_periodo(proyecto=proyecto, inicio=fecha_inicio, fin=fecha_fin)

        else:
            return JsonResponse({"error": json.loads(form.errors.as_json())}, status=HttpResponseBadRequest.status_code)

    else:
        return JsonResponse({"error": "Debes pertenecer al proyecto"}, status=HttpResponseForbidden.status_code)
