from __future__ import annotations
from typing import List
from django.db import IntegrityError
from django.db.models import F, QuerySet
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User
from fases.permisos import PermisoFase
from item.constantes import EstadosItem
from linea_base.models import LineaBase
from .constantes import EstadoSolicitud
from proyecto.models import Proyecto
from item.models import Item
from datetime import datetime
import uuid as uuid
import pytz


class Comite(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    proyecto = models.OneToOneField(Proyecto, on_delete=models.CASCADE, null=False, related_name="comite")
    integrantes = models.ManyToManyField(User, related_name="comites")

    @staticmethod
    def crear(proyecto: Proyecto, integrantes: List[User]) -> Comite:
        """Crea un cómite dentro de un proyecto con ciertos integrantes, los cuales deben ser mayor o igual a 3
        integrantes y debe haber una cantidad par de integrantes

        :param proyecto: Proyecto donde se va a crear el cómite
        :param integrantes: Integrantes del cómite
        :return: El cómite recien creado
        :rtype: Comite
        """
        # Precondiciones
        assert isinstance(integrantes, List), "Se requiere una lista de usuarios"
        assert isinstance(proyecto, Proyecto), "Se requiere proyecto como argumento"
        assert proyecto.en_configuracion() or proyecto.en_ejecucion(), \
            "El proyecto debe estar en etapa de configuración o en ejecución"
        participantes_proyecto = proyecto.participantes.all()

        numero_integrantes = len(integrantes)
        assert numero_integrantes >= 3, "Mínimo de 3 integrantes para un comite de cambios"
        assert numero_integrantes % 2 == 1, "El comite de cambios debe estar formado por un número impar"

        for usuario in integrantes:
            assert isinstance(usuario, User)
            assert usuario in participantes_proyecto, "El usuario no es participante del proyecto"

        # Creamos el comite
        comite = Comite(proyecto=proyecto)
        comite.save()

        # Agregamos los participantes
        comite.integrantes.set(integrantes)

        # Retornamos el cómite
        return comite

    def asignar_integrantes(self, integrantes: List[User]) -> None:
        """Asigna integrantes al cómite del proyecto

        :param integrantes: Integrantes que se desea que sean partes del Cómite
        :type integrantes: List[User]
        :rtype: None
        """
        # Precondiciones
        assert isinstance(integrantes, list), "integrantes no es una lista"
        assert isinstance(self.proyecto, Proyecto), "Se requiere proyecto como argumento"
        assert self.proyecto.en_configuracion() or self.proyecto.en_ejecucion(), \
            "El proyecto debe estar en etapa de configuración o en ejecución"
        participantes_proyecto = self.proyecto.participantes.all()

        numero_integrantes = len(integrantes)
        assert numero_integrantes >= 3, "Mínimo de 3 integrantes para un comite de cambios"
        assert numero_integrantes % 2 == 1, "El comite de cambios debe estar formado por un número impar"

        for usuario in integrantes:
            assert isinstance(usuario, User)
            assert usuario in participantes_proyecto, "El usuario no es participante del proyecto"

        # Agregamos los participantes
        self.integrantes.set(integrantes)

    def aJSON(self) -> dict:
        """Representación json del Comite

        :return: El comité de cambios representado como un JSON
        :rtype: dict
        """
        return {
            "uuid": self.uuid,
            "proyecto": self.proyecto.id,
            "integrantes": list(
                self.integrantes.all().values(apellido=F('last_name'), correo=F('email'), nombre=F('first_name')))
        }


class SolicitudCambio(models.Model):
    ESTADO_SOLICITUD = [
        (EstadoSolicitud.PENDIENTE, "Solicitud pendiente"),
        (EstadoSolicitud.APROBADO, "Solicitud Aprobada"),
        (EstadoSolicitud.RECHAZADO, "Solicitud Rechazada")
    ]
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    item_solicitante = models.ForeignKey(Item, on_delete=models.CASCADE, null=False, related_name="solicitudes")
    usuario_solicitante = models.ForeignKey(User, on_delete=models.CASCADE, null=False, related_name="solicitudes")
    motivo = models.TextField("Motivo de la solicitud")
    fecha_solicitud = models.DateTimeField("Fecha de solicitud", default=timezone.now, null=False)
    fecha_respuesta = models.DateTimeField("Fecha de aprobación/rechazo", null=True)
    estado = models.CharField("Estado de la solicitud", max_length=1, choices=ESTADO_SOLICITUD)

    @staticmethod
    def crear(item_solicitante: Item, usuario_solicitante: User, motivo: str) -> SolicitudCambio:
        """Crea una solicitud de cambio sobre un item en línea base cerrada.

        Para que se pueda crear una solicitud de cambio se debe cumplir que:

        1. El proyecto debe de estar en ejecución.
        2. El ítem debe estar en estado de Línea Base o Revisión.
        3. El usuario que solicita debe tener el permiso en la fase del ítem para solicitar cambios.
        4. El ítem debe de estar en una línea base cerrada.
        5. La fase del ítem debe estar abierta.

        :param item_solicitante: Item sobre el cual se quiere generar la solicitud de cambio.
        :param usuario_solicitante: Usuario que esta solicitando.
        :param motivo: Motivo por el cual el Usuario desea realizar la solicitud de cambio.
        :return: La solicitud de cambio hecha.
        :rtype: SolicitudCambio
        """
        # Precondiciones
        assert isinstance(item_solicitante, Item), "No es instancia de ítem (item_solicitante)"
        assert isinstance(usuario_solicitante, User), "No es instancia de User (user_solicitante)"
        assert isinstance(motivo, str), "No es una cadena (motivo)"
        assert item_solicitante.fase.proyecto.en_ejecucion(), "El proyecto debe estar en ejecución"
        assert item_solicitante.estado in [EstadosItem.LINEA_BASE,
                                           EstadosItem.REVISION], "Solo se puede crear solicitudes sobre items " \
                                                                  "aprobados, en línea base o en revisión "
        assert usuario_solicitante.has_perm(PermisoFase.SOLICITAR_CAMBIO,
                                            item_solicitante.fase), "No posee permisos para crear " \
                                                                    "solicitud "

        assert item_solicitante.linea_base.esta_cerrado(), "La línea base debe estar cerrada"
        assert not item_solicitante.fase.esta_cerrado(), "La fase no puede estar cerrada"

        # Creamos la solicitud
        solicitud = SolicitudCambio(item_solicitante=item_solicitante, usuario_solicitante=usuario_solicitante,
                                    motivo=motivo, estado=EstadoSolicitud.PENDIENTE)

        solicitud.save()

        return solicitud

    def aprobar(self) -> None:
        """Aprueba la solicitud de cambio, rompiendo la línea base y poniendo en desarrollo el ítem.

        Todos los ítems que estaban en la línea base rota y los sucesores e hijos se pondran en
        revisión.

        :return: No retorna
        :rtype: None
        """
        # Marcar como aprobado
        self.estado = EstadoSolicitud.APROBADO
        self.save()

        # Romper la línea base
        self.item_solicitante.linea_base.romper()

        self.item_solicitante.refresh_from_db()
        # Pasar el item_solicitante a desarrollo
        self.item_solicitante.poner_en_desarrollo()

        # Poner todas las relaciones del item en revisión
        for item in self.item_solicitante.obtener_sucesores_queryset() \
                .filter(estado__in=[EstadosItem.APROBADO, EstadosItem.LINEA_BASE]):
            item.poner_en_revision()

        for item in self.item_solicitante.obtener_hijos_queryset() \
                .filter(estado__in=[EstadosItem.APROBADO, EstadosItem.LINEA_BASE]):
            item.poner_en_revision()

    def rechazar(self) -> None:
        """Rechaza la solicitud de cambio

        :return: Nada
        :rtype: None
        """
        # Marcar como rechazado
        self.estado = EstadoSolicitud.RECHAZADO
        self.save()

    def ejercer_voto(self, votante: User, a_favor: bool) -> VotoComite:
        """Ejerce un voto en la solicitud de cambio

        :param votante: Usuario que esta votando
        :param a_favor: True si esta a favor, False si esta en contra
        :return: Voto del Usuario
        :rtype: VotoComite
        """
        assert isinstance(votante, User), "Se requiere usuario"
        assert isinstance(a_favor, bool), "Se requiere boleano"
        assert self.estado == EstadoSolicitud.PENDIENTE, "Se requiere que la solicitud este pendiente"

        try:
            voto = VotoComite.crear(votante=votante, solicitud=self, a_favor=a_favor)
        except IntegrityError:
            voto = VotoComite.objects.get(votante=votante, solicitud=self)

        votos_validos: QuerySet[VotoComite] = self.votos_validos

        if self.comite.integrantes.count() == votos_validos.count():
            # Podemos ejecutar un veredicto
            if votos_validos.filter(a_favor=True).count() > votos_validos.filter(a_favor=False).count():
                self.aprobar()
            else:
                self.rechazar()

        return voto

    @property
    def votos_validos(self):
        """Retorna los votos de la solicitud de miembros actuales del cómite

        :return:
        """
        return self.votos.filter(votante__comites__uuid=self.comite.uuid)

    def aJSON(self) -> dict:
        """Representación json
        """
        asuncion = pytz.timezone("America/Asuncion")

        return {
            "uuid": self.uuid,
            "item_solicitante": self.item_solicitante.uuid,
            "fase": self.item_solicitante.fase.id,
            "usuario_solicitante": self.usuario_solicitante.email,
            "motivo": self.motivo,
            "fecha_solicitud": datetime.date(self.fecha_solicitud.astimezone(asuncion)),
            "fecha_respuesta": self.fecha_respuesta,
            "estado": self.estado,
            "votos": [voto.aJSON() for voto in self.votos.all()]
        }

    @property
    def comite(self):
        return self.item_solicitante.fase.proyecto.comite


class VotoComite(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    votante = models.ForeignKey(User, on_delete=models.CASCADE, null=False, related_name="votos")
    solicitud = models.ForeignKey(SolicitudCambio, on_delete=models.CASCADE, null=False, related_name="votos")
    a_favor = models.BooleanField("Si estuvo a favor", null=False)
    fecha = models.DateTimeField("Fecha del voto", default=timezone.now, null=False)

    @staticmethod
    def crear(votante: User, solicitud: SolicitudCambio, a_favor: bool) -> VotoComite:
        """Emite un voto sobre una solicitud de cambio.

        :param votante: Usuario que hace el voto
        :param solicitud: La solicitud de cambio donde se va a votar.
        :param a_favor: True si es que esta a favor, False si esta en contra de la solicitud.
        :return: El voto emitido por el usuario
        :rtype: VotoComite
        """
        # Precondiciones
        assert isinstance(votante, User), "Se necesita usuario"
        assert isinstance(solicitud, SolicitudCambio), "Se necesita una solicitud de cambio"
        assert isinstance(a_favor, bool), "Se necesita saber si esta a favor"

        # Creamos el voto
        voto = VotoComite(votante=votante, solicitud=solicitud, a_favor=a_favor)

        voto.save()

        return voto

    def aJSON(self):
        """Representación JSON de un voto

        :return: Representación JSON de un voto
        """
        return {
            "uuid": self.uuid,
            "votante": self.votante.email,
            "solicitud": self.solicitud.uuid,
            "a_favor": self.a_favor,
            "fecha": self.fecha,
        }

    def __str__(self):
        return "Votante: {voto.votante.email} A Favor: {voto.a_favor}".format(voto=self)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['votante', 'solicitud'], name="Un solo voto por solicitud"),
        ]
