from typing import List
from celery import shared_task
from django.core.mail import send_mail
from django.template.loader import render_to_string

from comite_de_cambio.constantes import EstadoSolicitud
from comite_de_cambio.models import SolicitudCambio
from proyecto.models import Proyecto
from uuid import UUID


@shared_task
def correo_comite_asignado(emails: List[str], proyecto_pk: int) -> None:
    """Manda un correo a los emails de la lista que fueron asignados como comité del proyecto

    :param emails: Lista de emails recipientes.
    :param proyecto_pk: Identificador del proyecto
    """
    # Buscar proyecto
    proyecto: Proyecto = Proyecto.objects.get(pk=proyecto_pk)

    # Crear el correo
    contexto = {
        "proyecto": proyecto
    }

    # Renderizamos y mandamos
    html = render_to_string("email_templates/comite_asignado.html", context=contexto)
    send_mail(subject="Notificación: Te asignaron como miembro de comité",
              message="Se te ha designado como miembro de comité en el proyecto {}".format(proyecto.nombre),
              recipient_list=emails, from_email=None, fail_silently=False, html_message=html)

    return None


@shared_task
def correo_solicitud_de_cambio_creado(solicitud_uuid: UUID) -> None:
    """Manda un correo a los miembros del comité con la solicitud de cambio

    :param solicitud_uuid: UUID de la Solicitud de Cambio
    """
    # Buscar la solicitud recien creada
    solicitud = SolicitudCambio.objects.get(pk=solicitud_uuid)
    item = solicitud.item_solicitante
    proyecto = item.fase.proyecto

    # Crear contexto
    contexto = {
        "solicitud": solicitud,
        "item": item,
        "proyecto": proyecto,
        "usuario": solicitud.usuario_solicitante
    }

    # Preparar email
    html = render_to_string("email_templates/solicitud_de_cambio_creado.html", context=contexto)  # Body

    recipientes = [usuario.email for usuario in solicitud.item_solicitante.fase.proyecto.comite.integrantes.all()]

    send_mail(subject="Notificación: Solicitud de cambio creado en proyecto -> {}".format(proyecto.nombre),
              message="Se ha creado una solicitud de cambio sobre el ítem '{}'".format(item.nombre),
              recipient_list=recipientes, from_email=None, fail_silently=False, html_message=html)

    return None


@shared_task
def correo_solicitud_de_cambio_resultado(solicitud_uuid: UUID) -> None:
    """Manda un correo a los miembros del comité y al usuario que solicitó el cambio con los resultados de
    la solicitud de cambio

    :param solicitud_uuid: UUID de la Solicitud de Cambio
    """
    # Buscar la solicitud recien creada
    solicitud = SolicitudCambio.objects.get(pk=solicitud_uuid)
    item = solicitud.item_solicitante
    proyecto = item.fase.proyecto

    # Crear contexto
    contexto = {
        "solicitud": solicitud,
        "item": item,
        "proyecto": proyecto,
        "usuario": solicitud.usuario_solicitante,
        "veredicto": "Aprobado" if solicitud.estado == EstadoSolicitud.APROBADO else "Rechazado",
    }

    # Preparar email
    html = render_to_string("email_templates/solicitud_de_cambio_veredicto.html", context=contexto)  # Body

    recipientes = [usuario.email for usuario in solicitud.item_solicitante.fase.proyecto.comite.integrantes.all()]

    recipientes.append(solicitud.usuario_solicitante.email)  # Agregamos al usuario que hizo la solicitud

    # Preparar el mail
    send_mail(subject="Notificación: Solicitud de cambio resuelto en proyecto -> {}".format(proyecto.nombre),
              message="Se ha resuelto un veredicto en una solicitud de cambio sobre el ítem '{}'".format(item.nombre),
              recipient_list=recipientes, from_email=None, fail_silently=False, html_message=html)

    return None
