from django.apps import AppConfig


class ComiteDeCambioConfig(AppConfig):
    name = 'comite_de_cambio'
