#!/usr/bin/env bash
# Buscar el directorio donde esta install.sh
DIR_IS2="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DIR_VENV="$DIR_IS2/venv"
SCRIPT_ENV="/etc/profile.d/IS2_DJANGO.sh"
# Crear la carpeta de static
STATIC_DIR="$DIR_IS2/static_apache"
# DEFAULTS
DEFAULT_HOST_PERMITIDOS='ingenieria-de.software:localhost'
DEFAULT_DATABASE_URL="postgres://is2:is2@localhost:5432/is2"
# Leer configuración
read -p "[POSTGRES] Ingrese la url de la base de datos [$DEFAULT_DATABASE_URL]: " DATABASE_URL
read -p "Ingrese los hosts permitidos por el servidor [$DEFAULT_HOST_PERMITIDOS]: " HOST_PERMITIDOS
read -p "[Login Social] Ingrese el Client Id de Google: " GOOGLE_CLIENT_ID
read -p "[Login Social] Ingrese el Secreto de Google: " GOOGLE_SECRET
read -p "[Cloud Storage] Ingrese el path de los credenciales de Google: " GOOGLE_APPLICATION_CREDENTIALS
read -p "[Cloud Storage] Ingrese el ID de proyecto de Google: " GS_PROJECT_ID
read -p "[Cloud Storage] Ingrese el nombre del bucket: " GS_BUCKET_NAME
read -p "[Cloud Storage] Ingrese el Endpoint: " GS_CUSTOM_ENDPOINT

# Cargamos default a las variables sin datos
DATABASE_URL="${DATABASE_URL:-${DEFAULT_DATABASE_URL}}"
HOST_PERMITIDOS="${HOST_PERMITIDOS:-${DEFAULT_HOST_PERMITIDOS}}"
# Configuración del ambiente virtual
echo "Creando ambiente virtual de python"
rm -Rf venv >/dev/null
virtualenv venv --python 3.7
echo "Activando el entorno virtual"
source venv/bin/activate
# Instalación de los archivos del backend & frontend
echo "Instalando dependencias de python"
pip install --trusted-host pypi.python.org --trusted-host files.pythonhosted.org --trusted-host pypi.org -r requirements.txt
DJANGO_SECRET_IS2=$(python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())')
echo "Instalando las herramientas para construir el frontend"
cd frontend || exit
npm install
echo "Construyendo el Frontend"
npm run build
cd .. || exit
# Configuración del DJANGO
echo "Configurando DJANGO"
echo "
  # Base de datos postgresql
  DATABASE_URL=$DATABASE_URL
  # Secreto del DJANGO
  DJANGO_SECRET_IS2=$DJANGO_SECRET_IS2
  # Configuración del django
  IS2_DEBUG=False
  HOST_PERMITIDOS=$HOST_PERMITIDOS
  CARPETA_ESTATICOS=$STATIC_DIR
  # Login de google
  GOOGLE_CLIENT_ID=$GOOGLE_CLIENT_ID
  GOOGLE_SECRET=$GOOGLE_SECRET
  # Cloud Storage
  GOOGLE_APPLICATION_CREDENTIALS=$GOOGLE_APPLICATION_CREDENTIALS
  GS_PROJECT_ID=$GS_PROJECT_ID
  GS_BUCKET_NAME=$GS_BUCKET_NAME
  GS_CUSTOM_ENDPOINT=$GS_CUSTOM_ENDPOINT
  GS_DEFAULT_ACL='publicRead'
" > "$DIR_IS2/is2_django/.env"
# Configuración de los archivos estaticos
echo "Recolectando los archivos estaticos"
python manage.py collectstatic
# Configuración de la base de datos
echo "Configurando base de datos"
python manage.py migrate
python manage.py actualizar_config
# Configuración del apache
deactivate
echo "Configurando Apache"
ARCHIVO_DEFAULT='/etc/apache2/sites-available/000-default.conf'
sudo echo "
    <VirtualHost *:80>
      ServerAdmin izembrandt@gmail.com
      DocumentRoot /var/www/html
      Alias /static $STATIC_DIR
      <Directory $STATIC_DIR>
	Require all granted
      </Directory>
      <Directory $DIR_IS2/is2_django>
	<Files wsgi.py>
		Require all granted
	</Files>
      </Directory>
      WSGIDaemonProcess is2 python-home=$DIR_VENV/ python-path=$DIR_IS2
      WSGIProcessGroup is2
      WSGIScriptAlias / $DIR_IS2/is2_django/wsgi.py

      ErrorLog \${APACHE_LOG_DIR}/error.log
      CustomLog \${APACHE_LOG_DIR}/access.log combined

    </VirtualHost>" | sudo tee $ARCHIVO_DEFAULT > /dev/null
sudo service apache2 restart
# Agregar el path de la instalación a las variables de ambiente
sudo echo "export IS2_DJANGO_HOME=$DIR_IS2" | sudo tee $SCRIPT_ENV > /dev/null
echo "Instalación Completa"
echo "No se podra actualizar la configuración de la instalación hasta que
reinicie el shell"

