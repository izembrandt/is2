# Entrar al directorio
cd $IS2_DJANGO_HOME
# Actualizar
git checkout -- .
git pull
sudo chown -R www-data:www-data "$IS2_DJANGO_HOME"
sudo chmod -R 775 "$IS2_DJANGO_HOME"
# Aplicar migraciones
source venv/bin/activate
python manage.py migrate
python manage.py actualizar_config
deactivate
echo "Fin del Script"
