# Entramos a la carpeta del proyecto
cd $IS2_DJANGO_HOME
# Abrimos el nano en la configuración
sudo nano is2_django/.env
# Actualizamos la base de datos
source venv/bin/activate
python manage.py migrate || echo "Hubo problema al migrar la base de datos"
python manage.py actualizar_config || echo "Hubo problema al migrar las credenciales"
deactivate
echo "Fin del Script"
