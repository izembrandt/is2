#!/bin/bash
# Verificar archivo de configuracion
RELEASE_TAR_BZ2="https://gitlab.com/izembrandt/is2/-/archive/Test_Celery/is2-Test_Celery.tar.bz2"
SCRIPT_ENV="/etc/profile.d/IS2_DJANGO.sh"
case $# in
  1)
    ARCHIVO=$1
    ;;
  *)
    echo "Uso: bash desplegar.sh ARCHIVO_CONFIGURACION"
    exit 1
esac


if [ -f "$ARCHIVO" ]
    then
      echo "Se encontró el archivo de configuración: $ARCHIVO"
    else
      echo "No se encontró el archivo de configuración: $ARCHIVO"
      exit 1
fi

# Cargamos la configuracion
source "$ARCHIVO"

if [ "$?" -ne "0" ]
  then
    echo "No se pudo leer el archivo de configuración: $ARCHIVO"
    exit 1
fi

#Se crea la base de datos
echo "
DROP DATABASE IF EXISTS $DB_NAME;
DROP USER IF EXISTS $DB_NAME;
CREATE DATABASE $DB_NAME;
CREATE USER $DB_USER WITH PASSWORD '$DB_PASS';
GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO $DB_USER;
ALTER USER $DB_USER CREATEDB;
" | psql "host=$HOST_DB port=$PORT_DB user=$POSTGRES_USER password=$POSTGRES_PASS"

#Se configura la base de datos con django
DATABASE_URL="DATABASE_URL=postgres://$DB_USER:$DB_PASS@$HOST_DB:$PORT_DB/$DB_NAME"


# Descargamos el sistema
echo "Descargar el release"
rm "Release.tar.bz2" > /dev/null
wget "$RELEASE_TAR_BZ2" -O "Release.tar.bz2"
DESCOMPRIMIDO=$(tar -vxjf "Release.tar.bz2" | sed -e 's@/.*@@' | uniq)
rm "Release.tar.bz2" > /dev/null

# Copiamos el archivo de configuración
ARCHIVO_CONFIGURACION_DESTINO="$DESCOMPRIMIDO/is2_django/.env"
cp "$ARCHIVO" "$ARCHIVO_CONFIGURACION_DESTINO"
echo -e "\n$DATABASE_URL" >> $ARCHIVO_CONFIGURACION_DESTINO

# Copiamos el proyecto a la carpeta de instalación
DIR_IS2="$(pwd)/$DESCOMPRIMIDO"
# Entramos al directorio e instalamos
cd "$DIR_IS2"

# Configuración del ambiente virtual
echo "Creando ambiente virtual de python"
rm -Rf venv > /dev/null
virtualenv venv --python=python3.7
echo "Activando el entorno virtual"
source venv/bin/activate

# Instalación de los archivos del backend & frontend
echo "Instalando dependencias de python"
pip install --trusted-host pypi.python.org --trusted-host files.pythonhosted.org --trusted-host pypi.org -r requirements.txt
echo "Instalando las herramientas para construir el frontend"
cd frontend || exit
npm install
echo "Construyendo el Frontend"
npm run build
cd .. || exit

# Configuración de la base de datos
echo "Configurando base de datos"
python manage.py migrate
python manage.py actualizar_config
echo "Recolectando archivos estaticos"
python manage.py collectstatic
echo "Crear una cuenta de mantenimiento"
python manage.py createsuperuser
deactivate

# Configuración apache
echo "Configurando Apache"
STATIC_DIR="$DIR_IS2/$CARPETA_ESTATICOS"
DIR_VENV="$DIR_IS2/venv"
ARCHIVO_DEFAULT='/etc/apache2/sites-available/000-default.conf'
sudo echo "
    <VirtualHost *:80>
      ServerAdmin izembrandt@gmail.com
      DocumentRoot /var/www/html
      Alias /static $STATIC_DIR
      <Directory $STATIC_DIR>
	Require all granted
      </Directory>
      <Directory $DIR_IS2/is2_django>
	<Files wsgi.py>
		Require all granted
	</Files>
      </Directory>
      WSGIDaemonProcess is2 python-home=$DIR_VENV/ python-path=$DIR_IS2
      WSGIProcessGroup is2
      WSGIScriptAlias / $DIR_IS2/is2_django/wsgi.py

      ErrorLog \${APACHE_LOG_DIR}/error.log
      CustomLog \${APACHE_LOG_DIR}/access.log combined

    </VirtualHost>" | sudo tee $ARCHIVO_DEFAULT > /dev/null

sudo chown -R www-data:www-data "$DIR_IS2"
sudo chmod -R 775 "$DIR_IS2"
sudo usermod -a -G www-data "$(whoami)"

sudo service apache2 restart
# Agregar el path de la instalación a las variables de ambiente
sudo echo "export IS2_DJANGO_HOME=$DIR_IS2" | sudo tee $SCRIPT_ENV > /dev/null

# Agregar el celery worker

# Configuracion
CELERY_SERVICE_FILE="/etc/systemd/system/celeryIS2.service"
CELERY_CONF_FILE="/etc/default/celeryIS2"
echo "
CELERY_APP='is2_django'
CELERYD_NODES='worker'
CELERYD_OPTS=''
CELERY_BIN='$DIR_VENV/bin/celery'
CELERYD_PID_FILE='/var/run/celery/%n.pid'
CELERYD_LOG_FILE='/var/log/celery/%n%I.log'
CELERYD_LOG_LEVEL='INFO'
" | sudo tee "$CELERY_CONF_FILE"

# Service
sed "s+DIRECTORIOIS2+$DIR_IS2+" "$DIR_IS2"/scripts/inits/celeryis2.service | sudo tee "$CELERY_SERVICE_FILE"

sudo systemctl daemon-reload
sudo systemctl enable celeryIS2.service
sudo systemctl start celeryIS2.service

# Fin de instalación
echo "Instalación Completa"
