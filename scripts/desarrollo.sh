#!/bin/bash

# DATOS
REPOSITORIO="https://gitlab.com/izembrandt/is2.git"
CARPETA="IS2_DESARROLLO"
HOST_DB="localhost"
PORT_DB="5432"

# Verificar archivo de configuracion
case $# in
  1)
    ARCHIVO=$1
    ;;
  *)
    echo "Uso: bash desarollo.sh ARCHIVO_CONFIGURACION"
    exit 1
esac


if [ -f "$ARCHIVO" ]
    then
      echo "Se encontró el archivo de configuración: $ARCHIVO"
    else
      echo "No se encontró el archivo de configuración: $ARCHIVO"
      exit 1
fi

# Cargamos la configuracion
source "$ARCHIVO"

if [ "$?" -ne "0" ]
  then
    echo "No se pudo leer el archivo de configuración: $ARCHIVO"
    exit 1
fi

# Verificar la carpeta
if [ -d "$CARPETA" ]
  then
    echo "Ya existe la carpeta: $CARPETA, no se puede continuar."
    exit 1
fi

# Se clona el proyecto
git clone "$REPOSITORIO" "$CARPETA"

#Se accede al directorio del repositorio
cd "$CARPETA"

# Se intenta hacer checkout

select BRANCH in $(git branch -r | sed 's:.*/::' | grep -v 'master');
do
  echo "Seleccione un branch de la lista"
  case $BRANCH in
    "")
      ;;
    *)
      echo "Se selecciona el branch: $BRANCH"
      git checkout $BRANCH || exit 1
      break
      ;;
  esac
done


# Configuracion del ambiente virtual
echo "Creando ambiente virtual de python"
rm -Rf venv > /dev/null
virtualenv venv --python=python3.7
echo "Activando el entorno virtual"
source venv/bin/activate

# Instalacion de los archivos del backend & frontend
echo "Instalando dependencias de python"
pip install --trusted-host pypi.python.org --trusted-host files.pythonhosted.org --trusted-host pypi.org -r requirements.txt
echo "Instalando las herramientas para construir el frontend"
cd frontend || exit
npm install
echo "Construyendo el Frontend"
npm run build
cd .. || exit

#Se crea la base de datos
echo "
DROP DATABASE IF EXISTS $DB_NAME;
DROP USER IF EXISTS $DB_NAME;
CREATE DATABASE $DB_NAME;
CREATE USER $DB_USER WITH PASSWORD '$DB_PASS';
GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO $DB_USER;
ALTER USER $DB_USER CREATEDB;
" | psql "host=$HOST_DB port=$PORT_DB user=$POSTGRES_USER password=$POSTGRES_PASS"

#Se configura la base de datos con django
DATABASE_URL="DATABASE_URL=postgres://$DB_USER:$DB_PASS@$HOST_DB:$PORT_DB/$DB_NAME"

# Copiamos el archivo de configuración
ARCHIVO_CONFIGURACION_DESTINO="$CARPETA/is2_django/.env"
cd ..
cp "$ARCHIVO" "$ARCHIVO_CONFIGURACION_DESTINO"
echo -e "\n$DATABASE_URL" >> $ARCHIVO_CONFIGURACION_DESTINO
cd "$CARPETA"

# Configuración de la base de datos
echo "Configurando base de datos"
python manage.py migrate
python manage.py actualizar_config
echo "Crear una cuenta de mantenimiento"
python manage.py createsuperuser
deactivate

