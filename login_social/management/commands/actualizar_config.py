from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Recarga en la base de datos los permisos globales del Administrador y los Usuarios. Recarga el TOKEN del Login"

    def handle(self, *args, **options):
        """Actualiza los permisos globales del Administrador y del Usuario.
        Actualiza el token del login social.

        Los datos se obtienen de los settings.

        Se podrá ejecutar esta actualización con el comando ´python manage.py actualizar_config´
        """
        from django.contrib.auth.models import Group, Permission
        from django.conf import settings

        # Crear rol administrador
        admin, _ = Group.objects.get_or_create(name="Administrador")
        permiso_admin = []

        for codename in settings.PERMISOS_ADMINISTRADOR:
            permiso_admin.append(Permission.objects.get(codename=codename))

        admin.permissions.set(permiso_admin)

        # Crear rol Usuario
        usuario, _ = Group.objects.get_or_create(name="Usuario")
        permiso_user = []

        for codename in settings.PERMISOS_USUARIO:
            permiso_user.append(Permission.objects.get(codename=codename))

        usuario.permissions.set(permiso_user)

        # Actualizar gerentes
        from roles.models import Rol
        querysetGerentes = Rol.objects.filter(es_gerente=True)

        for gerente in querysetGerentes:
            gerente.setear_permisos_nuevos(settings.PERMISOS_GERENTE_PROYECTO, settings.PERMISOS_GERENTE_FASES)

        # Actualizar TOKEN del Login Social
        from allauth.socialaccount.models import SocialApp
        from django.contrib.sites.models import Site

        socialApp, _ = SocialApp.objects.get_or_create(name='GOOGLE API')

        socialApp.client_id = settings.GOOGLE_CLIENT_ID
        socialApp.secret = settings.GOOGLE_SECRET
        socialApp.provider = settings.GOOGLE_PROVIDER
        socialApp.sites.set([Site.objects.get(pk=settings.SITE_ID)])

        socialApp.save()
