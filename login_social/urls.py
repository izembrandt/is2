from django.urls import path
from . import views

urlpatterns = [
    path('listar_usuarios/', views.lista_usuarios_sistema_json, name='AJAX Listar Usuarios Sistema'),
    path('fetchUsuario/', views.ajax_info_usuario, name='AJAX Obtener Info Usuario'),
]
