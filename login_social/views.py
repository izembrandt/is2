from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.http import JsonResponse
from menu_principal.views import _json_response_datos_usuario

# Create your views here.


@login_required
@permission_required('proyecto.agregar_proyecto', raise_exception=True)
def lista_usuarios_sistema_json(request):
    from django.db.models.functions import Concat
    from django.db.models import Value as V
    lista_usuarios_query = User.objects \
        .exclude(email='')

    lista_usuario = list(lista_usuarios_query.values('id', 'email', nombre=Concat('first_name', V(' '), 'last_name')))

    respuesta = {
        "message": "",
        "value": lista_usuario,
        "code": 200,
        "redirect": "",
    }

    return JsonResponse(respuesta, status=200)


@login_required
@permission_required('proyecto.listar_proyectos', login_url='Acceso Pendiente')
def ajax_info_usuario(request):
    """Función que retorna la info de un usuario

    :param request: Request del usuario
    :return: JsonResponse con la info del usuario
    """
    if request.method == 'GET':
        return _json_response_datos_usuario(request)

    return JsonResponse({"error": "No es GET"}, status=400)
