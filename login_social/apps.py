from django.apps import AppConfig


class LoginSocialConfig(AppConfig):
    name = 'login_social'
