import pytest
from django.contrib.auth.models import User, Group

@pytest.mark.django_db
def test_administrador():
    """Verifica si un administrador podrá crear proyectos
    """
    me = User.objects.create_user('test_django_pytest',None,'test_django_pytest')
    grupo = Group.objects.get(name='Administrador')
    me.groups.add(grupo)
    assert me.has_perm('proyecto.agregar_proyecto'), "El Administrador debe poder crear proyectos"


@pytest.mark.django_db
def test_no_administrador():
    """Verifica si un Usuario común no podrá crear proyectos
    """
    me = User.objects.create_user('test_django_pytest', None, 'test_django_pytest')
    grupo = Group.objects.get(name='Usuario')
    me.groups.add(grupo)
    assert me.has_perm('proyecto.agregar_proyecto') == False, "El usuario común no debe tener permisos para crear proyectos"


@pytest.mark.django_db
def test_con_acceso():
    """Verifica que un usuario con acceso al sistema tenga el permiso para acceder
    """
    me = User.objects.create_user('test_django_pytest', None, 'test_django_pytest')
    grupo = Group.objects.get(name='Usuario')
    me.groups.add(grupo)
    assert me.has_perm('proyecto.listar_proyectos'), "El usuario común  debe tener permisos para acceder al sistema"

@pytest.mark.django_db
def test_sin_acceso():
    """Verifica que un usuario sin acceso al sistema no tenga el permiso para acceder
    """
    me = User.objects.create_user('test_django_pytest', None, 'test_django_pytest')
    assert not me.has_perm('proyecto.listar_proyectos'), "El usuario sin autorización no debe tener permisos para acceder al sistema"


