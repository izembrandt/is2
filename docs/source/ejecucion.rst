=====================
Ejecución del sistema
=====================

Para ejecutar el sistema usaremos el ``django-admin`` , para nuestra suerte, el proyecto incluye un archivo
``manage.py`` que actua como ``django-admin``. Para correr el servidor, solo necesitaremos ejecutar::

  python manage.py runserver

Mientras el ``django-admin`` se mantenga en ejecución, nuestro sistema se podra acceder desde `<localhost:8000>`_.


Primeros Pasos
**************

La primera vez que ejecuta el sistema, podrá acceder a `<localhost:8000>`_ , sin embargo, cuando inicie sesión notará que
aún no tiene acceso al sistema y lo redirigirá a una interfaz de advertencia, donde aparece el siguiente mensaje
``Autorización pendiente del administrador``.

Esto se debe a que que aún no hemos configurado el Administrador del sistema. Para eso, debemos cerrar sesión e ingresar a
`<localhost:8000/admin>`_.

Iniciamos sesión con las credenciales del **Super usuario** y hacemos click en el apartado de **Usuarios**. Aquí
seleccionaremos la cuenta cuyo acceso ha sido denegada y le asignaremos el Grupo ``Administrador`` en este caso,
tambien se le puede asignar el Grupo ``Usuario``.

Cerramos sesión y ya tendremos acceso a nuestro sistema en `<localhost:8000>`_. Para los nuevos usuarios, no es necesario
entrar en el Administrador de django; sino que podrá autorizar su acceso desde ajustes, en la sección ``Autorizar Usuarios``.


Menu Principal
**************

En el menu principal podrá ver los proyectos que su cuenta tiene asignado. Si utiliza una cuenta de **Administrador** podrá
tambíen crear proyectos.

La lista de proyectos se separa en tres secciones, y los proyectos se filtran por estado en cada sección.

#. Tus Proyectos: Proyectos que estan en estado pendiente o en ejecución.
#. Cancelados: Proyectos cancelados y que por ende no son relevantes mostrarlos en **Tus Proyectos**.
#. Finalizados: Así como **Cancelados** solo que se muestran proyectos finalizados.

Al estar dentro del menu principal, puede realizar modificaciones sobre sus datos personales, puede hacer clic sobre su icono de usuario
y le dará la opción de ir a ajustes. Podrá actualizar su Nombres y/o Apellidos.

Proyecto
********

Al ingresar a un proyecto, verá el dashboard de este. En el caso de que esté en el Grupo de Usuario solo podrá ver la información
sobre el proyecto; si eres **Administrador** entonces verás también un apartado más que será el de asignar gerente.
Aquí podrá introducir el correo electrónico de un usuario en el sistema, el cuál poseerá el rol de **Gerente de Proyecto**
en el Proyecto.
También en podrás realizar la edición de los datos del proyecto, tanto el nombre  como la descripción, ésto en el caso de que seas
**Administrador**.


Opcion de Usuarios
******************

Participantes
-------------
Al ingresar a un proyecto y si el **administrador** lo ha asignado como **gerente**, o se ha autoasignado la gerencia del proyecto,
entonces le aparecerá ésta opción de ver a los demás participantes participantes del proyecto.
Usted podrá eliminarlos o asignarles un rol, acción que solo siendo **Administrador** del Sistema no podrá hacer.

Roles
-----
Al ingresar a un proyecto y si el **administrador** lo ha asignado como **gerente**, usted podrá realizar las siguientes tares:
- Crear roles: permite crear roles al proyecto con los permisos que se encuentran divididas por proyecto, por cada fase asignar
de las fases. Estos roles apareceran en la tabla de roles.
- Ver rol : posibilita vizualizar la información del rol.
- Editar rol : permite realizar la modificación del rol, tanto la informacion básica como también de los permisos disponibles.
- Eliminar rol : permite eliminar el rol seleccionado, el cual no podrá ser recuperado.

Opcion de Desarrollo
********************

Fases
-----
Al ingresar a un proyecto y si el **administrador** lo ha asignado como **gerente**, o en el caso de que sea un **Usuario**
con un **Rol** asignado (con los permisos necesarios de crear , modificar, mover y/o eliminar una fase) en **Fases** usted
podrá realizar las siguientes tares:

- Crear Fase : permite crear una fase, con los campos requeridos.
- Editar Fase : permite realizar la modificación de la informacion básica de la **fase**.
- Eliminar Fase : permite eliminar la fase** seleccionada, la cual no podrá ser recuperada.
- Mover Fase: le permite cambiar el orden de las **fases**.


Tipo de Item
------------
Al ingresar a un proyecto y si el **administrador** lo ha asignado como **gerente**, o en el caso de que sea un **Usuario**
con un **Rol** asignado (con los permisos necesarios de crear, modificar y/o eliminar un Tipo de Item) usted podrá realizar las siguientes tares:

- Crear Tipo de Item : permite crear un Tipo de Item, con los campos requeridos.
- Editar Tipo de Item : permite realizar la modificación de la informacion del **Tipo de Itemfase**.
- Eliminar Tipo de Item : permite eliminar el **tipo de item** seleccionada, la cual no podrá ser recuperada.
- Importar Tipo de Item : le permite importar los **tipo de items** en otros proyectos.

Tablero
********
Al ingresar al proyecto como gerente tiene la opción de ingresar al tablero, donde visualizará las fases de su proyecto, donde podrá agregar los Ítems que corresponden a cada fase.

- Agregar Ítem a fase :  permite al usuario agregar los ítems que desee a las fases, en la primera fase puede crear los items con libertad, pero en las siguientes fases existe una condición que es la relacionar un ítem con un ítem que se encuentre en linea base cerrada.

Ingresar al Ítem creado se despliega un formulario donde se presenta un menú para el usuario.

- Modificar item : ésta opción permite al usuario modificar el item.
- Relaciones item : esta opcion permite al usuario ver las relaciones del item, y si tiene los permisos poder modificar las relaciones del mismo.
- Historial item : permite al usuario visualizar las versiones del item, y restaurar alguna versión anterior.
- Desactivar item : esta opcion permite al usuario desactivar un item en desarrollo.
- Solicitar Cambio : una vez que los items se encuentran en linea base, se podrá realizar esta solicitud de romper.
- Mostrar impacto : se puede visualizar el impacto del item dentro del proyecto.
- Graficar Tazabilidad : se puede visualizar la trazabilidad del item de forma gráfica, con sus relaciones, entre otros.



Linea Base
**********

- Agregar Linea Base : una vez que se tienen items aprobados, se habilitará esta opción. Aqui se despliega un formulario donde se podrán completar los campos y agregar los items aprobados a una linea base.Una vez creada la linea base, ya se encuentra cerrada.



Comité de Cambio
*******************

- Si el usuario tiene los permisos, puede crear el comité de cambio, el mismo debe tener un número impar de miembros.

Solicitud de Cambio
*******************

Entre las opciones del menú de se cuentra la opción de Solicitud de Cambio.
Aqui se muestran las solicitudes de cambios a los miembros del comité, los mismos tienen la opcion de ingreasar a cada solicitud, aceptar, rechazar, visualizar el progreso de las votaciones y tambien generar unreporte de las solicitudes que tiene.