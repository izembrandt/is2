=====
Items
=====

Modelo
======

Tipo de Item
************

.. autoclass:: item.models.TipoItem(django.db.models.Model)

Item
****

.. autoclass:: item.models.Item
   :members:

Atributo
********

.. autoclass:: item.models.Atributo
   :members:

Archivo
*******

.. autoclass:: item.models.Archivo
   :members:


Formularios
===========

.. automodule:: item.forms
   :members:




