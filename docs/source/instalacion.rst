===========
Instalación
===========

Interprete Python
=================

Esta aplicación requiere una versión mayor o igual a 3.7 de python para poder funcionar. Esta aplicación **no es compatible**
con python2 ni con versiones menores a 3.7.

Puede descargar e instalar python desde su `pagina oficial <https://www.python.org/>`_

Para no interferir con las instalaciones del sistema operativo, nuestro sistema utiliza virtualenv como virtualizador de
ambiente de ejecución. Puede instalar virtualenv utilizando el siguiente comando::

    pip3 install virtualenv

El script de instalación requiere de este paquete y no funcionará correctamente sin este.

Base de datos
=============

Utilizaremos postgres, por ende, debemos asegurarnos que esta instalado en nuestra maquina ``postgresql-12``, aunque
``postgresql-10`` tambien servirá.

Tenga a mano las credenciales de postgres que tenga el permiso de crear base de datos y crear usuarios, pues el script
de instalación utilizará esa información para configurar la base de datos para ser usada con el sistema.

También tenga a mano las credenciales que desea que se creen para asignarle al sistema, estas son las credenciales que
el sistema usará durante toda su vida.

``postgresql`` normalmente incluye la herramienta psql en su instalación, por favor verifique que exista esta
herramienta en su sistema, de otro modo el script fallará.


Ejecución del Script de desarrollo
==================================

.. note::
   El script de desarrollo esta intencionado unicamente para el desarrollo del sistema, para utilizar el sistema en
   un entorno de producción, por favor utilice el script de despliegue.

Descargue el script ``desarrollo.sh`` en la carpeta donde desea descargar el proyecto, y luego ejecute::

 bash desarrollo.sh ARCHIVO_CONFIGURACION

El script se encargará de enlanzar el sistema a la base de datos, crear las tablas y configurar el ambiente virtual.

El ``ARCHIVO_CONFIGURACION`` debe ser el nombre o el path de un archivo de configuración que contiene dentro de el los
siguientes ítems de ejemplo::

    # Base de datos postgresql
    POSTGRES_USER="postgres"
    POSTGRES_PASS="postgres"
    DB_NAME="is2_script"
    DB_USER="is2_script"
    DB_PASS="is2_script"
    # Secreto del DJANGO
    DJANGO_SECRET_IS2='($k!*sfc2vo9u6e$x!&1!yk%e75$bgk%2l4d78hv@z=k8fdxxp'
    # Configuración del django
    IS2_DEBUG=True
    HOST_PERMITIDOS='localhost'
    CARPETA_ESTATICOS='static_apache'
    # Login de google
    GOOGLE_CLIENT_ID='979023583795-pfnp2jldtcl64qo103nihkjncdkbfi0h6.apps.googleusercontent.com'
    GOOGLE_SECRET='nXrk_Y-hlFoUDA55B_HSP3Qq'
    # Cloud Storage
    GOOGLE_APPLICATION_CREDENTIALS='
    {
      "type": "service_account",
      "project_id": "elaborate-hash-270211",
      "private_key_id": "f3782629bff4947dbb394fcc117dffba6",
      "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqh...Ieug==\n-----END PRIVATE KEY-----\n",
      "client_email": "django@elaborate-hash-270211.iam.gserviceaccount.com",
      "client_id": "13486814793710896182",
      "auth_uri": "https://accounts.google.com/o/oauth2/auth",
      "token_uri": "https://oauth2.googleapis.com/token",
      "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
      "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/django%40elaborate-hash-270211.iam.gserviceaccount.com"
    }
    '
    CARPETA_ARCHIVOS_SUBIDOS='archivos_proyectos/'
    GS_PROJECT_ID='elaborate-hash-270621'
    GS_BUCKET_NAME='cloud.storage.ingenieria-de.software'
    GS_CUSTOM_ENDPOINT='http://cloud.storage.ingenieria-de.software'
    GS_DEFAULT_ACL='publicRead'
    # Correo
    DEFAULT_FROM_EMAIL='noreply@correo.ingenieria-de.software'
    EMAIL_HOST='smtp.mailgun.org'
    EMAIL_USE_TLS=True
    EMAIL_PORT=587
    EMAIL_HOST_USER='usuario@correo.ingenieria-de.software'
    EMAIL_HOST_PASSWORD='2iubebfifviyeve62f4597'
    # Celery
    CLOUDAMQP_URL='amqp://localhost:5672'


Estos ítems los deberas modificar según tus servicios y credenciales de la base de datos. ``POSTGRES_USER`` y
``POSTGRES_PASSWORD`` se asumen que son las credenciales de postgres que tendrán permisos para crear usuarios y
base de datos, mientras que ``DB_NAME``, ``DB_USER`` y ``DB_PASS`` será la base de datos y su usuario que se le
asignará al sistema.

``HOST_PERMITIDOS`` define desde que host se utilizará el sistema, si se utilizará más de un host entonces deben ir
separados por un ":" entre cada host. Para utilizar cualquier host puede utilizar el valor "*".

``GOOGLE_CLIENT_ID`` y ``GOOGLE_SECRET`` serán sus credenciales para el Login Social que creó.

``GOOGLE_APPLICATION_CREDENTIALS`` contiene el Service account que utilizará para el almacenamiento online en google
cloud storage. Si prefiere, puede especificar el path a un archivo .json que contenga estos credenciales.

``GS_PROJECT_ID``, ``GS_BUCKET_NAME``, ``GS_CUSTOM_ENDPOINT`` y ``GS_DEFAULT_ACL`` definen configuraciones relacionados
al bucket donde se almacenarán los archivos.


Configuraciones Iniciales
=========================

.. note::
   Estos pasos iniciales solo se utilizan durante la instalación manual, si usted utiliza el script de desarrollo puede
   saltar este paso.

Esta aplicación requiere de ``django`` y ciertos paquetes de terceros para funcionar, por ende debemos asegurarnos
de que los tengamos instalados utilizando ``pip`` desde la raiz del proyecto::

  pip install -r requirements.txt

Esto se asegurará de que todos los paquetes necesarios esten disponibles para la ejecución del django y sus componentes.

Antes de ejecutar el sistema, necesitamos que nuestra base de datos tenga las tablas y relaciones requeridas por el
sistema.
Para cargar las tablas y relaciones automáticamente, utilizaremos el siguiente comando::

  python manage.py migrate

Y cuando termine ejecutaremos::

  python manage.py actualizar_config

Esto se asegurará que el login social (Google) y los roles de administrador esten configurados antes de ejecutar el
sistema.

Tambien necesitaremos configurar un super usuario que habilitará el administrador del sistema y será necesario para
configurar nuestro sistema en la primera ejecución.::

  python manage.py createsuperuser

Solo le daremos usuario y contraseña. Para los demas campos, apretamos enter nada más.

Esto es suficiente para correr nuestro sistema localmente.
