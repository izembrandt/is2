Pruebas Unitarias
=================

Pruebas realizadas con ``pytest``

Proyecto
--------

.. automodule:: proyecto.test_proyectos
   :members:

Items y Tipos de Items
----------------------

.. automodule:: item.test_items
   :members:



Fases de Proyecto
-----------------

.. automodule:: fases.test_fases
   :members:


Roles de Proyecto
-----------------

.. automodule:: roles.test_roles
   :members:

Usuarios
--------

.. automodule:: login_social.test_usuarios
   :members:

Linea Base
----------

.. automodule:: linea_base.test_linea_base
   :members:

Comite de cambios
-----------------

.. automodule:: comite_de_cambio.test_comite
   :members:


Solicitud de cambio
-------------------

.. automodule:: comite_de_cambio.test_solicitud_de_cambio
   :members:

Configuración de las pruebas unitarias
--------------------------------------

.. automodule:: conftest
   :members: