Comite de cambios
=================

Modelo
------

.. automodule:: comite_de_cambio.models
   :members:
   :show-inheritance:

Vistas
-------

.. automodule:: comite_de_cambio.views
   :members:
   :show-inheritance:


Formularios
-----------

.. automodule:: comite_de_cambio.forms
   :members:




