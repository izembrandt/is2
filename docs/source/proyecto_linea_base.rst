===========
Linea Base
===========

Modelo
=======

Linea Base
************

.. autoclass:: linea_base.models.LineaBase(models.Model)
   :members:

Vistas
======

.. note::
   Las vistas de las Lineas Base requieren que el usuario este autenticado y con acceso autorizado
   por el administrador. Además requiere que el usuario posea permisos para ver las opciones disponibles
   en las vistas de las lineas bases

.. automodule:: linea_base.views
   :members:


Formularios
===========

.. automodule:: linea_base.forms
   :members:




