==================
Manual de Usuario
==================

**R3M**
*******

**"Sistema de Gestión de Proyectos de Ingenieria de Software"**

*Integrantes*

- Mateo Fidabel
- Ruben Izembrandt
- Celeste Mallorquín


Página de Inicio
*****************
Para ir a la página de inicio del sistema utilice el siguiente link https://qa.ingenieria-de.software

La página de inicio del sistema le ofrece la alternativa de iniciar sesión con su cuenta de google.

.. image:: project_img/login.png

Dar un clic en el boton de *Iniciar Sesión con Google* el mismo le redireccionará a la página donde deberá ingresar el correo y la contraseña de su cuenta de gmail.

.. image:: project_img/ingresar_correo.png

.. image:: project_img/pass.png

Una vez iniciada la sesión, en el caso de que usted no sea un administrador, el sistema le mostrará el siguiente mensaje.


.. image:: project_img/acceso_restringido.png


Esto significa que el administrador del sistema debe autorizar su ingreso.
En este caso es el administrador quien debe ingrese en el sistema, realiza el mismo procedimiento de ingreso con su cuenta de google.
y se le presentará la página de proyectos, donde en en el menú lateral de la izquierza se encuentra la opcion de *Autorizar Usuarios*
ahí es donde autorizará el ingreso de otras personas al sistema.

.. image:: project_img/autorizar_usuario.png

Una vez que el administrador del sistema haya autorizado su solicitud, podrá ingresar.

El usuario también tiene la opción de cerrar sesión en cualquier momento.

.. image:: project_img/cerrar_sesion.png

Página principal de Proyectos
******************************

Si el administrador del sistema es quien ingresa por primera vez a la aplicación se le mostrará la página principal de los proyectos como se muestra mas abajo.
Es aqui donde él es quien tiene la posibilidad de crear proyectos.

.. image:: project_img/proy_crear.png

Si es un usuario que no es administrador del sistema, se le presentará la página de inicio de *proyectos*, donde solo podrá ver los proyectos en el que es participante, como se muestra en la figura de abajo.
(donde en este caso aún no posee proyectos, y el mismo no puede crearlos)

.. image:: project_img/usuario_autorizado.png

En el menú lateral de la izquierda a parte de vizualizar los proyectos el usuario tiene la opción de *mi perfil*, actualizar su nombre y apellido de ser necesario.

.. image:: project_img/mi_perfil.png

Y por último la opción de *Autorizar* o *Desautorizar* usuarios (detallado en la página de inicio), ésto en el caso de que el usuario sea Administrador del sistema, si no lo es entonces aparecerán las 2 opciones detalladas más arriba.


Crear un proyecto
------------------
Al dar un clic en el botón de crear proyecto, se despliega un formulario donde deberá completar los campos que aparecen en la imagen de abajo, se completan datos como el nombre y la descripción del proyecto, y luego le damos al boton de crear.

.. image:: project_img/crear_proy.png

Y aquí nos redirecciona a la página del Dashboard.

Menú del Proyecto
******************
Dashboard
-----------
Aqui se presenta la pantalla general del proyecto, donde se puede visualizar la informacion del proyecto, y se puede agregar un gerente, aqui se podrá ejecutar, cancela r y finalizar el proyecto.

.. image:: project_img/dashboard.png

Usuarios
----------

Participantes
~~~~~~~~~~~~~~~
Aqui se puede agregar participantes al proyecto. Tambien, una vez que se tengan roles se podrán asignar a los mismos.

.. image:: project_img/participantes.png

Roles
~~~~~~
Aqui se pueden crear los roles para los usuarios, con los permisos del proyecto y de las fases.

.. image:: project_img/pantalla_roles.png

Desarrollo
-----------


Fases
~~~~~~~~
Podemos crear varias fases, tantas fases se requiera y una vez ejecutado el proyecto ya no se podrá modeficar, y ya no se podrá configurar una vez que el usuario ejecute el proyecto ya no

.. image:: project_img/crear_fase.png

Tipo de Ítem
~~~~~~~~~~~~~~~~~~~~~~~~
Para crear un tipo de item se debe ingresar a crear, donde aparece el formulario para completar los campos del tipo de item, donde se pueden añadir atributos nuevos de diferentes tipos, archivos, textos, fechas.

.. image:: project_img/agregar_atributos_tipo_item.png

Tablero
--------

En este espacio el usuario tiene la opción de *agregar los items en cada fase*. La primera fase de su proyecto no tiene restriccion alguna para la creación de ítems.
Desde la segunda fase usted deberá tener como minimo una linea base cerrada con ítems que pueda realacionar a la segunda fase y así ṕueda crear ítems con relaciones.


Linea Base
-----------
Aqui es donde se pueden crear las lineas bases y se tiene la opcion de elegir los items necesarios para las lineas bases, podremos diferenciarlos por nombre, por color, entre otros.

.. image:: project_img/creacion_linea_base.png

Solicitud de Cambio
--------------------
Aqui es donde se pude generar una solicud de cambio.

.. image:: project_img/solicitud_cambio.png

¡Ahora a comenzar!

