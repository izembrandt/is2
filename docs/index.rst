.. Proyecto IS2 documentation master file, created by
   sphinx-quickstart on Sun Mar 15 11:41:06 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenido a la documentación de nuestro proyecto de IS2
========================================================

Integrantes

- Mateo Fidabel
- Ruben Izembrandt
- Celeste Mallorquín

.. toctree::
   :maxdepth: 2
   :caption: Contenidos:

   source/instalacion
   source/ejecucion
   source/manual_usuario
   source/modules
   source/pruebas_unitarias
